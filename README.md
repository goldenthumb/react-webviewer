# React-Webviewer
A component for making a web-based remote control desktop.

- [Demo](http://dev.remotecall.io:7777/)

```bash
$ git clone https://gitlab.rsupport.com/yhchoi/React-Webviewer.git
$ cd React-Webviewer/demo
$ npm install
$ npm run dev

Now open this URL in your browser: http://localhost:3000/
```

------

#### Technical Documentation

- [Installing](#installing)
- [Webviewer Usage](#webviewer-usage)
- [Validation](#validation)
- [Protocol](#protocol)
- [Web Interface](#web-interface)


### Installing

```bash
$ npm install git+https://gitlab.rsupport.com/yhchoi/React-Webviewer.git
```

### Webviewer Usage


```js
import React from 'react';
import ReactDOM from 'react-dom';
import Webviewer from 'react-webviewer';
import { VIEWER_EVENT } from 'react-webviewer';

class App extends React.Element {
  handleEvent = (name, data) => {
    switch (name) {
      case VIEWER_EVENT.WAIT_OK:
        // viewer connect success and viewer wait
        break;
      case VIEWER_EVENT.JOIN_OK:
        // host join and start webviewer
        break;
      case VIEWER_EVENT.CLOSE:
        // webviewer close (viewer or host)
        // console.log(data)
        break;
      case VIEWER_EVENT.ERROR:
        // error (connect fail, not support browser, ... etc)
        // console.log(data)
        break;
      case VIEWER_EVENT.LOG:
        // log (remote control protocol, ... etc)
        // console.log(data)
        break;
      default:
        break;
    }
  };

  render() {
    const locale = 'ko' // ko, ja, en (default en);
    const connectInfo = {
      accessCode: ''  // [string] your access code,
      pushServers: [
        { 
          host, // [string] your push server ip
          viewerPort, // [number] viewer using port
          clientPort // [number] host using port 
        },
        ...
      ],
      relayServers: [
        { 
          host, // [string] your relay server ip
          port // [number] relay port
        }
        ...
      ]
    };
    
    return (
      <Webviewer
        locale={locale}
        connectInfo={connectInfo}
        onReceivedEvent={(name, data) => this.handleEvent(name, data)}
      />
    );
  }
}

ReactDOM.render(<App/>, document.body);
```

### Validation

#### example
```js
import { ConnectInfoStruct } from 'react-webviewer';

const connectInfo = {
  accessCode,
  pushServers,
  relayServers
}

try {
  ConnectInfoStruct(connectInfo);
} catch (error) {
  throw { ...error };
}
```

#### ConnectInfoStruct scheme
```js
const PushServers = struct({
  id: 'optional string',
  host: 'required string',
  viewerPort: 'required number',
  clientPort: 'required number'
});

const RelayServers = struct({
  id: 'optional string',
  host: 'required string',
  port: 'required number'
});

const ConnectInfoStruct = struct({
  accessCode: 'required string',
  pushServers: [PushServers], // object array
  relayServers: [RelayServers], // object array
});
```

### Protocol
- [MQTT message protocol](https://docs.google.com/spreadsheets/d/1U1OewvAO3Vw6tDnsjmGweEGmYETwYshjxiQ66IAPfnI/edit#gid=0) 

### Web Interface
- [remotecall](https://docs.google.com/spreadsheets/d/1LYqWDN0RhMIZSWofSE85FDDb3HaahnceKIYgNHvmGgk/edit#gid=403732179) 
