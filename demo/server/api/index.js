const Router = require('koa-router');
const fs = require('fs-extra');

const api = new Router();
const { tempDB } = require('../tempDB');

api.get('/viewer/wait', async (ctx) => {
  try {
    const accessCode =  tempDB.createAccessCode();
    const sessionId = tempDB.getSession(accessCode);
    const room = tempDB.createRoom({
      name: accessCode,
      sessionId: sessionId
    });

    console.log('create room', room);

    ctx.body = {
      room: { access_code: accessCode },
      session_id: sessionId
    };
  } catch (e) {
    console.log(e);
  }
});

api.get('/viewer/wait_info', async (ctx) => {
  try {
    const sessionId = ctx.query.session_id;
    const hasSessionId = tempDB.findSessionId(sessionId);

    if (!hasSessionId) {
      ctx.status = 400;
      return;
    }

    ctx.body = {
      push_servers: tempDB.pushServers,
      rsnet_servers: tempDB.relayServers
    };
  } catch (e) {
    console.log(e);
  }

});

api.post('/viewer/wait_ok', async (ctx) => {
  try {
    const sessionId = ctx.request.body.session_id;
    const pushServerId = ctx.request.body.push_server_id;
    const relayServerId = ctx.request.body.rsnet_server_id;
    const waitingRoom = tempDB.waitingRoom({ sessionId, pushServerId, relayServerId });
    console.log('waiting room', waitingRoom);

    ctx.status = 200;
  } catch (e) {
    console.log(e);
  }
});

api.post('/viewer/close_session', async (ctx) => {
  try {
    ctx.status = 200;
  } catch (e) {
    console.log(e);
  }
});

api.get('/client/join', async (ctx) => {
  try {
    const accessCode = ctx.query.access_code && tempDB.findAccessCode(ctx.query.access_code).name;
    const sessionId = tempDB.getSession(accessCode);
    const waitingRoom = tempDB.findWaitingRoom(sessionId);

    if (!accessCode || !waitingRoom) {
      ctx.status = 400;
      return;
    }

    const pushServer = tempDB.getPushServer(waitingRoom.pushServerId);
    const relayServer = tempDB.getRelayServer(waitingRoom.relayServerId);

    ctx.body = {
      room: { access_code: accessCode },
      session_id: sessionId,
      push_server: pushServer,
      rsnet_server: relayServer
    };
  } catch (e) {
    console.log(e);
  }
});

api.get('/client/download_file/', async (ctx) => {
  try {
    const fileName = ctx.query.file_name + '.exe';
    ctx.attachment(fileName);
    ctx.body = fs.createReadStream('./public/file/coronastarter.exe');
  } catch (e) {
    console.log(e);
  }
});

api.post('/client/join_ok', async (ctx) => {
  try {
    const sessionId = ctx.request.body.session_id;
    const successRoom = tempDB.successRoom(sessionId);
    console.log('connect success room', successRoom);

    ctx.status = 200;
  } catch (e) {
    console.log(e);
  }
});

api.post('/client/close_session', async (ctx) => {
  try {
    ctx.body = {
      code: 1000
    };
  } catch (e) {
    console.log(e);
  }
});

module.exports = api;