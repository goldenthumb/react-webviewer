const find = fn => arr => arr.find(fn);
const findItem = (key, value) => find(item => item[key] === value);
const objectToArray = (obj) => Object.values(obj);
const compose = (...pipes) => v => pipes.reduce((prev, pipe) => pipe(prev), v);
// const map = fn => arr => arr.map(fn);
// const flatten = arr => arr.reduce((acc, arr)=>acc.concat(arr), []);
// const flatMap = fn => compose(map(fn), flattern);

class TempDB {
  constructor() {
    this.sessionData = {
      id: 0,
      sessions: {}
    };

    this.accessCodeData = {
      id: 0,
      accessCodes: {}
    };

    this.roomData = {
      id: 0,
      rooms: {},
      connected: {}
    };

    this.waitingRoomData = {
      id: 0,
      rooms: {}
    };

    this.successRoomData = {
      id: 0,
      rooms: {}
    };

    this.pushServersData = [
      {
        id: '1',
        public_host: 'stpush.startsupport.com',
        port_viewer: 4433,
        port_client: 443
      },
      {
        id: '2',
        public_host: 'stpush.rview.com',
        port_viewer: 4433,
        port_client: 443
      },
      {
        id: 'PS02',
        public_host: 'ps01.113366.com',
        port_viewer: 4433,
        port_client: 443
      }
    ];

    this.relayServersData = [
      {
        id: '2',
        public_host: 'stgwalpa.startsupport.com',
        port_viewer: 443,
        port_client: 443
      },
      {
        id: 'RS02',
        public_host: 'strwkr1.rsupport.com',
        port_viewer: 443,
        port_client: 443
      }
    ];
  }

  createAccessCode() {
    const id = this.accessCodeData.id++;
    const accessCode = (Math.floor(Math.random() * (100)) + new Date().getTime().toString()).substr(0, 6);
    this.accessCodeData.accessCodes[id] = { id, name: accessCode };
    return this.accessCodeData.accessCodes[id].name;
  }

  getSession(accessCode) {
    const sessionId = (accessCode * accessCode).toString();
    if (this.findSessionId(sessionId)) return sessionId;

    const id = this.sessionData.id++;
    this.sessionData.sessions[id] = { id, name: sessionId };
    return this.sessionData.sessions[id].name;
  }

  createRoom ({ name, sessionId } = {}) {
    const id = this.roomData.id++;
    this.roomData.rooms[id] = { id, name, sessionId };
    return this.roomData.rooms;
  }

  waitingRoom({ sessionId, pushServerId, relayServerId } = {}) {
    const id = this.waitingRoomData.id++;
    this.waitingRoomData.rooms[id] = { id, sessionId, pushServerId, relayServerId };
    return this.waitingRoomData.rooms;
  }

  successRoom(sessionId) {
    const foundWaitingRoom = this.findWaitingRoom(sessionId);
    const id = this.successRoomData.id++;
    this.successRoomData.rooms[id] = { ...foundWaitingRoom, id };
    delete this.waitingRoomData.rooms[foundWaitingRoom.id];
    return this.successRoomData.rooms;
  }

  findAccessCode (name) {
    return compose(
      objectToArray,
      findItem('name', name)
    )(this.accessCodeData.accessCodes);
  }

  findSessionId (name) {
    return compose(
      objectToArray,
      findItem('name', name)
    )(this.sessionData.sessions);
  }

  findWaitingRoom (sessionId) {
    return compose(
      objectToArray,
      findItem('sessionId', sessionId)
    )(this.waitingRoomData.rooms);
  }

  getPushServer(id) {
    return compose(findItem('id', id))(this.pushServersData);
  }

  getRelayServer(id) {
    return compose(findItem('id', id))(this.relayServersData);
  }

  get pushServers() {
    return this.pushServersData;
  }
  get relayServers() {
    return this.relayServersData;
  }
}

exports.tempDB = new TempDB();