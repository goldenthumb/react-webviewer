const path = require('path');
const session = require('koa-session');
const fs = require('fs-extra');
const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const Negotiator = require('negotiator');
const Router = require('koa-router');
const serve = require('koa-static');
const api = require('./api');

const app = new Koa();
const router = new Router();
const sessionConfig = { maxAge: 86400000 }; // 1day

app.use(bodyParser());
app.use(session(sessionConfig, app));
app.keys = ['webviewer'];
app.use(serve(path.resolve(__dirname, './../')));
app.use(router.routes()).use(router.allowedMethods());
router.use(api.routes());

let locale = 'en';
const gateHtml = fs.readFileSync(path.resolve('./views/index.html'), { encoding: 'utf8' });
const port = 7777;

router.get('/', (ctx) => {
  const negotiator = new Negotiator(ctx.request);
  locale = negotiator.language(['en', 'ko', 'ja']);

  ctx.redirect('/' + locale + '/');
});

router.get('/:locale', (ctx) => {
  locale = ctx.params.locale;
  ctx.body = gateHtml;
});

router.get('/viewer/:accessCode', (ctx) => {
  const accessCode = ctx.params.accessCode;
  ctx.redirect('/' + locale + '/viewer/' + accessCode);
});

router.get('/:locale/viewer/:accessCode', (ctx) => {
  locale = ctx.params.locale;
  ctx.body = gateHtml;
});

app.listen(port, () => {
  console.log(`listening to port ${port}`);
});