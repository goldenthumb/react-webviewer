const {resolve} = require('path');

const devConfig = require('./dev');
const buildConfig = require('./build');

module.exports = env => {
  const config = {
    ...devConfig,
    devServer: {
      port: 3000,
      host: /([^(https?):\/\/:\s]+)/.exec(env.proxy)[0],
      contentBase: resolve(__dirname, '../views'),
      publicPath: '/dist/js',
      proxy: {'**': env.proxy}
    }
  };
  
  config.output.filename = buildConfig.output.filename;
  config.output.publicPath = '/dist/js';

  return config;
};