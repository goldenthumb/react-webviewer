const {resolve} = require('path');

module.exports = {
  mode: 'development',
  devtool: 'source-map',
  entry: ['babel-polyfill', 'core-js/fn/promise', resolve(__dirname, '../src/index.js')],
  output: {
    filename: 'webviewer.dev.js',
    path: resolve(__dirname, '../dist/js')
  },
  resolve: {
    alias: {
      src: resolve(__dirname, '../../src')
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: { loader: 'babel-loader' },
        exclude: /node_modules/
      },
      {
        test: /(\.scss$)/,
        use: [
          'style-loader', 
          {
            loader: 'css-loader',
            options: {
              module: true,
              importLoaders: 1,
              localIdentName: '[local]_[hash:base64:3]'
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [
                require('autoprefixer')('last 2 versions', 'ie 10')
              ]
            }
          },
          'sass-loader'
        ]
      },
      {
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        loader: 'url-loader',
        options: {
          limit: 10000
        }
      }
    ]
  }
};