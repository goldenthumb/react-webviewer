import axios from 'axios';
import storage from '../lib/storage';

export function createAccessCode() {
  return axios.get('/viewer/wait');
}

export function getConnectInfo(sessionId) {
  const session_id = sessionId || storage.get('sessionId');
  return axios.get('/viewer/wait_info', { params: { session_id } });
}

export function waitSuccess(data) {
  const session_id = storage.get('sessionId');
  const push_server_id = data.pushServer.id;
  const rsnet_server_id = data.relayServer.id;
  return axios.post('/viewer/wait_ok', { session_id, push_server_id, rsnet_server_id });
}

export function viewerCloseSession() {
  const session_id = storage.get('sessionId');
  return axios.post('/viewer/close_session', { session_id });
}

export function getHostConnectInfo(access_code)  {
  return axios.get('/client/join', { params: { access_code } });
}

export function joinSuccess() {
  const session_id = storage.get('sessionId');
  return axios.post('/client/join_ok', { session_id });
}

export function downloadHostFileUrl(file)  {
  location.href = `/client/download_file/?file_name=${file}`;
}

export function hostCloseSession() {
  const session_id = storage.get('sessionId');
  return axios.post('/client/close_session', { session_id });
}
