import React, {Component} from 'react';

import storage from '../../src/lib/storage';
import * as STATUS from '../lib/enums/status';
import * as api from '../../src/api';

import Webviewer from 'react-webviewer';
import { VIEWER_EVENT } from 'react-webviewer';
// import Webviewer from '../../../src/index';
// import { VIEWER_EVENT } from '../../../src/index';

import {useBase} from '../contexts/base';

class WebviewerContainer extends Component {
  onClose = () => {
    const locale = storage.get('locale');
    location.href = `/${locale ? locale + '/' : ''}`;
  };

  handleEvent = (name, data) => {
    const {i18n, baseActions} = this.props;

    switch (name) {
      case VIEWER_EVENT.WAIT_OK:
        baseActions.setViewerStatus(STATUS.VIEWER.WAITING);
        api.waitSuccess(data);
        break;
      case VIEWER_EVENT.JOIN_OK:
        baseActions.setViewerStatus(STATUS.VIEWER.RUN);
        api.joinSuccess();
        break;
      case VIEWER_EVENT.CLOSE:
        data === 'viewer' && api.viewerCloseSession();
        data === 'host' && api.hostCloseSession();
        this.onClose();
        break;
      case VIEWER_EVENT.ERROR:
        alert(i18n.message('serverConnectFail'));
        this.onClose();
        break;
      case VIEWER_EVENT.LOG:
        console.log(data);
        break;
      default:
        break;
    }
  };

  render() {
    const {viewer} = this.props;
    const accessCode = viewer.accessCode || this.props.accessCode;
    const pushServers = viewer.pushServers || storage.get('pushServers');
    const relayServers = viewer.relayServers || storage.get('relayServers');
    const willMessage = {
      url: document.location.origin + '/viewer/close_session',
      params: {sessionId: storage.get('sessionId')}
    };

    if (!accessCode) return null;

    return (
      <Webviewer
        locale={storage.get('locale')}
        connectInfo={{accessCode, pushServers, relayServers, willMessage}}
        onReceivedEvent={(name, data) => this.handleEvent(name, data)}
      />
    );
  }
}

export default useBase(WebviewerContainer);