import React, {Component} from 'react';

import * as STATUS from '../lib/enums/status';

import {useBase} from '../contexts/base';
import Entry from '../../src/components/Entry';

class EntryContainer extends Component {
  state = {
    joinStatus: STATUS.JOIN.IDLE
  };

  setJoinStatus = (status) => {
    this.setState({
      joinStatus: status
    });
  };

  componentDidMount() {
    const {baseActions} = this.props;
    window.addEventListener('popstate', baseActions.initializeViewer);
  }

  componentWillUnmount() {
    const {baseActions} = this.props;
    window.removeEventListener('popstate', baseActions.initializeViewer);
  }

  render() {
    const {joinStatus} = this.state;
    const {i18n, viewer} = this.props;
    const visible = viewer.status === STATUS.VIEWER.IDLE;

    if (!visible) return null;

    return (
      <Entry
        i18n={i18n}
        viewer={viewer}
        joinStatus={joinStatus}
        setJoinStatus={this.setJoinStatus}
      />
    );
  }
}

export default useBase(EntryContainer);
