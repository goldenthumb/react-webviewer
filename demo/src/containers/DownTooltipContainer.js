import React, {Component} from 'react';

import * as STATUS from '../lib/enums/status';

import {useBase} from '../contexts/base';
import DownTooltip from '../components/DownTooltip';

class DownTooltipContainer extends Component {
  constructor(props) {
    super(props);

    this.timeoutValue = null;
  }

  closeDownTooltip = () => {
    clearTimeout(this.timeoutValue);
    const {setJoinStatus} = this.props;
    setJoinStatus(STATUS.JOIN.IDLE);
  };

  componentWillUnmount() {
    clearTimeout(this.timeoutValue);
  }

  render() {
    const {i18n, joinStatus} = this.props;
    const visible = joinStatus === STATUS.JOIN.SUCCESS;

    if (!visible) return null;

    this.timeoutValue = setTimeout(this.closeDownTooltip, 10000);

    return (
      <DownTooltip i18n={i18n} onClose={this.closeDownTooltip}/>
    );
  }
}

export default useBase(DownTooltipContainer);
