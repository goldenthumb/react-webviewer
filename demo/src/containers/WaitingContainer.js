import React, {Component} from 'react';

import storage from '../../src/lib/storage';
import * as STATUS from '../lib/enums/status';
import * as api from '../../src/api';

import {useBase} from '../contexts/base';
import Waiting from '../../src/components/Waiting';

class WaitingContainer extends Component {
  onClose = () => {
    const locale = storage.get('locale');
    api.viewerCloseSession();
    location.href = `/${locale ? locale + '/' : ''}`;
  };

  render() {
    const {i18n, viewer} = this.props;
    const visible = viewer.status === STATUS.VIEWER.WAITING;

    if (!visible) return null;

    return (
      <Waiting
        i18n={i18n}
        accessCode={viewer.accessCode}
        onClose={this.onClose}
      />
    );
  }
}

export default useBase(WaitingContainer);