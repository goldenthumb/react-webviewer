import React, {Component} from 'react';

import {BaseProvider} from '../contexts/base';
import EntryContainer from '../../src/containers/EntryContainer';
import WaitingContainer from '../../src/containers/WaitingContainer';
import WebviewerContainer from '../../src/containers/WebviewerContainer';

class InitializeContainer extends Component {
  render() {
    const {i18n, accessCode} = this.props;

    return (
      <BaseProvider i18n={i18n} accessCode={accessCode}>
        <EntryContainer/>
        <WaitingContainer/>
        <WebviewerContainer/>
      </BaseProvider>
    );
  }
}

export default InitializeContainer;