import React, {Component} from 'react';

import {detect} from 'detect-browser';
import i18n from '../lib/i18n';
import storage from '../lib/storage';
import * as api from '../api';
import * as STATUS from '../lib/enums/status';

import {useBase} from '../contexts/base';
import Selector from '../components/Selector';

class SelectorContainer extends Component {
  state = {
    accessCode: '',
    isActiveJoin: false,
    isVisibleDownTooltip: false
  };

  handleCreate = async () => {
    try {
      const {setWaitInfo} = this.props.baseActions;
      const waitInfo = (await api.createAccessCode()).data;
      const accessCode = waitInfo.room.access_code;
      const sessionId = waitInfo.session_id;
      const connectInfo = (await api.getConnectInfo(sessionId)).data;
      const viewerPath = `${location.pathname}viewer/${accessCode}`;
      const pushServers = connectInfo.push_servers
        .map(({id, public_host: host, port_viewer: viewerPort, port_client: clientPort}) => ({
          id, host, viewerPort, clientPort
        }));
      const relayServers = connectInfo.rsnet_servers
        .map(({id, public_host: host, port_viewer: port}) => ({id, host, port}));

      setWaitInfo({accessCode, pushServers, relayServers});

      storage.set('sessionId', sessionId);
      storage.set('accessCode', accessCode);
      storage.set('pushServers', pushServers);
      storage.set('relayServers', relayServers);
      history.pushState(null, null, viewerPath);
    } catch (error) {
      console.log(error);
    }
  };

  downloadHostFile(pushInfo, hostId, viewerId, accessId, type) {
    const fileName = [pushInfo.public_host, pushInfo.port_client.toString(), hostId, viewerId, accessId, type];
    const encodedFileName = btoa(fileName.join('|') + '|');
    api.downloadHostFileUrl(encodedFileName);
  }

  async joinClient(accessCode) {
    const {setJoinStatus} = this.props;
    try {
      const browser = detect().os.split(' ');
      const os = browser[0];
      const osVersion = parseInt(browser[1]);

      if (!(os === 'Windows' && osVersion >= 7)) {
        alert(i18n.message('notSupportedOS'));
        return;
      }
      const connectInfo = (await api.getHostConnectInfo(accessCode)).data;
      const pushInfo = connectInfo.push_server;
      this.downloadHostFile(pushInfo, accessCode + '_h0', accessCode + '_v0', accessCode, '1');
      setJoinStatus(STATUS.JOIN.SUCCESS);
    } catch (err) {
      console.log('client join fail', err);
      setJoinStatus(STATUS.JOIN.FAIL);
    }

    this.setState({
      accessCode: ''
    });
  }

  handleChange = ({target}) => {
    const value = target.value;
    const accessCode = value.replace(/\s|\D/g, '').replace(/(\d{3})\B/g, '$1 ');
    const codeSize = accessCode.length;

    if (codeSize > 7) return;

    this.setState({
      [target.name]: accessCode,
      isActiveJoin: codeSize === 7
    });
  };

  handleJoinRoom = () => {
    const {accessCode, isActiveJoin} = this.state;
    const formatCode = accessCode.replace(/\s|\D/g, '');

    if (!isActiveJoin) return;

    this.joinClient(formatCode);
  };

  handleKeyPress = ({key}) => {
    if (key === 'Enter') {
      this.handleJoinRoom();
    }
  };

  handleJoinStart = () => {
    const {setJoinStatus} = this.props;
    setJoinStatus(STATUS.JOIN.START);
  };

  onClose = () => {
    const {setJoinStatus} = this.props;
    this.setState({
      accessCode: '',
      isActiveJoin: false
    });

    setJoinStatus(STATUS.JOIN.IDLE);
  };

  render() {
    const {i18n, viewer, joinStatus} = this.props;
    const {accessCode, isActiveJoin} = this.state;

    return (
      <Selector
        i18n={i18n}
        accessCode={accessCode}
        isActiveJoin={isActiveJoin}
        joinStatus={joinStatus}
        loading={!!viewer.accessCode}
        onJoinStart={this.handleJoinStart}
        onCreate={this.handleCreate}
        onChange={this.handleChange}
        onJoinRoom={this.handleJoinRoom}
        onKeyPress={this.handleKeyPress}
        onClose={this.onClose}
      />
    );
  }
}

export default useBase(SelectorContainer);