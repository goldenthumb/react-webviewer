import React, {Component, Fragment} from 'react';
import styles from './Entry.scss';

import * as STATUS from '../../lib/enums/status';

import SelectorContainer from '../../containers/SelectorContainer';
import DownTooltipContainer from '../../containers/DownTooltipContainer';

class Entry extends Component {
  render() {
    const {i18n, joinStatus, setJoinStatus} = this.props;

    const joinStart = joinStatus === STATUS.JOIN.START || joinStatus === STATUS.JOIN.FAIL;
    const entryStyle = `entry${joinStart ? '--active-join' : ''}`;
    const tableStyle = `table-image${joinStart ? '--bottom' : ''}`;
    const computerStyle = `computer${joinStart ? '--bottom' : ''}`;

    return (
      <Fragment>
        <div className={styles[entryStyle]}>
          <div className={styles['content']}>
            <h1 className={styles['title']}>{i18n.message('entryTitle')}</h1>
            <h2 className={styles['sub-title']} dangerouslySetInnerHTML={{__html: i18n.message('entrySubTitle')}}/>
            <SelectorContainer joinStatus={joinStatus} setJoinStatus={setJoinStatus}/>
            {joinStart && <div className={styles['notice']}>{i18n.message('entryJoinNotice')}</div>}
            {joinStatus === STATUS.JOIN.FAIL &&
            <div className={styles['notice-error']}>{i18n.message('invalidNumber')}</div>}
          </div>
          <div className={styles[tableStyle]}/>
          <div className={styles[computerStyle]}/>
        </div>
        <DownTooltipContainer joinStatus={joinStatus} setJoinStatus={setJoinStatus}/>
      </Fragment>
    );
  }
}

export default Entry;