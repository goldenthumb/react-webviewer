import React, {Component, Fragment} from 'react';
import styles from './Selector.scss';

import * as STATUS from '../../lib/enums/status';

import Loading from '../Loading'

class Selector extends Component {
  componentDidUpdate() {
    const {joinStatus} = this.props;
    const joinStart = joinStatus === STATUS.JOIN.START || joinStatus === STATUS.JOIN.FAIL;
    joinStart && this.input && this.input.focus();
  }

  render() {
    const {i18n, accessCode, joinStatus, isActiveJoin, loading, onJoinStart, onCreate, onChange, onJoinRoom, onKeyPress, onClose} = this.props;
    const joinStart = joinStatus === STATUS.JOIN.START || joinStatus === STATUS.JOIN.FAIL;
    const buttonStyle = `button-right${joinStart ? isActiveJoin ? '--active' : '--disable' : ''}`;

    const InputForm = (
      !joinStart ?
        <Fragment>
          <button
            className={styles['button-left']}
            onClick={onCreate}
          >
            {i18n.message('supportStart')}
          </button>
          <div className={styles['divide']} style={{visibility: loading ? 'hidden' : 'visible '}}/>
        </Fragment> :
        <Fragment>
          <button className={styles['close']} onClick={onClose}/>
          <input
            ref={(ref) => this.input = ref}
            className={styles['input']}
            name="accessCode"
            onChange={onChange}
            onKeyPress={onKeyPress}
            value={accessCode}
          />
        </Fragment>
    );

    return (
      <div className={styles['selector']}>
        {InputForm}
        <button
          className={styles[buttonStyle]}
          onClick={joinStart ? onJoinRoom : onJoinStart}
        >
          {i18n.message('join')}
        </button>
        {loading && <Loading/>}
      </div>
    );
  }
}

export default Selector;
