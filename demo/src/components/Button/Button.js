import React, {Component} from 'react';
import styles from './Button.scss';

class Button extends Component {
  render() {
    const {children, onClick, theme, disabled} = this.props;
    return (
      <button
        className={styles[theme]}
        onClick={disabled ? () => null : onClick}>
        {children}
      </button>
    );
  }
}

export default Button;
