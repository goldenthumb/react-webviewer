import React, {Component} from 'react';
import styles from './Waiting.scss';

class Waiting extends Component {
  state = {
    timeLimit: 20 * 60 * 1000 // 20분
  };

  constructor(props) {
    super(props);
    this.interval = null;
  }

  msToMinAndSec(ms) {
    const {i18n} = this.props;
    const min = Math.floor(ms / 60000);
    const sec = ((ms % 60000) / 1000).toFixed(0);

    return (min ? (min + i18n.message('min')) : '') + (sec < 10 ? '0' : '') + sec + i18n.message('sec');
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      const {timeLimit} = this.state;
      const {onClose} = this.props;
      const nextTimeLimit = timeLimit - 1000;

      if (nextTimeLimit <= 0) onClose();

      this.setState({
        timeLimit: nextTimeLimit
      });

    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const {i18n, accessCode, onClose} = this.props;
    const {timeLimit} = this.state;
    const formatCode = accessCode.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1 ');

    return (
      <div className={styles['waiting']}>
        <div className={styles['content']}>
          <h1 className={styles['title']}>{i18n.message('entryTitle')}</h1>
          <h2 className={styles['sub-title']} dangerouslySetInnerHTML={{__html: i18n.message('entrySubTitle')}}/>
          <div className={styles['selector']}>
            <label htmlFor="access-code" className={styles['label']}>
              <span className={styles['label-name']}>{i18n.message('supportStart')}</span>
              <input id="access-code" className={styles['input']} value={formatCode} readOnly/>
            </label>
            <button className={styles['close']} onClick={onClose}/>
          </div>
          <div className={styles['notice']} dangerouslySetInnerHTML={{__html: i18n.message('waitingNotice')}}/>
          {i18n.message('timeLeft')} : {this.msToMinAndSec(timeLimit)}
        </div>
        <div className={styles['table-image']}/>
        <div className={styles['computer']}/>
      </div>
    );
  }
}

export default Waiting;