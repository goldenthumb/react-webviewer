import React from 'react';
import styles from './Loading.scss';

import LoadingSpinner from '../LoadingSpinner';

const Loading = () => {
  return (
    <div className={styles['loading']}>
      <LoadingSpinner theme='fade-circle'/>
    </div>
  );
};

export default Loading;