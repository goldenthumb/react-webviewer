import React, {Component} from 'react';
import styles from './DownTooltip.scss';
import {detect} from 'detect-browser';

class DownTooltip extends Component {
  state = {
    browserName: this.getBrowserName
  };

  get getBrowserName() {
    const browser = detect();
    return /chrome|ie|edge/.test(browser.name) ? browser.name : false;
  }

  render() {
    const {browserName} = this.state;
    const {i18n, onClose} = this.props;

    if (!browserName) return null;

    return (
      <div className={styles[`down-tooltip--${browserName}`]}>
        {onClose && <div className={styles['close']} onClick={onClose}>&#10005;</div>}
        <div className={styles['emblem']}/>
        <span className={styles['arrow--up']}/>
        <div className={styles['title']}>{i18n.message('downToolTipTitle')}</div>
        <div className={styles['sub-title']}>{i18n.message('downToolTipSubTitle')}</div>
      </div>
    );
  }
}

export default DownTooltip;
