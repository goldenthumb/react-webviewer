import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Entry, Viewer } from './pages';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/:locale" component={Entry} />
          <Route exact path="/:locale/viewer/:accessCode" component={Viewer} />
        </Switch>
      </Router>
    );
  }
}

export default App;
