const VIEWER = {
  IDLE: 0,
  WAITING: 1,
  RUN: 2,
  EXIT: 3
};

const JOIN = {
  IDLE: 0,
  START: 1,
  SUCCESS: 2,
  FAIL: 3
};

export { VIEWER, JOIN };