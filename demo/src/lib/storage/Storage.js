const session = window.sessionStorage;

export default class Storage {
  get(k) {
    return JSON.parse(session.getItem(k));
  }

  set(k, v) {
    return session.setItem(k, JSON.stringify(v));
  }

  clear() {
    return session.clear();
  }
}