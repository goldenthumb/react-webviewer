export function availableBrowser() {
  if (navigator.appName === 'Microsoft Internet Explorer') {
    let rv = -1;
    const ua = navigator.userAgent;
    const re  = new RegExp(/MSIE ([0-9]{1,}[.0-9]{0,})/);
    if (re.exec(ua) != null) rv = parseFloat( RegExp.$1 );

    return rv > 9;
  }

  return true;
}

export const compose = (...functions) => functions.reduce((a, b) => (...args)=> a(b(...args)));