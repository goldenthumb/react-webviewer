import React from 'react';

import storage from '../lib/storage';
import languages from '../../src/lib/i18n';

import InitializeContainer from '../../src/containers/InitializeContainer';

const Entry = ({ match }) => {
  const locale = match.params.locale;
  const i18n = languages.setLocale(match.params.locale);

  storage.set('locale', locale);

  return (
    <InitializeContainer
      locale={locale}
      i18n={i18n}
    />
  );
};

export default Entry;