import React from 'react';

import storage from '../lib/storage';
import languages from '../../src/lib/i18n';

import InitializeContainer from '../../src/containers/InitializeContainer';

const Viewer = ({ match }) => {
  const locale = match.params.locale;
  const accessCode = match.params.accessCode;
  const i18n = languages.setLocale(locale);

  storage.set('locale', locale);

  return (
    <InitializeContainer
      locale={locale}
      i18n={i18n}
      accessCode={accessCode}
    />
  );
};

export default Viewer;