import React, { Component, createContext } from 'react';

import * as STATUS from '../lib/enums/status';

const Context = createContext();
const { Provider, Consumer } = Context;

class BaseProvider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      i18n: props.i18n,
      viewer: {
        accessCode: props.accessCode || '',
        pushServers: null,
        relayServers: null,
        status: props.accessCode ? STATUS.VIEWER.WAITING : STATUS.VIEWER.IDLE
      }
    };

    this.actions = {
      initializeViewer: () => {
        this.setState({
          viewer: {
            accessCode: null,
            pushServers: null,
            relayServers: null,
            status: STATUS.VIEWER.IDLE
          }
        })
      },
      setViewerStatus: (status) => {
        this.setState({
          viewer: {
            ...this.state.viewer,
            status
          }
        });
      },
      setWaitInfo: ({ accessCode, pushServers, relayServers }) => {
        this.setState({
          viewer: {
            ...this.state.viewer,
            accessCode,
            pushServers,
            relayServers
          }
        });
      }
    };
  }

  render() {
    const { state, actions } = this;
    const value = { state, actions };

    return (
      <Provider value={value}>
        {this.props.children}
      </Provider>
    );
  }
}

const useBase = (WrappedComponent) => {
  return (props) => {
    return (
      <Consumer>
        {
          ({ state, actions }) => (
            <WrappedComponent
              {...props}
              {...state}
              baseActions={actions}
            />
          )
        }
      </Consumer>
    );
  };
};

export { BaseProvider, useBase };