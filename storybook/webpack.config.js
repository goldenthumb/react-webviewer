const path = require('path');
const autoprefixer = require('autoprefixer');

module.exports = {
  resolve: {
    alias: {
      src: path.resolve(__dirname, '../src')
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader'
        },
        exclude: /node_modules/
      },
      {
        test: /(\.scss$)/,
        use: [
          "style-loader",
          {
            loader: 'css-loader',
            options: {
              sourceMap: false,
              module: true,
              importLoaders: 1,
              localIdentName: '[local]_[hash:base64:5]'
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: function () {
                return [autoprefixer('last 2 versions', 'ie 10')];
              }
            }
          },
          "sass-loader"
        ]
      }
    ]
  }
};