const path = require('path');
const autoprefixer = require('autoprefixer');

module.exports = {
  devtool: false,
  entry: {
    index: './src/index.js'
  },
  externals: {
    'react': {
      commonjs2: 'react'
    },
    'react-dom': {
      commonjs2: 'react-dom'
    }
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].js',
    publicPath: '/',
    libraryTarget: "commonjs2"
  },
  resolve: {
    alias: {
      src: path.resolve(__dirname, './src')
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader'
        },
        exclude: /node_modules\/(?!superstruct).*/
      },
      {
        test: /(\.scss$)/,
        use: [
          "style-loader",
          {
            loader: 'css-loader',
            options: {
              sourceMap: false,
              module: true,
              importLoaders: 1,
              localIdentName: '_[hash:base64:12]'
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: function () {
                return [autoprefixer('last 2 versions', 'ie 10')];
              }
            }
          },
          "sass-loader"
        ]
      },
      {
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        loader: 'url-loader',
        options: {
          limit: 10000
        }
      }
    ]
  }
};