import React, {Component, createContext} from 'react';
import * as STATUS from 'src/lib/enums/status';
import storage from 'src/lib/storage/webviewer';

const Context = createContext();
const {Provider, Consumer} = Context;

class ViewerProvider extends Component {
  state = {
    viewer: {
      width: 0,
      height: 0,
      paintColor: 0,
      isScreenOverflow: false,
      status: STATUS.VIEWER.IDLE,
      screenRatio: storage.get('screenRatio') || 0,
      screenCaptureNumber: storage.get('screenCaptureNumber') || 0,
      activatedControlApps: storage.get('activatedControlApps') || [],
      pressedFunctionKeys: []
    }
  };

  actions = {
    updateOperatingStatus: (status) => {
      const {viewer} = this.state;

      this.setState({
        viewer: {
          ...viewer,
          status
        }
      });
    },
    updateActivatedControlApp: (activatedControlApps) => {
      const {viewer} = this.state;

      this.setState({
        viewer: {
          ...viewer,
          activatedControlApps
        }
      });
    },
    updatePressedFunctionKeys: (pressedFunctionKeys) => {
      const {viewer} = this.state;

      this.setState({
        viewer: {
          ...viewer,
          pressedFunctionKeys
        }
      });
    },
    changeScreenSize: (screenData) => {
      const {viewer} = this.state;

      this.setState({
        viewer: {
          ...viewer,
          ...screenData
        }
      });
    },
    changePaintColor: (paintColorIndex) => {
      const {viewer} = this.state;

      this.setState({
        viewer: {
          ...viewer,
          paintColor: paintColorIndex
        }
      });
    },
    updateScreenCaptureCount: () => {
      const {viewer} = this.state;

      this.setState({
        viewer: {
          ...viewer,
          screenCaptureNumber: viewer.screenCaptureNumber + 1
        }
      });
    },
    changeScreenRatio: (screenRatio) => {
      const {viewer} = this.state;

      this.setState({
        viewer: {
          ...viewer,
          screenRatio
        }
      });
    }
  };

  render() {
    const {state, actions} = this;
    const value = {state, actions};

    return (
      <Provider value={value}>
        {this.props.children}
      </Provider>
    )
  }
}

const useViewer = (WrappedComponent) => {
  return (props) => {
    return (
      <Consumer>
        {
          ({state, actions}) => (
            <WrappedComponent
              {...props}
              viewer={state.viewer}
              viewerActions={actions}
            />
          )
        }
      </Consumer>
    )
  }
};

export {ViewerProvider, useViewer};