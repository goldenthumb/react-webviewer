import React, {Component, createContext} from 'react';

const Context = createContext();
const {Provider, Consumer} = Context;

class BaseProvider extends Component {
  constructor(props) {
    super(props);
    const {base, onReceivedEvent} = props;
    const {i18n, config, client, remoteControl, screenControl} = base;

    this.state = {
      i18n,
      config,
      client,
      remoteControl,
      screenControl,
      modal: {
        exitViewer: false
      }
    };

    this.actions = {
      showModal: (modalName) => {
        this.setState({
          modal: {
            [modalName]: true
          }
        });
      },
      hideModal: (modalName) => {
        this.setState({
          modal: {
            [modalName]: false
          }
        });
      },
      onReceivedEvent: (...args) => {
        if (typeof onReceivedEvent === 'function') onReceivedEvent(...args);
      }
    };
  }

  render() {
    const {state, actions} = this;
    const value = {state, actions};

    return (
      <Provider value={value}>
        {this.props.children}
      </Provider>
    )
  }
}

const useBase = (WrappedComponent) => {
  return (props) => {
    return (
      <Consumer>
        {
          ({state, actions}) => (
            <WrappedComponent
              {...props}
              {...state}
              baseActions={actions}
            />
          )
        }
      </Consumer>
    )
  }
};

export {BaseProvider, useBase};