import React, {Component, createContext} from 'react';
import * as STATUS from 'src/lib/enums/status';

const Context = createContext();
const {Provider, Consumer} = Context;

class HostProvider extends Component {
  state = {
    host: {
      status: STATUS.HOST.IDLE,
      useKmControl: false,
      monitorInfo: null
    }
  };

  actions = {
    updateOperatingStatus: (status) => {
      const {host} = this.state;

      this.setState({
        host: {
          ...host,
          status
        }
      });
    },
    updateControlStatus: (useKmControl) => {
      const {host} = this.state;

      this.setState({
        host: {
          ...host,
          useKmControl
        }
      });
    },
    updateMonitorInfo: (monitorInfo) => {
      const {host} = this.state;

      this.setState({
        host: {
          ...host,
          monitorInfo
        }
      });
    }
  };

  render() {
    const {state, actions} = this;
    const value = {state, actions};

    return (
      <Provider value={value}>
        {this.props.children}
      </Provider>
    )
  }
}

const useHost = (WrappedComponent) => {
  return (props) => {
    return (
      <Consumer>
        {
          ({state, actions}) => (
            <WrappedComponent
              {...props}
              host={state.host}
              hostActions={actions}
            />
          )
        }
      </Consumer>
    )
  }
};

export {HostProvider, useHost};