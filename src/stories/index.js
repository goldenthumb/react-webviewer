import React from 'react';
import {storiesOf} from '@storybook/react';

import ConfirmModal from 'src/components/ConfirmModal';
import languages from 'src/lib/i18n';

const locale = 'ko';
const i18n = languages.setLocale(locale);

storiesOf('component', module)
  .add('ConfirmModal', () => (
    <ConfirmModal
      visible
      i18n={i18n}
      description={i18n.message('confirmExitRemoteControl')}
      onCancel={() => {}}
      onConfirm={() => {}}
    />
  ));