class I18n {
  constructor(locale, messages) {
    this._locale = locale || 'en';
    this._locales = [];
    this._messages = null;
    if (messages) this.addMessages(messages);
  }

  addMessage(obj) {
    this.languageResource.push(obj.key ? obj : Object.assign({key: this.key++}, obj));
    return this;
  }

  addMessages(messages) {
    this._messages = {...messages};
    this._locales = Object.keys(messages);
    return this;
  }

  setLocale(locale) {
    if (!this._locales.includes(locale)) locale = 'en';
    this._locale = locale;
    return this;
  }

  message(key, values = {}) {
    let message = this._messages[this._locale][key];
    if (!message) return key;

    for (const key in values) {
      if (!values.hasOwnProperty(key)) continue;
      message = message.replace(new RegExp(`\\{\\s*${key}\\s*\\}`, 'g'), values[key]);
    }

    return message;
  }

  all() {
    return {...this.messages[this._locale]};
  }
}

const i18n = new I18n(['ko', 'ja', 'en']);

i18n.addMessages({
  ko: {
    mouseAndKeyboard: '마우스 / 키보드 제어',
    laserPointer: '레이저 포인터',
    functionKey: '기능키',
    shortKey: '단축키',
    screenKeyboard: '화상 키보드',
    draw: '그리기',
    allClear: '전체 지우기',
    screenRatio: '화면 비율 조정',
    fullScreen: '전체화면',
    changeMonitor: '모니터 변경',
    screenCapture: '화면캡쳐',
    exit: '종료하기',
    fold: '접기',
    expand: '펼치기',
    notGetScreen: '원격지의 화면을 가져올 수 없습니다.',
    checkRemoteStatus: '원격지의 상태를 확인해주세요.',
    hostPause: '상대방이 화면을 일시정지 하였습니다.',
    confirmExitRemoteControl: '원격제어를 종료하시겠습니까?',
    yes: '예',
    no: '아니오',
    notSupportBrowser: '지원하지 않는 브라우저 입니다.',
    timeLeft: '남은 시간',
    min: '분',
    sec: '초',
    serverConnectFail: '서버와 연결에 실패하였습니다.',
    notSupportedOS: '지원하지 않는 OS 입니다.'
  },
  en: {
    mouseAndKeyboard: 'Mouse / keyboard control',
    laserPointer: 'Laser pointer',
    functionKey: 'Function key',
    shortKey: 'Shortcut key',
    screenKeyboard: 'Virtual keyboard',
    draw: 'Draw',
    allClear: 'Erase all',
    screenRatio: 'Fit to screen',
    fullScreen: 'Full screen',
    changeMonitor: 'Switch display',
    screenCapture: 'Capture screen',
    exit: 'End',
    fold: 'open',
    expand: 'close',
    notGetScreen: 'Remote system\',s screen could not be retrieved.',
    checkRemoteStatus: 'Please, verify the remote system\',s status.',
    hostPause: 'Customer has paused the screen.',
    confirmExitRemoteControl: 'Do you want to end the remote control?',
    yes: 'Yes',
    no: 'No',
    notSupportBrowser: 'Browser not supported.',
    timeLeft: 'Time left',
    min: 'Min',
    sec: 'Sec',
    serverConnectFail: 'Failed to connect with the server.',
    notSupportedOS: 'This OS is not supported.'
  },
  ja: {
    mouseAndKeyboard: 'マウス/キーボード制御',
    laserPointer: 'レーザーポインタ',
    functionKey: '機能キー',
    shortKey: 'ショートカットキー',
    screenKeyboard: '仮想キーボード',
    draw: '描画',
    allClear: '全体消去',
    screenRatio: '画面自動調整 ',
    fullScreen: 'フルスクリーン',
    changeMonitor: 'モニター変更',
    screenCapture: '画面キャプチャ',
    exit: '終了する',
    fold: '閉じる',
    expand: '開く',
    notGetScreen: 'お客様画面が取得できません。',
    checkRemoteStatus: 'お客様の状態を確認してください。',
    hostPause: 'お客様が画面を一時停止しました。',
    confirmExitRemoteControl: 'リモートコントロールを終了しますか？',
    yes: 'はい',
    no: 'いいえ',
    notSupportBrowser: 'このブラウザは未対応です。',
    timeLeft: '残り時間',
    min: '分',
    sec: '秒',
    serverConnectFail: 'サーバへの接続に失敗しました。',
    notSupportedOS: '対応していないOSです。'
  }
});

export default i18n;

