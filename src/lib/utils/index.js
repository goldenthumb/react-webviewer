import MobileDetect from 'mobile-detect';

export function fileDownload({dataURL, filename} = {}) {
  const tempLink = document.createElement('a');
  tempLink.style.display = 'none';
  tempLink.href = dataURL;
  tempLink.setAttribute('download', filename);

  if (typeof tempLink.download === 'undefined') {
    tempLink.setAttribute('target', '_blank');
  }

  document.body.appendChild(tempLink);
  tempLink.click();
  document.body.removeChild(tempLink);
}

export function isAvailableBrowser() {
  const isChrome = /Chrome/i.test(navigator.userAgent);
  const isSafari = /constructor/i.test(window.HTMLElement) ||
    (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] ||
      (typeof safari !== 'undefined' && safari.pushNotification));

  return isChrome || isSafari;
}

export function jsonToQueryString(json) {
  return '?' +
    Object.keys(json).map(function(key) {
      return encodeURIComponent(key) + '=' +
        encodeURIComponent(json[key]);
    }).join('&');
}

export const mobileDetect = new MobileDetect(window.navigator.userAgent);

export const compose = (...functions) => functions.reduce((a, b) => (...args)=> a(b(...args)));