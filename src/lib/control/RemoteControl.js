import Message from 'src/lib/connection/MQTT/Message';

import * as WEBVIEWER from 'src/lib/enums/webviewer';
import {
  CATEGORY, TAG, TYPE, BOOL, STATUS_MESSAGE, VIEWER_STATUS, LASER_POINTER_MESSAGE, DRAW_MESSAGE
} from 'src/lib/enums/remoteControl';

export default class RemoteControl extends Message {
  constructor (...args) {
    super(...args);

    this.accessCode = null;
    this.viewerId = null;
    this.hostId = null;
    this.viewerTopic = null;
    this.hostTopic = null;
    this.willTopic = null;
    this.onReceivedEvent = null;
  }

  set({ accessCode, viewerId, hostId, onReceivedEvent }) {
    this.accessCode = accessCode;
    this.viewerId = viewerId;
    this.hostId = hostId;
    this.viewerTopic = `/client/remote_control/${viewerId}/${accessCode}`;
    this.hostTopic = `/client/remote_control/${hostId}/${accessCode}`;
    this.willTopic = `/client/force_disconnect/${accessCode}`;
    this.onReceivedEvent = onReceivedEvent;

    this.addSubscribe(this.viewerTopic);
    this.addSubscribe(this.willTopic);
  }

  getTopic() {
    return {
      viewer: this.viewerTopic,
      host: this.hostTopic
    }
  }

  relayInfo(data) {
    this.publish(this.hostTopic, {
      tag: TAG.RELAY_INFO,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [response] relay info',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.RELAY_INFO,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  requestMonitorsInfo() {
    const data = {
      type: TYPE.REQUEST,
      category: CATEGORY.MONITOR,
      message: BOOL.FALSE,
      reset: BOOL.FALSE
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [request] host monitor info',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  requestSelectedMonitorInfo(selectedMonitorIndex) {
    const data = {
      type: TYPE.REQUEST,
      category: CATEGORY.MONITOR,
      message: BOOL.TRUE,
      index: selectedMonitorIndex
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [request] selected host monitor info',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  viewerStatus(status) {
    const data = {
      type: TYPE.EVENT,
      category: CATEGORY.STATUS,
      message: STATUS_MESSAGE.VIEWER_STATUS,
      status: status
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [event] viewer status',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  activeChange(active) {
    const data = {
      type: TYPE.EVENT,
      category: CATEGORY.STATUS,
      message: STATUS_MESSAGE.VIEWER_STATUS,
      status: VIEWER_STATUS.ACTIVE_CHANGED,
      change: active ? BOOL.TRUE : BOOL.FALSE
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [event] active change',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  screenLock(lock) {
    const data = {
      type: TYPE.EVENT,
      category: CATEGORY.STATUS,
      message: STATUS_MESSAGE.VIEWER_STATUS,
      status: VIEWER_STATUS.SCREEN_LOCK,
      lock: lock ? BOOL.TRUE : BOOL.FALSE
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [event] screen lock',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  requestKmControl() {
    const data = {
      type: TYPE.REQUEST,
      category: CATEGORY.KM_CONTROL,
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [request] mouse, keyboard control',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  startKmControl() {
    const data = {
      type: TYPE.START,
      category: CATEGORY.KM_CONTROL,
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [start] mouse, keyboard control',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  endKmControl() {
    const data = {
      type: TYPE.END,
      category: CATEGORY.KM_CONTROL,
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [end] mouse, keyboard control',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  startLaserPointer(x_pos = 0, y_pos = 0) {
    const data = {
      type: TYPE.START,
      category: CATEGORY.LASER_POINTER,
      message: LASER_POINTER_MESSAGE.CIRCLE,
      x_pos,
      y_pos
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [start] laser pointer',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  endLaserPointer() {
    const data = {
      type: TYPE.END,
      category: CATEGORY.LASER_POINTER,
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [end] laser pointer',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  moveLaserPointer(x_pos, y_pos, message) {
    const data = {
      type: TYPE.EVENT,
      category: CATEGORY.LASER_POINTER,
      message: message ? BOOL.TRUE : BOOL.FALSE,
      x_pos,
      y_pos
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [event] move laser pointer',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  controlMouse(type, x_pos, y_pos, xdata) {
    const data = {
      type: TYPE.EVENT,
      category: CATEGORY.MOUSE,
      message: type,
      x_pos,
      y_pos
    };

    xdata && (data.xdata = xdata);

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [event] control mouse',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  controlKeyboard(type, keyCode) {
    const data = {
      type: TYPE.EVENT,
      category: CATEGORY.KEYBOARD,
      message: type,
      keycode: keyCode
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [event] control keyboard',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  sendSpecialKey(message) {
    const data = {
      type: TYPE.EVENT,
      category: CATEGORY.SPECIAL_KEY,
      message
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [event] send special key',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  startDraw() {
    const data = {
      type: TYPE.START,
      category: CATEGORY.DRAW,
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [start] draw',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  endDraw() {
    const data = {
      type: TYPE.END,
      category: CATEGORY.DRAW,
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [end] draw',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  drawInfo({ type = 0, color = 0, thickness = 3 } = {}) {
    const data = {
      type: TYPE.EVENT,
      category: CATEGORY.DRAW,
      message: DRAW_MESSAGE.INFO,
      drawtype: type,
      color,
      thickness
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [event] draw info',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  drawPaint(points) {
    const data = {
      type: TYPE.EVENT,
      category: CATEGORY.DRAW,
      message: DRAW_MESSAGE.DATA,
      count: points.length,
      points
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [event] draw paint',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  clearPaint() {
    const data = {
      type: TYPE.EVENT,
      category: CATEGORY.DRAW,
      message: DRAW_MESSAGE.CLEAR
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [event] draw paint',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  startScreenKeyboard() {
    const data = {
      type: TYPE.START,
      category: CATEGORY.SCREEN_KEYBOARD,
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [start] screen keyboard',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  endScreenKeyboard() {
    const data = {
      type: TYPE.END,
      category: CATEGORY.SCREEN_KEYBOARD,
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [end] exit screen keyboard',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );
  }

  exit() {
    const data = {
      type: TYPE.END,
      category: CATEGORY.CONNECTION,
    };

    this.publish(this.hostTopic, {
      tag: TAG.MESSAGE_PROTOCOL,
      identifier_id: this.viewerId,
      data
    });

    this.onReceivedEvent(
      WEBVIEWER.VIEWER_EVENT.LOG,
      {
        type: 'protocol',
        message: '[viewer to host] [end] exit remote control',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data
          }
        }
      }
    );

    this.disconnect();
  }
}