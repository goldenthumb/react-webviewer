import EventEmitter from 'event-emitter';
import { PRODUCT_CODE, LOGIN_OK, PING, SCREEN_EVENT} from 'src/lib/enums/screenControl';

const intToByteArray = (val) => {
  let byteArr= [0, 0, 0, 0];
  for (let i = 0; i < byteArr.length; i++) {
    const byteVal = val & 0xff;
    byteArr[i] = byteVal;
    val = (val - byteVal) / 256;
  }
  return byteArr;
};

const getJoinBinaryData = (accessCode, viewerId) => {
  const joinProductCode = PRODUCT_CODE;
  const accessCodeSize = accessCode.length;
  const viewerIdSize = viewerId.length;
  const size = 4 + 4 + viewerIdSize + 4 + accessCodeSize; // product code + viewerIdSize + 4 + accessCodeSize
  let data = new Uint8Array(size);
  let dataPos = 0;

  data.set(intToByteArray(joinProductCode), dataPos);
  dataPos += 4;

  data.set(intToByteArray(viewerIdSize), dataPos);
  dataPos += 4;

  for (let i = 0; i < viewerIdSize; ++i, ++dataPos) {
    data[dataPos] += viewerId.charCodeAt(i);
  }

  data.set(intToByteArray(accessCodeSize), dataPos);
  dataPos += 4;

  for (let j = 0; j < accessCodeSize; ++j, ++dataPos) {
    data[dataPos] += accessCode.charCodeAt(j);
  }

  return data;
};

const getPingBinaryData = () => {
  const data = new Uint8Array(4);
  data.set(intToByteArray(PING), 0);
  return data;
};

export default class ScreenControl {
  constructor (webSocket) {
    this._isJoin = false;
    this._keepAliveInterval = null;
    this._emitter = new EventEmitter();
    this._webSocket = webSocket;
    this._webSocket.onmessage = this._onMessage.bind(this);
    this._webSocket.onclose = this._onClose.bind(this);
  }

  setBinaryType(type) {
    this._webSocket.binaryType = type;
  }

  send(data) {
    this._webSocket.send(data);
  }

  on (eventName, listener) {
    this._emitter.on(eventName, listener);
  }

  join(accessCode, viewerId) {
    const joinData = getJoinBinaryData(accessCode, viewerId);
    this.send(joinData);
  }

  _keepAlive() {
    if (!this._isJoin) return;

    const pingData = getPingBinaryData();
    this._keepAliveInterval = setInterval(() => {
      this.send(pingData);
    }, 3000);
  }

  _onMessage(event) {
    if (!this._isJoin) {
      const dv = new DataView(event.data);
      const code = dv.getInt32(0, true);

      if (code === LOGIN_OK) {
        this._isJoin = true;
        this._emitter.emit(SCREEN_EVENT.JOIN_SUCCESS);
        this._keepAlive();
        return;
      }

      throw new Error('relay server access fail');
    }

    this._emitter.emit(SCREEN_EVENT.SCREEN_DATA, event.data);
  }

  _onClose(event) {
    clearInterval(this._keepAliveInterval);
    this._emitter.emit(SCREEN_EVENT.ON_CLOSE, event);
  }
}