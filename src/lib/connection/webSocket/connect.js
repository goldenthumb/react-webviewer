const connect = address => {
  return new Promise((resolve, reject) => {
    const webSocket = new WebSocket(`wss://${address.host}:${address.port}`);
    webSocket.onopen = () =>resolve(webSocket);
    webSocket.onerror = () => reject(new Error('faild connect!'));
  });
};

export default async function (relayServers) {
  for (const server of relayServers) {
    const webSocket = await connect(server);
    return {webSocket, connectedServer: server};
  }
}