import { Client } from 'rsup-mqtt';
import eventTypes from './eventTypes';
import storage from 'src/lib/storage/webviewer';

class Connection extends Client {
  constructor(...args) {
    super(...args);
    this._disconnected = false;
    
    this._setReconnect();
    this._setEvents();
  }

  publish(...args) {
    if(!this._disconnected) super.publish(...args);
  }
    
  _setReconnect() {
    let retries = 0;
    let span = timespan();
  
    this.on('close', async () => {
      this._disconnected = true;
      
      // 5초이내 짧은 주기의 재시도가 3회이상 반복되면 reload 합니다.
      retries = span() < 5 ? retries + 1 : 0;

      if (retries > 3) {
        const currentTime = new Date().getTime();
        const reconnectedTime = storage.get('reconnectedTime');

        // 15초 이내 reload 재 발생 시 stop 합니다.
        if (currentTime - reconnectedTime <= 15000) {
          storage.remove('reconnectedTime');
          alert('연결 실패');
          return;
        }

        storage.set('reconnectedTime', currentTime);
        return location.reload(true);
      }

      await this.reconnect();

      // after reconnected
      span = timespan();
      this._disconnected = false;
    });
  }
  
  _setEvents() {
    this.on('message', (topic, message) => {
      window.mqttLog && console.log({topic, message: message.json});

      for(const name in eventTypes){
        const match = eventTypes[name];
        const matched = match(message.json);
        
        if(matched) return this._emitter.emit(name, ...[].concat(matched));
      }
    });
  }
}

export default Connection;

function timespan() {
  const start = Date.now(); 
  return () => (Date.now() - start ) / 1000;
}
