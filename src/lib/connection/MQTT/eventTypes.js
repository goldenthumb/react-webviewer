import {
  BOOL, TAG, TYPE, CATEGORY, CONNECTION_MESSAGE, MONITOR_MESSAGE, STATUS_MESSAGE, HOST_STATUS,
  RESPONSE_VALUE, HOST_EVENT
} from 'src/lib/enums/remoteControl';

export default {
  [HOST_EVENT.RELAY_INFO]({tag}) {
    return tag === TAG.RELAY_INFO;
  },

  [HOST_EVENT.READY]({tag, data: {type, category, status}}) {
    return (
      tag === TAG.MESSAGE_PROTOCOL 
      && type === TYPE.EVENT
      && category === CATEGORY.STATUS
      && status === HOST_STATUS.HOST_READY
    );
  },

  [HOST_EVENT.ENGINE_START]({tag, data: {type, category, status}}) {
    return (
      tag === TAG.MESSAGE_PROTOCOL 
      && type === TYPE.EVENT 
      && category === CATEGORY.STATUS
      && status === HOST_STATUS.ENGINE_START
    );
  },

  [HOST_EVENT.SESSION_CHANGE_ON]({tag, data: {type, category, status, message, change}}) {
    return (
      tag === TAG.MESSAGE_PROTOCOL
      && type === TYPE.EVENT
      && category === CATEGORY.STATUS
      && status === HOST_STATUS.SESSION_CHANGED
      && message === STATUS_MESSAGE.HOST_STATUS
      && change === BOOL.TRUE
    );
  },

  [HOST_EVENT.SESSION_CHANGE_OFF]({tag, data: {type, category, status, message, change}}) {
    return (
      tag === TAG.MESSAGE_PROTOCOL
      && type === TYPE.EVENT
      && category === CATEGORY.STATUS
      && status === HOST_STATUS.SESSION_CHANGED
      && message === STATUS_MESSAGE.HOST_STATUS
      && change === BOOL.FALSE
    );
  },

  [HOST_EVENT.SCREEN_PAUSE]({tag, data: {type, category, message}}) {
    return (
      tag === TAG.MESSAGE_PROTOCOL 
      && type === TYPE.EVENT 
      && category === CATEGORY.SCREEN
      && message === BOOL.FALSE
    );
  },

  [HOST_EVENT.SCREEN_RESUME]({tag, data: {type, category, message}}) {
    return (
      tag === TAG.MESSAGE_PROTOCOL 
      && type === TYPE.EVENT 
      && category === CATEGORY.SCREEN
      && message === BOOL.TRUE
    );
  },

  [HOST_EVENT.MONITOR_INFO]({tag, data: {type, category, message, ...data}}) {
    if (
      tag === TAG.MESSAGE_PROTOCOL 
      && category === CATEGORY.MONITOR
      && (type === TYPE.RESPONSE || type === TYPE.EVENT)
      && message === MONITOR_MESSAGE.MONITORS
    ) {
      return {
        count: data.count,
        cx: data.cx,
        cy: data.cy,
        monitors: data.monitors
      };
    }
  },

  [HOST_EVENT.SELECTED_MONITOR_INDEX]({tag, data: {type, category, message, index}}) {
    if (
      tag === TAG.MESSAGE_PROTOCOL 
      && category === CATEGORY.MONITOR
      && (type === TYPE.RESPONSE || type === TYPE.EVENT)
      && message === MONITOR_MESSAGE.SELECTED_MONITOR
    ) {
      return [index];
    }
  },

  [HOST_EVENT.DISPLAY_CHANGE]({tag, data}) {
    const {type, category, message} = data;

    if (
      tag === TAG.MESSAGE_PROTOCOL 
      && category === CATEGORY.MONITOR
      && (type === TYPE.RESPONSE || type === TYPE.EVENT)
      && message === MONITOR_MESSAGE.DISPLAY_CHANGE
    ) {
      return data;
    }
  },

  [HOST_EVENT.KM_CONTROL]({tag, data: {type, category, message}}) {
    if (
      tag === TAG.MESSAGE_PROTOCOL
      && category === CATEGORY.KM_CONTROL
      && type === TYPE.RESPONSE
    ) {
      return [message === RESPONSE_VALUE.YES];
    }
  },

  [HOST_EVENT.SCREEN_KEYBOARD_ON]({tag, data: {type, category, message}}) {
    return (
      tag === TAG.MESSAGE_PROTOCOL
      && type === TYPE.EVENT
      && category === CATEGORY.SCREEN_KEYBOARD
      && message === BOOL.TRUE
    );
  },

  [HOST_EVENT.SCREEN_KEYBOARD_OFF]({tag, data: {type, category, message}}) {
    return (
      tag === TAG.MESSAGE_PROTOCOL
      && type === TYPE.EVENT
      && category === CATEGORY.SCREEN_KEYBOARD
      && message === BOOL.FALSE
    );
  },

  [HOST_EVENT.DISCONNECT]({data: {type, category}}) {
    return (
      type === TYPE.END
      && category === CATEGORY.CONNECTION
    );
  },

  [HOST_EVENT.FORCE_DISCONNECT]({data: {type, category, message}}) {
    return (
      type === TYPE.EVENT
      && category === CATEGORY.CONNECTION
      && message === CONNECTION_MESSAGE.DISCONNECT
    );
  },

};