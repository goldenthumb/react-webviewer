export default class Message {
  constructor (client) {
    this._mqtt = client;
  }

  hasTopic(topic){
    return this._mqtt.subscribed().includes(topic);
  }

  addSubscribe(topic) {
    this._mqtt.subscribe(topic);
  }

  removeSubscribe(topic) {
    if(this.hasTopic(topic)) this._mqtt.unsubscribe(topic);
  }

  publish(topic, payload, qos = 2) {
    this._mqtt.publish(topic, payload, {qos});
  }

  disconnect() {
    this._mqtt.disconnect();
  }
}