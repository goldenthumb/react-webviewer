import { connect } from 'rsup-mqtt';
import Connection from './Connection';

export default async function (servers = [], opts = {}) {
  opts = {
    timeout: 2,
    ssl : true,
    keepalive: 10,
    cleanSession: false,
    ...opts
  };

  for (const server of servers) {
    const client =  await connect({...opts, host: server.host, port: server.viewerPort}, Connection);
    return {client, connectedServer: server};
  }
}

