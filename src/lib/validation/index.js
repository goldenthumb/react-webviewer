import { struct } from 'superstruct';

const PushServers = struct({
  id: 'string?',
  host: 'string',
  viewerPort: 'number',
  clientPort: 'number'
});

const RelayServers = struct({
  id: 'string?',
  host: 'string',
  port: 'number'
});

const WillMessage = struct({
  url: 'string',
  params: 'object'
});

const ConnectInfoStruct = struct({
  accessCode: 'string',
  pushServers: [PushServers],
  relayServers: [RelayServers],
  willMessage: 'object?'  // TODO: 추후 수정해야함.
});

export { ConnectInfoStruct };