import version from 'src/lib/version';
const sessionStorage = window.sessionStorage;
const { setItem, clear } = sessionStorage;

const __KEY__ = '__webviewer__?v=' + version;

sessionStorage.setItem = (k, v) => {
  setItem.call(sessionStorage, k, v);
  if (k === __KEY__) instance._load();
};

sessionStorage.clear = () => {
  clear.call(sessionStorage);
  instance._load();
};

class Storage{
  constructor() {
    this._load();
  }

  get(k) {
    return this._data[k];
  }

  set(k, v) {
    this._data[k] = v;
    this._save();
  }

  remove(k) {
    delete  this._data[k];
    this._save();
  }

  clear() {
    this._data = {};
    this._save();
  }

  _load() {
    this._data = JSON.parse(sessionStorage.getItem(__KEY__)) || {}
  }

  _save() {
    setItem.call(
      sessionStorage, __KEY__,
      JSON.stringify(this._data)
    );
  }
}

const storage = new Storage();
export default storage;