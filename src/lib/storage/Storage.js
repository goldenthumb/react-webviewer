const storage = window.sessionStorage;

export default class Storage {
  get(key) {
    return storage.getItem(key);
  }

  set(key, val) {
    return storage.setItem(key, val);
  }

  clear() {
    storage.clear();
  }
}
