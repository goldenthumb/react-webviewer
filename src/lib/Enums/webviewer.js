const VIEWER_EVENT = {
  WAIT_OK: '/success/wait',
  JOIN_OK: '/success/join',
  CLOSE: '/close',
  ERROR: '/error',
  LOG: '/log'
};

export { VIEWER_EVENT };