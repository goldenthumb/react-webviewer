const TAG = {
  RELAY_INFO: 20310,    // relay 정보 요청
  MESSAGE_PROTOCOL: 20308,    // message protocol
};

const TYPE = {
  REQUEST: 0,   // 특정동작에 대한 요청을 보낼 때 사용. (ex 키보드, 마우스 제어)
  RESPONSE: 1,   // Request에 대한 응답을 보낼 때 사용.
  START: 2,   // 특정동작에 대한 시작.
  EVENT: 3,   // Action, Status 등에 대한 이벤트.
  END: 4    // 특정동작에 대한 종료.
};

const CATEGORY = {
  CONNECTION: 0,   // 연결관련 정보.
  KM_CONTROL: 1,   // 마우스, 키보드 제어에 대한 통합 요청을 보낼 때 사용.
  MOUSE: 2,   // 마우스 제어.
  KEYBOARD: 3,   // 키보드 제어.
  SCREEN: 4,   // 일시정지 기능.
  MONITOR: 5,   // 모니터 정보.
  STATUS: 6,   // PCHost, Webviewer 상태.
  DRAW: 7,   // 그리기
  LASER_POINTER: 8,    // 레이저 포인터
  SCREEN_KEYBOARD: 9,    // 화상 키보드
  SPECIAL_KEY: 10   // 특수키(조합키)
};

const BOOL = {
  FALSE: 0,
  TRUE: 1
};

const RESPONSE_VALUE = {
  FALSE: 0,
  TRUE: 1,
  YES: 2,
  NO: 3,
  OK: 4,
  CANCEL: 5
};

const CONNECTION_MESSAGE = {
  DISCONNECT: 0
};

const MONITOR_MESSAGE = {
  MONITORS: 0,
  SELECTED_MONITOR: 1,
  DISPLAY_CHANGE: 2
};

const STATUS_MESSAGE = {
  VIEWER_STATUS: 0,
  HOST_STATUS: 1
};

const MOUSE_MESSAGE = {
  MOVE: 0,
  L_BTN_DOWN: 1,
  L_BTN_UP: 2,
  R_BTN_DOWN: 3,
  R_BTN_UP: 4,
  M_BTN_DOWN: 5,
  M_BTN_UP: 6,
  X_BTN_DOWN: 7,
  X_BTN_UP: 8,
  WHEEL_DOWN: 9,
  WHEEL_UP: 10
};

const LASER_POINTER_MESSAGE = {
  ARROW: 0,
  CIRCLE: 1
};

const KEYBOARD_MESSAGE = {
  DOWN: 0,
  UP: 1
};

const SPECIAL_KEY_MESSAGE = {
  CTRL_ALT_DEL: 0
};

const DRAW_MESSAGE = {
  INFO: 0,
  DATA: 1,
  CLEAR: 2
};

const HOST_STATUS = {
  IDLE: 0,
  HOST_READY: 1,
  ENGINE_START: 2,
  SESSION_CHANGED: 3
};

const VIEWER_STATUS = {
  IDLE: 0,
  SCREEN_READY: 1,
  ACTIVE_CHANGED: 2,
  SCREEN_LOCK: 3
};

const HOST_EVENT = {
  RELAY_INFO: '/request/relay-info',
  READY: '/response/host/ready',
  MONITOR_INFO: '/response/host/monitor-info',
  SELECTED_MONITOR_INDEX: '/response/host/selected-monitor-index',
  DISPLAY_CHANGE: '/response/host/display-change',
  ENGINE_START: '/response/host/engine-start',
  KM_CONTROL: '/response/host/km-control',
  SCREEN_PAUSE: '/response/host/screen-pause',
  SCREEN_RESUME: '/response/host/screen-resume',
  SESSION_CHANGE_ON: '/response/host/session-change-on',
  SESSION_CHANGE_OFF: '/response/host/session-change-off',
  SCREEN_KEYBOARD_ON: '/response/host/screen-keyboard-on',
  SCREEN_KEYBOARD_OFF: '/response/host/screen-keyboard-off',
  DISCONNECT: '/response/host/disconnect',
  FORCE_DISCONNECT: '/response/host/force_disconnect'
};

export {
  TAG, TYPE, CATEGORY, BOOL, CONNECTION_MESSAGE, MONITOR_MESSAGE, STATUS_MESSAGE, MOUSE_MESSAGE,
  LASER_POINTER_MESSAGE, SPECIAL_KEY_MESSAGE, KEYBOARD_MESSAGE, DRAW_MESSAGE, HOST_STATUS, VIEWER_STATUS,
  RESPONSE_VALUE, HOST_EVENT
};