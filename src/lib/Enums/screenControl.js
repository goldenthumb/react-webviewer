const PRODUCT_CODE = 1200;
const LOGIN_OK = 1004;
const PING = 1005;

const SCREEN_EVENT = {
  JOIN_SUCCESS: '/response/join-success',
  SCREEN_DATA: '/response/screen-data',
  ON_CLOSE: 'response/close-event'
};

export { PRODUCT_CODE, LOGIN_OK, PING, SCREEN_EVENT };