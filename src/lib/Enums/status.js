const HOST = {
  IDLE: 0,
  RUN: 1,
  EXIT: 2,
  PAUSE: 3,
  SESSION_CHANGE: 4,
  DISPLAY_CHANGE: 5
};

const VIEWER = {
  IDLE: 0,
  WAITING: 1,
  RUN: 2,
  EXIT: 3
};

export { HOST, VIEWER };