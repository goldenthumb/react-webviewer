import './index.scss';
import Webviewer from './containers/Webviewer';
import viewerStorage from 'src/lib/storage/webviewer';
import {VIEWER_EVENT} from 'src/lib/enums/webviewer';
import {ConnectInfoStruct} from 'src/lib/validation';

export default Webviewer;
export {viewerStorage, VIEWER_EVENT, ConnectInfoStruct};