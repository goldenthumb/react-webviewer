import React, {Component} from 'react';
import storage from 'src/lib/storage/webviewer';
import languages from 'src/lib/i18n';
import {isAvailableBrowser} from 'src/lib/utils';
import {ConnectInfoStruct} from 'src/lib/validation';
import * as WEBVIEWER from 'src/lib/enums/webviewer';

const withInitialize = () => (WrappedComponent) => {
  return class Wrapper extends Component {
    state = {
      i18n: null
    };

    componentDidMount() {
      const {locale, connectInfo, onReceivedEvent} = this.props;

      window.MediaSource = window.MediaSource || window.WebKitMediaSource;

      if (!isAvailableBrowser()) {
        const message = 'This browser not support. (chrome, safari only)';
        onReceivedEvent(WEBVIEWER.VIEWER_EVENT.ERROR, {type: 'not support browser', message});
        return;
      }

      if (!window.MediaSource) {
        const message = 'MediaSource API is not available';
        onReceivedEvent(WEBVIEWER.VIEWER_EVENT.ERROR, {type: 'not support browser', message});
        return;
      }

      if (!window.WebSocket) {
        const message = 'This browser does not support webSocket';
        onReceivedEvent(WEBVIEWER.VIEWER_EVENT.ERROR, {type: 'not support browser', message});
        return;
      }

      if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
        const message = 'This device(iOS) not support.';
        onReceivedEvent(WEBVIEWER.VIEWER_EVENT.ERROR, {type: 'not support browser', message});
        return;
      }

      try {
        ConnectInfoStruct(connectInfo);
      } catch (error) {
        throw {...error};
      }

      storage.set('locale', locale);

      this.setState({
        i18n: languages.setLocale(locale)
      });

      if (storage.get('accessCode') !== connectInfo.accessCode) {
        storage.set('accessCode', connectInfo.accessCode);
        storage.remove('activatedControlApps');
        storage.remove('pushServer');
        storage.remove('relayServer');
      }
    }

    render() {
      const {i18n} = this.state;

      return (
        i18n && <WrappedComponent {...this.props} i18n={i18n}/>
      );
    }
  }
};

export default withInitialize;