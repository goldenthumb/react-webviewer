import React, {Component} from 'react';
import {compose} from 'src/lib/utils';

import * as STATUS from 'src/lib/enums/status';
import * as REMOTE_CONTROL from 'src/lib/enums/remoteControl';
import * as WEBVIEWER from 'src/lib/enums/webviewer';
import storage from 'src/lib/storage/webviewer';

import {useBase} from 'src/contexts/base';
import {useHost} from 'src/contexts/host';
import {useViewer} from 'src/contexts/viewer';

import Viewer from 'src/components/Viewer';

class ViewerContainer extends Component {
  constructor(props) {
    super(props);

    const {config, remoteControl, baseActions} = props;

    remoteControl.set({
      accessCode: config.accessCode,
      viewerId: config.viewerId,
      hostId: config.hostId,
      onReceivedEvent: baseActions.onReceivedEvent
    });

    this.prevHostStatus = null;
  }

  componentDidMount() {
    this.runRemoteControl();
  }

  runRemoteControl() {
    const {client, config, remoteControl, baseActions, hostActions} = this.props;
    const topic = remoteControl.getTopic();

    client.on(REMOTE_CONTROL.HOST_EVENT.RELAY_INFO, () => {
      const relayServer = storage.get('relayServer');

      hostActions.updateOperatingStatus(STATUS.HOST.RUN);

      baseActions.onReceivedEvent(
        WEBVIEWER.VIEWER_EVENT.LOG,
        {
          type: 'protocol',
          message: '[host to viewer] [request] relay info',
          data: {
            topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.RELAY_INFO,
              identifier_id: config.hostId
            }
          }
        }
      );

      remoteControl.relayInfo({
        relay: {
          ip: relayServer.host,
          port: relayServer.port.toString(),
          domain: relayServer.host
        },
        roomid: config.accessCode
      });
    });

    client.on(REMOTE_CONTROL.HOST_EVENT.READY, () => {
      baseActions.onReceivedEvent(
        WEBVIEWER.VIEWER_EVENT.LOG,
        {
          type: 'protocol',
          message: '[host to viewer] [event] host ready',
          data: {
            topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        }
      );

      remoteControl.requestMonitorsInfo();
    });

    client.on(REMOTE_CONTROL.HOST_EVENT.MONITOR_INFO, (data) => {
      const monitorIndex = 0; // 첫 번째 모니터 선택

      hostActions.updateMonitorInfo({
        ...data,
        selectedMonitor: monitorIndex
      });

      baseActions.onReceivedEvent(
        WEBVIEWER.VIEWER_EVENT.LOG,
        {
          type: 'protocol',
          message: '[host to viewer] [response] host monitor info',
          data: {
            topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId,
              data
            }
          }
        }
      );

      remoteControl.requestSelectedMonitorInfo(monitorIndex);
    });

    client.on(REMOTE_CONTROL.HOST_EVENT.SELECTED_MONITOR_INDEX, (index) => {
      const {host} = this.props;

      if (host.status === STATUS.HOST.DISPLAY_CHANGE) {
        hostActions.updateOperatingStatus(this.prevHostStatus);
        this.prevHostStatus = null;
      }

      hostActions.updateMonitorInfo({
        ...host.monitorInfo,
        selectedMonitor: index
      });

      baseActions.onReceivedEvent(
        WEBVIEWER.VIEWER_EVENT.LOG,
        {
          type: 'protocol',
          message: '[host to viewer] [response] selected host monitor info',
          data: {
            topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId,
              data: index
            }
          }
        }
      );

      remoteControl.viewerStatus(REMOTE_CONTROL.VIEWER_STATUS.SCREEN_READY);
    });

    client.on(REMOTE_CONTROL.HOST_EVENT.ENGINE_START, () => {
      this.updateViewerStatus(STATUS.VIEWER.RUN);

      baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.JOIN_OK);
      baseActions.onReceivedEvent(
        WEBVIEWER.VIEWER_EVENT.LOG,
        {
          type: 'protocol',
          message: '[host to viewer] [event] engine start (join ok)',
          data: {
            topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        }
      );
    });

    client.on(REMOTE_CONTROL.HOST_EVENT.KM_CONTROL, (available) => {
      const kmControl = this.hasActivatedApp(0);
      const laserPointer = this.hasActivatedApp(1);
      const draw = this.hasActivatedApp(2);

      kmControl && remoteControl.startKmControl();
      laserPointer && remoteControl.startLaserPointer();
      draw && remoteControl.startDraw();

      hostActions.updateControlStatus(available);
      baseActions.onReceivedEvent(
        WEBVIEWER.VIEWER_EVENT.LOG,
        {
          type: 'protocol',
          message: '[host to viewer] [response] mouse, keyboard control',
          data: {
            topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId,
              data: available
            }
          }
        }
      );
    });

    client.on(REMOTE_CONTROL.HOST_EVENT.SCREEN_PAUSE, () => {
      const {host} = this.props;

      this.prevHostStatus = host.status;
      hostActions.updateOperatingStatus(STATUS.HOST.PAUSE);
      baseActions.onReceivedEvent(
        WEBVIEWER.VIEWER_EVENT.LOG,
        {
          type: 'protocol',
          message: '[host to viewer] [event] screen pause',
          data: {
            topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        }
      );
    });

    client.on(REMOTE_CONTROL.HOST_EVENT.SCREEN_RESUME, () => {
      hostActions.updateOperatingStatus(this.prevHostStatus);
      this.prevHostStatus = null;
      baseActions.onReceivedEvent(
        WEBVIEWER.VIEWER_EVENT.LOG,
        {
          type: 'protocol',
          message: '[host to viewer] [event] screen resume',
          data: {
            topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        }
      );
    });

    client.on(REMOTE_CONTROL.HOST_EVENT.DISPLAY_CHANGE, (data) => {
      const {host} = this.props;

      this.prevHostStatus = host.status;
      hostActions.updateOperatingStatus(STATUS.HOST.DISPLAY_CHANGE);
      baseActions.onReceivedEvent(
        WEBVIEWER.VIEWER_EVENT.LOG,
        {
          type: 'protocol',
          message: '[host to viewer] [event] display change',
          data: {
            topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId,
              data
            }
          }
        }
      );

      remoteControl.requestMonitorsInfo();
    });

    client.on(REMOTE_CONTROL.HOST_EVENT.SESSION_CHANGE_ON, () => {
      const {host} = this.props;

      this.prevHostStatus = host.status;
      hostActions.updateOperatingStatus(STATUS.HOST.SESSION_CHANGE);
      baseActions.onReceivedEvent(
        WEBVIEWER.VIEWER_EVENT.LOG,
        {
          type: 'protocol',
          message: '[host to viewer] [event] session change on',
          data: {
            topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        }
      );
    });

    client.on(REMOTE_CONTROL.HOST_EVENT.SESSION_CHANGE_OFF, () => {
      hostActions.updateOperatingStatus(this.prevHostStatus);
      this.prevHostStatus = null;
      baseActions.onReceivedEvent(
        WEBVIEWER.VIEWER_EVENT.LOG,
        {
          type: 'protocol',
          message: '[host to viewer] [event] session change off',
          data: {
            topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        }
      );
    });

    client.on(REMOTE_CONTROL.HOST_EVENT.SCREEN_KEYBOARD_ON, () => {
      const screenKeyboard = this.findControlApp(4);
      this.addActivatedControlApp(screenKeyboard);

      baseActions.onReceivedEvent(
        WEBVIEWER.VIEWER_EVENT.LOG,
        {
          type: 'protocol',
          message: '[host to viewer] [event] screen keyboard on',
          data: {
            topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        }
      );
    });

    client.on(REMOTE_CONTROL.HOST_EVENT.SCREEN_KEYBOARD_OFF, () => {
      const screenKeyboard = this.findControlApp(4);
      this.removeActivatedControlApp(screenKeyboard);

      baseActions.onReceivedEvent(
        WEBVIEWER.VIEWER_EVENT.LOG,
        {
          type: 'protocol',
          message: '[host to viewer] [event] screen keyboard off',
          data: {
            topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        }
      );
    });

    client.on(REMOTE_CONTROL.HOST_EVENT.DISCONNECT, () => {
      hostActions.updateOperatingStatus(STATUS.HOST.EXIT);
      baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.CLOSE, {
        target: 'host',
        server: 'mqtt',
        normal: true
      });

      client.disconnect();
    });

    client.on(REMOTE_CONTROL.HOST_EVENT.FORCE_DISCONNECT, () => {
      hostActions.updateOperatingStatus(STATUS.HOST.EXIT);
      baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.CLOSE, {
        target: 'host',
        server: 'mqtt',
        normal: false
      });

      client.disconnect();
    });
  }

  hasActivatedApp(id) {
    const {activatedControlApps} = this.props.viewer;
    return !!activatedControlApps.find(app => app.appId === id);
  }

  findControlApp(id) {
    const {controlApps} = this.props.config;
    return controlApps.find(app => app.id === id);
  }

  updateViewerStatus = (status) => {
    const {viewerActions} = this.props;
    viewerActions.updateOperatingStatus(status);
  };

  addActivatedControlApp = (selectedApp) => {
    const {viewer, viewerActions} = this.props;
    const filteredActivatedControlApps = viewer.activatedControlApps.filter(app => app.groupId !== selectedApp.groupId);

    viewerActions.updateActivatedControlApp([
      ...filteredActivatedControlApps,
      {appId: selectedApp.id, groupId: selectedApp.groupId}
    ]);
  };

  removeActivatedControlApp = (selectedApp) => {
    const {viewer, viewerActions} = this.props;
    const filteredActivatedControlApps = viewer.activatedControlApps.filter(app => app.appId !== selectedApp.id);

    viewerActions.updateActivatedControlApp([
      ...filteredActivatedControlApps
    ]);
  };

  render() {
    const {viewer, host} = this.props;

    return (
      <Viewer viewer={viewer} host={host}/>
    );
  }
}

const enhance = compose(
  useBase,
  useHost,
  useViewer
);

export default enhance(ViewerContainer);