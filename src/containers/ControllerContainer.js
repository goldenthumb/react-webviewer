import React, {Component} from 'react';
import {compose} from 'src/lib/utils';

import {useBase} from 'src/contexts/base';
import {useHost} from 'src/contexts/host';
import {useViewer} from 'src/contexts/viewer';

import Controller from 'src/components/Controller';

class ControllerContainer extends Component {
  render() {
    const {i18n, host, viewer} = this.props;

    return (
      <Controller i18n={i18n} host={host} viewer={viewer}/>
    );
  }
}

const enhance = compose(
  useBase,
  useHost,
  useViewer
);

export default enhance(ControllerContainer);