import React, {Component} from 'react';

import * as REMOTE_CONTROL from 'src/lib/enums/remoteControl';
import * as STATUS from 'src/lib/enums/status';

import {compose} from 'src/lib/utils';

import {useViewer} from 'src/contexts/viewer';
import {useBase} from 'src/contexts/base';
import {useHost} from 'src/contexts/host';

import FunctionKeyboard from 'src/components/FunctionKeyboard';

class FunctionKeyboardContainer extends Component {
  hasActivatedApp(id) {
    const {activatedControlApps} = this.props.viewer;
    return !!activatedControlApps.find(app => app.appId === id);
  }

  isUseKmControl() {
    const {host, viewer} = this.props;
    return (host.useKmControl && viewer.status === STATUS.VIEWER.RUN);
  }

  handleKeyboardControl = (type, keyCode) => {
    if (!this.isUseKmControl()) return;

    const {remoteControl} = this.props;
    remoteControl.controlKeyboard(type, keyCode);
  };

  updatePressedFunctionKeys = (pressedFunctionKeys) => {
    const {viewerActions} = this.props;
    viewerActions.updatePressedFunctionKeys(pressedFunctionKeys);
  };

  changeFunctionKeyStatus = (id) => {
    const {viewer} = this.props;
    const {pressedFunctionKeys} = viewer;
    const findKeyId = pressedFunctionKeys.includes(id);
    const nextPressedFunctionKeys = findKeyId ?
      pressedFunctionKeys.filter(keyId => keyId !== id) :
      pressedFunctionKeys.concat(id);

    this.updatePressedFunctionKeys(nextPressedFunctionKeys);
  };

  clearPressedFunctionKeys() {
    const {config, viewer} = this.props;

    if (!viewer.pressedFunctionKeys.length) return;

    config.functionKeys
      .filter(key => viewer.pressedFunctionKeys.includes(key.id))
      .map(key => key.keyCode)
      .forEach(keyCode => this.handleKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.UP, keyCode));

    this.updatePressedFunctionKeys([]);
  }

  componentDidUpdate() {
    const kmControl = this.hasActivatedApp(0);
    const functionKeyboard = this.hasActivatedApp(3);

    if (!kmControl || !functionKeyboard) {
      this.clearPressedFunctionKeys();
    }
  }

  render() {
    const {config, host, viewer, remoteControl} = this.props;
    const kmControl = this.hasActivatedApp(0);
    const functionKeyboard = this.hasActivatedApp(3);

    if (!kmControl || !functionKeyboard) return null;

    return (
      <FunctionKeyboard
        config={config}
        host={host}
        viewer={viewer}
        remoteControl={remoteControl}
        updatePressedFunctionKeys={this.updatePressedFunctionKeys}
        changeFunctionKeyStatus={this.changeFunctionKeyStatus}
        onKeyboardControl={this.handleKeyboardControl}
        clearPressedFunctionKeys={this.clearPressedFunctionKeys}
      />
    );
  }
}

const enhance = compose(
  useBase,
  useHost,
  useViewer
);

export default enhance(FunctionKeyboardContainer);