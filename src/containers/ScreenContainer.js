import React, {Component} from 'react';
import {compose} from 'src/lib/utils';
import * as STATUS from 'src/lib/enums/status';
import * as WEBVIEWER from 'src/lib/enums/webviewer';

import {useBase} from 'src/contexts/base';
import {useHost} from 'src/contexts/host';
import {useViewer} from 'src/contexts/viewer';

import Screen from 'src/components/Screen';

class ScreenContainer extends Component {
  addActivatedControlApp = (selectedApp) => {
    const {viewer, viewerActions} = this.props;
    const filteredActivatedControlApps = viewer.activatedControlApps.filter(app => app.groupId !== selectedApp.groupId);

    viewerActions.updateActivatedControlApp([
      ...filteredActivatedControlApps,
      {appId: selectedApp.id, groupId: selectedApp.groupId}
    ]);
  };

  removeActivatedControlApp = (selectedApp) => {
    const {viewer, viewerActions} = this.props;
    const filteredActivatedControlApps = viewer.activatedControlApps.filter(app => app.appId !== selectedApp.id);

    viewerActions.updateActivatedControlApp([
      ...filteredActivatedControlApps
    ]);
  };

  updateViewerStatus = (status) => {
    const {viewerActions} = this.props;
    viewerActions.updateOperatingStatus(status);
  };

  updatePressedFunctionKeys = (pressedFunctionKeys) => {
    const {viewerActions} = this.props;
    viewerActions.updatePressedFunctionKeys(pressedFunctionKeys);
  };

  changeScreenSize = ({width, height, overflow}) => {
    const {viewerActions} = this.props;
    viewerActions.changeScreenSize({width, height, isScreenOverflow: overflow});
  };

  exitRemoteControl = () => {
    const {baseActions, hostActions} = this.props;
    hostActions.updateOperatingStatus(STATUS.HOST.EXIT);
    baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.CLOSE, {
      target: 'host',
      server: 'relay',
      normal: true
    });
  };

  render() {
    const {i18n, config, host, viewer, remoteControl, screenControl, baseActions} = this.props;

    return (
      <Screen
        i18n={i18n}
        host={host}
        viewer={viewer}
        config={config}
        screenControl={screenControl}
        remoteControl={remoteControl}
        baseActions={baseActions}
        addActivatedControlApp={this.addActivatedControlApp}
        removeActivatedControlApp={this.removeActivatedControlApp}
        updateViewerStatus={this.updateViewerStatus}
        updatePressedFunctionKeys={this.updatePressedFunctionKeys}
        changeScreenSize={this.changeScreenSize}
        exitRemoteControl={this.exitRemoteControl}
      />
    );
  }
}

const enhance = compose(
  useBase,
  useHost,
  useViewer
);

export default enhance(ScreenContainer);