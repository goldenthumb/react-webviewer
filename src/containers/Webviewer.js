import React, {Component} from 'react';

import {compose} from 'src/lib/utils';
import {SPECIAL_KEY_MESSAGE} from 'src/lib/enums/remoteControl';

import RemoteControl from 'src/lib/control/RemoteControl';
import ScreenControl from 'src/lib/control/ScreenControl';

import withInitialize from 'src/containers/withInitialize';
import withConnect from 'src/containers/withConnect';

import {BaseProvider} from 'src/contexts/base';
import {HostProvider} from 'src/contexts/host';
import {ViewerProvider} from 'src/contexts/viewer';

import ViewerContainer from 'src/containers/ViewerContainer';

class Webviewer extends Component {
  constructor(props) {
    super(props);

    const {connectInfo, client, webSocket, viewerId, hostId} = this.props;
    const {accessCode} = connectInfo;

    this.remoteControl = new RemoteControl(client);
    this.screenControl = new ScreenControl(webSocket);

    this.config = {
      accessCode,
      viewerId,
      hostId,
      controlApps: [
        {id: 0, name: 'kmControl', groupId: 0},
        {id: 1, name: 'laserPointer', groupId: 0},
        {id: 2, name: 'draw', groupId: 0},
        {id: 3, name: 'functionKeyboard', groupId: 1},
        {id: 4, name: 'screenKeyboard', groupId: 1},
        {id: 5, name: 'fullScreen', groupId: 2}
      ],
      canvasTool: {
        isDrawing: false,
        drawPoints: [],
        x: -1,
        y: -1,
        colors: [
          {id: 0, name: 'red', hex: '#fe0002'},
          {id: 1, name: 'yellow', hex: '#fffe03'},
          {id: 2, name: 'green', hex: '#00fe00'},
          {id: 3, name: 'sky', hex: '#00ffff'},
          {id: 4, name: 'pink', hex: '#ff00ff'}
        ],
        thickness: 3,
      },
      functionKeys: [
        {id: 0, name: 'Esc', keyCode: 27},
        {id: 1, name: 'Tab', keyCode: 9},
        {id: 2, name: 'Ctrl+Alt+Del', keyCode: SPECIAL_KEY_MESSAGE.CTRL_ALT_DEL, specialKey: true},
        {id: 3, name: 'Shift', keyCode: 16, pressable: true},
        {id: 4, name: 'Ctrl', keyCode: 17, pressable: true},
        {id: 5, name: 'Alt', keyCode: 18, pressable: true},
        {id: 6, name: 'Win', keyCode: 91, pressable: true}
      ],
      screenRatios: ['fit', '100', '125', '150']
    };
  }

  render() {
    const {i18n, client, onReceivedEvent} = this.props;
    const {config, remoteControl, screenControl} = this;
    const base = {i18n, config, client, remoteControl, screenControl};

    return (
      <BaseProvider base={base} onReceivedEvent={onReceivedEvent}>
        <HostProvider>
          <ViewerProvider>
            <ViewerContainer/>
          </ViewerProvider>
        </HostProvider>
      </BaseProvider>
    );
  }
}

const enhance = compose(
  withInitialize(),
  withConnect()
);

export default enhance(Webviewer);