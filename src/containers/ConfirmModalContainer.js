import React, {Component} from 'react';
import {compose} from 'src/lib/utils';
import * as STATUS from 'src/lib/enums/status';
import * as WEBVIEWER from 'src/lib/enums/webviewer';

import {useViewer} from 'src/contexts/viewer';
import {useBase} from 'src/contexts/base';

import ConfirmModal from 'src/components/ConfirmModal';

class ConfirmModalContainer extends Component {
  handleCancel = () => {
    const {baseActions} = this.props;
    baseActions.hideModal('exitViewer');
  };

  handleConfirm = () => {
    const {baseActions, viewerActions, remoteControl} = this.props;
    baseActions.hideModal('exitViewer');
    remoteControl.exit();
    viewerActions.updateOperatingStatus(STATUS.VIEWER.EXIT);
    baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.CLOSE, {
      target: 'viewer',
      normal: true
    });
  };

  render() {
    const {i18n, modal, viewer} = this.props;
    const {handleCancel, handleConfirm} = this;

    return (
      <ConfirmModal
        i18n={i18n}
        visible={modal.exitViewer}
        fixed={viewer.isScreenOverflow}
        description={i18n.message('confirmExitRemoteControl')}
        onCancel={handleCancel}
        onConfirm={handleConfirm}
      />
    );
  }
}

const enhance = compose(
  useBase,
  useViewer
);

export default enhance(ConfirmModalContainer);