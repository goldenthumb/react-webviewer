import React, {Component} from 'react';
import {compose} from 'src/lib/utils';

import {useViewer} from 'src/contexts/viewer';
import {useBase} from 'src/contexts/base';
import {useHost} from 'src/contexts/host';

import ToolBar from 'src/components/ToolBar';

class ToolbarContainer extends Component {
  addActivatedControlApp = (selectedApp) => {
    const {viewer, viewerActions} = this.props;
    const filteredActivatedControlApps = viewer.activatedControlApps.filter(app => app.groupId !== selectedApp.groupId);

    viewerActions.updateActivatedControlApp([
      ...filteredActivatedControlApps,
      {appId: selectedApp.id, groupId: selectedApp.groupId}
    ]);
  };

  removeActivatedControlApp = (selectedApp) => {
    const {viewer, viewerActions} = this.props;
    const filteredActivatedControlApps = viewer.activatedControlApps.filter(app => app.appId !== selectedApp.id);

    viewerActions.updateActivatedControlApp([
      ...filteredActivatedControlApps
    ]);
  };

  changePaintColor = (paintColorIndex) => {
    const {viewer} = this.props;
    if (viewer.paintColor === paintColorIndex) return;

    const {viewerActions} = this.props;
    viewerActions.changePaintColor(paintColorIndex);
  };

  handleScreenCapture = () => {
    const {viewerActions} = this.props;
    viewerActions.updateScreenCaptureCount();
  };

  changeScreenRatio = ({ratio} = {}) => {
    const {viewer, config} = this.props;
    const lastScreenRatio = config.screenRatios.length - 1;
    let nextScreenRatio = viewer.screenRatio === lastScreenRatio ? 0 : viewer.screenRatio + 1;

    if (ratio) nextScreenRatio = config.screenRatios.indexOf(ratio);

    const {viewerActions} = this.props;
    viewerActions.changeScreenRatio(nextScreenRatio);
  };

  handleConfirmModal = () => {
    const {baseActions} = this.props;
    baseActions.showModal('exitViewer');
  };

  render() {
    const {config, i18n, host, viewer, remoteControl} = this.props;

    return (
      <ToolBar
        config={config}
        i18n={i18n}
        host={host}
        viewer={viewer}
        remoteControl={remoteControl}
        addActivatedControlApp={this.addActivatedControlApp}
        removeActivatedControlApp={this.removeActivatedControlApp}
        changePaintColor={this.changePaintColor}
        onScreenCapture={this.handleScreenCapture}
        changeScreenRatio={this.changeScreenRatio}
        onConfirmModal={this.handleConfirmModal}
      />
    );
  }
}

const enhance = compose(
  useBase,
  useHost,
  useViewer
);

export default enhance(ToolbarContainer);