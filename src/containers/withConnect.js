import React, {Component} from 'react';

import * as WEBVIEWER from 'src/lib/enums/webviewer';
import storage from 'src/lib/storage/webviewer';
import {jsonToQueryString} from 'src/lib/utils';

import mqttConnect from 'src/lib/connection/MQTT';
import wsConnect from 'src/lib/connection/webSocket';

const withConnect = (option) => (WrappedComponent) => {
  return class extends Component {
    state = {
      client: null,
      webSocket: null
    };

    constructor(props) {
      super(props);

      const {accessCode, willMessage, viewerId, hostId} = this.props.connectInfo;

      this.viewerId = viewerId || accessCode + '_v0';
      this.hostId = hostId || accessCode + '_h0';

      this.option = option || {
        timeout: 2,
        useSSL: true,
        mqttVersion: 3,
        keepAliveInterval: 10,
        cleanSession: false,
        will: willMessage ? {
          topic: '/client/force_disconnect/' + accessCode,
          payload: {
            url: willMessage.url + jsonToQueryString(willMessage.params),
            identifier_id: this.viewerId,
            data: willMessage.data || {}
          },
          qos: 0,
          retain: true
        } : null
      };

      this.initialize(this.option);
    }

    async initialize(option) {
      const {onReceivedEvent, connectInfo} = this.props;
      const getServers = type => [].concat(storage.get(type) || connectInfo[type + 's']);
      const pushServers = getServers('pushServer');
      const relayServers = getServers('relayServer');
      const concurrentTasks = [mqttConnect(pushServers, option), wsConnect(relayServers)];
      const [mqttInfo, wsInfo] = await Promise.all(concurrentTasks);

      if (!mqttInfo || !wsInfo) {
        const message = !mqttInfo && !wsInfo ? 'push, relay' : !mqttInfo ? 'push' : 'relay';
        onReceivedEvent(WEBVIEWER.VIEWER_EVENT.ERROR, {type: 'connect fail', message});
        return;
      }

      storage.set('pushServer', mqttInfo.connectedServer);
      storage.set('relayServer', wsInfo.connectedServer);

      this.setState({
        client: mqttInfo.client,
        webSocket: wsInfo.webSocket
      });
    }

    render() {
      const {viewerId, hostId} = this;
      const {client, webSocket} = this.state;

      if (!client || !webSocket) return null;

      return (
        <WrappedComponent
          {...this.props}
          client={client}
          webSocket={webSocket}
          viewerId={viewerId}
          hostId={hostId}
        />
      );
    }
  };
};

export default withConnect;