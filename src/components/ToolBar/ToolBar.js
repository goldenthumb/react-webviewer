import React, {Component} from 'react';
import styles from './ToolBar.scss';

import storage from 'src/lib/storage/webviewer';
import {mobileDetect} from 'src/lib/utils';

import ScreenNav from 'src/components/ScreenNav';
import ToolBarBaseDropdown from 'src/components/ToolBarBaseDropdown';
import ToolBarColorPickerDropdown from 'src/components/ToolBarColorPickerDropdown';

class ToolBar extends Component {
  state = {
    isOpenToolbar: true,
    isOpenDropdown: false,
    activatedDropdown: null,
    selectedMouseEvent: this.hasActivatedApp(1) ? 'laser-pointer' : 'km-control',
    selectedKeyboardEvent: this.hasActivatedApp(4) ? 'screen-keyboard' : 'function-keyboard'
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const {remoteControl} = this.props;
    const hasActivatedControlApps = storage.get('activatedControlApps') || false;
    const isActivatedControlApps = hasActivatedControlApps && hasActivatedControlApps.length;
    isActivatedControlApps ? remoteControl.requestKmControl() : this.toggleKmControl();
  }

  componentDidUpdate(prevProps) {
    const prevActivatedControlApps = prevProps.viewer.activatedControlApps;
    const activatedControlApps = this.props.viewer.activatedControlApps;

    if (prevActivatedControlApps !== activatedControlApps) {
      this.updateActiveRemoteControl(prevActivatedControlApps, activatedControlApps);
    }
  }

  findControlApp(id) {
    const {controlApps} = this.props.config;
    return controlApps.find(app => app.id === id);
  }

  findActivatedControlApp(selectedApp) {
    const {activatedControlApps} = this.props.viewer;
    return !(activatedControlApps.findIndex(app => app.appId === selectedApp.id) === -1);
  }

  hasActivatedApp(id) {
    const {activatedControlApps} = this.props.viewer;
    return !!activatedControlApps.find(app => app.appId === id);
  }

  hasActivatedDropdown(target) {
    const {isOpenDropdown, activatedDropdown} = this.state;
    return (isOpenDropdown && activatedDropdown === target);
  }

  toggleDropdown(target) {
    const {activatedDropdown} = this.state;

    this.setState({
      activatedDropdown: target,
      isOpenDropdown: (activatedDropdown !== target) ? true : !this.state.isOpenDropdown
    });
  }

  getMouseGroupAppName(activatedControlApps) {
    const item = activatedControlApps.find(app => app.groupId === 0);

    if (!item) return false;
    if (item.appId === 0) return 'kmControl';
    if (item.appId === 1) return 'laserPointer';
    if (item.appId === 2) return 'draw';
  }

  updateActiveRemoteControl(prevActivatedControlApps, activatedControlApps) {
    const {remoteControl} = this.props;
    const prevActivatedApp = this.getMouseGroupAppName(prevActivatedControlApps);
    const activatedApp = this.getMouseGroupAppName(activatedControlApps);

    if (activatedApp === prevActivatedApp) return;

    // 비활성화
    this.clearKeyboardEvent();
    if (prevActivatedApp === 'kmControl') remoteControl.endKmControl();
    if (prevActivatedApp === 'laserPointer') remoteControl.endLaserPointer();
    if (prevActivatedApp === 'draw') remoteControl.endDraw();

    // 활성화
    if (activatedApp === 'kmControl') remoteControl.requestKmControl();
    if (activatedApp === 'laserPointer') remoteControl.startLaserPointer();
    if (activatedApp === 'draw') remoteControl.startDraw();
  }

  clearKeyboardEvent() {
    const {toggleFunctionKeyboard, toggleScreenKeyboard} = this;
    const {activatedControlApps} = this.props.viewer;
    const activatedApp = activatedControlApps.find(app => app.groupId === 1);
    const activatedFunctionKeyboard = activatedApp && activatedApp.appId === 3;
    const activatedScreenKeyboard = activatedApp && activatedApp.appId === 4;

    // app 비활성화
    if (activatedFunctionKeyboard) toggleFunctionKeyboard('function-keyboard');
    if (activatedScreenKeyboard) toggleScreenKeyboard('screen-keyboard');
  }

  hideDropdown = () => {
    this.setState({isOpenDropdown: false});
  };

  onToggle = () => {
    this.setState({
      isOpenToolbar: !this.state.isOpenToolbar
    });
  };

  toggleMouseMenu = (target) => {
    const {isOpenDropdown} = this.state;

    if (isOpenDropdown || !target) {
      this.toggleDropdown('mouseGroup');
    }

    if (target) {
      this.setState({
        selectedMouseEvent: target
      });
    }
  };

  toggleKeyboardMenu = (target) => {
    const {isOpenDropdown} = this.state;

    if (isOpenDropdown || !target) {
      this.toggleDropdown('virtualKeyGroup');
    }

    if (target) {
      target === 'function-keyboard' && this.toggleFunctionKeyboard();
      target === 'screen-keyboard' && this.toggleScreenKeyboard();

      this.setState({
        selectedKeyboardEvent: target
      });
    }
  };

  changeMouseEvent = (target) => {
    // app 활성화
    if (target === 'km-control') {
      this.toggleKmControl();
      this.toggleMouseMenu(target);
      return;
    }

    if (target === 'laser-pointer') {
      this.toggleLaserPointer();
      this.toggleMouseMenu(target);
      return;
    }

    if (target === 'draw') this.toggleDraw();
  };

  changeKeyboardEvent = (target) => {
    // app 비활성화
    this.clearKeyboardEvent();

    // app 활성화
    this.toggleKeyboardMenu(target);
  };

  toggleKmControl = () => {
    const {addActivatedControlApp, removeActivatedControlApp} = this.props;
    const kmControlApp = this.findControlApp(0);
    const isActivated = !this.findActivatedControlApp(kmControlApp);

    if (isActivated) {
      addActivatedControlApp(kmControlApp);
    } else {
      removeActivatedControlApp(kmControlApp);
    }
  };

  toggleLaserPointer = () => {
    const {addActivatedControlApp, removeActivatedControlApp} = this.props;
    const laserPointerApp = this.findControlApp(1);
    const isActivated = !this.findActivatedControlApp(laserPointerApp);

    if (isActivated) {
      addActivatedControlApp(laserPointerApp);
    } else {
      removeActivatedControlApp(laserPointerApp);
    }
  };

  toggleDraw = () => {
    const {addActivatedControlApp, removeActivatedControlApp} = this.props;
    const drawApp = this.findControlApp(2);
    const isActivated = !this.findActivatedControlApp(drawApp);

    if (isActivated) {
      addActivatedControlApp(drawApp);
    } else {
      removeActivatedControlApp(drawApp);
    }
  };

  toggleFunctionKeyboard = () => {
    const kmControl = this.hasActivatedApp(0);
    if (!kmControl) return;

    const {addActivatedControlApp, removeActivatedControlApp} = this.props;
    const functionKeyboardApp = this.findControlApp(3);
    const isActivated = !this.findActivatedControlApp(functionKeyboardApp);

    if (isActivated) {
      addActivatedControlApp(functionKeyboardApp);
    } else {
      removeActivatedControlApp(functionKeyboardApp);
    }
  };

  toggleScreenKeyboard = () => {
    const {remoteControl} = this.props;
    const screenKeyboard = this.findControlApp(4);
    const isActivated = !this.findActivatedControlApp(screenKeyboard);

    if (isActivated) {
      remoteControl.startScreenKeyboard();
    } else {
      remoteControl.endScreenKeyboard();
    }
  };

  clearPaint = () => {
    const {remoteControl} = this.props;
    remoteControl.clearPaint();
    this.toggleDropdown('drawGroup');
  };

  handleFullScreen = () => {
    const fullScreen = this.findControlApp(5);
    const isActivated = !this.findActivatedControlApp(fullScreen);

    if (isActivated) {
      document.documentElement.requestFullScreen && document.documentElement.requestFullScreen();
      document.documentElement.webkitRequestFullScreen && document.documentElement.webkitRequestFullScreen();
      document.documentElement.mozRequestFullScreen && document.documentElement.mozRequestFullScreen();
      document.documentElement.msRequestFullscreen && document.documentElement.msRequestFullscreen();
    } else {
      document.exitFullscreen && document.exitFullscreen();
      document.webkitExitFullscreen && document.webkitExitFullscreen();
      document.mozCancelFullScreen && document.mozCancelFullScreen();
      document.msExitFullscreen && document.msExitFullscreen();
    }
  };

  render() {
    const {isOpenToolbar, selectedMouseEvent, selectedKeyboardEvent} = this.state;
    const {
      onToggle, toggleMouseMenu, toggleKeyboardMenu, clearPaint, hideDropdown,
      changeMouseEvent, changeKeyboardEvent, toggleFunctionKeyboard,
      toggleScreenKeyboard
    } = this;
    const {
      i18n, host, viewer, config, remoteControl, changePaintColor, onScreenCapture, changeScreenRatio, onConfirmModal
    } = this.props;

    const {monitorInfo} = host;
    const kmControl = this.hasActivatedApp(0);
    const laserPointer = this.hasActivatedApp(1);
    const draw = this.hasActivatedApp(2);
    const functionKeyboard = this.hasActivatedApp(3);
    const screenKeyboard = this.hasActivatedApp(4);
    const fullScreen = this.hasActivatedApp(5);
    const screenRatio = config.screenRatios[viewer.screenRatio];
    const monitorSize = monitorInfo.monitors.length;
    const mouseMenu = this.hasActivatedDropdown('mouseGroup');
    const keyboardMenu = this.hasActivatedDropdown('virtualKeyGroup');
    const drawMenu = this.hasActivatedDropdown('drawGroup');

    const mouseMenuItems = [
      {id: 0, target: 'km-control', value: i18n.message('mouseAndKeyboard'), active: kmControl},
      {id: 1, target: 'laser-pointer', value: i18n.message('laserPointer'), active: laserPointer}
    ];

    const keyboardMenuItems = [
      {id: 0, target: 'function-keyboard', value: i18n.message('functionKey'), active: functionKeyboard},
      {id: 1, target: 'screen-keyboard', value: i18n.message('screenKeyboard'), active: screenKeyboard}
    ];

    const kmControlStyle = `toolbar-open__item--km-control${kmControl ? '--active' : ''}`;
    const laserPointerStyle = `toolbar-open__item--laser-pointer${laserPointer ? '--active' : ''}`;
    const functionKeyStyle = `toolbar-open__item--function-keyboard${ kmControl ? functionKeyboard ? '--active' : '' : '--disable' }`;
    const screenKeyStyle = `toolbar-open__item--screen-keyboard${ kmControl ? screenKeyboard ? '--active' : '' : '--disable' }`;
    const toolbarOpenStyle = `toolbar-open${viewer.isScreenOverflow ? '--fixed' : ''}`;
    const keyboardSelectStyle = `toolbar-open__select${!kmControl ? '--disable' : ''}`;
    const drawOpenStyle = `toolbar-open__item--draw${draw ? '--active' : ''}`;
    const drawSelectStyle = `toolbar-open__select${!draw ? '--disable' : ''}`;
    const fullScreenStyle = `toolbar-open__item--full-screen-${fullScreen ? 'minimize' : 'expand'}`;
    const changeMonitorStyle = `toolbar-open__item--change-monitor${monitorSize <= 1 ? '--none' : ''}`;
    const toolbarCloseStyle = `toolbar-close${viewer.isScreenOverflow ? '--fixed' : ''}`;

    const mouseEventGroup = {
      ['km-control']: (
        <div
          className={styles[kmControlStyle]}
          onClick={() => changeMouseEvent('km-control')}
        >
          <div className={styles['toolbar-open__tooltip-wrap']}>
            <span className={styles['toolbar-open__tooltip']}>{i18n.message('mouseAndKeyboard')}</span>
          </div>
        </div>
      ),
      ['laser-pointer']: (
        <div
          className={styles[laserPointerStyle]}
          onClick={() => changeMouseEvent('laser-pointer')}
        >
          <div className={styles['toolbar-open__tooltip-wrap']}>
            <span className={styles['toolbar-open__tooltip']}>{i18n.message('laserPointer')}</span>
          </div>
        </div>
      )
    };

    const keyboardEventGroup = {
      ['function-keyboard']: (
        <div
          className={styles[functionKeyStyle]}
          onClick={kmControl ? toggleFunctionKeyboard : null}
        >
          <div className={styles['toolbar-open__tooltip-wrap']}>
            <span className={styles['toolbar-open__tooltip']}>{i18n.message('functionKey')}</span>
          </div>
        </div>
      ),
      ['screen-keyboard']: (
        <div
          className={styles[screenKeyStyle]}
          onClick={kmControl ? toggleScreenKeyboard : null}
        >
          <div className={styles['toolbar-open__tooltip-wrap']}>
            <span className={styles['toolbar-open__tooltip']}>{i18n.message('screenKeyboard')}</span>
          </div>
        </div>
      )
    };

    const openedToolbar = (
      <div className={styles[toolbarOpenStyle]}>
        <div className={styles['toolbar-open__group']}>
          {mouseEventGroup[selectedMouseEvent]}
          <div
            className={styles['toolbar-open__select']}
            onClick={() => toggleMouseMenu()}
          />

          <ToolBarBaseDropdown
            visible={mouseMenu}
            items={mouseMenuItems}
            onSelectItem={changeMouseEvent}
            onClose={hideDropdown}
          />
          {keyboardEventGroup[mobileDetect.mobile() ? 'screen-keyboard' : selectedKeyboardEvent]}
          {
            !mobileDetect.mobile() &&
            <div
              className={styles[keyboardSelectStyle]}
              onClick={kmControl ? () => toggleKeyboardMenu() : null}
            />
          }
          <ToolBarBaseDropdown
            visible={keyboardMenu}
            items={keyboardMenuItems}
            onSelectItem={changeKeyboardEvent}
            onClose={hideDropdown}
          />
          <div
            className={styles[drawOpenStyle]}
            onClick={() => changeMouseEvent('draw')}
          >
            <div className={styles['toolbar-open__tooltip-wrap']}>
              <span className={styles['toolbar-open__tooltip']}>{i18n.message('draw')}</span>
            </div>
          </div>
          <div
            className={styles[drawSelectStyle]}
            onClick={draw ? () => this.toggleDropdown('drawGroup') : null}
          />
          <ToolBarColorPickerDropdown
            i18n={i18n}
            visible={drawMenu && draw}
            colors={config.canvasTool.colors}
            paintColor={viewer.paintColor}
            changePaintColor={changePaintColor}
            clearPaint={clearPaint}
            onClose={hideDropdown}
          />
        </div>
        <div className={styles['toolbar-open__item-divide']}/>
        <div className={styles['toolbar-open__group']}>
          <div
            className={styles['toolbar-open__item--screen-capture']}
            onClick={onScreenCapture}
          >
            {
              viewer.screenCaptureNumber > 0 &&
              <div className={styles['toolbar-open__item--event-number-notice']}>
                {viewer.screenCaptureNumber}
              </div>
            }
            <div className={styles['toolbar-open__tooltip-wrap']}>
              <span className={styles['toolbar-open__tooltip']}>{i18n.message('screenCapture')}</span>
            </div>
          </div>
          <div
            className={styles[`toolbar-open__item--screen-ratio-${screenRatio}`]}
            onClick={changeScreenRatio}
          >
            <div className={styles['toolbar-open__tooltip-wrap']}>
              <span className={styles['toolbar-open__tooltip']}>{i18n.message('screenRatio')}</span>
            </div>
          </div>
          <div
            className={styles[fullScreenStyle]}
            onClick={this.handleFullScreen}
          >
            <div className={styles['toolbar-open__tooltip-wrap']}>
              <span className={styles['toolbar-open__tooltip']}>{i18n.message('fullScreen')}</span>
            </div>
          </div>
          <div className={styles[changeMonitorStyle]}>
            <div className={styles['toolbar-open__screen-nav-wrap']}>
              <ScreenNav
                monitorInfo={monitorInfo}
                remoteControl={remoteControl}
              />
            </div>
            <div className={styles['toolbar-open__tooltip-wrap']}>
              <span className={styles['toolbar-open__tooltip']}>{i18n.message('changeMonitor')}</span>
            </div>
          </div>
        </div>
        <div
          className={styles['toolbar-open__item--close']}
          onClick={onConfirmModal}
        >
          <div className={styles['toolbar-open__tooltip-wrap']}>
            <span className={styles['toolbar-open__tooltip']}>{i18n.message('exit')}</span>
          </div>
        </div>
        <div className={styles['toolbar-open__item--fold']} onClick={onToggle}>
          <div className={styles['toolbar-open__tooltip-wrap']}>
            <span className={styles['toolbar-open__tooltip']}>{i18n.message('fold')}</span>
          </div>
        </div>
      </div>
    );

    const closedToolbar = (
      <div className={styles[toolbarCloseStyle]} onClick={onToggle}>
        <div className={styles['toolbar-close--toolbar-expand']}/>
        <div className={styles['toolbar-close__tooltip-wrap']}>
          <span className={styles['toolbar-close__tooltip']}>{i18n.message('expand')}</span>
        </div>
      </div>
    );

    return (
      isOpenToolbar ? openedToolbar : closedToolbar
    );
  }
}

export default ToolBar;