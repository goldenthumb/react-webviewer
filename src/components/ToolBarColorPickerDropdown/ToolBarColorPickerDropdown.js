import React from 'react';
import styles from './ToolBarColorPickerDropdown.scss';

import DropdownWrapper from 'src/components/DropdownWrapper';
import ColorPicker from 'src/components/ColorPicker';

const ToolBarColorPickerDropdown = ({i18n, visible, colors, paintColor, changePaintColor, clearPaint, onClose,}) => {
  return (
    <DropdownWrapper visible={visible} onClose={onClose}>
      <div className={styles['dropdown']}>
        <div className={styles['dropdown-item--color-picker']}>
          <ColorPicker
            paintColor={paintColor}
            colors={colors}
            changePaintColor={changePaintColor}
            onCloseDrawMenu={onClose}
          />
        </div>
        <div
          className={styles['dropdown-item--clear-paint']}
          onClick={clearPaint}
        >
          {i18n.message('allClear')}
        </div>
      </div>
    </DropdownWrapper>
  );
};

export default ToolBarColorPickerDropdown;