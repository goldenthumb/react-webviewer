import React from 'react';
import styles from './LoadingSpinner.scss';

const LoadingSpinner = ({theme}) => {
  const bars = [...Array(12).keys()].map((num) => {
    return <div key={num}/>;
  });

  const loadingStyle = `loading${theme ? '--' + theme : ''}`;

  return (
    <div className={styles[loadingStyle]}>
      {theme === 'fade-circle' ? bars : ''}
    </div>
  );
};

export default LoadingSpinner;