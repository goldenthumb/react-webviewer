import React, {Component} from 'react';
import Draggable from 'react-draggable';
import styles from './FunctionKeyboard.scss';
import * as REMOTE_CONTROL from 'src/lib/enums/remoteControl';

class FunctionKeyboard extends Component {
  componentDidUpdate() {
    const {viewer, clearPressedFunctionKeys} = this.props;
    const isPressedFunctionKeys = !!viewer.pressedFunctionKeys.length;
    const functionKeyboard = this.hasActivatedApp(3);

    if (isPressedFunctionKeys && !functionKeyboard) {
      clearPressedFunctionKeys();
    }
  }

  hasActivatedApp(id) {
    const {activatedControlApps} = this.props.viewer;
    return !!activatedControlApps.find(app => app.appId === id);
  }

  hasPressedKey = (id) => {
    const {pressedFunctionKeys} = this.props.viewer;
    return pressedFunctionKeys.includes(id);
  };

  sendKeyBoardPressEvent = (keyCode) => {
    const {onKeyboardControl} = this.props;
    onKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.DOWN, keyCode);
    onKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.UP, keyCode);
  };

  sendSpecialKey = (keyCode) => {
    const {remoteControl} = this.props;
    remoteControl.sendSpecialKey(keyCode);
  };

  sendKeyBoardDownEvent = (id, keyCode) => {
    const {onKeyboardControl, changeFunctionKeyStatus} = this.props;
    onKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.DOWN, keyCode);
    changeFunctionKeyStatus(id);
  };

  sendKeyBoardUpEvent = (id, keyCode) => {
    const {onKeyboardControl, changeFunctionKeyStatus} = this.props;
    onKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.UP, keyCode);
    changeFunctionKeyStatus(id);
  };

  render() {
    const {viewer, config} = this.props;
    const {sendKeyBoardPressEvent, sendSpecialKey, sendKeyBoardDownEvent, sendKeyBoardUpEvent, hasPressedKey} = this;

    const keys = config.functionKeys.filter(item => !item.pressable);
    const pressableKeys = config.functionKeys.filter(item => item.pressable);
    const defaultPosition = {
      x: (Math.min(window.innerWidth, viewer.width) / 2) - (425 / 2),
      y: 140
    };

    return (
      <Draggable handle={`.${styles['handle']}`} bounds="parent" defaultPosition={defaultPosition}>
        <div className={styles['function-keyboard']}>
          <img className={styles['handle']} alt=""/>
          <div className={styles['function-keyboard__item-wrap']}>
            {
              keys.map(key => {
                const keyStyle = `function-keyboard__item${hasPressedKey(key.id) ? '--active' : ''}`;

                return (
                  <div
                    key={key.id}
                    className={styles[keyStyle]}
                    onClick={() => (key.specialKey ? sendSpecialKey : sendKeyBoardPressEvent)(key.keyCode)}
                  >
                    {key.name}
                  </div>
                );
              })
            }
          </div>

          <div className={styles['function-keyboard__item--plus-wrap']}>
            {
              pressableKeys.map(key => {
                const plusKeyStyle = `function-keyboard__item--plus${hasPressedKey(key.id) ? '--active' : ''}`;
                const pressedKeyStyle = `plus${hasPressedKey(key.id) ? '--active' : ''}`;

                return (
                  <div
                    key={key.id}
                    className={styles[plusKeyStyle]}
                    onClick={() => (hasPressedKey(key.id) ? sendKeyBoardUpEvent : sendKeyBoardDownEvent)(key.id, key.keyCode)}
                  >
                    {key.name}<span className={styles[pressedKeyStyle]}>&#43;</span>
                  </div>
                );
              })
            }
          </div>
          <img className={styles['handle']} alt=""/>
        </div>
      </Draggable>
    );
  }
}

export default FunctionKeyboard;
