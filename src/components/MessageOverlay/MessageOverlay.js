import React, {Component} from 'react';
import styles from './MessageOverlay.scss';

class MessageOverlay extends Component {
  render() {
    const {children, visible, fixed, icon} = this.props;
    if (!visible) return null;

    const wrapperStyle = `wrapper${fixed ? '--fixed' : ''}`;

    return (
      <div className={styles['message-overlay']}>
        <div className={styles[wrapperStyle]}>
          <div className={styles['message']}>
            {icon}
            <div className={styles['text']}>
              {children}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MessageOverlay;