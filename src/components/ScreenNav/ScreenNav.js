import React from 'react';
import styles from './ScreenNav.scss';

const ScreenNav = ({monitorInfo, remoteControl}) => {
  const changeMonitor = (selectedIndex) => {
    remoteControl.requestSelectedMonitorInfo(selectedIndex);
  };

  return (
    <div className={styles['screen-nav']}>
      {
        monitorInfo.monitors.map((monitor, i) => {
          const monitorStyle = `monitor${monitorInfo.selectedMonitor === monitor.index ? '--active' : '' }`;

          return (
            <div
              key={monitor.index}
              className={styles[monitorStyle]}
              onClick={() => changeMonitor(monitor.index)}
            >
              {i + 1}
            </div>
          );
        })
      }
    </div>
  );
};

export default ScreenNav;
