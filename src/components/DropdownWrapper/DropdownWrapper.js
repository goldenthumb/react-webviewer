import React, {Component} from 'react';

class DropdownWrapper extends Component {
  constructor(props) {
    super(props);
    this.el = null;
  }

  componentDidMount() {
    document.body.addEventListener('click', evt => {
      const {el} = this;
      const {onClose} = this.props;

      if (el && !this.el.contains(evt.target)) {
        evt.stopImmediatePropagation();
        onClose();
      }

    }, {passive: true, capture: true});
  }

  render() {
    const {visible, children} = this.props;
    if (!visible) return null;

    return (
      <div ref={ref => this.el = ref}>
        {children}
      </div>
    );
  }
}

export default DropdownWrapper;