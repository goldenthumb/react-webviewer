import React from 'react';
import styles from './Loading.scss';

import LoadingSpinner from 'src/components/LoadingSpinner';

const Loading = ({text}) => {
  return (
    <div className={styles['loading']}>
      <LoadingSpinner theme='fade-circle'/>
      <div className={styles['text']}>{text ? text : 'Loading'}</div>
    </div>
  );
};

export default Loading;