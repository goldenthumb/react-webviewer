import React, {Component} from 'react';
import styles from './Viewer.scss';

import * as STATUS from 'src/lib/enums/status';

import ScreenContainer from 'src/containers/ScreenContainer';
import ControllerContainer from 'src/containers/ControllerContainer';
import storage from 'src/lib/storage/webviewer';

class Viewer extends Component {
  componentDidMount() {
    window.addEventListener('beforeunload', this.handleLeave);
  }

  componentWillUnmount() {
    window.removeEventListener('beforeunload', this.handleLeave);
  }

  handleLeave = () => {
    const {host, viewer} = this.props;

    if (host.status === STATUS.HOST.EXIT || viewer.status === STATUS.VIEWER.EXIT) return;

    // 전체화면은 제외
    const activatedControlApps = viewer.activatedControlApps.filter(app => app.groupId !== 2);

    storage.set('activatedControlApps', activatedControlApps);
    storage.set('screenCaptureNumber', viewer.screenCaptureNumber);
    storage.set('screenRatio', viewer.screenRatio);
  };

  render() {
    const {viewer} = this.props;
    const viewerStyle = `viewer${viewer.status === STATUS.VIEWER.RUN ? '' : '--disable'}`;

    return (
      <div className={styles['viewer-wrap']}>
        <div className={styles[viewerStyle]}>
          <div
            className={styles['screen-wrap']}
            onContextMenu={(e) => {
              e.preventDefault();
              return false;
            }}
          >
            <ScreenContainer/>
            <ControllerContainer/>
          </div>
        </div>
      </div>
    );
  }
}

export default Viewer;