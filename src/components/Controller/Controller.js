import React, {Component, Fragment} from 'react';
import * as STATUS from 'src/lib/enums/status';

import ToolbarContainer from 'src/containers/ToolbarContainer';
import ConfirmModalContainer from 'src/containers/ConfirmModalContainer';
import FunctionKeyboardContainer from 'src/containers/FunctionKeyboardContainer';
import HostMessageOverlay from 'src/components/HostMessageOverlay';

class Controller extends Component {
  render() {
    const {i18n, host, viewer} = this.props;
    if (viewer.status !== STATUS.VIEWER.RUN) return null;

    return (
      <Fragment>
        <ToolbarContainer/>
        <FunctionKeyboardContainer/>
        <ConfirmModalContainer/>
        <HostMessageOverlay
          i18n={i18n}
          status={host.status}
          fixed={viewer.isScreenOverflow}
        />
      </Fragment>
    );
  }
}

export default Controller;