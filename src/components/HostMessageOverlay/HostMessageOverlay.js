import React, {Component} from 'react';
import * as STATUS from 'src/lib/enums/status';
import styles from './HostMessageOverlay.scss';

import MessageOverlay from 'src/components/MessageOverlay';
import LoadingSpinner from 'src/components/LoadingSpinner';

class HostMessageOverlay extends Component {
  render() {
    const {i18n, status, fixed} = this.props;

    let visible = false;
    let icon = false;
    let message = '';

    switch (status) {
      case STATUS.HOST.DISPLAY_CHANGE:
        visible = true;
        icon = <LoadingSpinner />;
        message = '';
        break;
      case STATUS.HOST.SESSION_CHANGE:
        visible = true;
        icon = <LoadingSpinner />;
        message = <div>{i18n.message('notGetScreen')} <br />{i18n.message('checkRemoteStatus')}</div>;
        break;
      case STATUS.HOST.PAUSE:
        visible = true;
        icon = <img className={styles['icon-pause']} alt="" />;
        message = i18n.message('hostPause');
        break;
      default:
        visible = false;
        break;
    }

    return (
      <MessageOverlay visible={visible} fixed={fixed} icon={icon}>
        {message}
      </MessageOverlay>
    );
  }
}

export default HostMessageOverlay;