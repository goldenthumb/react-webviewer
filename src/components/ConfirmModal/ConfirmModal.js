import React from 'react';
import styles from './ConfirmModal.scss';

import ModalWrapper from '../ModalWrapper';
import Button from 'src/components/Button';

const ConfirmModal = ({i18n, visible, fixed, onConfirm, onCancel, description}) => {
  return (
    <ModalWrapper visible={visible} fixed={fixed}>
      <div className={styles['content']}>
        <div className={styles['body']}>
          {description}
        </div>
        <div className={styles['footer']}>
          <Button theme="modal-confirm" onClick={onConfirm}>{i18n.message('yes')}</Button>
          <Button theme="modal-cancel" onClick={onCancel}>{i18n.message('no')}</Button>
        </div>
      </div>

    </ModalWrapper>
  );
};

export default ConfirmModal;