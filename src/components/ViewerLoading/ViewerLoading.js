import React from 'react';
import styles from './ViewerLoading.scss';

import Loading from 'src/components/Loading';

const ViewerLoading = () => {
  return (
    <div className={styles['viewer-loading']}>
      <Loading/>
    </div>
  );
};

export default ViewerLoading;