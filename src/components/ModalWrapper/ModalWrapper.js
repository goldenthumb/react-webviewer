import React, {Component, Fragment} from 'react';
import styles from './ModalWrapper.scss';

class ModalWrapper extends Component {
  render() {
    const {children, visible, fixed} = this.props;
    if (!visible) return null;

    const backgroundStyle = `background${fixed ? '--fixed' : ''}`;
    const wrapperStyle = `wrapper${fixed ? '--fixed' : ''}`;

    return (
      <Fragment>
        <div className={styles[backgroundStyle]}/>
        <div className={styles[wrapperStyle]}>
          {children}
        </div>
      </Fragment>
    );
  }
}

export default ModalWrapper;