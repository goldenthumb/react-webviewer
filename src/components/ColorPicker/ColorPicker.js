import React from 'react';
import styles from './ColorPicker.scss';

const ColorPicker = ({paintColor, colors, changePaintColor, onCloseDrawMenu}) => {
  const changeColor = (id) => {
    changePaintColor(id);
    onCloseDrawMenu();
  };

  return (
    <div className={styles['colors']}>
      {
        colors.map((color) => {
          const colorStyle = `color-wrap${color.id === paintColor ? '--active' : ''}`;

          return (
            <div
              key={color.id}
              className={styles[colorStyle]}
              onClick={() => changeColor(color.id)}
            >
              <div className={styles[`color-item--${color.name}`]}/>
            </div>
          );
        })
      }
    </div>
  );
};

export default ColorPicker;
