import React, {Component, Fragment} from 'react';
import styles from './Screen.scss';

import storage from 'src/lib/storage/webviewer';
import {fileDownload, mobileDetect} from 'src/lib/utils';

import * as REMOTE_CONTROL from 'src/lib/enums/remoteControl';
import * as SCREEN_CONTROL from 'src/lib/enums/screenControl';
import * as WEBVIEWER from 'src/lib/enums/webviewer';
import * as STATUS from 'src/lib/enums/status';

class Screen extends Component {
  constructor(props) {
    super(props);

    this.mediaSource = new MediaSource();
    this.buffer = null;
    this.queue = [];
    this.ctx = null;
    this.isMouseDown = false;
    this.fullScreenPrevRatio = 0;
  }

  componentDidMount() {
    const {screen, canvas, mediaSource} = this;
    screen.src = window.URL.createObjectURL(mediaSource);

    mediaSource.addEventListener('sourceopen', this.openMediaSource, false);
    mediaSource.addEventListener('error', this.errorMediaSource);

    this.ctx = canvas.getContext('2d');

    screen.addEventListener('mousemove', this.mouseMoveListener);
    screen.addEventListener('mousedown', this.mouseDownListener);
    screen.addEventListener('mouseup', this.mouseUpListener);
    screen.addEventListener('mousewheel', this.mouseWheelListener);
    screen.addEventListener('touchstart', this.touchStartListener);
    screen.addEventListener('touchmove', this.touchMoveListener);
    screen.addEventListener('touchend', this.touchEndListener);

    canvas.addEventListener('mousedown', this.initializeDraw);
    canvas.addEventListener('mousemove', this.drawPaint);
    canvas.addEventListener('mouseup', this.finishDraw);
    canvas.addEventListener('touchstart', this.touchStartDraw);
    canvas.addEventListener('touchmove', this.touchMoveDraw);
    canvas.addEventListener('touchend', this.touchEndDraw);

    window.addEventListener('resize', this.resizeListener);
    window.addEventListener('focus', this.focusListener);
    window.addEventListener('blur', this.blurListener);

    document.addEventListener('keydown', this.keyDownListener);
    document.addEventListener('keyup', this.keyUpListener);
    document.addEventListener('visibilitychange', this.visibilityChangeListener);
    document.addEventListener('fullscreenchange', this.toggleFullScreen);
    document.addEventListener('mozfullscreenchange', this.toggleFullScreen);
    document.addEventListener('webkitfullscreenchange', this.toggleFullScreen);
    document.addEventListener('MSFullscreenChange', this.toggleFullScreen);
  }

  componentWillUnmount() {
    const {screen, canvas, mediaSource} = this;

    mediaSource.removeEventListener('sourceopen', this.openMediaSource, false);
    mediaSource.removeEventListener('error', this.errorMediaSource);

    screen.removeEventListener('mousemove', this.mouseMoveListener);
    screen.removeEventListener('mousedown', this.mouseDownListener);
    screen.removeEventListener('mouseup', this.mouseUpListener);
    screen.removeEventListener('mousewheel', this.mouseWheelListener);
    screen.removeEventListener('touchstart', this.touchStartListener);
    screen.removeEventListener('touchmove', this.touchMoveListener);
    screen.removeEventListener('touchend', this.touchEndListener);

    canvas.removeEventListener('mousedown', this.initializeDraw);
    canvas.removeEventListener('mousemove', this.drawPaint);
    canvas.removeEventListener('mouseup', this.finishDraw);
    canvas.removeEventListener('touchstart', this.touchStartDraw);
    canvas.removeEventListener('touchmove', this.touchMoveDraw);
    canvas.removeEventListener('touchend', this.touchEndDraw);

    window.removeEventListener('resize', this.resizeListener);
    window.removeEventListener('focus', this.focusListener);
    window.removeEventListener('blur', this.blurListener);

    document.removeEventListener('keydown', this.keyDownListener);
    document.removeEventListener('keyup', this.keyUpListener);
    document.removeEventListener('visibilitychange', this.visibilityChangeListener);
    document.removeEventListener('fullscreenchange', this.toggleFullScreen);
    document.removeEventListener('mozfullscreenchange', this.toggleFullScreen);
    document.removeEventListener('webkitfullscreenchange', this.toggleFullScreen);
    document.removeEventListener('MSFullscreenChange', this.toggleFullScreen);
  }

  componentDidUpdate(prevProps) {
    // TODO: 수정해야함.
    const draw = this.hasActivatedApp(2);
    document.querySelector("meta[name=viewport]")
      .setAttribute('content', `user-scalable=${mobileDetect.mobile() && draw ? '0' : '1'}`);

    this.updateScreen(prevProps);
    this.screenCapture(prevProps);
  }

  runScreen() {
    const {config, screenControl, updateViewerStatus, baseActions} = this.props;

    screenControl.setBinaryType('arraybuffer');
    screenControl.join(config.accessCode, config.viewerId);
    screenControl.on(SCREEN_CONTROL.SCREEN_EVENT.JOIN_SUCCESS, () => {
      const pushServer = storage.get('pushServer');
      const relayServer = storage.get('relayServer');

      updateViewerStatus(STATUS.VIEWER.WAITING);
      baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.WAIT_OK, {pushServer, relayServer});
    });

    screenControl.on(SCREEN_CONTROL.SCREEN_EVENT.SCREEN_DATA, (data) => {
      if (this.buffer.updating || this.queue.length > 0) this.queue.push(data);
      else this.buffer.appendBuffer(data);
    });

    screenControl.on(SCREEN_CONTROL.SCREEN_EVENT.ON_CLOSE, () => {
      const {exitRemoteControl} = this.props;
      exitRemoteControl();
    });
  }

  updateScreen(prevProps) {
    const {viewer, changeScreenSize} = this.props;

    if (viewer.status === STATUS.VIEWER.RUN && this.isChangedViewerStatus(prevProps)) {
      this.changeScreenLock();
      changeScreenSize(this.getScreenSize());
      return;
    }

    if (this.isChangedHostMonitorInfo(prevProps)) {
      const {monitorInfo} = this.props.host;
      const monitor = monitorInfo.monitors[monitorInfo.selectedMonitor];
      changeScreenSize(this.getScreenSize({monitor}));
      return;
    }

    if (this.isChangedViewerScreenRatio(prevProps)) {
      const {config, viewer} = this.props;
      const ratio = config.screenRatios[viewer.screenRatio];
      changeScreenSize(this.getScreenSize({ratio}));
    }
  }

  screenCapture(prevProps) {
    if (this.isChangedScreenCaptureNumber(prevProps)) {
      this.canvas.getContext('2d').drawImage(this.screen, 0, 0, this.canvas.width, this.canvas.height);
      const dataURL = this.canvas.toDataURL('image/jpeg');
      const {screenCaptureNumber} = this.props.viewer;
      fileDownload({dataURL, filename: `screen-capture-${screenCaptureNumber}`});
    }
  }

  isChangedViewerStatus({viewer: prevViewer}) {
    return (this.props.viewer.status !== prevViewer.status);
  }

  isChangedHostMonitorInfo({host: prevHost}) {
    return (this.props.host.monitorInfo !== prevHost.monitorInfo);
  }

  isChangedViewerScreenRatio({viewer: prevViewer}) {
    return (this.props.viewer.screenRatio !== prevViewer.screenRatio);
  }

  isChangedScreenCaptureNumber({viewer: prevViewer}) {
    return (this.props.viewer.screenCaptureNumber !== prevViewer.screenCaptureNumber);
  }

  hasActivatedApp(id, activatedControlApps = this.props.viewer.activatedControlApps) {
    return !!activatedControlApps.find(app => app.appId === id);
  }

  isUseKmControl() {
    const {useKmControl, sessionChange, pause} = this.props.host;
    return (useKmControl && !sessionChange && !pause);
  }

  findControlApp(id) {
    const {controlApps} = this.props.config;
    return controlApps.find(app => app.id === id);
  }

  findActivatedControlApp(selectedApp) {
    const {activatedControlApps} = this.props.viewer;
    return !(activatedControlApps.findIndex(app => app.appId === selectedApp.id) === -1);
  }

  convertCoordinates = (clientX, clientY) => {
    const {screen} = this;
    const {monitorInfo} = this.props.host;
    const hostMonitor = monitorInfo.monitors[monitorInfo.selectedMonitor];
    const windowWidth = screen.getAttribute('width');
    const windowHeight = screen.getAttribute('height');
    const screenRect = screen.getBoundingClientRect();
    const convertedX = Math.round((clientX - screenRect.x) * (hostMonitor.width / windowWidth));
    const convertedY = Math.round((clientY - screenRect.y) * (hostMonitor.height / windowHeight));

    return {x: convertedX, y: convertedY};
  };

  getHostMonitor() {
    const {host} = this.props;
    const {monitorInfo} = host;
    return monitorInfo.monitors[monitorInfo.selectedMonitor];
  }

  getScreenRatio() {
    const {viewer, config} = this.props;
    const screenRatios = config.screenRatios;
    return screenRatios[viewer.screenRatio];
  }

  getScreenSize({monitor = this.getHostMonitor(), ratio = this.getScreenRatio()} = {}) {
    if (ratio === 'fit') {
      return this.getScreenFitSize(monitor);
    }

    if (ratio === '100') {
      const {width, height} = this.toEnlargeScreen({monitor, ratio: 1});
      const overflow = this.isScreenOverflow({width, height});
      return {width, height, overflow};
    }

    if (ratio === '125') {
      const {width, height} = this.toEnlargeScreen({monitor, ratio: 1.25});
      const overflow = this.isScreenOverflow({width, height});
      return {width, height, overflow};
    }

    if (ratio === '150') {
      const {width, height} = this.toEnlargeScreen({monitor, ratio: 1.5});
      const overflow = this.isScreenOverflow({width, height});
      return {width, height, overflow};
    }

    return false;
  }

  getScreenFitSize(hostMonitor) {
    const xRate = hostMonitor.height / hostMonitor.width;
    const yRate = hostMonitor.width / hostMonitor.height;
    const windowWidth = window.innerWidth;
    const windowHeight = window.innerHeight;

    if (windowWidth < windowHeight * yRate) {
      const width = windowWidth;
      const height = windowWidth * xRate;
      const overflow = this.isScreenOverflow({width, height});
      return {width, height, overflow};
    } else {
      const width = windowHeight * yRate;
      const height = windowHeight;
      const overflow = this.isScreenOverflow({width, height});
      return {width, height, overflow};
    }
  }

  isScreenOverflow({width, height} = {}) {
    return (width > window.innerWidth) || (height > window.innerHeight);
  }

  toEnlargeScreen({monitor, ratio} = {}) {
    return {width: monitor.width * ratio, height: monitor.height * ratio};
  }

  handleMouseControl(type, e) {
    if (!this.isUseKmControl()) return;

    const kmControlApp = this.findControlApp(0);
    const isActivated = this.findActivatedControlApp(kmControlApp);

    if (!isActivated) return;

    const {remoteControl} = this.props;
    const {x, y} = this.convertCoordinates(e.clientX, e.clientY);
    remoteControl.controlMouse(type, x, y);
  }

  handleLaserPointer({xPos, yPos, message = false}) {
    if (!this.isUseKmControl()) return;

    const laserPointerApp = this.findControlApp(1);
    const isActivated = this.findActivatedControlApp(laserPointerApp);

    if (!isActivated) return;

    const {remoteControl} = this.props;
    const {x, y} = this.convertCoordinates(xPos, yPos);
    remoteControl.moveLaserPointer(x, y, message);
  }

  handleKeyboardControl(type, keyCode) {
    if (!this.isUseKmControl()) return;
    const {remoteControl} = this.props;
    remoteControl.controlKeyboard(type, keyCode);
  }

  clearPressedFunctionKeys() {
    const {config, viewer, updatePressedFunctionKeys} = this.props;

    config.functionKeys
      .filter(key => viewer.pressedFunctionKeys.includes(key.id))
      .map(key => key.keyCode)
      .forEach(keyCode => this.handleKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.UP, keyCode));

    updatePressedFunctionKeys([]);
  }

  onPressMouse(press) {
    this.isMouseDown = press;
  }

  getPosition(e) {
    const canvasRect = this.canvas.getBoundingClientRect();
    const x = e.clientX - canvasRect.x;
    const y = e.clientY - canvasRect.y;
    return {x, y};
  }

  gatherDrawPoints(e) {
    const {canvasTool} = this.props.config;
    const {x, y} = this.convertCoordinates(e.clientX, e.clientY);
    canvasTool.drawPoints.push({x_pos: x, y_pos: y});
  }

  setDrawInfo(options) {
    const {remoteControl} = this.props;
    remoteControl.drawInfo(options);
  }

  changeScreenLock() {
    const {remoteControl} = this.props;

    const visible = document.visibilityState === 'visible';
    visible ? this.screen.play() : this.screen.pause();
    remoteControl.screenLock(!visible);
  }

  initializeDraw = (e) => {
    const {x, y} = this.getPosition(e);
    const {config, viewer} = this.props;
    const {canvasTool} = config;
    const {paintColor} = viewer;
    const selected = canvasTool.colors.find(color => color.id === paintColor);
    canvasTool.isDrawing = true;
    this.ctx.beginPath();
    this.ctx.lineWidth = canvasTool.thickness;
    this.ctx.strokeStyle = selected.hex;
    canvasTool.x = x;
    canvasTool.y = y;
    this.ctx.moveTo(canvasTool.x, canvasTool.y);

    this.setDrawInfo({color: paintColor});
  };

  drawPaint = (e) => {
    const {canvasTool} = this.props.config;

    if (!canvasTool.isDrawing) return;

    const {x, y} = this.getPosition(e);
    this.ctx.lineTo(x, y);
    canvasTool.x = x;
    canvasTool.y = y;
    this.ctx.stroke();

    this.gatherDrawPoints(e);
  };

  finishDraw = () => {
    const {screen} = this;
    const {config, remoteControl} = this.props;
    const {canvasTool} = config;
    canvasTool.isDrawing = false;
    canvasTool.x = -1;
    canvasTool.y = -1;
    this.ctx.clearRect(0, 0, screen.width, screen.height);
    remoteControl.drawPaint(canvasTool.drawPoints);
    canvasTool.drawPoints = [];
  };

  toggleFullScreen = () => {
    const {addActivatedControlApp, removeActivatedControlApp} = this.props;
    const fullScreen = this.findControlApp(5);
    const isActivated = !this.findActivatedControlApp(fullScreen);

    if (isActivated) {
      addActivatedControlApp(fullScreen);
    } else {
      removeActivatedControlApp(fullScreen);
    }
  };

  openMediaSource = () => {
    if (this.buffer) return;

    this.buffer = this.mediaSource.addSourceBuffer('video/mp4; codecs="avc1.64002A"');
    this.mediaSource.duration = 'Infinity';
    this.buffer.mode = 'sequence';
    this.buffer.addEventListener('updateend', () => {
      if (this.queue.length > 0 && !this.buffer.updating) {
        this.buffer.appendBuffer(this.queue.shift());
      }
    });

    this.runScreen();
  };

  errorMediaSource = () => {
    console.log('mediaSource error: ' + this.mediaSource.readyState);
  };

  mouseMoveListener = (e) => {
    const kmControl = this.hasActivatedApp(0);
    const laserPointer = this.hasActivatedApp(1);
    const {isMouseDown} = this;

    if (kmControl) {
      this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.MOVE, e);
    }

    if (laserPointer) {
      this.handleLaserPointer({xPos: e.clientX, yPos: e.clientY, message: isMouseDown});
    }
  };

  mouseDownListener = (e) => {
    const kmControl = this.hasActivatedApp(0);
    const laserPointer = this.hasActivatedApp(1);

    switch (e.which) {
      case 1:
        this.onPressMouse(true);

        if (kmControl) {
          this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.L_BTN_DOWN, e);
        }

        if (laserPointer) {
          this.handleLaserPointer({xPos: e.clientX, yPos: e.clientY, message: true});
        }
        break;
      case 2:
        this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.M_BTN_DOWN, e);
        break;
      case 3:
        this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.R_BTN_DOWN, e);
        break;
      default:
        break;
    }
  };

  mouseUpListener = (e) => {
    const kmControl = this.hasActivatedApp(0);
    const laserPointer = this.hasActivatedApp(1);

    switch (e.which) {
      case 1:
        this.onPressMouse(false);

        if (kmControl) {
          this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.L_BTN_UP, e);
        }

        if (laserPointer) {
          this.handleLaserPointer({xPos: e.clientX, yPos: e.clientY});
        }
        break;
      case 2:
        this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.M_BTN_UP, e);
        break;
      case 3:
        this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.R_BTN_UP, e);
        break;
      default:
        break;
    }
  };

  mouseWheelListener = (e) => {
    this.handleMouseControl(
      e.wheelDelta > 0 ? REMOTE_CONTROL.MOUSE_MESSAGE.WHEEL_UP :
        REMOTE_CONTROL.MOUSE_MESSAGE.WHEEL_DOWN, e
    );
  };

  touchStartListener = ({touches: [e]}) => {
    const kmControl = this.hasActivatedApp(0);
    const laserPointer = this.hasActivatedApp(1);

    this.onPressMouse(true);

    if (kmControl) {
      this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.L_BTN_DOWN, e);
    }

    if (laserPointer) {
      this.handleLaserPointer({xPos: e.clientX, yPos: e.clientY, message: true});
    }
  };

  touchMoveListener = ({touches: [e]}) => {
    const kmControl = this.hasActivatedApp(0);
    const laserPointer = this.hasActivatedApp(1);
    const {isMouseDown} = this;

    if (kmControl) {
      this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.MOVE, e);
    }

    if (laserPointer) {
      this.handleLaserPointer({xPos: e.clientX, yPos: e.clientY, message: isMouseDown});
    }
  };

  touchEndListener = ({touches: [e]}) => {
    const kmControl = this.hasActivatedApp(0);
    const laserPointer = this.hasActivatedApp(1);

    if (e && kmControl) {
      this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.L_BTN_UP, e);
    }

    if (e && laserPointer) {
      this.handleLaserPointer({xPos: e.clientX, yPos: e.clientY});
    }
  };

  touchStartDraw = ({touches: [e]}) => {
    this.initializeDraw(e);
  };

  touchMoveDraw = ({touches: [e]}) => {
    this.drawPaint(e);
  };

  touchEndDraw = () => {
    this.finishDraw();
  };

  resizeListener = () => {
    const {viewer, changeScreenSize} = this.props;
    if (viewer.status !== STATUS.VIEWER.RUN) return;

    changeScreenSize(this.getScreenSize());
  };

  focusListener = () => {
    const {viewer, remoteControl} = this.props;
    if (viewer.status !== STATUS.VIEWER.RUN) return;

    remoteControl.activeChange(true);
  };

  blurListener = () => {
    const {viewer, remoteControl} = this.props;
    if (viewer.status !== STATUS.VIEWER.RUN) return;

    remoteControl.activeChange(false);
  };

  keyDownListener = (e) => {
    this.handleKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.DOWN, e.keyCode);
  };

  keyUpListener = (e) => {
    this.handleKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.UP, e.keyCode);
    this.clearPressedFunctionKeys();
  };

  visibilityChangeListener = () => {
    const {viewer} = this.props;
    if (viewer.status === STATUS.VIEWER.RUN) this.changeScreenLock();
  };

  render() {
    const {viewer} = this.props;
    const draw = this.hasActivatedApp(2);
    const canvasStyle = `canvas${draw ? '--active' : ''}`;

    return (
      <Fragment>
        <video
          id="screen"
          className={styles['screen']}
          ref={ref => this.screen = ref}
          width={viewer.width}
          height={viewer.height}
          muted
          autoPlay="autoplay"
        >
          <source src="" type="video/mp4"/>
        </video>
        <canvas
          ref={ref => this.canvas = ref}
          className={styles[canvasStyle]}
          width={viewer.width}
          height={viewer.height}
        />
      </Fragment>
    );
  }
}

export default Screen;