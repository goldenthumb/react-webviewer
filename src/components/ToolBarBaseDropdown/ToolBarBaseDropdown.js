import React from 'react';
import styles from './ToolBarBaseDropdown.scss';

import DropdownWrapper from 'src/components/DropdownWrapper';

const ToolBarBaseDropdown = ({visible, items, onSelectItem, onClose}) => {
  return (
    <DropdownWrapper visible={visible} onClose={onClose}>
      <div className={styles['dropdown']}>
        {
          items.map((item) => {
            const dropdownStyle = `dropdown-item--${item.target}${item.active ? '--active' : '' }`;

            return (
              <div
                key={item.id}
                className={styles[dropdownStyle]}
                onClick={() => onSelectItem(item.target)}
              >
                {item.value}
              </div>
            );
          })
        }
      </div>
    </DropdownWrapper>
  );
};

export default ToolBarBaseDropdown;