module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 23);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target) {
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(26);

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertInto + " " + options.insertAt.before);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	options.attrs.type = "text/css";

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	options.attrs.type = "text/css";
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = options.transform(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.compose = exports.mobileDetect = undefined;
exports.fileDownload = fileDownload;
exports.isAvailableBrowser = isAvailableBrowser;
exports.jsonToQueryString = jsonToQueryString;

var _mobileDetect = __webpack_require__(28);

var _mobileDetect2 = _interopRequireDefault(_mobileDetect);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function fileDownload() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      dataURL = _ref.dataURL,
      filename = _ref.filename;

  var tempLink = document.createElement('a');
  tempLink.style.display = 'none';
  tempLink.href = dataURL;
  tempLink.setAttribute('download', filename);

  if (typeof tempLink.download === 'undefined') {
    tempLink.setAttribute('target', '_blank');
  }

  document.body.appendChild(tempLink);
  tempLink.click();
  document.body.removeChild(tempLink);
}

function isAvailableBrowser() {
  var isChrome = /Chrome/i.test(navigator.userAgent);
  var isSafari = /constructor/i.test(window.HTMLElement) || function (p) {
    return p.toString() === "[object SafariRemoteNotification]";
  }(!window['safari'] || typeof safari !== 'undefined' && safari.pushNotification);

  return isChrome || isSafari;
}

function jsonToQueryString(json) {
  return '?' + Object.keys(json).map(function (key) {
    return encodeURIComponent(key) + '=' + encodeURIComponent(json[key]);
  }).join('&');
}

var mobileDetect = exports.mobileDetect = new _mobileDetect2.default(window.navigator.userAgent);

var compose = exports.compose = function compose() {
  for (var _len = arguments.length, functions = Array(_len), _key = 0; _key < _len; _key++) {
    functions[_key] = arguments[_key];
  }

  return functions.reduce(function (a, b) {
    return function () {
      return a(b.apply(undefined, arguments));
    };
  });
};

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var HOST = {
  IDLE: 0,
  RUN: 1,
  EXIT: 2,
  PAUSE: 3,
  SESSION_CHANGE: 4,
  DISPLAY_CHANGE: 5
};

var VIEWER = {
  IDLE: 0,
  WAITING: 1,
  RUN: 2,
  EXIT: 3
};

exports.HOST = HOST;
exports.VIEWER = VIEWER;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _version = __webpack_require__(50);

var _version2 = _interopRequireDefault(_version);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var sessionStorage = window.sessionStorage;
var setItem = sessionStorage.setItem,
    clear = sessionStorage.clear;


var __KEY__ = '__webviewer__?v=' + _version2.default;

sessionStorage.setItem = function (k, v) {
  setItem.call(sessionStorage, k, v);
  if (k === __KEY__) instance._load();
};

sessionStorage.clear = function () {
  clear.call(sessionStorage);
  instance._load();
};

var Storage = function () {
  function Storage() {
    _classCallCheck(this, Storage);

    this._load();
  }

  _createClass(Storage, [{
    key: 'get',
    value: function get(k) {
      return this._data[k];
    }
  }, {
    key: 'set',
    value: function set(k, v) {
      this._data[k] = v;
      this._save();
    }
  }, {
    key: 'remove',
    value: function remove(k) {
      delete this._data[k];
      this._save();
    }
  }, {
    key: 'clear',
    value: function clear() {
      this._data = {};
      this._save();
    }
  }, {
    key: '_load',
    value: function _load() {
      this._data = JSON.parse(sessionStorage.getItem(__KEY__)) || {};
    }
  }, {
    key: '_save',
    value: function _save() {
      setItem.call(sessionStorage, __KEY__, JSON.stringify(this._data));
    }
  }]);

  return Storage;
}();

var storage = new Storage();
exports.default = storage;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var VIEWER_EVENT = {
  WAIT_OK: '/success/wait',
  JOIN_OK: '/success/join',
  CLOSE: '/close',
  ERROR: '/error',
  LOG: '/log'
};

exports.VIEWER_EVENT = VIEWER_EVENT;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var TAG = {
  RELAY_INFO: 20310, // relay 정보 요청
  MESSAGE_PROTOCOL: 20308 // message protocol
};

var TYPE = {
  REQUEST: 0, // 특정동작에 대한 요청을 보낼 때 사용. (ex 키보드, 마우스 제어)
  RESPONSE: 1, // Request에 대한 응답을 보낼 때 사용.
  START: 2, // 특정동작에 대한 시작.
  EVENT: 3, // Action, Status 등에 대한 이벤트.
  END: 4 // 특정동작에 대한 종료.
};

var CATEGORY = {
  CONNECTION: 0, // 연결관련 정보.
  KM_CONTROL: 1, // 마우스, 키보드 제어에 대한 통합 요청을 보낼 때 사용.
  MOUSE: 2, // 마우스 제어.
  KEYBOARD: 3, // 키보드 제어.
  SCREEN: 4, // 일시정지 기능.
  MONITOR: 5, // 모니터 정보.
  STATUS: 6, // PCHost, Webviewer 상태.
  DRAW: 7, // 그리기
  LASER_POINTER: 8, // 레이저 포인터
  SCREEN_KEYBOARD: 9, // 화상 키보드
  SPECIAL_KEY: 10 // 특수키(조합키)
};

var BOOL = {
  FALSE: 0,
  TRUE: 1
};

var RESPONSE_VALUE = {
  FALSE: 0,
  TRUE: 1,
  YES: 2,
  NO: 3,
  OK: 4,
  CANCEL: 5
};

var CONNECTION_MESSAGE = {
  DISCONNECT: 0
};

var MONITOR_MESSAGE = {
  MONITORS: 0,
  SELECTED_MONITOR: 1,
  DISPLAY_CHANGE: 2
};

var STATUS_MESSAGE = {
  VIEWER_STATUS: 0,
  HOST_STATUS: 1
};

var MOUSE_MESSAGE = {
  MOVE: 0,
  L_BTN_DOWN: 1,
  L_BTN_UP: 2,
  R_BTN_DOWN: 3,
  R_BTN_UP: 4,
  M_BTN_DOWN: 5,
  M_BTN_UP: 6,
  X_BTN_DOWN: 7,
  X_BTN_UP: 8,
  WHEEL_DOWN: 9,
  WHEEL_UP: 10
};

var LASER_POINTER_MESSAGE = {
  ARROW: 0,
  CIRCLE: 1
};

var KEYBOARD_MESSAGE = {
  DOWN: 0,
  UP: 1
};

var SPECIAL_KEY_MESSAGE = {
  CTRL_ALT_DEL: 0
};

var DRAW_MESSAGE = {
  INFO: 0,
  DATA: 1,
  CLEAR: 2
};

var HOST_STATUS = {
  IDLE: 0,
  HOST_READY: 1,
  ENGINE_START: 2,
  SESSION_CHANGED: 3
};

var VIEWER_STATUS = {
  IDLE: 0,
  SCREEN_READY: 1,
  ACTIVE_CHANGED: 2,
  SCREEN_LOCK: 3
};

var HOST_EVENT = {
  RELAY_INFO: '/request/relay-info',
  READY: '/response/host/ready',
  MONITOR_INFO: '/response/host/monitor-info',
  SELECTED_MONITOR_INDEX: '/response/host/selected-monitor-index',
  DISPLAY_CHANGE: '/response/host/display-change',
  ENGINE_START: '/response/host/engine-start',
  KM_CONTROL: '/response/host/km-control',
  SCREEN_PAUSE: '/response/host/screen-pause',
  SCREEN_RESUME: '/response/host/screen-resume',
  SESSION_CHANGE_ON: '/response/host/session-change-on',
  SESSION_CHANGE_OFF: '/response/host/session-change-off',
  SCREEN_KEYBOARD_ON: '/response/host/screen-keyboard-on',
  SCREEN_KEYBOARD_OFF: '/response/host/screen-keyboard-off',
  DISCONNECT: '/response/host/disconnect',
  FORCE_DISCONNECT: '/response/host/force_disconnect'
};

exports.TAG = TAG;
exports.TYPE = TYPE;
exports.CATEGORY = CATEGORY;
exports.BOOL = BOOL;
exports.CONNECTION_MESSAGE = CONNECTION_MESSAGE;
exports.MONITOR_MESSAGE = MONITOR_MESSAGE;
exports.STATUS_MESSAGE = STATUS_MESSAGE;
exports.MOUSE_MESSAGE = MOUSE_MESSAGE;
exports.LASER_POINTER_MESSAGE = LASER_POINTER_MESSAGE;
exports.SPECIAL_KEY_MESSAGE = SPECIAL_KEY_MESSAGE;
exports.KEYBOARD_MESSAGE = KEYBOARD_MESSAGE;
exports.DRAW_MESSAGE = DRAW_MESSAGE;
exports.HOST_STATUS = HOST_STATUS;
exports.VIEWER_STATUS = VIEWER_STATUS;
exports.RESPONSE_VALUE = RESPONSE_VALUE;
exports.HOST_EVENT = HOST_EVENT;

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useBase = exports.BaseProvider = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Context = (0, _react.createContext)();
var Provider = Context.Provider,
    Consumer = Context.Consumer;

var BaseProvider = function (_Component) {
  _inherits(BaseProvider, _Component);

  function BaseProvider(props) {
    _classCallCheck(this, BaseProvider);

    var _this = _possibleConstructorReturn(this, (BaseProvider.__proto__ || Object.getPrototypeOf(BaseProvider)).call(this, props));

    var base = props.base,
        _onReceivedEvent = props.onReceivedEvent;
    var i18n = base.i18n,
        config = base.config,
        client = base.client,
        remoteControl = base.remoteControl,
        screenControl = base.screenControl;


    _this.state = {
      i18n: i18n,
      config: config,
      client: client,
      remoteControl: remoteControl,
      screenControl: screenControl,
      modal: {
        exitViewer: false
      }
    };

    _this.actions = {
      showModal: function showModal(modalName) {
        _this.setState({
          modal: _defineProperty({}, modalName, true)
        });
      },
      hideModal: function hideModal(modalName) {
        _this.setState({
          modal: _defineProperty({}, modalName, false)
        });
      },
      onReceivedEvent: function onReceivedEvent() {
        if (typeof _onReceivedEvent === 'function') _onReceivedEvent.apply(undefined, arguments);
      }
    };
    return _this;
  }

  _createClass(BaseProvider, [{
    key: 'render',
    value: function render() {
      var state = this.state,
          actions = this.actions;

      var value = { state: state, actions: actions };

      return _react2.default.createElement(
        Provider,
        { value: value },
        this.props.children
      );
    }
  }]);

  return BaseProvider;
}(_react.Component);

var useBase = function useBase(WrappedComponent) {
  return function (props) {
    return _react2.default.createElement(
      Consumer,
      null,
      function (_ref) {
        var state = _ref.state,
            actions = _ref.actions;
        return _react2.default.createElement(WrappedComponent, _extends({}, props, state, {
          baseActions: actions
        }));
      }
    );
  };
};

exports.BaseProvider = BaseProvider;
exports.useBase = useBase;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useViewer = exports.ViewerProvider = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _status = __webpack_require__(4);

var STATUS = _interopRequireWildcard(_status);

var _webviewer = __webpack_require__(5);

var _webviewer2 = _interopRequireDefault(_webviewer);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Context = (0, _react.createContext)();
var Provider = Context.Provider,
    Consumer = Context.Consumer;

var ViewerProvider = function (_Component) {
  _inherits(ViewerProvider, _Component);

  function ViewerProvider() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, ViewerProvider);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = ViewerProvider.__proto__ || Object.getPrototypeOf(ViewerProvider)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      viewer: {
        width: 0,
        height: 0,
        paintColor: 0,
        isScreenOverflow: false,
        status: STATUS.VIEWER.IDLE,
        screenRatio: _webviewer2.default.get('screenRatio') || 0,
        screenCaptureNumber: _webviewer2.default.get('screenCaptureNumber') || 0,
        activatedControlApps: _webviewer2.default.get('activatedControlApps') || [],
        pressedFunctionKeys: []
      }
    }, _this.actions = {
      updateOperatingStatus: function updateOperatingStatus(status) {
        var viewer = _this.state.viewer;


        _this.setState({
          viewer: _extends({}, viewer, {
            status: status
          })
        });
      },
      updateActivatedControlApp: function updateActivatedControlApp(activatedControlApps) {
        var viewer = _this.state.viewer;


        _this.setState({
          viewer: _extends({}, viewer, {
            activatedControlApps: activatedControlApps
          })
        });
      },
      updatePressedFunctionKeys: function updatePressedFunctionKeys(pressedFunctionKeys) {
        var viewer = _this.state.viewer;


        _this.setState({
          viewer: _extends({}, viewer, {
            pressedFunctionKeys: pressedFunctionKeys
          })
        });
      },
      changeScreenSize: function changeScreenSize(screenData) {
        var viewer = _this.state.viewer;


        _this.setState({
          viewer: _extends({}, viewer, screenData)
        });
      },
      changePaintColor: function changePaintColor(paintColorIndex) {
        var viewer = _this.state.viewer;


        _this.setState({
          viewer: _extends({}, viewer, {
            paintColor: paintColorIndex
          })
        });
      },
      updateScreenCaptureCount: function updateScreenCaptureCount() {
        var viewer = _this.state.viewer;


        _this.setState({
          viewer: _extends({}, viewer, {
            screenCaptureNumber: viewer.screenCaptureNumber + 1
          })
        });
      },
      changeScreenRatio: function changeScreenRatio(screenRatio) {
        var viewer = _this.state.viewer;


        _this.setState({
          viewer: _extends({}, viewer, {
            screenRatio: screenRatio
          })
        });
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(ViewerProvider, [{
    key: 'render',
    value: function render() {
      var state = this.state,
          actions = this.actions;

      var value = { state: state, actions: actions };

      return _react2.default.createElement(
        Provider,
        { value: value },
        this.props.children
      );
    }
  }]);

  return ViewerProvider;
}(_react.Component);

var useViewer = function useViewer(WrappedComponent) {
  return function (props) {
    return _react2.default.createElement(
      Consumer,
      null,
      function (_ref2) {
        var state = _ref2.state,
            actions = _ref2.actions;
        return _react2.default.createElement(WrappedComponent, _extends({}, props, {
          viewer: state.viewer,
          viewerActions: actions
        }));
      }
    );
  };
};

exports.ViewerProvider = ViewerProvider;
exports.useViewer = useViewer;

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useHost = exports.HostProvider = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _status = __webpack_require__(4);

var STATUS = _interopRequireWildcard(_status);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Context = (0, _react.createContext)();
var Provider = Context.Provider,
    Consumer = Context.Consumer;

var HostProvider = function (_Component) {
  _inherits(HostProvider, _Component);

  function HostProvider() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, HostProvider);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = HostProvider.__proto__ || Object.getPrototypeOf(HostProvider)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      host: {
        status: STATUS.HOST.IDLE,
        useKmControl: false,
        monitorInfo: null
      }
    }, _this.actions = {
      updateOperatingStatus: function updateOperatingStatus(status) {
        var host = _this.state.host;


        _this.setState({
          host: _extends({}, host, {
            status: status
          })
        });
      },
      updateControlStatus: function updateControlStatus(useKmControl) {
        var host = _this.state.host;


        _this.setState({
          host: _extends({}, host, {
            useKmControl: useKmControl
          })
        });
      },
      updateMonitorInfo: function updateMonitorInfo(monitorInfo) {
        var host = _this.state.host;


        _this.setState({
          host: _extends({}, host, {
            monitorInfo: monitorInfo
          })
        });
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(HostProvider, [{
    key: 'render',
    value: function render() {
      var state = this.state,
          actions = this.actions;

      var value = { state: state, actions: actions };

      return _react2.default.createElement(
        Provider,
        { value: value },
        this.props.children
      );
    }
  }]);

  return HostProvider;
}(_react.Component);

var useHost = function useHost(WrappedComponent) {
  return function (props) {
    return _react2.default.createElement(
      Consumer,
      null,
      function (_ref2) {
        var state = _ref2.state,
            actions = _ref2.actions;
        return _react2.default.createElement(WrappedComponent, _extends({}, props, {
          host: state.host,
          hostActions: actions
        }));
      }
    );
  };
};

exports.HostProvider = HostProvider;
exports.useHost = useHost;

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAzNjAgMzYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzNjAgMzYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7b3BhY2l0eTowLjI7fQ0KCS5zdDF7ZmlsbDojRkZGRkZGO30NCjwvc3R5bGU+DQo8Zz4NCgk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMjkxLjUsMTAzLjNDMjkxLjUsMTAzLjMsMjkxLjUsMTAzLjMsMjkxLjUsMTAzLjNjLTIuMS0yLjctMTMuNi0xNi40LTMyLjgtMTcuOWMtOS43LTAuNy0xOS4zLDEuOS0yOC42LDcuOQ0KCQljLTM5LjUtMzAuMi02OC4yLTE2LjQtODUuNiwxbC01MC4zLDUwLjNjLTE1LjUsMTUuNS0yNSwzNi43LTI2LDU4LjNjLTEuMSwyMi43LDYuNyw0My42LDIyLjEsNTljMTUuOSwxNS45LDM3LjksMjMuOSw1OS45LDIzLjkNCgkJYzIyLjMsMCw0NC42LTguMSw2MC44LTI0LjRsNTAuMy01MC4zYzI1LTI1LDIzLjgtNTYuOC0zLjMtOTAuNGMyLjgsMSw1LjEsMy4yLDUuOCw0YzUuNyw3LjMsMTcsOC44LDI0LjUsMy4xDQoJCUMyOTYsMTIyLDI5Ny40LDExMC45LDI5MS41LDEwMy4zeiBNMjAzLjUsMTUxLjVsLTE3LjIsMTcuMmMtMS44LDEuOC01LDEuOC02LjgsMGwtMi41LTIuNWMtMS44LTEuOC0xLjgtNC45LDAtNi44bDE3LjItMTcuMg0KCQljMC45LTAuOSwyLjItMS40LDMuNC0xLjRzMi41LDAuNSwzLjQsMS40bDIuNSwyLjVDMjA1LjMsMTQ2LjUsMjA1LjMsMTQ5LjYsMjAzLjUsMTUxLjV6Ii8+DQoJPHBhdGggY2xhc3M9InN0MSIgZD0iTTI4NS42LDEwNy45Yy0xLjEtMS40LTExLTEzLjctMjcuNC0xNWMtOS43LTAuOC0xOS4yLDIuNy0yOC4zLDkuOGMtMjYuMi0yMS44LTU0LjMtMjguNy04MC0zLjFsLTUwLjMsNTAuMw0KCQljLTI5LjIsMjkuMi0zMy4xLDc3LjUtMy45LDEwNi43czgwLjksMjguNywxMTAuMS0wLjVsNTAuMy01MC4zYzI3LjktMjcuOSwxNS4xLTYwLjktMTEuNS04OS4yYzQuMi0yLjcsOC4yLTQsMTEuOS0zLjgNCgkJYzcuNywwLjUsMTMuMSw3LjEsMTMuMyw3LjNjMiwyLjUsNC45LDMuOCw3LjksMy44YzIuMSwwLDQuMy0wLjcsNi4xLTIuMUMyODguMSwxMTguNSwyODguOSwxMTIuMiwyODUuNiwxMDcuOXogTTIwOC44LDE1Ni44DQoJCUwxOTEuNiwxNzRjLTQuOCw0LjgtMTIuNiw0LjgtMTcuNCwwbC0yLjUtMi41Yy00LjgtNC44LTQuOC0xMi42LDAtMTcuNGwxNy4yLTE3LjJjNC44LTQuOCwxMi42LTQuOCwxNy40LDBsMi41LDIuNQ0KCQlDMjEzLjYsMTQ0LjIsMjEzLjYsMTUyLDIwOC44LDE1Ni44eiIvPg0KPC9nPg0KPC9zdmc+DQo="

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAzNjAgMzYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzNjAgMzYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7b3BhY2l0eTowLjI7fQ0KCS5zdDF7ZmlsbDojRkZGRkZGO30NCjwvc3R5bGU+DQo8Zz4NCgk8ZyBjbGFzcz0ic3QwIj4NCgkJPHBhdGggZD0iTTE4MS4xLDcxLjhjLTU5LjgsMC0xMDguNCw0OC42LTEwOC40LDEwOC40czQ4LjYsMTA4LjQsMTA4LjQsMTA4LjRTMjg5LjUsMjQwLDI4OS41LDE4MC4yUzI0MC45LDcxLjgsMTgxLjEsNzEuOHoNCgkJCSBNMjQyLjksMTgwLjJjMCwzNC4xLTI3LjcsNjEuOC02MS44LDYxLjhzLTYxLjgtMjcuNy02MS44LTYxLjhzMjcuNy02MS44LDYxLjgtNjEuOFMyNDIuOSwxNDYuMSwyNDIuOSwxODAuMnoiLz4NCgkJPGNpcmNsZSBjeD0iMTgxLjEiIGN5PSIxODAuMiIgcj0iMzcuOCIvPg0KCTwvZz4NCgk8Zz4NCgkJPHBhdGggY2xhc3M9InN0MSIgZD0iTTE4MS4xLDc5LjNjLTU1LjcsMC0xMDAuOSw0NS4yLTEwMC45LDEwMC45YzAsNTUuNyw0NS4yLDEwMC45LDEwMC45LDEwMC45UzI4MiwyMzYsMjgyLDE4MC4yDQoJCQlDMjgyLDEyNC41LDIzNi45LDc5LjMsMTgxLjEsNzkuM3ogTTE4MS4xLDI0OS41Yy0zOC4zLDAtNjkuMy0zMS02OS4zLTY5LjNjMC0zOC4zLDMxLTY5LjMsNjkuMy02OS4zczY5LjMsMzEsNjkuMyw2OS4zDQoJCQlDMjUwLjQsMjE4LjUsMjE5LjQsMjQ5LjUsMTgxLjEsMjQ5LjV6Ii8+DQoJCTxjaXJjbGUgY2xhc3M9InN0MSIgY3g9IjE4MS4xIiBjeT0iMTgwLjIiIHI9IjMwLjMiLz4NCgk8L2c+DQo8L2c+DQo8L3N2Zz4NCg=="

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAzNjAgMzYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzNjAgMzYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7b3BhY2l0eTowLjI7fQ0KCS5zdDF7ZmlsbDojRkZGRkZGO30NCjwvc3R5bGU+DQo8Zz4NCgk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMjQ2LjQsODIuMWMtNi43LTYuNC0xNS42LTEwLjEtMjQuOS0xMC4xaC04MC44Yy05LjIsMC0xOC4xLDMuNi0yNC45LDEwLjFjLTI5LjcsMi40LTUzLjMsMjcuNy01My4zLDU3LjcNCgkJdjgwLjZjMCwzMiwyNiw1OCw1OCw1OGgxMjEuM2MzMiwwLDU4LTI2LDU4LTU4di04MC42QzI5OS43LDEwOS44LDI3Ni4xLDg0LjUsMjQ2LjQsODIuMXogTTE0OC4zLDE4MC4zYzQuNy03LjIsOC4xLTE0LjEsMTAtMjAuNQ0KCQljMC4zLTEsMS40LTEuNywyLjUtMS40bDEuMSwwLjNjMC44LDAuMiwxLjMsMC45LDEuNCwxLjdjMC44LDUuNywzLDEwLDYuNywxMy4yYzMuOCwzLjEsOC44LDQuNywxNSw0LjdjNC44LDAsOC42LTEuMSwxMS40LTMuMg0KCQljMi42LTIuMSwzLjktNC44LDMuOS04LjNjMC00LjEtMS45LTcuNS01LjctMTAuMmMtMi44LTItOS00LTE4LjQtNS44Yy03LjgtMS41LTEzLjYtNC4yLTE3LjItNy44Yy0zLjctMy44LTUuNi05LTUuNi0xNS41DQoJCWMwLTYuOCwyLjUtMTIuNCw3LjQtMTYuN2M3LjgtNi44LDE5LjEtOC4yLDM1LjUtMy44YzMuMywwLjksNSwxLjEsNS45LDEuMWMxLjYsMCwzLjMtMC42LDUuMy0xLjdjMC42LTAuMywxLjItMC40LDEuOC0wLjFsMC42LDAuMw0KCQljMC41LDAuMiwxLDAuNywxLjEsMS4zYzAuMSwwLjYsMCwxLjItMC4zLDEuN2MtMS45LDIuNi0zLjUsNS40LTQuNyw4LjJjLTIuNSw2LjEtNC4xLDkuNy00LjksMTEuMWMtMC4xLDAuMS0wLjQsMC41LTAuNSwwLjYNCgkJYy0wLjUsMC42LTEuMiwwLjgtMS45LDAuN2MtMC43LTAuMS0xLjMtMC42LTEuNS0xLjNsLTAuNC0xLjFjLTEuMS0zLjQtMS45LTUuOC0yLjYtNy4xYy0wLjYtMS4xLTEuNS0yLjMtMi43LTMuNA0KCQljLTMuMi0yLjktNi44LTQuMy0xMC45LTQuM2MtMy44LDAtNi44LDEtOS4yLDNjLTIuMywyLTMuNSw0LjQtMy41LDcuM2MwLDUuNiw0LjQsOS40LDEzLjQsMTEuNmwxMCwyLjQNCgkJYzE1LjIsMy43LDIyLjksMTEuOSwyMi45LDI0LjVjMCw3LTIuNywxMy04LDE3LjhjLTUuMiw0LjctMTEuOCw3LjEtMTkuNCw3LjFjLTQuNiwwLTkuOS0wLjctMTUuNi0yLjFsLTYuOS0xLjcNCgkJYy0yLjYtMC42LTQuOC0wLjktNi40LTAuOWMtMiwwLTQuMiwwLjctNi40LDJjLTAuNSwwLjMtMSwwLjMtMS41LDAuMmMtMC41LTAuMS0xLTAuNS0xLjItMC45bC0wLjUtMC44DQoJCUMxNDcuOSwxODEuNywxNDcuOSwxODAuOSwxNDguMywxODAuM3ogTTEwNC40LDE3NWwtNC42LDEwdi00Ny41YzAtNC44LDEuNy05LjIsNC42LTEyLjdWMTc1eiBNMjQyLjEsMjQyLjlIMTIwLjENCgkJYy0yLjksMC01LjQtMC42LTcuNS0xLjVsMTIuMi0yNi43YzUsMi41LDEwLjQsMy44LDE1LjksMy44aDgwLjhjNC45LDAsOS42LTEsMTQtMi45bDEyLjEsMjYuNUMyNDUuOSwyNDIuNiwyNDQsMjQyLjksMjQyLjEsMjQyLjl6DQoJCSBNMjYyLjQsMTkwLjFsLTQuNi0xMHYtNTUuM2MyLjksMy41LDQuNiw3LjksNC42LDEyLjdWMTkwLjF6Ii8+DQoJPHBhdGggY2xhc3M9InN0MSIgZD0iTTI0Myw4OS41Yy01LjMtNi0xMi45LTkuOS0yMS41LTkuOWgtODAuOGMtOC42LDAtMTYuMiwzLjktMjEuNSw5LjlDOTIsOTAuMiw3MCwxMTIuNSw3MCwxMzkuOHY4MC42DQoJCWMwLDI3LjgsMjIuNyw1MC41LDUwLjUsNTAuNWgxMjEuM2MyNy44LDAsNTAuNS0yMi43LDUwLjUtNTAuNXYtODAuNkMyOTIuMiwxMTIuNSwyNzAuMiw5MC4yLDI0Myw4OS41eiBNMTQ4LjMsMTgwLjMNCgkJYzQuNy03LjIsOC4xLTE0LjEsMTAtMjAuNWMwLjMtMSwxLjQtMS43LDIuNS0xLjRsMS4xLDAuM2MwLjgsMC4yLDEuMywwLjksMS40LDEuN2MwLjgsNS43LDMsMTAsNi43LDEzLjJjMy44LDMuMSw4LjgsNC43LDE1LDQuNw0KCQljNC44LDAsOC42LTEuMSwxMS40LTMuMmMyLjYtMi4xLDMuOS00LjgsMy45LTguM2MwLTQuMS0xLjktNy41LTUuNy0xMC4yYy0yLjgtMi05LTQtMTguNC01LjhjLTcuOC0xLjUtMTMuNi00LjItMTcuMi03LjgNCgkJYy0zLjctMy44LTUuNi05LTUuNi0xNS41YzAtNi44LDIuNS0xMi40LDcuNC0xNi43YzcuOC02LjgsMTkuMS04LjIsMzUuNS0zLjhjMy4zLDAuOSw1LDEuMSw1LjksMS4xYzEuNiwwLDMuMy0wLjYsNS4zLTEuNw0KCQljMC42LTAuMywxLjItMC40LDEuOC0wLjFsMC42LDAuM2MwLjUsMC4yLDEsMC43LDEuMSwxLjNjMC4xLDAuNiwwLDEuMi0wLjMsMS43Yy0xLjksMi42LTMuNSw1LjQtNC43LDguMg0KCQljLTIuNSw2LjEtNC4xLDkuNy00LjksMTEuMWMtMC4xLDAuMS0wLjQsMC41LTAuNSwwLjZjLTAuNSwwLjYtMS4yLDAuOC0xLjksMC43Yy0wLjctMC4xLTEuMy0wLjYtMS41LTEuM2wtMC40LTEuMQ0KCQljLTEuMS0zLjQtMS45LTUuOC0yLjYtNy4xYy0wLjYtMS4xLTEuNS0yLjMtMi43LTMuNGMtMy4yLTIuOS02LjgtNC4zLTEwLjktNC4zYy0zLjgsMC02LjgsMS05LjIsM2MtMi4zLDItMy41LDQuNC0zLjUsNy4zDQoJCWMwLDUuNiw0LjQsOS40LDEzLjQsMTEuNmwxMCwyLjRjMTUuMiwzLjcsMjIuOSwxMS45LDIyLjksMjQuNWMwLDctMi43LDEzLTgsMTcuOGMtNS4yLDQuNy0xMS44LDcuMS0xOS40LDcuMQ0KCQljLTQuNiwwLTkuOS0wLjctMTUuNi0yLjFsLTYuOS0xLjdjLTIuNi0wLjYtNC44LTAuOS02LjQtMC45Yy0yLDAtNC4yLDAuNy02LjQsMmMtMC41LDAuMy0xLDAuMy0xLjUsMC4yYy0wLjUtMC4xLTEtMC41LTEuMi0wLjkNCgkJbC0wLjUtMC44QzE0Ny45LDE4MS43LDE0Ny45LDE4MC45LDE0OC4zLDE4MC4zeiBNOTIuNCwxMzcuNWMwLTEyLjQsOC4zLTIyLjgsMTkuNi0yNi4zdjY1LjVsLTE5LjYsNDIuOFYxMzcuNXogTTI0Mi4xLDI1MC40SDEyMC4xDQoJCWMtNi41LDAtMTIuNC0yLjMtMTcuMS02LjFsMTguNy00MC43YzUuMSw0LjYsMTEuNyw3LjQsMTkuMSw3LjRoODAuOGM2LjYsMCwxMi41LTIuMywxNy40LTZsMTguNyw0MC43DQoJCUMyNTMuMSwyNDguNywyNDcuOCwyNTAuNCwyNDIuMSwyNTAuNHogTTI2OS45LDIyMi43YzAsMC41LTAuMSwxLTAuMSwxLjVsLTE5LjQtNDIuNHYtNzAuNmMxMS4zLDMuNSwxOS42LDEzLjksMTkuNiwyNi4zVjIyMi43eiIvPg0KPC9nPg0KPC9zdmc+DQo="

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAzNjAgMzYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzNjAgMzYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7b3BhY2l0eTowLjI7fQ0KCS5zdDF7ZmlsbDojRkZGRkZGO30NCjwvc3R5bGU+DQo8Zz4NCgk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMjU5LjksOTEuOEgxMDIuNGMtMjIuMSwwLTQwLjIsMTgtNDAuMiw0MC4ydjk2LjljMCwyMi4xLDE4LDQwLjIsNDAuMiw0MC4yaDE1Ny41YzIyLjEsMCw0MC4yLTE4LDQwLjItNDAuMg0KCQl2LTk2LjlDMzAwLDEwOS44LDI4Miw5MS44LDI1OS45LDkxLjh6IE0xMDAsMjI5LjdjMC0xLjIsMS0yLjIsMi4yLTIuMkgyNjBjMS4yLDAsMi4yLDEsMi4yLDIuMnMtMSwyLjItMi4yLDIuMkgxMDIuMg0KCQlDMTAxLDIzMS45LDEwMCwyMzAuOSwxMDAsMjI5Ljd6IE0xMTIuOCwxNDguOGMwLDEtMC44LDEuOC0xLjgsMS44aC04LjRjLTEsMC0xLjgtMC44LTEuOC0xLjh2LTguNGMwLTEsMC44LTEuOCwxLjgtMS44aDguNA0KCQljMSwwLDEuOCwwLjgsMS44LDEuOFYxNDguOHogTTE2Mi4zLDE0OC44YzAsMS0wLjgsMS44LTEuOCwxLjhoLTguNGMtMSwwLTEuOC0wLjgtMS44LTEuOHYtOC40YzAtMSwwLjgtMS44LDEuOC0xLjhoOC40DQoJCWMxLDAsMS44LDAuOCwxLjgsMS44VjE0OC44eiBNMjExLjgsMTQ4LjhjMCwxLTAuOCwxLjgtMS44LDEuOGgtOC40Yy0xLDAtMS44LTAuOC0xLjgtMS44di04LjRjMC0xLDAuOC0xLjgsMS44LTEuOGg4LjQNCgkJYzEsMCwxLjgsMC44LDEuOCwxLjhWMTQ4Ljh6IE0yNjEuNCwxNDguOGMwLDEtMC44LDEuOC0xLjgsMS44aC04LjRjLTEsMC0xLjgtMC44LTEuOC0xLjh2LTguNGMwLTEsMC44LTEuOCwxLjgtMS44aDguNA0KCQljMSwwLDEuOCwwLjgsMS44LDEuOFYxNDguOHogTTIzNi42LDE4MXY4LjRjMCwxLTAuOCwxLjgtMS44LDEuOGgtOC40Yy0xLDAtMS44LTAuOC0xLjgtMS44VjE4MWMwLTEsMC44LTEuOCwxLjgtMS44aDguNA0KCQlDMjM1LjgsMTc5LjMsMjM2LjYsMTgwLjEsMjM2LjYsMTgxeiBNMTg3LjEsMTgxdjguNGMwLDEtMC44LDEuOC0xLjgsMS44aC04LjRjLTEsMC0xLjgtMC44LTEuOC0xLjhWMTgxYzAtMSwwLjgtMS44LDEuOC0xLjhoOC40DQoJCUMxODYuMywxNzkuMywxODcuMSwxODAuMSwxODcuMSwxODF6IE0xMzcuNSwxODF2OC40YzAsMS0wLjgsMS44LTEuOCwxLjhoLTguNGMtMSwwLTEuOC0wLjgtMS44LTEuOFYxODFjMC0xLDAuOC0xLjgsMS44LTEuOGg4LjQNCgkJQzEzNi43LDE3OS4zLDEzNy41LDE4MC4xLDEzNy41LDE4MXoiLz4NCgk8cGF0aCBjbGFzcz0ic3QxIiBkPSJNMjU5LjksOTkuM0gxMDIuNGMtMTgsMC0zMi43LDE0LjctMzIuNywzMi43djk2LjljMCwxOCwxNC43LDMyLjcsMzIuNywzMi43aDE1Ny41YzE4LDAsMzIuNy0xNC43LDMyLjctMzIuNw0KCQl2LTk2LjlDMjkyLjUsMTEzLjksMjc3LjgsOTkuMywyNTkuOSw5OS4zeiBNMjQxLjksMTQwLjRjMC01LjEsNC4yLTkuMyw5LjMtOS4zaDguNGM1LjEsMCw5LjMsNC4yLDkuMyw5LjN2OC40DQoJCWMwLDUuMS00LjIsOS4zLTkuMyw5LjNoLTguNGMtNS4xLDAtOS4zLTQuMi05LjMtOS4zVjE0MC40eiBNMjI2LjQsMTcxLjhoOC40YzUuMSwwLDkuMyw0LjIsOS4zLDkuM3Y4LjRjMCw1LjEtNC4yLDkuMy05LjMsOS4zDQoJCWgtOC40Yy01LjEsMC05LjMtNC4yLTkuMy05LjNWMTgxQzIxNy4yLDE3NS45LDIyMS4zLDE3MS44LDIyNi40LDE3MS44eiBNMTkyLjQsMTQwLjRjMC01LjEsNC4yLTkuMyw5LjMtOS4zaDguNA0KCQljNS4xLDAsOS4zLDQuMiw5LjMsOS4zdjguNGMwLDUuMS00LjIsOS4zLTkuMyw5LjNoLTguNGMtNS4xLDAtOS4zLTQuMi05LjMtOS4zVjE0MC40eiBNMTc2LjksMTcxLjhoOC40YzUuMSwwLDkuMyw0LjIsOS4zLDkuM3Y4LjQNCgkJYzAsNS4xLTQuMiw5LjMtOS4zLDkuM2gtOC40Yy01LjEsMC05LjMtNC4yLTkuMy05LjNWMTgxQzE2Ny42LDE3NS45LDE3MS44LDE3MS44LDE3Ni45LDE3MS44eiBNMTQyLjksMTQwLjRjMC01LjEsNC4yLTkuMyw5LjMtOS4zDQoJCWg4LjRjNS4xLDAsOS4zLDQuMiw5LjMsOS4zdjguNGMwLDUuMS00LjIsOS4zLTkuMyw5LjNoLTguNGMtNS4xLDAtOS4zLTQuMi05LjMtOS4zVjE0MC40eiBNMTI3LjQsMTcxLjhoOC40YzUuMSwwLDkuMyw0LjIsOS4zLDkuMw0KCQl2OC40YzAsNS4xLTQuMiw5LjMtOS4zLDkuM2gtOC40Yy01LjEsMC05LjMtNC4yLTkuMy05LjNWMTgxQzExOC4xLDE3NS45LDEyMi4zLDE3MS44LDEyNy40LDE3MS44eiBNOTMuMywxNDAuNA0KCQljMC01LjEsNC4yLTkuMyw5LjMtOS4zaDguNGM1LjEsMCw5LjMsNC4yLDkuMyw5LjN2OC40YzAsNS4xLTQuMiw5LjMtOS4zLDkuM2gtOC40Yy01LjEsMC05LjMtNC4yLTkuMy05LjNWMTQwLjR6IE0yNjAsMjM5LjRIMTAyLjINCgkJYy01LjMsMC05LjctNC40LTkuNy05LjdzNC40LTkuNyw5LjctOS43SDI2MGM1LjMsMCw5LjcsNC40LDkuNyw5LjdTMjY1LjQsMjM5LjQsMjYwLDIzOS40eiIvPg0KPC9nPg0KPC9zdmc+DQo="

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAzNjAgMzYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzNjAgMzYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7b3BhY2l0eTowLjI7fQ0KCS5zdDF7ZmlsbDojRkZGRkZGO30NCjwvc3R5bGU+DQo8Zz4NCgk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMjc0LjEsODQuOWMtMTQuMS0xNC4xLTM3LTE0LjEtNTEuMSwwYy00LjUsNC41LTYuNiw5LTYuMywxMy43YzAuMSwxLjUsMC41LDIuNywwLjksNA0KCQljLTEyLjctMi43LTI2LjUsMC44LTM2LjMsMTAuNmwtNjkuMSw2OS4xYy0xMi4xLDEyLjEtMTQuNywzMC41LTcuMyw0NS40bC0yLjktMi41Yy0yLjctMi40LTYuNS0zLjEtOS44LTEuOQ0KCQljLTMuNCwxLjItNS45LDQuMS02LjUsNy42bC03LjgsNDEuM2MtMC43LDMuNSwwLjYsNy4xLDMuMyw5LjRjMS44LDEuNiw0LjEsMi40LDYuNSwyLjRjMC44LDAsMi41LTAuMywzLjMtMC42bDM5LjYtMTMuOQ0KCQljMy40LTEuMiw1LjktNC4xLDYuNS03LjZjMC40LTIsMC4xLTQtMC43LTUuOGM0LjEsMS40LDguMywyLjIsMTIuNSwyLjJjMTAuMSwwLDIwLjItMy45LDI3LjgtMTEuNWw2OS4xLTY5LjENCgkJYzkuOC05LjgsMTMuMy0yMy42LDEwLjYtMzYuM2MxLjMsMC40LDIuNSwwLjksNCwwLjljMC4yLDAsMC41LDAsMC43LDBjNC41LDAsOC43LTIuMSwxMy02LjRDMjg4LjIsMTIyLDI4OC4yLDk5LDI3NC4xLDg0Ljl6DQoJCSBNMTYxLjksMTk3LjNMMTIyLDIzNy4zYy0xLDEtMS42LDIuMy0xLjksMy43bC00LjUtMy45YzEuNy0wLjIsMy4zLTAuOCw0LjUtMmwzOS44LTM5LjhjMC43LTAuNywxLjQtMC43LDIuMSwwDQoJCWMwLjMsMC4zLDAuNCwwLjcsMC40LDFTMTYyLjMsMTk3LDE2MS45LDE5Ny4zeiIvPg0KCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik05NywyMzAuOWMtMC43LTAuNi0xLjYtMC44LTIuNS0wLjVjLTAuOCwwLjMtMS41LDEtMS42LDEuOWwtNy44LDQxLjNjLTAuMiwwLjksMC4xLDEuOCwwLjgsMi40DQoJCWMwLjUsMC40LDEsMC42LDEuNiwwLjZjMC4zLDAsMC42LDAsMC44LTAuMWwzOS42LTEzLjljMC44LTAuMywxLjUtMSwxLjYtMS45cy0wLjEtMS44LTAuOC0yLjRMOTcsMjMwLjl6Ii8+DQoJPHBhdGggY2xhc3M9InN0MSIgZD0iTTIzMS45LDExOC41Yy0xMi41LTEyLjUtMzIuOC0xMi41LTQ1LjMsMGwtNjkuMSw2OS4xYy0xMS41LDExLjUtMTIuMiwyOS41LTIuNiw0Mi4xbDM5LjgtMzkuOA0KCQljMy41LTMuNSw5LjItMy41LDEyLjcsMHMzLjUsOS4yLDAsMTIuN2wtMzkuOSwzOS45YzEyLjUsMTEuMywzMiwxMS4xLDQ0LjEtMWw2OS4xLTY5LjFjMTIuNS0xMi41LDEyLjUtMzIuOCwwLTQ1LjNMMjMxLjksMTE4LjV6Ig0KCQkvPg0KCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik0yNjguOCw5MC4yYy0xMS4xLTExLjEtMjkuNC0xMS4xLTQwLjUsMGMtMTEuMSwxMS4xLDIuNCwxNS44LDEzLjUsMjd2MGMxMS4xLDExLjEsMTUuOCwyNC43LDI3LDEzLjUNCgkJQzI3OS45LDExOS42LDI3OS45LDEwMS40LDI2OC44LDkwLjJ6Ii8+DQo8L2c+DQo8L3N2Zz4NCg=="

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = function escape(url) {
    if (typeof url !== 'string') {
        return url
    }
    // If url is already wrapped in quotes, remove them
    if (/^['"].*['"]$/.test(url)) {
        url = url.slice(1, -1);
    }
    // Should url be wrapped?
    // See https://drafts.csswg.org/css-values-3/#urls
    if (/["'() \t\n]/.test(url)) {
        return '"' + url.replace(/"/g, '\\"').replace(/\n/g, '\\n') + '"'
    }

    return url
}


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _undefined = __webpack_require__(41)(); // Support ES3 engines

module.exports = function (val) {
 return (val !== _undefined) && (val !== null);
};


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var PRODUCT_CODE = 1200;
var LOGIN_OK = 1004;
var PING = 1005;

var SCREEN_EVENT = {
  JOIN_SUCCESS: '/response/join-success',
  SCREEN_DATA: '/response/screen-data',
  ON_CLOSE: 'response/close-event'
};

exports.PRODUCT_CODE = PRODUCT_CODE;
exports.LOGIN_OK = LOGIN_OK;
exports.PING = PING;
exports.SCREEN_EVENT = SCREEN_EVENT;

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ConnectInfoStruct = undefined;

var _superstruct = __webpack_require__(52);

var PushServers = (0, _superstruct.struct)({
  id: 'string?',
  host: 'string',
  viewerPort: 'number',
  clientPort: 'number'
});

var RelayServers = (0, _superstruct.struct)({
  id: 'string?',
  host: 'string',
  port: 'number'
});

var WillMessage = (0, _superstruct.struct)({
  url: 'string',
  params: 'object'
});

var ConnectInfoStruct = (0, _superstruct.struct)({
  accessCode: 'string',
  pushServers: [PushServers],
  relayServers: [RelayServers],
  willMessage: 'object?' // TODO: 추후 수정해야함.
});

exports.ConnectInfoStruct = ConnectInfoStruct;

/***/ }),
/* 20 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "connect", function() { return connect; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Client", function() { return Client; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_paho_mqtt_js__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_paho_mqtt_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_paho_mqtt_js__);
/*!
 * rsup-mqtt v1.0.12
 * (c) 2018-present skt-t1-byungi <tiniwz@gmail.com>
 * Released under the MIT License.
 */


function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function (obj) {
      return typeof obj;
    };
  } else {
    _typeof = function (obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

var EventEmitter =
/*#__PURE__*/
function () {
  function EventEmitter() {
    _classCallCheck(this, EventEmitter);

    this._listeners = {};
  }

  _createClass(EventEmitter, [{
    key: "on",
    value: function on(name, listener) {
      (this._listeners[name] || (this._listeners[name] = [])).push(listener);
    }
  }, {
    key: "once",
    value: function once(name, listener) {
      var _this = this;

      var wrapper = function wrapper() {
        listener.apply(void 0, arguments);

        _this.off(name, listener);
      };

      this.on(name, wrapper);
    }
  }, {
    key: "off",
    value: function off(name) {
      var listener = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      if (listener) {
        this._listeners[name] = (this._listeners[name] || []).filter(function (fn) {
          return fn !== listener;
        });
      } else {
        delete this._listeners[name];
      }
    }
  }, {
    key: "emit",
    value: function emit(name) {
      for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      (this._listeners[name] || []).slice().forEach(function (listener) {
        return listener.apply(void 0, args);
      });
    }
  }]);

  return EventEmitter;
}();

var Subscription =
/*#__PURE__*/
function () {
  function Subscription(topic, client) {
    _classCallCheck(this, Subscription);

    this._topic = topic;
    this._client = client;
  }

  _createClass(Subscription, [{
    key: "on",
    value: function on(listener) {
      this._client.onMessage(this._topic, listener);

      return this;
    }
  }, {
    key: "off",
    value: function off() {
      var listener = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      this._client.removeMessageListener(this._topic, listener);

      return this;
    }
  }, {
    key: "unsubscribe",
    value: function unsubscribe() {
      var removeListeners = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      this._client.unsubscribe(this._topic, removeListeners);

      return this;
    }
  }, {
    key: "send",
    value: function send() {
      var _client;

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      (_client = this._client).send.apply(_client, [this._topic].concat(args));

      return this;
    }
  }, {
    key: "publish",
    value: function publish() {
      this.send.apply(this, arguments);
      return this;
    }
  }]);

  return Subscription;
}();

var Message =
/*#__PURE__*/
function () {
  function Message(pahoMessage) {
    _classCallCheck(this, Message);

    this._pahoMessage = pahoMessage;
  }

  _createClass(Message, [{
    key: "toString",
    value: function toString() {
      return this._pahoMessage.payloadString;
    }
  }, {
    key: "topic",
    get: function get() {
      return this._pahoMessage.destinationName;
    }
  }, {
    key: "json",
    get: function get() {
      return JSON.parse(this.string);
    }
  }, {
    key: "string",
    get: function get() {
      return this._pahoMessage.payloadString;
    }
  }, {
    key: "bytes",
    get: function get() {
      return this._pahoMessage.payloadBytes;
    }
  }]);

  return Message;
}();

function isBuffer(value) {
  return value instanceof ArrayBuffer || ArrayBuffer.isView(value);
}

function makePahoMessage(topic, payload) {
  var qos = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 2;
  var retain = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

  if (_typeof(payload) === 'object' && !isBuffer(payload)) {
    payload = JSON.stringify(payload);
  }

  var message = new __WEBPACK_IMPORTED_MODULE_0_paho_mqtt_js___default.a.Message(payload);
  message.destinationName = topic;
  message.qos = qos;
  message.retained = retain;
  return message;
}

var Client =
/*#__PURE__*/
function () {
  function Client(_ref) {
    var paho = _ref.paho,
        pahoOpts = _ref.pahoOpts;

    _classCallCheck(this, Client);

    this._paho = paho;
    this._pahoOpts = pahoOpts;
    this._subscriptions = {};
    this._emitter = new EventEmitter();
    paho.onMessageArrived = this._handleOnMessage.bind(this);
    paho.onConnectionLost = this._handleOnClose.bind(this);
  }

  _createClass(Client, [{
    key: "_handleOnMessage",
    value: function _handleOnMessage(pahoMessage) {
      var message = new Message(pahoMessage);
      var topic = message.topic;

      try {
        this._emitter.emit("message:".concat(topic), message);

        this._emitter.emit('message', topic, message);
      } catch (error) {
        setTimeout(function () {
          throw error;
        }, 0);
      }
    }
  }, {
    key: "_handleOnClose",
    value: function _handleOnClose(response) {
      try {
        this._emitter.emit('close', response);
      } catch (err) {
        setTimeout(function () {
          throw err;
        }, 0);
      }
    }
  }, {
    key: "on",
    value: function on(eventName, listener) {
      this._emitter.on(eventName, listener);
    }
  }, {
    key: "onMessage",
    value: function onMessage(topic, listener) {
      this.on("message:".concat(topic), listener);
    }
  }, {
    key: "once",
    value: function once(eventName, listener) {
      this._emitter.once(eventName, listener);
    }
  }, {
    key: "off",
    value: function off(eventName) {
      var listener = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      this._emitter.off(eventName, listener);
    }
  }, {
    key: "removeMessageListener",
    value: function removeMessageListener(topic) {
      var listener = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      this.off("message:".concat(topic), listener);
    }
  }, {
    key: "subscribe",
    value: function subscribe(topic) {
      this._paho.subscribe(topic);

      return this._subscriptions[topic] || (this._subscriptions[topic] = new Subscription(topic, this));
    }
  }, {
    key: "unsubscribe",
    value: function unsubscribe(topic) {
      var removeListeners = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      this._paho.unsubscribe(topic);

      delete this._subscriptions[topic];

      if (removeListeners) {
        this.off(topic);
      }
    }
  }, {
    key: "subscribed",
    value: function subscribed() {
      return Object.keys(this._subscriptions);
    }
  }, {
    key: "send",
    value: function send(topic, payload) {
      var _ref2 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {
        qos: 2,
        retain: false
      },
          qos = _ref2.qos,
          retain = _ref2.retain;

      this._paho.send(makePahoMessage(topic, payload, qos, retain));
    }
    /**
     * @alias this.send
     */

  }, {
    key: "publish",
    value: function publish() {
      this.send.apply(this, arguments);
    }
  }, {
    key: "disconnect",
    value: function disconnect() {
      this._paho.disconnect();
    }
  }, {
    key: "reconnect",
    value: function reconnect() {
      var _this = this;

      return new Promise(function (resolve, reject) {
        _this._paho.connect(Object.assign({}, _this._pahoOpts, {
          onSuccess: function onSuccess() {
            resolve();

            _this._emitter.emit('reconnect');
          },
          onFailure: function onFailure(error) {
            return reject(error);
          }
        }));
      });
    }
  }]);

  return Client;
}();

function wrapPahoWill(_ref) {
  var topic = _ref.topic,
      payload = _ref.payload,
      qos = _ref.qos,
      retain = _ref.retain;
  return makePahoMessage(topic, payload, qos, retain);
}

function connect(options) {
  var Ctor = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : Client;
  if (typeof Ctor !== 'function') throw new TypeError('The second argument must be a function, or a constructor.');

  var _options$port = options.port,
      port = _options$port === void 0 ? 4433 : _options$port,
      _options$path = options.path,
      path = _options$path === void 0 ? '/mqtt' : _options$path,
      _options$ssl = options.ssl,
      ssl = _options$ssl === void 0 ? false : _options$ssl,
      _options$clientId = options.clientId,
      clientId = _options$clientId === void 0 ? 'mqttjs_' + Math.random().toString(16).substr(2, 8) : _options$clientId,
      _options$keepalive = options.keepalive,
      keepalive = _options$keepalive === void 0 ? 20 : _options$keepalive,
      host = options.host,
      will = options.will,
      username = options.username,
      etcOptions = _objectWithoutProperties(options, ["port", "path", "ssl", "clientId", "keepalive", "host", "will", "username"]);

  return new Promise(function (resolve, reject) {
    var pahoOpts = Object.assign({
      // rsupport default option
      timeout: 3,
      cleanSession: true,
      mqttVersion: 3,
      useSSL: ssl,
      keepAliveInterval: keepalive
    }, etcOptions);
    if (username) pahoOpts.userName = username;
    if (will) pahoOpts.willMessage = wrapPahoWill(will);
    var paho = new __WEBPACK_IMPORTED_MODULE_0_paho_mqtt_js___default.a.Client(host, port, path, clientId);
    paho.connect(Object.assign({}, pahoOpts, {
      onSuccess: function onSuccess() {
        try {
          var instance = Client === Ctor || Ctor.prototype instanceof Client ? new Ctor({
            paho: paho,
            pahoOpts: pahoOpts
          }) : Ctor({
            paho: paho,
            pahoOpts: pahoOpts
          });
          resolve(instance);
        } catch (err) {
          reject(err);
        }
      },
      onFailure: function onFailure(err) {
        return reject(err);
      }
    }));
  });
}




/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAzNjAgMzYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzNjAgMzYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7b3BhY2l0eTowLjI7fQ0KCS5zdDF7ZmlsbDojRkZGRkZGO30NCjwvc3R5bGU+DQo8Zz4NCgk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMjQyLjksMTgxLjdMMjQyLjksMTgxLjdsLTQ3LjItNDcuMmMtMy45LTMuOS04LjktNi0xNC4zLTUuOWMtNS44LTAuMy0xMSwxLjktMTUsNS45bC00Ny4yLDQ3LjINCgkJYy03LjksNy45LTcuOSwyMC44LDAsMjguN2wxLjUsMS41YzQsNCw5LjIsNS45LDE0LjQsNS45YzUuMiwwLDEwLjQtMiwxNC40LTUuOWwzMS42LTMxLjZsMzEuNywzMS42YzcuNyw3LjcsMjEuMSw3LjYsMjguNywwDQoJCWwxLjUtMS41QzI1MC45LDIwMi41LDI1MC45LDE4OS42LDI0Mi45LDE4MS43eiIvPg0KCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik0yMzcuNiwxODdsLTQ3LjItNDcuMmMtMi41LTIuNS01LjktMy43LTkuMi0zLjdjLTMuNC0wLjEtNi44LDEuMS05LjQsMy43TDEyNC42LDE4N2MtNSw1LTUsMTMuMSwwLDE4LjENCgkJbDEuNSwxLjVjNSw1LDEzLjEsNSwxOC4xLDBsMzYuOS0zNi45bDM3LDM2LjljNSw1LDEzLjEsNSwxOC4xLDBsMS41LTEuNUMyNDIuNiwyMDAuMSwyNDIuNiwxOTEuOSwyMzcuNiwxODd6Ii8+DQo8L2c+DQo8L3N2Zz4NCg=="

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _DropdownWrapper = __webpack_require__(97);

var _DropdownWrapper2 = _interopRequireDefault(_DropdownWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _DropdownWrapper2.default;

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ConnectInfoStruct = exports.VIEWER_EVENT = exports.viewerStorage = undefined;

__webpack_require__(24);

var _Webviewer = __webpack_require__(27);

var _Webviewer2 = _interopRequireDefault(_Webviewer);

var _webviewer = __webpack_require__(5);

var _webviewer2 = _interopRequireDefault(_webviewer);

var _webviewer3 = __webpack_require__(6);

var _validation = __webpack_require__(19);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Webviewer2.default;
exports.viewerStorage = _webviewer2.default;
exports.VIEWER_EVENT = _webviewer3.VIEWER_EVENT;
exports.ConnectInfoStruct = _validation.ConnectInfoStruct;

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(25);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(2)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../node_modules/css-loader/index.js??ref--1-1!../node_modules/postcss-loader/lib/index.js??ref--1-2!../node_modules/sass-loader/lib/loader.js!./index.scss", function() {
		var newContent = require("!!../node_modules/css-loader/index.js??ref--1-1!../node_modules/postcss-loader/lib/index.js??ref--1-2!../node_modules/sass-loader/lib/loader.js!./index.scss");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/***/ }),
/* 26 */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _utils = __webpack_require__(3);

var _remoteControl = __webpack_require__(7);

var _RemoteControl = __webpack_require__(30);

var _RemoteControl2 = _interopRequireDefault(_RemoteControl);

var _ScreenControl = __webpack_require__(32);

var _ScreenControl2 = _interopRequireDefault(_ScreenControl);

var _withInitialize = __webpack_require__(49);

var _withInitialize2 = _interopRequireDefault(_withInitialize);

var _withConnect = __webpack_require__(54);

var _withConnect2 = _interopRequireDefault(_withConnect);

var _base = __webpack_require__(8);

var _host = __webpack_require__(10);

var _viewer = __webpack_require__(9);

var _ViewerContainer = __webpack_require__(63);

var _ViewerContainer2 = _interopRequireDefault(_ViewerContainer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Webviewer = function (_Component) {
  _inherits(Webviewer, _Component);

  function Webviewer(props) {
    _classCallCheck(this, Webviewer);

    var _this = _possibleConstructorReturn(this, (Webviewer.__proto__ || Object.getPrototypeOf(Webviewer)).call(this, props));

    var _this$props = _this.props,
        connectInfo = _this$props.connectInfo,
        client = _this$props.client,
        webSocket = _this$props.webSocket,
        viewerId = _this$props.viewerId,
        hostId = _this$props.hostId;
    var accessCode = connectInfo.accessCode;


    _this.remoteControl = new _RemoteControl2.default(client);
    _this.screenControl = new _ScreenControl2.default(webSocket);

    _this.config = {
      accessCode: accessCode,
      viewerId: viewerId,
      hostId: hostId,
      controlApps: [{ id: 0, name: 'kmControl', groupId: 0 }, { id: 1, name: 'laserPointer', groupId: 0 }, { id: 2, name: 'draw', groupId: 0 }, { id: 3, name: 'functionKeyboard', groupId: 1 }, { id: 4, name: 'screenKeyboard', groupId: 1 }, { id: 5, name: 'fullScreen', groupId: 2 }],
      canvasTool: {
        isDrawing: false,
        drawPoints: [],
        x: -1,
        y: -1,
        colors: [{ id: 0, name: 'red', hex: '#fe0002' }, { id: 1, name: 'yellow', hex: '#fffe03' }, { id: 2, name: 'green', hex: '#00fe00' }, { id: 3, name: 'sky', hex: '#00ffff' }, { id: 4, name: 'pink', hex: '#ff00ff' }],
        thickness: 3
      },
      functionKeys: [{ id: 0, name: 'Esc', keyCode: 27 }, { id: 1, name: 'Tab', keyCode: 9 }, { id: 2, name: 'Ctrl+Alt+Del', keyCode: _remoteControl.SPECIAL_KEY_MESSAGE.CTRL_ALT_DEL, specialKey: true }, { id: 3, name: 'Shift', keyCode: 16, pressable: true }, { id: 4, name: 'Ctrl', keyCode: 17, pressable: true }, { id: 5, name: 'Alt', keyCode: 18, pressable: true }, { id: 6, name: 'Win', keyCode: 91, pressable: true }],
      screenRatios: ['fit', '100', '125', '150']
    };
    return _this;
  }

  _createClass(Webviewer, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          i18n = _props.i18n,
          client = _props.client,
          onReceivedEvent = _props.onReceivedEvent;
      var config = this.config,
          remoteControl = this.remoteControl,
          screenControl = this.screenControl;

      var base = { i18n: i18n, config: config, client: client, remoteControl: remoteControl, screenControl: screenControl };

      return _react2.default.createElement(
        _base.BaseProvider,
        { base: base, onReceivedEvent: onReceivedEvent },
        _react2.default.createElement(
          _host.HostProvider,
          null,
          _react2.default.createElement(
            _viewer.ViewerProvider,
            null,
            _react2.default.createElement(_ViewerContainer2.default, null)
          )
        )
      );
    }
  }]);

  return Webviewer;
}(_react.Component);

var enhance = (0, _utils.compose)((0, _withInitialize2.default)(), (0, _withConnect2.default)());

exports.default = enhance(Webviewer);

/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

// THIS FILE IS GENERATED - DO NOT EDIT!
/*!mobile-detect v1.4.1 2017-12-24*/
/*global module:false, define:false*/
/*jshint latedef:false*/
/*!@license Copyright 2013, Heinrich Goebl, License: MIT, see https://github.com/hgoebl/mobile-detect.js*/
(function (define, undefined) {
define(function () {
    'use strict';

    var impl = {};

    impl.mobileDetectRules = {
    "phones": {
        "iPhone": "\\biPhone\\b|\\biPod\\b",
        "BlackBerry": "BlackBerry|\\bBB10\\b|rim[0-9]+",
        "HTC": "HTC|HTC.*(Sensation|Evo|Vision|Explorer|6800|8100|8900|A7272|S510e|C110e|Legend|Desire|T8282)|APX515CKT|Qtek9090|APA9292KT|HD_mini|Sensation.*Z710e|PG86100|Z715e|Desire.*(A8181|HD)|ADR6200|ADR6400L|ADR6425|001HT|Inspire 4G|Android.*\\bEVO\\b|T-Mobile G1|Z520m|Android [0-9.]+; Pixel",
        "Nexus": "Nexus One|Nexus S|Galaxy.*Nexus|Android.*Nexus.*Mobile|Nexus 4|Nexus 5|Nexus 6",
        "Dell": "Dell[;]? (Streak|Aero|Venue|Venue Pro|Flash|Smoke|Mini 3iX)|XCD28|XCD35|\\b001DL\\b|\\b101DL\\b|\\bGS01\\b",
        "Motorola": "Motorola|DROIDX|DROID BIONIC|\\bDroid\\b.*Build|Android.*Xoom|HRI39|MOT-|A1260|A1680|A555|A853|A855|A953|A955|A956|Motorola.*ELECTRIFY|Motorola.*i1|i867|i940|MB200|MB300|MB501|MB502|MB508|MB511|MB520|MB525|MB526|MB611|MB612|MB632|MB810|MB855|MB860|MB861|MB865|MB870|ME501|ME502|ME511|ME525|ME600|ME632|ME722|ME811|ME860|ME863|ME865|MT620|MT710|MT716|MT720|MT810|MT870|MT917|Motorola.*TITANIUM|WX435|WX445|XT300|XT301|XT311|XT316|XT317|XT319|XT320|XT390|XT502|XT530|XT531|XT532|XT535|XT603|XT610|XT611|XT615|XT681|XT701|XT702|XT711|XT720|XT800|XT806|XT860|XT862|XT875|XT882|XT883|XT894|XT901|XT907|XT909|XT910|XT912|XT928|XT926|XT915|XT919|XT925|XT1021|\\bMoto E\\b|XT1068|XT1092",
        "Samsung": "\\bSamsung\\b|SM-G950F|SM-G955F|SM-G9250|GT-19300|SGH-I337|BGT-S5230|GT-B2100|GT-B2700|GT-B2710|GT-B3210|GT-B3310|GT-B3410|GT-B3730|GT-B3740|GT-B5510|GT-B5512|GT-B5722|GT-B6520|GT-B7300|GT-B7320|GT-B7330|GT-B7350|GT-B7510|GT-B7722|GT-B7800|GT-C3010|GT-C3011|GT-C3060|GT-C3200|GT-C3212|GT-C3212I|GT-C3262|GT-C3222|GT-C3300|GT-C3300K|GT-C3303|GT-C3303K|GT-C3310|GT-C3322|GT-C3330|GT-C3350|GT-C3500|GT-C3510|GT-C3530|GT-C3630|GT-C3780|GT-C5010|GT-C5212|GT-C6620|GT-C6625|GT-C6712|GT-E1050|GT-E1070|GT-E1075|GT-E1080|GT-E1081|GT-E1085|GT-E1087|GT-E1100|GT-E1107|GT-E1110|GT-E1120|GT-E1125|GT-E1130|GT-E1160|GT-E1170|GT-E1175|GT-E1180|GT-E1182|GT-E1200|GT-E1210|GT-E1225|GT-E1230|GT-E1390|GT-E2100|GT-E2120|GT-E2121|GT-E2152|GT-E2220|GT-E2222|GT-E2230|GT-E2232|GT-E2250|GT-E2370|GT-E2550|GT-E2652|GT-E3210|GT-E3213|GT-I5500|GT-I5503|GT-I5700|GT-I5800|GT-I5801|GT-I6410|GT-I6420|GT-I7110|GT-I7410|GT-I7500|GT-I8000|GT-I8150|GT-I8160|GT-I8190|GT-I8320|GT-I8330|GT-I8350|GT-I8530|GT-I8700|GT-I8703|GT-I8910|GT-I9000|GT-I9001|GT-I9003|GT-I9010|GT-I9020|GT-I9023|GT-I9070|GT-I9082|GT-I9100|GT-I9103|GT-I9220|GT-I9250|GT-I9300|GT-I9305|GT-I9500|GT-I9505|GT-M3510|GT-M5650|GT-M7500|GT-M7600|GT-M7603|GT-M8800|GT-M8910|GT-N7000|GT-S3110|GT-S3310|GT-S3350|GT-S3353|GT-S3370|GT-S3650|GT-S3653|GT-S3770|GT-S3850|GT-S5210|GT-S5220|GT-S5229|GT-S5230|GT-S5233|GT-S5250|GT-S5253|GT-S5260|GT-S5263|GT-S5270|GT-S5300|GT-S5330|GT-S5350|GT-S5360|GT-S5363|GT-S5369|GT-S5380|GT-S5380D|GT-S5560|GT-S5570|GT-S5600|GT-S5603|GT-S5610|GT-S5620|GT-S5660|GT-S5670|GT-S5690|GT-S5750|GT-S5780|GT-S5830|GT-S5839|GT-S6102|GT-S6500|GT-S7070|GT-S7200|GT-S7220|GT-S7230|GT-S7233|GT-S7250|GT-S7500|GT-S7530|GT-S7550|GT-S7562|GT-S7710|GT-S8000|GT-S8003|GT-S8500|GT-S8530|GT-S8600|SCH-A310|SCH-A530|SCH-A570|SCH-A610|SCH-A630|SCH-A650|SCH-A790|SCH-A795|SCH-A850|SCH-A870|SCH-A890|SCH-A930|SCH-A950|SCH-A970|SCH-A990|SCH-I100|SCH-I110|SCH-I400|SCH-I405|SCH-I500|SCH-I510|SCH-I515|SCH-I600|SCH-I730|SCH-I760|SCH-I770|SCH-I830|SCH-I910|SCH-I920|SCH-I959|SCH-LC11|SCH-N150|SCH-N300|SCH-R100|SCH-R300|SCH-R351|SCH-R400|SCH-R410|SCH-T300|SCH-U310|SCH-U320|SCH-U350|SCH-U360|SCH-U365|SCH-U370|SCH-U380|SCH-U410|SCH-U430|SCH-U450|SCH-U460|SCH-U470|SCH-U490|SCH-U540|SCH-U550|SCH-U620|SCH-U640|SCH-U650|SCH-U660|SCH-U700|SCH-U740|SCH-U750|SCH-U810|SCH-U820|SCH-U900|SCH-U940|SCH-U960|SCS-26UC|SGH-A107|SGH-A117|SGH-A127|SGH-A137|SGH-A157|SGH-A167|SGH-A177|SGH-A187|SGH-A197|SGH-A227|SGH-A237|SGH-A257|SGH-A437|SGH-A517|SGH-A597|SGH-A637|SGH-A657|SGH-A667|SGH-A687|SGH-A697|SGH-A707|SGH-A717|SGH-A727|SGH-A737|SGH-A747|SGH-A767|SGH-A777|SGH-A797|SGH-A817|SGH-A827|SGH-A837|SGH-A847|SGH-A867|SGH-A877|SGH-A887|SGH-A897|SGH-A927|SGH-B100|SGH-B130|SGH-B200|SGH-B220|SGH-C100|SGH-C110|SGH-C120|SGH-C130|SGH-C140|SGH-C160|SGH-C170|SGH-C180|SGH-C200|SGH-C207|SGH-C210|SGH-C225|SGH-C230|SGH-C417|SGH-C450|SGH-D307|SGH-D347|SGH-D357|SGH-D407|SGH-D415|SGH-D780|SGH-D807|SGH-D980|SGH-E105|SGH-E200|SGH-E315|SGH-E316|SGH-E317|SGH-E335|SGH-E590|SGH-E635|SGH-E715|SGH-E890|SGH-F300|SGH-F480|SGH-I200|SGH-I300|SGH-I320|SGH-I550|SGH-I577|SGH-I600|SGH-I607|SGH-I617|SGH-I627|SGH-I637|SGH-I677|SGH-I700|SGH-I717|SGH-I727|SGH-i747M|SGH-I777|SGH-I780|SGH-I827|SGH-I847|SGH-I857|SGH-I896|SGH-I897|SGH-I900|SGH-I907|SGH-I917|SGH-I927|SGH-I937|SGH-I997|SGH-J150|SGH-J200|SGH-L170|SGH-L700|SGH-M110|SGH-M150|SGH-M200|SGH-N105|SGH-N500|SGH-N600|SGH-N620|SGH-N625|SGH-N700|SGH-N710|SGH-P107|SGH-P207|SGH-P300|SGH-P310|SGH-P520|SGH-P735|SGH-P777|SGH-Q105|SGH-R210|SGH-R220|SGH-R225|SGH-S105|SGH-S307|SGH-T109|SGH-T119|SGH-T139|SGH-T209|SGH-T219|SGH-T229|SGH-T239|SGH-T249|SGH-T259|SGH-T309|SGH-T319|SGH-T329|SGH-T339|SGH-T349|SGH-T359|SGH-T369|SGH-T379|SGH-T409|SGH-T429|SGH-T439|SGH-T459|SGH-T469|SGH-T479|SGH-T499|SGH-T509|SGH-T519|SGH-T539|SGH-T559|SGH-T589|SGH-T609|SGH-T619|SGH-T629|SGH-T639|SGH-T659|SGH-T669|SGH-T679|SGH-T709|SGH-T719|SGH-T729|SGH-T739|SGH-T746|SGH-T749|SGH-T759|SGH-T769|SGH-T809|SGH-T819|SGH-T839|SGH-T919|SGH-T929|SGH-T939|SGH-T959|SGH-T989|SGH-U100|SGH-U200|SGH-U800|SGH-V205|SGH-V206|SGH-X100|SGH-X105|SGH-X120|SGH-X140|SGH-X426|SGH-X427|SGH-X475|SGH-X495|SGH-X497|SGH-X507|SGH-X600|SGH-X610|SGH-X620|SGH-X630|SGH-X700|SGH-X820|SGH-X890|SGH-Z130|SGH-Z150|SGH-Z170|SGH-ZX10|SGH-ZX20|SHW-M110|SPH-A120|SPH-A400|SPH-A420|SPH-A460|SPH-A500|SPH-A560|SPH-A600|SPH-A620|SPH-A660|SPH-A700|SPH-A740|SPH-A760|SPH-A790|SPH-A800|SPH-A820|SPH-A840|SPH-A880|SPH-A900|SPH-A940|SPH-A960|SPH-D600|SPH-D700|SPH-D710|SPH-D720|SPH-I300|SPH-I325|SPH-I330|SPH-I350|SPH-I500|SPH-I600|SPH-I700|SPH-L700|SPH-M100|SPH-M220|SPH-M240|SPH-M300|SPH-M305|SPH-M320|SPH-M330|SPH-M350|SPH-M360|SPH-M370|SPH-M380|SPH-M510|SPH-M540|SPH-M550|SPH-M560|SPH-M570|SPH-M580|SPH-M610|SPH-M620|SPH-M630|SPH-M800|SPH-M810|SPH-M850|SPH-M900|SPH-M910|SPH-M920|SPH-M930|SPH-N100|SPH-N200|SPH-N240|SPH-N300|SPH-N400|SPH-Z400|SWC-E100|SCH-i909|GT-N7100|GT-N7105|SCH-I535|SM-N900A|SGH-I317|SGH-T999L|GT-S5360B|GT-I8262|GT-S6802|GT-S6312|GT-S6310|GT-S5312|GT-S5310|GT-I9105|GT-I8510|GT-S6790N|SM-G7105|SM-N9005|GT-S5301|GT-I9295|GT-I9195|SM-C101|GT-S7392|GT-S7560|GT-B7610|GT-I5510|GT-S7582|GT-S7530E|GT-I8750|SM-G9006V|SM-G9008V|SM-G9009D|SM-G900A|SM-G900D|SM-G900F|SM-G900H|SM-G900I|SM-G900J|SM-G900K|SM-G900L|SM-G900M|SM-G900P|SM-G900R4|SM-G900S|SM-G900T|SM-G900V|SM-G900W8|SHV-E160K|SCH-P709|SCH-P729|SM-T2558|GT-I9205|SM-G9350|SM-J120F|SM-G920F|SM-G920V|SM-G930F|SM-N910C|SM-A310F|GT-I9190|SM-J500FN|SM-G903F",
        "LG": "\\bLG\\b;|LG[- ]?(C800|C900|E400|E610|E900|E-900|F160|F180K|F180L|F180S|730|855|L160|LS740|LS840|LS970|LU6200|MS690|MS695|MS770|MS840|MS870|MS910|P500|P700|P705|VM696|AS680|AS695|AX840|C729|E970|GS505|272|C395|E739BK|E960|L55C|L75C|LS696|LS860|P769BK|P350|P500|P509|P870|UN272|US730|VS840|VS950|LN272|LN510|LS670|LS855|LW690|MN270|MN510|P509|P769|P930|UN200|UN270|UN510|UN610|US670|US740|US760|UX265|UX840|VN271|VN530|VS660|VS700|VS740|VS750|VS910|VS920|VS930|VX9200|VX11000|AX840A|LW770|P506|P925|P999|E612|D955|D802|MS323)",
        "Sony": "SonyST|SonyLT|SonyEricsson|SonyEricssonLT15iv|LT18i|E10i|LT28h|LT26w|SonyEricssonMT27i|C5303|C6902|C6903|C6906|C6943|D2533",
        "Asus": "Asus.*Galaxy|PadFone.*Mobile",
        "NokiaLumia": "Lumia [0-9]{3,4}",
        "Micromax": "Micromax.*\\b(A210|A92|A88|A72|A111|A110Q|A115|A116|A110|A90S|A26|A51|A35|A54|A25|A27|A89|A68|A65|A57|A90)\\b",
        "Palm": "PalmSource|Palm",
        "Vertu": "Vertu|Vertu.*Ltd|Vertu.*Ascent|Vertu.*Ayxta|Vertu.*Constellation(F|Quest)?|Vertu.*Monika|Vertu.*Signature",
        "Pantech": "PANTECH|IM-A850S|IM-A840S|IM-A830L|IM-A830K|IM-A830S|IM-A820L|IM-A810K|IM-A810S|IM-A800S|IM-T100K|IM-A725L|IM-A780L|IM-A775C|IM-A770K|IM-A760S|IM-A750K|IM-A740S|IM-A730S|IM-A720L|IM-A710K|IM-A690L|IM-A690S|IM-A650S|IM-A630K|IM-A600S|VEGA PTL21|PT003|P8010|ADR910L|P6030|P6020|P9070|P4100|P9060|P5000|CDM8992|TXT8045|ADR8995|IS11PT|P2030|P6010|P8000|PT002|IS06|CDM8999|P9050|PT001|TXT8040|P2020|P9020|P2000|P7040|P7000|C790",
        "Fly": "IQ230|IQ444|IQ450|IQ440|IQ442|IQ441|IQ245|IQ256|IQ236|IQ255|IQ235|IQ245|IQ275|IQ240|IQ285|IQ280|IQ270|IQ260|IQ250",
        "Wiko": "KITE 4G|HIGHWAY|GETAWAY|STAIRWAY|DARKSIDE|DARKFULL|DARKNIGHT|DARKMOON|SLIDE|WAX 4G|RAINBOW|BLOOM|SUNSET|GOA(?!nna)|LENNY|BARRY|IGGY|OZZY|CINK FIVE|CINK PEAX|CINK PEAX 2|CINK SLIM|CINK SLIM 2|CINK +|CINK KING|CINK PEAX|CINK SLIM|SUBLIM",
        "iMobile": "i-mobile (IQ|i-STYLE|idea|ZAA|Hitz)",
        "SimValley": "\\b(SP-80|XT-930|SX-340|XT-930|SX-310|SP-360|SP60|SPT-800|SP-120|SPT-800|SP-140|SPX-5|SPX-8|SP-100|SPX-8|SPX-12)\\b",
        "Wolfgang": "AT-B24D|AT-AS50HD|AT-AS40W|AT-AS55HD|AT-AS45q2|AT-B26D|AT-AS50Q",
        "Alcatel": "Alcatel",
        "Nintendo": "Nintendo 3DS",
        "Amoi": "Amoi",
        "INQ": "INQ",
        "GenericPhone": "Tapatalk|PDA;|SAGEM|\\bmmp\\b|pocket|\\bpsp\\b|symbian|Smartphone|smartfon|treo|up.browser|up.link|vodafone|\\bwap\\b|nokia|Series40|Series60|S60|SonyEricsson|N900|MAUI.*WAP.*Browser"
    },
    "tablets": {
        "iPad": "iPad|iPad.*Mobile",
        "NexusTablet": "Android.*Nexus[\\s]+(7|9|10)",
        "SamsungTablet": "SAMSUNG.*Tablet|Galaxy.*Tab|SC-01C|GT-P1000|GT-P1003|GT-P1010|GT-P3105|GT-P6210|GT-P6800|GT-P6810|GT-P7100|GT-P7300|GT-P7310|GT-P7500|GT-P7510|SCH-I800|SCH-I815|SCH-I905|SGH-I957|SGH-I987|SGH-T849|SGH-T859|SGH-T869|SPH-P100|GT-P3100|GT-P3108|GT-P3110|GT-P5100|GT-P5110|GT-P6200|GT-P7320|GT-P7511|GT-N8000|GT-P8510|SGH-I497|SPH-P500|SGH-T779|SCH-I705|SCH-I915|GT-N8013|GT-P3113|GT-P5113|GT-P8110|GT-N8010|GT-N8005|GT-N8020|GT-P1013|GT-P6201|GT-P7501|GT-N5100|GT-N5105|GT-N5110|SHV-E140K|SHV-E140L|SHV-E140S|SHV-E150S|SHV-E230K|SHV-E230L|SHV-E230S|SHW-M180K|SHW-M180L|SHW-M180S|SHW-M180W|SHW-M300W|SHW-M305W|SHW-M380K|SHW-M380S|SHW-M380W|SHW-M430W|SHW-M480K|SHW-M480S|SHW-M480W|SHW-M485W|SHW-M486W|SHW-M500W|GT-I9228|SCH-P739|SCH-I925|GT-I9200|GT-P5200|GT-P5210|GT-P5210X|SM-T311|SM-T310|SM-T310X|SM-T210|SM-T210R|SM-T211|SM-P600|SM-P601|SM-P605|SM-P900|SM-P901|SM-T217|SM-T217A|SM-T217S|SM-P6000|SM-T3100|SGH-I467|XE500|SM-T110|GT-P5220|GT-I9200X|GT-N5110X|GT-N5120|SM-P905|SM-T111|SM-T2105|SM-T315|SM-T320|SM-T320X|SM-T321|SM-T520|SM-T525|SM-T530NU|SM-T230NU|SM-T330NU|SM-T900|XE500T1C|SM-P605V|SM-P905V|SM-T337V|SM-T537V|SM-T707V|SM-T807V|SM-P600X|SM-P900X|SM-T210X|SM-T230|SM-T230X|SM-T325|GT-P7503|SM-T531|SM-T330|SM-T530|SM-T705|SM-T705C|SM-T535|SM-T331|SM-T800|SM-T700|SM-T537|SM-T807|SM-P907A|SM-T337A|SM-T537A|SM-T707A|SM-T807A|SM-T237|SM-T807P|SM-P607T|SM-T217T|SM-T337T|SM-T807T|SM-T116NQ|SM-T116BU|SM-P550|SM-T350|SM-T550|SM-T9000|SM-P9000|SM-T705Y|SM-T805|GT-P3113|SM-T710|SM-T810|SM-T815|SM-T360|SM-T533|SM-T113|SM-T335|SM-T715|SM-T560|SM-T670|SM-T677|SM-T377|SM-T567|SM-T357T|SM-T555|SM-T561|SM-T713|SM-T719|SM-T813|SM-T819|SM-T580|SM-T355Y?|SM-T280|SM-T817A|SM-T820|SM-W700|SM-P580|SM-T587|SM-P350|SM-P555M|SM-P355M|SM-T113NU|SM-T815Y",
        "Kindle": "Kindle|Silk.*Accelerated|Android.*\\b(KFOT|KFTT|KFJWI|KFJWA|KFOTE|KFSOWI|KFTHWI|KFTHWA|KFAPWI|KFAPWA|WFJWAE|KFSAWA|KFSAWI|KFASWI|KFARWI|KFFOWI|KFGIWI|KFMEWI)\\b|Android.*Silk\/[0-9.]+ like Chrome\/[0-9.]+ (?!Mobile)",
        "SurfaceTablet": "Windows NT [0-9.]+; ARM;.*(Tablet|ARMBJS)",
        "HPTablet": "HP Slate (7|8|10)|HP ElitePad 900|hp-tablet|EliteBook.*Touch|HP 8|Slate 21|HP SlateBook 10",
        "AsusTablet": "^.*PadFone((?!Mobile).)*$|Transformer|TF101|TF101G|TF300T|TF300TG|TF300TL|TF700T|TF700KL|TF701T|TF810C|ME171|ME301T|ME302C|ME371MG|ME370T|ME372MG|ME172V|ME173X|ME400C|Slider SL101|\\bK00F\\b|\\bK00C\\b|\\bK00E\\b|\\bK00L\\b|TX201LA|ME176C|ME102A|\\bM80TA\\b|ME372CL|ME560CG|ME372CG|ME302KL| K010 | K011 | K017 | K01E |ME572C|ME103K|ME170C|ME171C|\\bME70C\\b|ME581C|ME581CL|ME8510C|ME181C|P01Y|PO1MA|P01Z|\\bP027\\b",
        "BlackBerryTablet": "PlayBook|RIM Tablet",
        "HTCtablet": "HTC_Flyer_P512|HTC Flyer|HTC Jetstream|HTC-P715a|HTC EVO View 4G|PG41200|PG09410",
        "MotorolaTablet": "xoom|sholest|MZ615|MZ605|MZ505|MZ601|MZ602|MZ603|MZ604|MZ606|MZ607|MZ608|MZ609|MZ615|MZ616|MZ617",
        "NookTablet": "Android.*Nook|NookColor|nook browser|BNRV200|BNRV200A|BNTV250|BNTV250A|BNTV400|BNTV600|LogicPD Zoom2",
        "AcerTablet": "Android.*; \\b(A100|A101|A110|A200|A210|A211|A500|A501|A510|A511|A700|A701|W500|W500P|W501|W501P|W510|W511|W700|G100|G100W|B1-A71|B1-710|B1-711|A1-810|A1-811|A1-830)\\b|W3-810|\\bA3-A10\\b|\\bA3-A11\\b|\\bA3-A20\\b|\\bA3-A30",
        "ToshibaTablet": "Android.*(AT100|AT105|AT200|AT205|AT270|AT275|AT300|AT305|AT1S5|AT500|AT570|AT700|AT830)|TOSHIBA.*FOLIO",
        "LGTablet": "\\bL-06C|LG-V909|LG-V900|LG-V700|LG-V510|LG-V500|LG-V410|LG-V400|LG-VK810\\b",
        "FujitsuTablet": "Android.*\\b(F-01D|F-02F|F-05E|F-10D|M532|Q572)\\b",
        "PrestigioTablet": "PMP3170B|PMP3270B|PMP3470B|PMP7170B|PMP3370B|PMP3570C|PMP5870C|PMP3670B|PMP5570C|PMP5770D|PMP3970B|PMP3870C|PMP5580C|PMP5880D|PMP5780D|PMP5588C|PMP7280C|PMP7280C3G|PMP7280|PMP7880D|PMP5597D|PMP5597|PMP7100D|PER3464|PER3274|PER3574|PER3884|PER5274|PER5474|PMP5097CPRO|PMP5097|PMP7380D|PMP5297C|PMP5297C_QUAD|PMP812E|PMP812E3G|PMP812F|PMP810E|PMP880TD|PMT3017|PMT3037|PMT3047|PMT3057|PMT7008|PMT5887|PMT5001|PMT5002",
        "LenovoTablet": "Lenovo TAB|Idea(Tab|Pad)( A1|A10| K1|)|ThinkPad([ ]+)?Tablet|YT3-850M|YT3-X90L|YT3-X90F|YT3-X90X|Lenovo.*(S2109|S2110|S5000|S6000|K3011|A3000|A3500|A1000|A2107|A2109|A1107|A5500|A7600|B6000|B8000|B8080)(-|)(FL|F|HV|H|)|TB-X103F|TB-X304F|TB-8703F",
        "DellTablet": "Venue 11|Venue 8|Venue 7|Dell Streak 10|Dell Streak 7",
        "YarvikTablet": "Android.*\\b(TAB210|TAB211|TAB224|TAB250|TAB260|TAB264|TAB310|TAB360|TAB364|TAB410|TAB411|TAB420|TAB424|TAB450|TAB460|TAB461|TAB464|TAB465|TAB467|TAB468|TAB07-100|TAB07-101|TAB07-150|TAB07-151|TAB07-152|TAB07-200|TAB07-201-3G|TAB07-210|TAB07-211|TAB07-212|TAB07-214|TAB07-220|TAB07-400|TAB07-485|TAB08-150|TAB08-200|TAB08-201-3G|TAB08-201-30|TAB09-100|TAB09-211|TAB09-410|TAB10-150|TAB10-201|TAB10-211|TAB10-400|TAB10-410|TAB13-201|TAB274EUK|TAB275EUK|TAB374EUK|TAB462EUK|TAB474EUK|TAB9-200)\\b",
        "MedionTablet": "Android.*\\bOYO\\b|LIFE.*(P9212|P9514|P9516|S9512)|LIFETAB",
        "ArnovaTablet": "97G4|AN10G2|AN7bG3|AN7fG3|AN8G3|AN8cG3|AN7G3|AN9G3|AN7dG3|AN7dG3ST|AN7dG3ChildPad|AN10bG3|AN10bG3DT|AN9G2",
        "IntensoTablet": "INM8002KP|INM1010FP|INM805ND|Intenso Tab|TAB1004",
        "IRUTablet": "M702pro",
        "MegafonTablet": "MegaFon V9|\\bZTE V9\\b|Android.*\\bMT7A\\b",
        "EbodaTablet": "E-Boda (Supreme|Impresspeed|Izzycomm|Essential)",
        "AllViewTablet": "Allview.*(Viva|Alldro|City|Speed|All TV|Frenzy|Quasar|Shine|TX1|AX1|AX2)",
        "ArchosTablet": "\\b(101G9|80G9|A101IT)\\b|Qilive 97R|Archos5|\\bARCHOS (70|79|80|90|97|101|FAMILYPAD|)(b|c|)(G10| Cobalt| TITANIUM(HD|)| Xenon| Neon|XSK| 2| XS 2| PLATINUM| CARBON|GAMEPAD)\\b",
        "AinolTablet": "NOVO7|NOVO8|NOVO10|Novo7Aurora|Novo7Basic|NOVO7PALADIN|novo9-Spark",
        "NokiaLumiaTablet": "Lumia 2520",
        "SonyTablet": "Sony.*Tablet|Xperia Tablet|Sony Tablet S|SO-03E|SGPT12|SGPT13|SGPT114|SGPT121|SGPT122|SGPT123|SGPT111|SGPT112|SGPT113|SGPT131|SGPT132|SGPT133|SGPT211|SGPT212|SGPT213|SGP311|SGP312|SGP321|EBRD1101|EBRD1102|EBRD1201|SGP351|SGP341|SGP511|SGP512|SGP521|SGP541|SGP551|SGP621|SGP612|SOT31",
        "PhilipsTablet": "\\b(PI2010|PI3000|PI3100|PI3105|PI3110|PI3205|PI3210|PI3900|PI4010|PI7000|PI7100)\\b",
        "CubeTablet": "Android.*(K8GT|U9GT|U10GT|U16GT|U17GT|U18GT|U19GT|U20GT|U23GT|U30GT)|CUBE U8GT",
        "CobyTablet": "MID1042|MID1045|MID1125|MID1126|MID7012|MID7014|MID7015|MID7034|MID7035|MID7036|MID7042|MID7048|MID7127|MID8042|MID8048|MID8127|MID9042|MID9740|MID9742|MID7022|MID7010",
        "MIDTablet": "M9701|M9000|M9100|M806|M1052|M806|T703|MID701|MID713|MID710|MID727|MID760|MID830|MID728|MID933|MID125|MID810|MID732|MID120|MID930|MID800|MID731|MID900|MID100|MID820|MID735|MID980|MID130|MID833|MID737|MID960|MID135|MID860|MID736|MID140|MID930|MID835|MID733|MID4X10",
        "MSITablet": "MSI \\b(Primo 73K|Primo 73L|Primo 81L|Primo 77|Primo 93|Primo 75|Primo 76|Primo 73|Primo 81|Primo 91|Primo 90|Enjoy 71|Enjoy 7|Enjoy 10)\\b",
        "SMiTTablet": "Android.*(\\bMID\\b|MID-560|MTV-T1200|MTV-PND531|MTV-P1101|MTV-PND530)",
        "RockChipTablet": "Android.*(RK2818|RK2808A|RK2918|RK3066)|RK2738|RK2808A",
        "FlyTablet": "IQ310|Fly Vision",
        "bqTablet": "Android.*(bq)?.*(Elcano|Curie|Edison|Maxwell|Kepler|Pascal|Tesla|Hypatia|Platon|Newton|Livingstone|Cervantes|Avant|Aquaris ([E|M]10|M8))|Maxwell.*Lite|Maxwell.*Plus",
        "HuaweiTablet": "MediaPad|MediaPad 7 Youth|IDEOS S7|S7-201c|S7-202u|S7-101|S7-103|S7-104|S7-105|S7-106|S7-201|S7-Slim|M2-A01L",
        "NecTablet": "\\bN-06D|\\bN-08D",
        "PantechTablet": "Pantech.*P4100",
        "BronchoTablet": "Broncho.*(N701|N708|N802|a710)",
        "VersusTablet": "TOUCHPAD.*[78910]|\\bTOUCHTAB\\b",
        "ZyncTablet": "z1000|Z99 2G|z99|z930|z999|z990|z909|Z919|z900",
        "PositivoTablet": "TB07STA|TB10STA|TB07FTA|TB10FTA",
        "NabiTablet": "Android.*\\bNabi",
        "KoboTablet": "Kobo Touch|\\bK080\\b|\\bVox\\b Build|\\bArc\\b Build",
        "DanewTablet": "DSlide.*\\b(700|701R|702|703R|704|802|970|971|972|973|974|1010|1012)\\b",
        "TexetTablet": "NaviPad|TB-772A|TM-7045|TM-7055|TM-9750|TM-7016|TM-7024|TM-7026|TM-7041|TM-7043|TM-7047|TM-8041|TM-9741|TM-9747|TM-9748|TM-9751|TM-7022|TM-7021|TM-7020|TM-7011|TM-7010|TM-7023|TM-7025|TM-7037W|TM-7038W|TM-7027W|TM-9720|TM-9725|TM-9737W|TM-1020|TM-9738W|TM-9740|TM-9743W|TB-807A|TB-771A|TB-727A|TB-725A|TB-719A|TB-823A|TB-805A|TB-723A|TB-715A|TB-707A|TB-705A|TB-709A|TB-711A|TB-890HD|TB-880HD|TB-790HD|TB-780HD|TB-770HD|TB-721HD|TB-710HD|TB-434HD|TB-860HD|TB-840HD|TB-760HD|TB-750HD|TB-740HD|TB-730HD|TB-722HD|TB-720HD|TB-700HD|TB-500HD|TB-470HD|TB-431HD|TB-430HD|TB-506|TB-504|TB-446|TB-436|TB-416|TB-146SE|TB-126SE",
        "PlaystationTablet": "Playstation.*(Portable|Vita)",
        "TrekstorTablet": "ST10416-1|VT10416-1|ST70408-1|ST702xx-1|ST702xx-2|ST80208|ST97216|ST70104-2|VT10416-2|ST10216-2A|SurfTab",
        "PyleAudioTablet": "\\b(PTBL10CEU|PTBL10C|PTBL72BC|PTBL72BCEU|PTBL7CEU|PTBL7C|PTBL92BC|PTBL92BCEU|PTBL9CEU|PTBL9CUK|PTBL9C)\\b",
        "AdvanTablet": "Android.* \\b(E3A|T3X|T5C|T5B|T3E|T3C|T3B|T1J|T1F|T2A|T1H|T1i|E1C|T1-E|T5-A|T4|E1-B|T2Ci|T1-B|T1-D|O1-A|E1-A|T1-A|T3A|T4i)\\b ",
        "DanyTechTablet": "Genius Tab G3|Genius Tab S2|Genius Tab Q3|Genius Tab G4|Genius Tab Q4|Genius Tab G-II|Genius TAB GII|Genius TAB GIII|Genius Tab S1",
        "GalapadTablet": "Android.*\\bG1\\b",
        "MicromaxTablet": "Funbook|Micromax.*\\b(P250|P560|P360|P362|P600|P300|P350|P500|P275)\\b",
        "KarbonnTablet": "Android.*\\b(A39|A37|A34|ST8|ST10|ST7|Smart Tab3|Smart Tab2)\\b",
        "AllFineTablet": "Fine7 Genius|Fine7 Shine|Fine7 Air|Fine8 Style|Fine9 More|Fine10 Joy|Fine11 Wide",
        "PROSCANTablet": "\\b(PEM63|PLT1023G|PLT1041|PLT1044|PLT1044G|PLT1091|PLT4311|PLT4311PL|PLT4315|PLT7030|PLT7033|PLT7033D|PLT7035|PLT7035D|PLT7044K|PLT7045K|PLT7045KB|PLT7071KG|PLT7072|PLT7223G|PLT7225G|PLT7777G|PLT7810K|PLT7849G|PLT7851G|PLT7852G|PLT8015|PLT8031|PLT8034|PLT8036|PLT8080K|PLT8082|PLT8088|PLT8223G|PLT8234G|PLT8235G|PLT8816K|PLT9011|PLT9045K|PLT9233G|PLT9735|PLT9760G|PLT9770G)\\b",
        "YONESTablet": "BQ1078|BC1003|BC1077|RK9702|BC9730|BC9001|IT9001|BC7008|BC7010|BC708|BC728|BC7012|BC7030|BC7027|BC7026",
        "ChangJiaTablet": "TPC7102|TPC7103|TPC7105|TPC7106|TPC7107|TPC7201|TPC7203|TPC7205|TPC7210|TPC7708|TPC7709|TPC7712|TPC7110|TPC8101|TPC8103|TPC8105|TPC8106|TPC8203|TPC8205|TPC8503|TPC9106|TPC9701|TPC97101|TPC97103|TPC97105|TPC97106|TPC97111|TPC97113|TPC97203|TPC97603|TPC97809|TPC97205|TPC10101|TPC10103|TPC10106|TPC10111|TPC10203|TPC10205|TPC10503",
        "GUTablet": "TX-A1301|TX-M9002|Q702|kf026",
        "PointOfViewTablet": "TAB-P506|TAB-navi-7-3G-M|TAB-P517|TAB-P-527|TAB-P701|TAB-P703|TAB-P721|TAB-P731N|TAB-P741|TAB-P825|TAB-P905|TAB-P925|TAB-PR945|TAB-PL1015|TAB-P1025|TAB-PI1045|TAB-P1325|TAB-PROTAB[0-9]+|TAB-PROTAB25|TAB-PROTAB26|TAB-PROTAB27|TAB-PROTAB26XL|TAB-PROTAB2-IPS9|TAB-PROTAB30-IPS9|TAB-PROTAB25XXL|TAB-PROTAB26-IPS10|TAB-PROTAB30-IPS10",
        "OvermaxTablet": "OV-(SteelCore|NewBase|Basecore|Baseone|Exellen|Quattor|EduTab|Solution|ACTION|BasicTab|TeddyTab|MagicTab|Stream|TB-08|TB-09)|Qualcore 1027",
        "HCLTablet": "HCL.*Tablet|Connect-3G-2.0|Connect-2G-2.0|ME Tablet U1|ME Tablet U2|ME Tablet G1|ME Tablet X1|ME Tablet Y2|ME Tablet Sync",
        "DPSTablet": "DPS Dream 9|DPS Dual 7",
        "VistureTablet": "V97 HD|i75 3G|Visture V4( HD)?|Visture V5( HD)?|Visture V10",
        "CrestaTablet": "CTP(-)?810|CTP(-)?818|CTP(-)?828|CTP(-)?838|CTP(-)?888|CTP(-)?978|CTP(-)?980|CTP(-)?987|CTP(-)?988|CTP(-)?989",
        "MediatekTablet": "\\bMT8125|MT8389|MT8135|MT8377\\b",
        "ConcordeTablet": "Concorde([ ]+)?Tab|ConCorde ReadMan",
        "GoCleverTablet": "GOCLEVER TAB|A7GOCLEVER|M1042|M7841|M742|R1042BK|R1041|TAB A975|TAB A7842|TAB A741|TAB A741L|TAB M723G|TAB M721|TAB A1021|TAB I921|TAB R721|TAB I720|TAB T76|TAB R70|TAB R76.2|TAB R106|TAB R83.2|TAB M813G|TAB I721|GCTA722|TAB I70|TAB I71|TAB S73|TAB R73|TAB R74|TAB R93|TAB R75|TAB R76.1|TAB A73|TAB A93|TAB A93.2|TAB T72|TAB R83|TAB R974|TAB R973|TAB A101|TAB A103|TAB A104|TAB A104.2|R105BK|M713G|A972BK|TAB A971|TAB R974.2|TAB R104|TAB R83.3|TAB A1042",
        "ModecomTablet": "FreeTAB 9000|FreeTAB 7.4|FreeTAB 7004|FreeTAB 7800|FreeTAB 2096|FreeTAB 7.5|FreeTAB 1014|FreeTAB 1001 |FreeTAB 8001|FreeTAB 9706|FreeTAB 9702|FreeTAB 7003|FreeTAB 7002|FreeTAB 1002|FreeTAB 7801|FreeTAB 1331|FreeTAB 1004|FreeTAB 8002|FreeTAB 8014|FreeTAB 9704|FreeTAB 1003",
        "VoninoTablet": "\\b(Argus[ _]?S|Diamond[ _]?79HD|Emerald[ _]?78E|Luna[ _]?70C|Onyx[ _]?S|Onyx[ _]?Z|Orin[ _]?HD|Orin[ _]?S|Otis[ _]?S|SpeedStar[ _]?S|Magnet[ _]?M9|Primus[ _]?94[ _]?3G|Primus[ _]?94HD|Primus[ _]?QS|Android.*\\bQ8\\b|Sirius[ _]?EVO[ _]?QS|Sirius[ _]?QS|Spirit[ _]?S)\\b",
        "ECSTablet": "V07OT2|TM105A|S10OT1|TR10CS1",
        "StorexTablet": "eZee[_']?(Tab|Go)[0-9]+|TabLC7|Looney Tunes Tab",
        "VodafoneTablet": "SmartTab([ ]+)?[0-9]+|SmartTabII10|SmartTabII7|VF-1497",
        "EssentielBTablet": "Smart[ ']?TAB[ ]+?[0-9]+|Family[ ']?TAB2",
        "RossMoorTablet": "RM-790|RM-997|RMD-878G|RMD-974R|RMT-705A|RMT-701|RME-601|RMT-501|RMT-711",
        "iMobileTablet": "i-mobile i-note",
        "TolinoTablet": "tolino tab [0-9.]+|tolino shine",
        "AudioSonicTablet": "\\bC-22Q|T7-QC|T-17B|T-17P\\b",
        "AMPETablet": "Android.* A78 ",
        "SkkTablet": "Android.* (SKYPAD|PHOENIX|CYCLOPS)",
        "TecnoTablet": "TECNO P9",
        "JXDTablet": "Android.* \\b(F3000|A3300|JXD5000|JXD3000|JXD2000|JXD300B|JXD300|S5800|S7800|S602b|S5110b|S7300|S5300|S602|S603|S5100|S5110|S601|S7100a|P3000F|P3000s|P101|P200s|P1000m|P200m|P9100|P1000s|S6600b|S908|P1000|P300|S18|S6600|S9100)\\b",
        "iJoyTablet": "Tablet (Spirit 7|Essentia|Galatea|Fusion|Onix 7|Landa|Titan|Scooby|Deox|Stella|Themis|Argon|Unique 7|Sygnus|Hexen|Finity 7|Cream|Cream X2|Jade|Neon 7|Neron 7|Kandy|Scape|Saphyr 7|Rebel|Biox|Rebel|Rebel 8GB|Myst|Draco 7|Myst|Tab7-004|Myst|Tadeo Jones|Tablet Boing|Arrow|Draco Dual Cam|Aurix|Mint|Amity|Revolution|Finity 9|Neon 9|T9w|Amity 4GB Dual Cam|Stone 4GB|Stone 8GB|Andromeda|Silken|X2|Andromeda II|Halley|Flame|Saphyr 9,7|Touch 8|Planet|Triton|Unique 10|Hexen 10|Memphis 4GB|Memphis 8GB|Onix 10)",
        "FX2Tablet": "FX2 PAD7|FX2 PAD10",
        "XoroTablet": "KidsPAD 701|PAD[ ]?712|PAD[ ]?714|PAD[ ]?716|PAD[ ]?717|PAD[ ]?718|PAD[ ]?720|PAD[ ]?721|PAD[ ]?722|PAD[ ]?790|PAD[ ]?792|PAD[ ]?900|PAD[ ]?9715D|PAD[ ]?9716DR|PAD[ ]?9718DR|PAD[ ]?9719QR|PAD[ ]?9720QR|TelePAD1030|Telepad1032|TelePAD730|TelePAD731|TelePAD732|TelePAD735Q|TelePAD830|TelePAD9730|TelePAD795|MegaPAD 1331|MegaPAD 1851|MegaPAD 2151",
        "ViewsonicTablet": "ViewPad 10pi|ViewPad 10e|ViewPad 10s|ViewPad E72|ViewPad7|ViewPad E100|ViewPad 7e|ViewSonic VB733|VB100a",
        "VerizonTablet": "QTAQZ3|QTAIR7|QTAQTZ3|QTASUN1|QTASUN2|QTAXIA1",
        "OdysTablet": "LOOX|XENO10|ODYS[ -](Space|EVO|Xpress|NOON)|\\bXELIO\\b|Xelio10Pro|XELIO7PHONETAB|XELIO10EXTREME|XELIOPT2|NEO_QUAD10",
        "CaptivaTablet": "CAPTIVA PAD",
        "IconbitTablet": "NetTAB|NT-3702|NT-3702S|NT-3702S|NT-3603P|NT-3603P|NT-0704S|NT-0704S|NT-3805C|NT-3805C|NT-0806C|NT-0806C|NT-0909T|NT-0909T|NT-0907S|NT-0907S|NT-0902S|NT-0902S",
        "TeclastTablet": "T98 4G|\\bP80\\b|\\bX90HD\\b|X98 Air|X98 Air 3G|\\bX89\\b|P80 3G|\\bX80h\\b|P98 Air|\\bX89HD\\b|P98 3G|\\bP90HD\\b|P89 3G|X98 3G|\\bP70h\\b|P79HD 3G|G18d 3G|\\bP79HD\\b|\\bP89s\\b|\\bA88\\b|\\bP10HD\\b|\\bP19HD\\b|G18 3G|\\bP78HD\\b|\\bA78\\b|\\bP75\\b|G17s 3G|G17h 3G|\\bP85t\\b|\\bP90\\b|\\bP11\\b|\\bP98t\\b|\\bP98HD\\b|\\bG18d\\b|\\bP85s\\b|\\bP11HD\\b|\\bP88s\\b|\\bA80HD\\b|\\bA80se\\b|\\bA10h\\b|\\bP89\\b|\\bP78s\\b|\\bG18\\b|\\bP85\\b|\\bA70h\\b|\\bA70\\b|\\bG17\\b|\\bP18\\b|\\bA80s\\b|\\bA11s\\b|\\bP88HD\\b|\\bA80h\\b|\\bP76s\\b|\\bP76h\\b|\\bP98\\b|\\bA10HD\\b|\\bP78\\b|\\bP88\\b|\\bA11\\b|\\bA10t\\b|\\bP76a\\b|\\bP76t\\b|\\bP76e\\b|\\bP85HD\\b|\\bP85a\\b|\\bP86\\b|\\bP75HD\\b|\\bP76v\\b|\\bA12\\b|\\bP75a\\b|\\bA15\\b|\\bP76Ti\\b|\\bP81HD\\b|\\bA10\\b|\\bT760VE\\b|\\bT720HD\\b|\\bP76\\b|\\bP73\\b|\\bP71\\b|\\bP72\\b|\\bT720SE\\b|\\bC520Ti\\b|\\bT760\\b|\\bT720VE\\b|T720-3GE|T720-WiFi",
        "OndaTablet": "\\b(V975i|Vi30|VX530|V701|Vi60|V701s|Vi50|V801s|V719|Vx610w|VX610W|V819i|Vi10|VX580W|Vi10|V711s|V813|V811|V820w|V820|Vi20|V711|VI30W|V712|V891w|V972|V819w|V820w|Vi60|V820w|V711|V813s|V801|V819|V975s|V801|V819|V819|V818|V811|V712|V975m|V101w|V961w|V812|V818|V971|V971s|V919|V989|V116w|V102w|V973|Vi40)\\b[\\s]+",
        "JaytechTablet": "TPC-PA762",
        "BlaupunktTablet": "Endeavour 800NG|Endeavour 1010",
        "DigmaTablet": "\\b(iDx10|iDx9|iDx8|iDx7|iDxD7|iDxD8|iDsQ8|iDsQ7|iDsQ8|iDsD10|iDnD7|3TS804H|iDsQ11|iDj7|iDs10)\\b",
        "EvolioTablet": "ARIA_Mini_wifi|Aria[ _]Mini|Evolio X10|Evolio X7|Evolio X8|\\bEvotab\\b|\\bNeura\\b",
        "LavaTablet": "QPAD E704|\\bIvoryS\\b|E-TAB IVORY|\\bE-TAB\\b",
        "AocTablet": "MW0811|MW0812|MW0922|MTK8382|MW1031|MW0831|MW0821|MW0931|MW0712",
        "MpmanTablet": "MP11 OCTA|MP10 OCTA|MPQC1114|MPQC1004|MPQC994|MPQC974|MPQC973|MPQC804|MPQC784|MPQC780|\\bMPG7\\b|MPDCG75|MPDCG71|MPDC1006|MP101DC|MPDC9000|MPDC905|MPDC706HD|MPDC706|MPDC705|MPDC110|MPDC100|MPDC99|MPDC97|MPDC88|MPDC8|MPDC77|MP709|MID701|MID711|MID170|MPDC703|MPQC1010",
        "CelkonTablet": "CT695|CT888|CT[\\s]?910|CT7 Tab|CT9 Tab|CT3 Tab|CT2 Tab|CT1 Tab|C820|C720|\\bCT-1\\b",
        "WolderTablet": "miTab \\b(DIAMOND|SPACE|BROOKLYN|NEO|FLY|MANHATTAN|FUNK|EVOLUTION|SKY|GOCAR|IRON|GENIUS|POP|MINT|EPSILON|BROADWAY|JUMP|HOP|LEGEND|NEW AGE|LINE|ADVANCE|FEEL|FOLLOW|LIKE|LINK|LIVE|THINK|FREEDOM|CHICAGO|CLEVELAND|BALTIMORE-GH|IOWA|BOSTON|SEATTLE|PHOENIX|DALLAS|IN 101|MasterChef)\\b",
        "MiTablet": "\\bMI PAD\\b|\\bHM NOTE 1W\\b",
        "NibiruTablet": "Nibiru M1|Nibiru Jupiter One",
        "NexoTablet": "NEXO NOVA|NEXO 10|NEXO AVIO|NEXO FREE|NEXO GO|NEXO EVO|NEXO 3G|NEXO SMART|NEXO KIDDO|NEXO MOBI",
        "LeaderTablet": "TBLT10Q|TBLT10I|TBL-10WDKB|TBL-10WDKBO2013|TBL-W230V2|TBL-W450|TBL-W500|SV572|TBLT7I|TBA-AC7-8G|TBLT79|TBL-8W16|TBL-10W32|TBL-10WKB|TBL-W100",
        "UbislateTablet": "UbiSlate[\\s]?7C",
        "PocketBookTablet": "Pocketbook",
        "KocasoTablet": "\\b(TB-1207)\\b",
        "HisenseTablet": "\\b(F5281|E2371)\\b",
        "Hudl": "Hudl HT7S3|Hudl 2",
        "TelstraTablet": "T-Hub2",
        "GenericTablet": "Android.*\\b97D\\b|Tablet(?!.*PC)|BNTV250A|MID-WCDMA|LogicPD Zoom2|\\bA7EB\\b|CatNova8|A1_07|CT704|CT1002|\\bM721\\b|rk30sdk|\\bEVOTAB\\b|M758A|ET904|ALUMIUM10|Smartfren Tab|Endeavour 1010|Tablet-PC-4|Tagi Tab|\\bM6pro\\b|CT1020W|arc 10HD|\\bTP750\\b|\\bQTAQZ3\\b"
    },
    "oss": {
        "AndroidOS": "Android",
        "BlackBerryOS": "blackberry|\\bBB10\\b|rim tablet os",
        "PalmOS": "PalmOS|avantgo|blazer|elaine|hiptop|palm|plucker|xiino",
        "SymbianOS": "Symbian|SymbOS|Series60|Series40|SYB-[0-9]+|\\bS60\\b",
        "WindowsMobileOS": "Windows CE.*(PPC|Smartphone|Mobile|[0-9]{3}x[0-9]{3})|Window Mobile|Windows Phone [0-9.]+|WCE;",
        "WindowsPhoneOS": "Windows Phone 10.0|Windows Phone 8.1|Windows Phone 8.0|Windows Phone OS|XBLWP7|ZuneWP7|Windows NT 6.[23]; ARM;",
        "iOS": "\\biPhone.*Mobile|\\biPod|\\biPad|AppleCoreMedia",
        "MeeGoOS": "MeeGo",
        "MaemoOS": "Maemo",
        "JavaOS": "J2ME\/|\\bMIDP\\b|\\bCLDC\\b",
        "webOS": "webOS|hpwOS",
        "badaOS": "\\bBada\\b",
        "BREWOS": "BREW"
    },
    "uas": {
        "Chrome": "\\bCrMo\\b|CriOS|Android.*Chrome\/[.0-9]* (Mobile)?",
        "Dolfin": "\\bDolfin\\b",
        "Opera": "Opera.*Mini|Opera.*Mobi|Android.*Opera|Mobile.*OPR\/[0-9.]+|Coast\/[0-9.]+",
        "Skyfire": "Skyfire",
        "Edge": "Mobile Safari\/[.0-9]* Edge",
        "IE": "IEMobile|MSIEMobile",
        "Firefox": "fennec|firefox.*maemo|(Mobile|Tablet).*Firefox|Firefox.*Mobile|FxiOS",
        "Bolt": "bolt",
        "TeaShark": "teashark",
        "Blazer": "Blazer",
        "Safari": "Version.*Mobile.*Safari|Safari.*Mobile|MobileSafari",
        "UCBrowser": "UC.*Browser|UCWEB",
        "baiduboxapp": "baiduboxapp",
        "baidubrowser": "baidubrowser",
        "DiigoBrowser": "DiigoBrowser",
        "Puffin": "Puffin",
        "Mercury": "\\bMercury\\b",
        "ObigoBrowser": "Obigo",
        "NetFront": "NF-Browser",
        "GenericBrowser": "NokiaBrowser|OviBrowser|OneBrowser|TwonkyBeamBrowser|SEMC.*Browser|FlyFlow|Minimo|NetFront|Novarra-Vision|MQQBrowser|MicroMessenger",
        "PaleMoon": "Android.*PaleMoon|Mobile.*PaleMoon"
    },
    "props": {
        "Mobile": "Mobile\/[VER]",
        "Build": "Build\/[VER]",
        "Version": "Version\/[VER]",
        "VendorID": "VendorID\/[VER]",
        "iPad": "iPad.*CPU[a-z ]+[VER]",
        "iPhone": "iPhone.*CPU[a-z ]+[VER]",
        "iPod": "iPod.*CPU[a-z ]+[VER]",
        "Kindle": "Kindle\/[VER]",
        "Chrome": [
            "Chrome\/[VER]",
            "CriOS\/[VER]",
            "CrMo\/[VER]"
        ],
        "Coast": [
            "Coast\/[VER]"
        ],
        "Dolfin": "Dolfin\/[VER]",
        "Firefox": [
            "Firefox\/[VER]",
            "FxiOS\/[VER]"
        ],
        "Fennec": "Fennec\/[VER]",
        "Edge": "Edge\/[VER]",
        "IE": [
            "IEMobile\/[VER];",
            "IEMobile [VER]",
            "MSIE [VER];",
            "Trident\/[0-9.]+;.*rv:[VER]"
        ],
        "NetFront": "NetFront\/[VER]",
        "NokiaBrowser": "NokiaBrowser\/[VER]",
        "Opera": [
            " OPR\/[VER]",
            "Opera Mini\/[VER]",
            "Version\/[VER]"
        ],
        "Opera Mini": "Opera Mini\/[VER]",
        "Opera Mobi": "Version\/[VER]",
        "UCBrowser": [
            "UCWEB[VER]",
            "UC.*Browser\/[VER]"
        ],
        "MQQBrowser": "MQQBrowser\/[VER]",
        "MicroMessenger": "MicroMessenger\/[VER]",
        "baiduboxapp": "baiduboxapp\/[VER]",
        "baidubrowser": "baidubrowser\/[VER]",
        "SamsungBrowser": "SamsungBrowser\/[VER]",
        "Iron": "Iron\/[VER]",
        "Safari": [
            "Version\/[VER]",
            "Safari\/[VER]"
        ],
        "Skyfire": "Skyfire\/[VER]",
        "Tizen": "Tizen\/[VER]",
        "Webkit": "webkit[ \/][VER]",
        "PaleMoon": "PaleMoon\/[VER]",
        "Gecko": "Gecko\/[VER]",
        "Trident": "Trident\/[VER]",
        "Presto": "Presto\/[VER]",
        "Goanna": "Goanna\/[VER]",
        "iOS": " \\bi?OS\\b [VER][ ;]{1}",
        "Android": "Android [VER]",
        "BlackBerry": [
            "BlackBerry[\\w]+\/[VER]",
            "BlackBerry.*Version\/[VER]",
            "Version\/[VER]"
        ],
        "BREW": "BREW [VER]",
        "Java": "Java\/[VER]",
        "Windows Phone OS": [
            "Windows Phone OS [VER]",
            "Windows Phone [VER]"
        ],
        "Windows Phone": "Windows Phone [VER]",
        "Windows CE": "Windows CE\/[VER]",
        "Windows NT": "Windows NT [VER]",
        "Symbian": [
            "SymbianOS\/[VER]",
            "Symbian\/[VER]"
        ],
        "webOS": [
            "webOS\/[VER]",
            "hpwOS\/[VER];"
        ]
    },
    "utils": {
        "Bot": "Googlebot|facebookexternalhit|AdsBot-Google|Google Keyword Suggestion|Facebot|YandexBot|YandexMobileBot|bingbot|ia_archiver|AhrefsBot|Ezooms|GSLFbot|WBSearchBot|Twitterbot|TweetmemeBot|Twikle|PaperLiBot|Wotbox|UnwindFetchor|Exabot|MJ12bot|YandexImages|TurnitinBot|Pingdom",
        "MobileBot": "Googlebot-Mobile|AdsBot-Google-Mobile|YahooSeeker\/M1A1-R2D2",
        "DesktopMode": "WPDesktop",
        "TV": "SonyDTV|HbbTV",
        "WebKit": "(webkit)[ \/]([\\w.]+)",
        "Console": "\\b(Nintendo|Nintendo WiiU|Nintendo 3DS|PLAYSTATION|Xbox)\\b",
        "Watch": "SM-V700"
    }
};

    // following patterns come from http://detectmobilebrowsers.com/
    impl.detectMobileBrowsers = {
        fullPattern: /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i,
        shortPattern: /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i,
        tabletPattern: /android|ipad|playbook|silk/i
    };

    var hasOwnProp = Object.prototype.hasOwnProperty,
        isArray;

    impl.FALLBACK_PHONE = 'UnknownPhone';
    impl.FALLBACK_TABLET = 'UnknownTablet';
    impl.FALLBACK_MOBILE = 'UnknownMobile';

    isArray = ('isArray' in Array) ?
        Array.isArray : function (value) { return Object.prototype.toString.call(value) === '[object Array]'; };

    function equalIC(a, b) {
        return a != null && b != null && a.toLowerCase() === b.toLowerCase();
    }

    function containsIC(array, value) {
        var valueLC, i, len = array.length;
        if (!len || !value) {
            return false;
        }
        valueLC = value.toLowerCase();
        for (i = 0; i < len; ++i) {
            if (valueLC === array[i].toLowerCase()) {
                return true;
            }
        }
        return false;
    }

    function convertPropsToRegExp(object) {
        for (var key in object) {
            if (hasOwnProp.call(object, key)) {
                object[key] = new RegExp(object[key], 'i');
            }
        }
    }

    function prepareUserAgent(userAgent) {
        return (userAgent || '').substr(0, 500); // mitigate vulnerable to ReDoS
    }

    (function init() {
        var key, values, value, i, len, verPos, mobileDetectRules = impl.mobileDetectRules;
        for (key in mobileDetectRules.props) {
            if (hasOwnProp.call(mobileDetectRules.props, key)) {
                values = mobileDetectRules.props[key];
                if (!isArray(values)) {
                    values = [values];
                }
                len = values.length;
                for (i = 0; i < len; ++i) {
                    value = values[i];
                    verPos = value.indexOf('[VER]');
                    if (verPos >= 0) {
                        value = value.substring(0, verPos) + '([\\w._\\+]+)' + value.substring(verPos + 5);
                    }
                    values[i] = new RegExp(value, 'i');
                }
                mobileDetectRules.props[key] = values;
            }
        }
        convertPropsToRegExp(mobileDetectRules.oss);
        convertPropsToRegExp(mobileDetectRules.phones);
        convertPropsToRegExp(mobileDetectRules.tablets);
        convertPropsToRegExp(mobileDetectRules.uas);
        convertPropsToRegExp(mobileDetectRules.utils);

        // copy some patterns to oss0 which are tested first (see issue#15)
        mobileDetectRules.oss0 = {
            WindowsPhoneOS: mobileDetectRules.oss.WindowsPhoneOS,
            WindowsMobileOS: mobileDetectRules.oss.WindowsMobileOS
        };
    }());

    /**
     * Test userAgent string against a set of rules and find the first matched key.
     * @param {Object} rules (key is String, value is RegExp)
     * @param {String} userAgent the navigator.userAgent (or HTTP-Header 'User-Agent').
     * @returns {String|null} the matched key if found, otherwise <tt>null</tt>
     * @private
     */
    impl.findMatch = function(rules, userAgent) {
        for (var key in rules) {
            if (hasOwnProp.call(rules, key)) {
                if (rules[key].test(userAgent)) {
                    return key;
                }
            }
        }
        return null;
    };

    /**
     * Test userAgent string against a set of rules and return an array of matched keys.
     * @param {Object} rules (key is String, value is RegExp)
     * @param {String} userAgent the navigator.userAgent (or HTTP-Header 'User-Agent').
     * @returns {Array} an array of matched keys, may be empty when there is no match, but not <tt>null</tt>
     * @private
     */
    impl.findMatches = function(rules, userAgent) {
        var result = [];
        for (var key in rules) {
            if (hasOwnProp.call(rules, key)) {
                if (rules[key].test(userAgent)) {
                    result.push(key);
                }
            }
        }
        return result;
    };

    /**
     * Check the version of the given property in the User-Agent.
     *
     * @param {String} propertyName
     * @param {String} userAgent
     * @return {String} version or <tt>null</tt> if version not found
     * @private
     */
    impl.getVersionStr = function (propertyName, userAgent) {
        var props = impl.mobileDetectRules.props, patterns, i, len, match;
        if (hasOwnProp.call(props, propertyName)) {
            patterns = props[propertyName];
            len = patterns.length;
            for (i = 0; i < len; ++i) {
                match = patterns[i].exec(userAgent);
                if (match !== null) {
                    return match[1];
                }
            }
        }
        return null;
    };

    /**
     * Check the version of the given property in the User-Agent.
     * Will return a float number. (eg. 2_0 will return 2.0, 4.3.1 will return 4.31)
     *
     * @param {String} propertyName
     * @param {String} userAgent
     * @return {Number} version or <tt>NaN</tt> if version not found
     * @private
     */
    impl.getVersion = function (propertyName, userAgent) {
        var version = impl.getVersionStr(propertyName, userAgent);
        return version ? impl.prepareVersionNo(version) : NaN;
    };

    /**
     * Prepare the version number.
     *
     * @param {String} version
     * @return {Number} the version number as a floating number
     * @private
     */
    impl.prepareVersionNo = function (version) {
        var numbers;

        numbers = version.split(/[a-z._ \/\-]/i);
        if (numbers.length === 1) {
            version = numbers[0];
        }
        if (numbers.length > 1) {
            version = numbers[0] + '.';
            numbers.shift();
            version += numbers.join('');
        }
        return Number(version);
    };

    impl.isMobileFallback = function (userAgent) {
        return impl.detectMobileBrowsers.fullPattern.test(userAgent) ||
            impl.detectMobileBrowsers.shortPattern.test(userAgent.substr(0,4));
    };

    impl.isTabletFallback = function (userAgent) {
        return impl.detectMobileBrowsers.tabletPattern.test(userAgent);
    };

    impl.prepareDetectionCache = function (cache, userAgent, maxPhoneWidth) {
        if (cache.mobile !== undefined) {
            return;
        }
        var phone, tablet, phoneSized;

        // first check for stronger tablet rules, then phone (see issue#5)
        tablet = impl.findMatch(impl.mobileDetectRules.tablets, userAgent);
        if (tablet) {
            cache.mobile = cache.tablet = tablet;
            cache.phone = null;
            return; // unambiguously identified as tablet
        }

        phone = impl.findMatch(impl.mobileDetectRules.phones, userAgent);
        if (phone) {
            cache.mobile = cache.phone = phone;
            cache.tablet = null;
            return; // unambiguously identified as phone
        }

        // our rules haven't found a match -> try more general fallback rules
        if (impl.isMobileFallback(userAgent)) {
            phoneSized = MobileDetect.isPhoneSized(maxPhoneWidth);
            if (phoneSized === undefined) {
                cache.mobile = impl.FALLBACK_MOBILE;
                cache.tablet = cache.phone = null;
            } else if (phoneSized) {
                cache.mobile = cache.phone = impl.FALLBACK_PHONE;
                cache.tablet = null;
            } else {
                cache.mobile = cache.tablet = impl.FALLBACK_TABLET;
                cache.phone = null;
            }
        } else if (impl.isTabletFallback(userAgent)) {
            cache.mobile = cache.tablet = impl.FALLBACK_TABLET;
            cache.phone = null;
        } else {
            // not mobile at all!
            cache.mobile = cache.tablet = cache.phone = null;
        }
    };

    // t is a reference to a MobileDetect instance
    impl.mobileGrade = function (t) {
        // impl note:
        // To keep in sync w/ Mobile_Detect.php easily, the following code is tightly aligned to the PHP version.
        // When changes are made in Mobile_Detect.php, copy this method and replace:
        //     $this-> / t.
        //     self::MOBILE_GRADE_(.) / '$1'
        //     , self::VERSION_TYPE_FLOAT / (nothing)
        //     isIOS() / os('iOS')
        //     [reg] / (nothing)   <-- jsdelivr complaining about unescaped unicode character U+00AE
        var $isMobile = t.mobile() !== null;

        if (
            // Apple iOS 3.2-5.1 - Tested on the original iPad (4.3 / 5.0), iPad 2 (4.3), iPad 3 (5.1), original iPhone (3.1), iPhone 3 (3.2), 3GS (4.3), 4 (4.3 / 5.0), and 4S (5.1)
            t.os('iOS') && t.version('iPad')>=4.3 ||
            t.os('iOS') && t.version('iPhone')>=3.1 ||
            t.os('iOS') && t.version('iPod')>=3.1 ||

            // Android 2.1-2.3 - Tested on the HTC Incredible (2.2), original Droid (2.2), HTC Aria (2.1), Google Nexus S (2.3). Functional on 1.5 & 1.6 but performance may be sluggish, tested on Google G1 (1.5)
            // Android 3.1 (Honeycomb)  - Tested on the Samsung Galaxy Tab 10.1 and Motorola XOOM
            // Android 4.0 (ICS)  - Tested on a Galaxy Nexus. Note: transition performance can be poor on upgraded devices
            // Android 4.1 (Jelly Bean)  - Tested on a Galaxy Nexus and Galaxy 7
            ( t.version('Android')>2.1 && t.is('Webkit') ) ||

            // Windows Phone 7-7.5 - Tested on the HTC Surround (7.0) HTC Trophy (7.5), LG-E900 (7.5), Nokia Lumia 800
            t.version('Windows Phone OS')>=7.0 ||

            // Blackberry 7 - Tested on BlackBerry Torch 9810
            // Blackberry 6.0 - Tested on the Torch 9800 and Style 9670
            t.is('BlackBerry') && t.version('BlackBerry')>=6.0 ||
            // Blackberry Playbook (1.0-2.0) - Tested on PlayBook
            t.match('Playbook.*Tablet') ||

            // Palm WebOS (1.4-2.0) - Tested on the Palm Pixi (1.4), Pre (1.4), Pre 2 (2.0)
            ( t.version('webOS')>=1.4 && t.match('Palm|Pre|Pixi') ) ||
            // Palm WebOS 3.0  - Tested on HP TouchPad
            t.match('hp.*TouchPad') ||

            // Firefox Mobile (12 Beta) - Tested on Android 2.3 device
            ( t.is('Firefox') && t.version('Firefox')>=12 ) ||

            // Chrome for Android - Tested on Android 4.0, 4.1 device
            ( t.is('Chrome') && t.is('AndroidOS') && t.version('Android')>=4.0 ) ||

            // Skyfire 4.1 - Tested on Android 2.3 device
            ( t.is('Skyfire') && t.version('Skyfire')>=4.1 && t.is('AndroidOS') && t.version('Android')>=2.3 ) ||

            // Opera Mobile 11.5-12: Tested on Android 2.3
            ( t.is('Opera') && t.version('Opera Mobi')>11 && t.is('AndroidOS') ) ||

            // Meego 1.2 - Tested on Nokia 950 and N9
            t.is('MeeGoOS') ||

            // Tizen (pre-release) - Tested on early hardware
            t.is('Tizen') ||

            // Samsung Bada 2.0 - Tested on a Samsung Wave 3, Dolphin browser
            // @todo: more tests here!
            t.is('Dolfin') && t.version('Bada')>=2.0 ||

            // UC Browser - Tested on Android 2.3 device
            ( (t.is('UC Browser') || t.is('Dolfin')) && t.version('Android')>=2.3 ) ||

            // Kindle 3 and Fire  - Tested on the built-in WebKit browser for each
            ( t.match('Kindle Fire') ||
                t.is('Kindle') && t.version('Kindle')>=3.0 ) ||

            // Nook Color 1.4.1 - Tested on original Nook Color, not Nook Tablet
            t.is('AndroidOS') && t.is('NookTablet') ||

            // Chrome Desktop 11-21 - Tested on OS X 10.7 and Windows 7
            t.version('Chrome')>=11 && !$isMobile ||

            // Safari Desktop 4-5 - Tested on OS X 10.7 and Windows 7
            t.version('Safari')>=5.0 && !$isMobile ||

            // Firefox Desktop 4-13 - Tested on OS X 10.7 and Windows 7
            t.version('Firefox')>=4.0 && !$isMobile ||

            // Internet Explorer 7-9 - Tested on Windows XP, Vista and 7
            t.version('MSIE')>=7.0 && !$isMobile ||

            // Opera Desktop 10-12 - Tested on OS X 10.7 and Windows 7
            // @reference: http://my.opera.com/community/openweb/idopera/
            t.version('Opera')>=10 && !$isMobile

            ){
            return 'A';
        }

        if (
            t.os('iOS') && t.version('iPad')<4.3 ||
            t.os('iOS') && t.version('iPhone')<3.1 ||
            t.os('iOS') && t.version('iPod')<3.1 ||

            // Blackberry 5.0: Tested on the Storm 2 9550, Bold 9770
            t.is('Blackberry') && t.version('BlackBerry')>=5 && t.version('BlackBerry')<6 ||

            //Opera Mini (5.0-6.5) - Tested on iOS 3.2/4.3 and Android 2.3
            ( t.version('Opera Mini')>=5.0 && t.version('Opera Mini')<=6.5 &&
                (t.version('Android')>=2.3 || t.is('iOS')) ) ||

            // Nokia Symbian^3 - Tested on Nokia N8 (Symbian^3), C7 (Symbian^3), also works on N97 (Symbian^1)
            t.match('NokiaN8|NokiaC7|N97.*Series60|Symbian/3') ||

            // @todo: report this (tested on Nokia N71)
            t.version('Opera Mobi')>=11 && t.is('SymbianOS')
            ){
            return 'B';
        }

        if (
        // Blackberry 4.x - Tested on the Curve 8330
            t.version('BlackBerry')<5.0 ||
            // Windows Mobile - Tested on the HTC Leo (WinMo 5.2)
            t.match('MSIEMobile|Windows CE.*Mobile') || t.version('Windows Mobile')<=5.2

            ){
            return 'C';
        }

        //All older smartphone platforms and featurephones - Any device that doesn't support media queries
        //will receive the basic, C grade experience.
        return 'C';
    };

    impl.detectOS = function (ua) {
        return impl.findMatch(impl.mobileDetectRules.oss0, ua) ||
            impl.findMatch(impl.mobileDetectRules.oss, ua);
    };

    impl.getDeviceSmallerSide = function () {
        return window.screen.width < window.screen.height ?
            window.screen.width :
            window.screen.height;
    };

    /**
     * Constructor for MobileDetect object.
     * <br>
     * Such an object will keep a reference to the given user-agent string and cache most of the detect queries.<br>
     * <div style="background-color: #d9edf7; border: 1px solid #bce8f1; color: #3a87ad; padding: 14px; border-radius: 2px; margin-top: 20px">
     *     <strong>Find information how to download and install:</strong>
     *     <a href="https://github.com/hgoebl/mobile-detect.js/">github.com/hgoebl/mobile-detect.js/</a>
     * </div>
     *
     * @example <pre>
     *     var md = new MobileDetect(window.navigator.userAgent);
     *     if (md.mobile()) {
     *         location.href = (md.mobileGrade() === 'A') ? '/mobile/' : '/lynx/';
     *     }
     * </pre>
     *
     * @param {string} userAgent typically taken from window.navigator.userAgent or http_header['User-Agent']
     * @param {number} [maxPhoneWidth=600] <strong>only for browsers</strong> specify a value for the maximum
     *        width of smallest device side (in logical "CSS" pixels) until a device detected as mobile will be handled
     *        as phone.
     *        This is only used in cases where the device cannot be classified as phone or tablet.<br>
     *        See <a href="http://developer.android.com/guide/practices/screens_support.html">Declaring Tablet Layouts
     *        for Android</a>.<br>
     *        If you provide a value < 0, then this "fuzzy" check is disabled.
     * @constructor
     * @global
     */
    function MobileDetect(userAgent, maxPhoneWidth) {
        this.ua = prepareUserAgent(userAgent);
        this._cache = {};
        //600dp is typical 7" tablet minimum width
        this.maxPhoneWidth = maxPhoneWidth || 600;
    }

    MobileDetect.prototype = {
        constructor: MobileDetect,

        /**
         * Returns the detected phone or tablet type or <tt>null</tt> if it is not a mobile device.
         * <br>
         * For a list of possible return values see {@link MobileDetect#phone} and {@link MobileDetect#tablet}.<br>
         * <br>
         * If the device is not detected by the regular expressions from Mobile-Detect, a test is made against
         * the patterns of <a href="http://detectmobilebrowsers.com/">detectmobilebrowsers.com</a>. If this test
         * is positive, a value of <code>UnknownPhone</code>, <code>UnknownTablet</code> or
         * <code>UnknownMobile</code> is returned.<br>
         * When used in browser, the decision whether phone or tablet is made based on <code>screen.width/height</code>.<br>
         * <br>
         * When used server-side (node.js), there is no way to tell the difference between <code>UnknownTablet</code>
         * and <code>UnknownMobile</code>, so you will get <code>UnknownMobile</code> here.<br>
         * Be aware that since v1.0.0 in this special case you will get <code>UnknownMobile</code> only for:
         * {@link MobileDetect#mobile}, not for {@link MobileDetect#phone} and {@link MobileDetect#tablet}.
         * In versions before v1.0.0 all 3 methods returned <code>UnknownMobile</code> which was tedious to use.
         * <br>
         * In most cases you will use the return value just as a boolean.
         *
         * @returns {String} the key for the phone family or tablet family, e.g. "Nexus".
         * @function MobileDetect#mobile
         */
        mobile: function () {
            impl.prepareDetectionCache(this._cache, this.ua, this.maxPhoneWidth);
            return this._cache.mobile;
        },

        /**
         * Returns the detected phone type/family string or <tt>null</tt>.
         * <br>
         * The returned tablet (family or producer) is one of following keys:<br>
         * <br><tt>iPhone, BlackBerry, HTC, Nexus, Dell, Motorola, Samsung, LG, Sony, Asus,
         * NokiaLumia, Micromax, Palm, Vertu, Pantech, Fly, Wiko, iMobile, SimValley,
         * Wolfgang, Alcatel, Nintendo, Amoi, INQ, GenericPhone</tt><br>
         * <br>
         * If the device is not detected by the regular expressions from Mobile-Detect, a test is made against
         * the patterns of <a href="http://detectmobilebrowsers.com/">detectmobilebrowsers.com</a>. If this test
         * is positive, a value of <code>UnknownPhone</code> or <code>UnknownMobile</code> is returned.<br>
         * When used in browser, the decision whether phone or tablet is made based on <code>screen.width/height</code>.<br>
         * <br>
         * When used server-side (node.js), there is no way to tell the difference between <code>UnknownTablet</code>
         * and <code>UnknownMobile</code>, so you will get <code>null</code> here, while {@link MobileDetect#mobile}
         * will return <code>UnknownMobile</code>.<br>
         * Be aware that since v1.0.0 in this special case you will get <code>UnknownMobile</code> only for:
         * {@link MobileDetect#mobile}, not for {@link MobileDetect#phone} and {@link MobileDetect#tablet}.
         * In versions before v1.0.0 all 3 methods returned <code>UnknownMobile</code> which was tedious to use.
         * <br>
         * In most cases you will use the return value just as a boolean.
         *
         * @returns {String} the key of the phone family or producer, e.g. "iPhone"
         * @function MobileDetect#phone
         */
        phone: function () {
            impl.prepareDetectionCache(this._cache, this.ua, this.maxPhoneWidth);
            return this._cache.phone;
        },

        /**
         * Returns the detected tablet type/family string or <tt>null</tt>.
         * <br>
         * The returned tablet (family or producer) is one of following keys:<br>
         * <br><tt>iPad, NexusTablet, SamsungTablet, Kindle, SurfaceTablet, HPTablet, AsusTablet,
         * BlackBerryTablet, HTCtablet, MotorolaTablet, NookTablet, AcerTablet,
         * ToshibaTablet, LGTablet, FujitsuTablet, PrestigioTablet, LenovoTablet,
         * DellTablet, YarvikTablet, MedionTablet, ArnovaTablet, IntensoTablet, IRUTablet,
         * MegafonTablet, EbodaTablet, AllViewTablet, ArchosTablet, AinolTablet,
         * NokiaLumiaTablet, SonyTablet, PhilipsTablet, CubeTablet, CobyTablet, MIDTablet,
         * MSITablet, SMiTTablet, RockChipTablet, FlyTablet, bqTablet, HuaweiTablet,
         * NecTablet, PantechTablet, BronchoTablet, VersusTablet, ZyncTablet,
         * PositivoTablet, NabiTablet, KoboTablet, DanewTablet, TexetTablet,
         * PlaystationTablet, TrekstorTablet, PyleAudioTablet, AdvanTablet,
         * DanyTechTablet, GalapadTablet, MicromaxTablet, KarbonnTablet, AllFineTablet,
         * PROSCANTablet, YONESTablet, ChangJiaTablet, GUTablet, PointOfViewTablet,
         * OvermaxTablet, HCLTablet, DPSTablet, VistureTablet, CrestaTablet,
         * MediatekTablet, ConcordeTablet, GoCleverTablet, ModecomTablet, VoninoTablet,
         * ECSTablet, StorexTablet, VodafoneTablet, EssentielBTablet, RossMoorTablet,
         * iMobileTablet, TolinoTablet, AudioSonicTablet, AMPETablet, SkkTablet,
         * TecnoTablet, JXDTablet, iJoyTablet, FX2Tablet, XoroTablet, ViewsonicTablet,
         * VerizonTablet, OdysTablet, CaptivaTablet, IconbitTablet, TeclastTablet,
         * OndaTablet, JaytechTablet, BlaupunktTablet, DigmaTablet, EvolioTablet,
         * LavaTablet, AocTablet, MpmanTablet, CelkonTablet, WolderTablet, MiTablet,
         * NibiruTablet, NexoTablet, LeaderTablet, UbislateTablet, PocketBookTablet,
         * KocasoTablet, HisenseTablet, Hudl, TelstraTablet, GenericTablet</tt><br>
         * <br>
         * If the device is not detected by the regular expressions from Mobile-Detect, a test is made against
         * the patterns of <a href="http://detectmobilebrowsers.com/">detectmobilebrowsers.com</a>. If this test
         * is positive, a value of <code>UnknownTablet</code> or <code>UnknownMobile</code> is returned.<br>
         * When used in browser, the decision whether phone or tablet is made based on <code>screen.width/height</code>.<br>
         * <br>
         * When used server-side (node.js), there is no way to tell the difference between <code>UnknownTablet</code>
         * and <code>UnknownMobile</code>, so you will get <code>null</code> here, while {@link MobileDetect#mobile}
         * will return <code>UnknownMobile</code>.<br>
         * Be aware that since v1.0.0 in this special case you will get <code>UnknownMobile</code> only for:
         * {@link MobileDetect#mobile}, not for {@link MobileDetect#phone} and {@link MobileDetect#tablet}.
         * In versions before v1.0.0 all 3 methods returned <code>UnknownMobile</code> which was tedious to use.
         * <br>
         * In most cases you will use the return value just as a boolean.
         *
         * @returns {String} the key of the tablet family or producer, e.g. "SamsungTablet"
         * @function MobileDetect#tablet
         */
        tablet: function () {
            impl.prepareDetectionCache(this._cache, this.ua, this.maxPhoneWidth);
            return this._cache.tablet;
        },

        /**
         * Returns the (first) detected user-agent string or <tt>null</tt>.
         * <br>
         * The returned user-agent is one of following keys:<br>
         * <br><tt>Chrome, Dolfin, Opera, Skyfire, Edge, IE, Firefox, Bolt, TeaShark, Blazer,
         * Safari, UCBrowser, baiduboxapp, baidubrowser, DiigoBrowser, Puffin, Mercury,
         * ObigoBrowser, NetFront, GenericBrowser, PaleMoon</tt><br>
         * <br>
         * In most cases calling {@link MobileDetect#userAgent} will be sufficient. But there are rare
         * cases where a mobile device pretends to be more than one particular browser. You can get the
         * list of all matches with {@link MobileDetect#userAgents} or check for a particular value by
         * providing one of the defined keys as first argument to {@link MobileDetect#is}.
         *
         * @returns {String} the key for the detected user-agent or <tt>null</tt>
         * @function MobileDetect#userAgent
         */
        userAgent: function () {
            if (this._cache.userAgent === undefined) {
                this._cache.userAgent = impl.findMatch(impl.mobileDetectRules.uas, this.ua);
            }
            return this._cache.userAgent;
        },

        /**
         * Returns all detected user-agent strings.
         * <br>
         * The array is empty or contains one or more of following keys:<br>
         * <br><tt>Chrome, Dolfin, Opera, Skyfire, Edge, IE, Firefox, Bolt, TeaShark, Blazer,
         * Safari, UCBrowser, baiduboxapp, baidubrowser, DiigoBrowser, Puffin, Mercury,
         * ObigoBrowser, NetFront, GenericBrowser, PaleMoon</tt><br>
         * <br>
         * In most cases calling {@link MobileDetect#userAgent} will be sufficient. But there are rare
         * cases where a mobile device pretends to be more than one particular browser. You can get the
         * list of all matches with {@link MobileDetect#userAgents} or check for a particular value by
         * providing one of the defined keys as first argument to {@link MobileDetect#is}.
         *
         * @returns {Array} the array of detected user-agent keys or <tt>[]</tt>
         * @function MobileDetect#userAgents
         */
        userAgents: function () {
            if (this._cache.userAgents === undefined) {
                this._cache.userAgents = impl.findMatches(impl.mobileDetectRules.uas, this.ua);
            }
            return this._cache.userAgents;
        },

        /**
         * Returns the detected operating system string or <tt>null</tt>.
         * <br>
         * The operating system is one of following keys:<br>
         * <br><tt>AndroidOS, BlackBerryOS, PalmOS, SymbianOS, WindowsMobileOS, WindowsPhoneOS,
         * iOS, MeeGoOS, MaemoOS, JavaOS, webOS, badaOS, BREWOS</tt><br>
         *
         * @returns {String} the key for the detected operating system.
         * @function MobileDetect#os
         */
        os: function () {
            if (this._cache.os === undefined) {
                this._cache.os = impl.detectOS(this.ua);
            }
            return this._cache.os;
        },

        /**
         * Get the version (as Number) of the given property in the User-Agent.
         * <br>
         * Will return a float number. (eg. 2_0 will return 2.0, 4.3.1 will return 4.31)
         *
         * @param {String} key a key defining a thing which has a version.<br>
         *        You can use one of following keys:<br>
         * <br><tt>Mobile, Build, Version, VendorID, iPad, iPhone, iPod, Kindle, Chrome, Coast,
         * Dolfin, Firefox, Fennec, Edge, IE, NetFront, NokiaBrowser, Opera, Opera Mini,
         * Opera Mobi, UCBrowser, MQQBrowser, MicroMessenger, baiduboxapp, baidubrowser,
         * SamsungBrowser, Iron, Safari, Skyfire, Tizen, Webkit, PaleMoon, Gecko, Trident,
         * Presto, Goanna, iOS, Android, BlackBerry, BREW, Java, Windows Phone OS, Windows
         * Phone, Windows CE, Windows NT, Symbian, webOS</tt><br>
         *
         * @returns {Number} the version as float or <tt>NaN</tt> if User-Agent doesn't contain this version.
         *          Be careful when comparing this value with '==' operator!
         * @function MobileDetect#version
         */
        version: function (key) {
            return impl.getVersion(key, this.ua);
        },

        /**
         * Get the version (as String) of the given property in the User-Agent.
         * <br>
         *
         * @param {String} key a key defining a thing which has a version.<br>
         *        You can use one of following keys:<br>
         * <br><tt>Mobile, Build, Version, VendorID, iPad, iPhone, iPod, Kindle, Chrome, Coast,
         * Dolfin, Firefox, Fennec, Edge, IE, NetFront, NokiaBrowser, Opera, Opera Mini,
         * Opera Mobi, UCBrowser, MQQBrowser, MicroMessenger, baiduboxapp, baidubrowser,
         * SamsungBrowser, Iron, Safari, Skyfire, Tizen, Webkit, PaleMoon, Gecko, Trident,
         * Presto, Goanna, iOS, Android, BlackBerry, BREW, Java, Windows Phone OS, Windows
         * Phone, Windows CE, Windows NT, Symbian, webOS</tt><br>
         *
         * @returns {String} the "raw" version as String or <tt>null</tt> if User-Agent doesn't contain this version.
         *
         * @function MobileDetect#versionStr
         */
        versionStr: function (key) {
            return impl.getVersionStr(key, this.ua);
        },

        /**
         * Global test key against userAgent, os, phone, tablet and some other properties of userAgent string.
         *
         * @param {String} key the key (case-insensitive) of a userAgent, an operating system, phone or
         *        tablet family.<br>
         *        For a complete list of possible values, see {@link MobileDetect#userAgent},
         *        {@link MobileDetect#os}, {@link MobileDetect#phone}, {@link MobileDetect#tablet}.<br>
         *        Additionally you have following keys:<br>
         * <br><tt>Bot, MobileBot, DesktopMode, TV, WebKit, Console, Watch</tt><br>
         *
         * @returns {boolean} <tt>true</tt> when the given key is one of the defined keys of userAgent, os, phone,
         *                    tablet or one of the listed additional keys, otherwise <tt>false</tt>
         * @function MobileDetect#is
         */
        is: function (key) {
            return containsIC(this.userAgents(), key) ||
                   equalIC(key, this.os()) ||
                   equalIC(key, this.phone()) ||
                   equalIC(key, this.tablet()) ||
                   containsIC(impl.findMatches(impl.mobileDetectRules.utils, this.ua), key);
        },

        /**
         * Do a quick test against navigator::userAgent.
         *
         * @param {String|RegExp} pattern the pattern, either as String or RegExp
         *                        (a string will be converted to a case-insensitive RegExp).
         * @returns {boolean} <tt>true</tt> when the pattern matches, otherwise <tt>false</tt>
         * @function MobileDetect#match
         */
        match: function (pattern) {
            if (!(pattern instanceof RegExp)) {
                pattern = new RegExp(pattern, 'i');
            }
            return pattern.test(this.ua);
        },

        /**
         * Checks whether the mobile device can be considered as phone regarding <code>screen.width</code>.
         * <br>
         * Obviously this method makes sense in browser environments only (not for Node.js)!
         * @param {number} [maxPhoneWidth] the maximum logical pixels (aka. CSS-pixels) to be considered as phone.<br>
         *        The argument is optional and if not present or falsy, the value of the constructor is taken.
         * @returns {boolean|undefined} <code>undefined</code> if screen size wasn't detectable, else <code>true</code>
         *          when screen.width is less or equal to maxPhoneWidth, otherwise <code>false</code>.<br>
         *          Will always return <code>undefined</code> server-side.
         */
        isPhoneSized: function (maxPhoneWidth) {
            return MobileDetect.isPhoneSized(maxPhoneWidth || this.maxPhoneWidth);
        },

        /**
         * Returns the mobile grade ('A', 'B', 'C').
         *
         * @returns {String} one of the mobile grades ('A', 'B', 'C').
         * @function MobileDetect#mobileGrade
         */
        mobileGrade: function () {
            if (this._cache.grade === undefined) {
                this._cache.grade = impl.mobileGrade(this);
            }
            return this._cache.grade;
        }
    };

    // environment-dependent
    if (typeof window !== 'undefined' && window.screen) {
        MobileDetect.isPhoneSized = function (maxPhoneWidth) {
            return maxPhoneWidth < 0 ? undefined : impl.getDeviceSmallerSide() <= maxPhoneWidth;
        };
    } else {
        MobileDetect.isPhoneSized = function () {};
    }

    // should not be replaced by a completely new object - just overwrite existing methods
    MobileDetect._impl = impl;
    
    MobileDetect.version = '1.4.1 2017-12-24';

    return MobileDetect;
}); // end of call of define()
})((function (undefined) {
    if (typeof module !== 'undefined' && module.exports) {
        return function (factory) { module.exports = factory(); };
    } else if (true) {
        return __webpack_require__(29);
    } else if (typeof window !== 'undefined') {
        return function (factory) { window.MobileDetect = factory(); };
    } else {
        // please file a bug if you get this error!
        throw new Error('unknown environment');
    }
})());

/***/ }),
/* 29 */
/***/ (function(module, exports) {

module.exports = function() {
	throw new Error("define cannot be used indirect");
};


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Message2 = __webpack_require__(31);

var _Message3 = _interopRequireDefault(_Message2);

var _webviewer = __webpack_require__(6);

var WEBVIEWER = _interopRequireWildcard(_webviewer);

var _remoteControl = __webpack_require__(7);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RemoteControl = function (_Message) {
  _inherits(RemoteControl, _Message);

  function RemoteControl() {
    var _ref;

    _classCallCheck(this, RemoteControl);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = RemoteControl.__proto__ || Object.getPrototypeOf(RemoteControl)).call.apply(_ref, [this].concat(args)));

    _this.accessCode = null;
    _this.viewerId = null;
    _this.hostId = null;
    _this.viewerTopic = null;
    _this.hostTopic = null;
    _this.willTopic = null;
    _this.onReceivedEvent = null;
    return _this;
  }

  _createClass(RemoteControl, [{
    key: 'set',
    value: function set(_ref2) {
      var accessCode = _ref2.accessCode,
          viewerId = _ref2.viewerId,
          hostId = _ref2.hostId,
          onReceivedEvent = _ref2.onReceivedEvent;

      this.accessCode = accessCode;
      this.viewerId = viewerId;
      this.hostId = hostId;
      this.viewerTopic = '/client/remote_control/' + viewerId + '/' + accessCode;
      this.hostTopic = '/client/remote_control/' + hostId + '/' + accessCode;
      this.willTopic = '/client/force_disconnect/' + accessCode;
      this.onReceivedEvent = onReceivedEvent;

      this.addSubscribe(this.viewerTopic);
      this.addSubscribe(this.willTopic);
    }
  }, {
    key: 'getTopic',
    value: function getTopic() {
      return {
        viewer: this.viewerTopic,
        host: this.hostTopic
      };
    }
  }, {
    key: 'relayInfo',
    value: function relayInfo(data) {
      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.RELAY_INFO,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [response] relay info',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.RELAY_INFO,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'requestMonitorsInfo',
    value: function requestMonitorsInfo() {
      var data = {
        type: _remoteControl.TYPE.REQUEST,
        category: _remoteControl.CATEGORY.MONITOR,
        message: _remoteControl.BOOL.FALSE,
        reset: _remoteControl.BOOL.FALSE
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [request] host monitor info',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'requestSelectedMonitorInfo',
    value: function requestSelectedMonitorInfo(selectedMonitorIndex) {
      var data = {
        type: _remoteControl.TYPE.REQUEST,
        category: _remoteControl.CATEGORY.MONITOR,
        message: _remoteControl.BOOL.TRUE,
        index: selectedMonitorIndex
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [request] selected host monitor info',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'viewerStatus',
    value: function viewerStatus(status) {
      var data = {
        type: _remoteControl.TYPE.EVENT,
        category: _remoteControl.CATEGORY.STATUS,
        message: _remoteControl.STATUS_MESSAGE.VIEWER_STATUS,
        status: status
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [event] viewer status',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'activeChange',
    value: function activeChange(active) {
      var data = {
        type: _remoteControl.TYPE.EVENT,
        category: _remoteControl.CATEGORY.STATUS,
        message: _remoteControl.STATUS_MESSAGE.VIEWER_STATUS,
        status: _remoteControl.VIEWER_STATUS.ACTIVE_CHANGED,
        change: active ? _remoteControl.BOOL.TRUE : _remoteControl.BOOL.FALSE
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [event] active change',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'screenLock',
    value: function screenLock(lock) {
      var data = {
        type: _remoteControl.TYPE.EVENT,
        category: _remoteControl.CATEGORY.STATUS,
        message: _remoteControl.STATUS_MESSAGE.VIEWER_STATUS,
        status: _remoteControl.VIEWER_STATUS.SCREEN_LOCK,
        lock: lock ? _remoteControl.BOOL.TRUE : _remoteControl.BOOL.FALSE
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [event] screen lock',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'requestKmControl',
    value: function requestKmControl() {
      var data = {
        type: _remoteControl.TYPE.REQUEST,
        category: _remoteControl.CATEGORY.KM_CONTROL
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [request] mouse, keyboard control',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'startKmControl',
    value: function startKmControl() {
      var data = {
        type: _remoteControl.TYPE.START,
        category: _remoteControl.CATEGORY.KM_CONTROL
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [start] mouse, keyboard control',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'endKmControl',
    value: function endKmControl() {
      var data = {
        type: _remoteControl.TYPE.END,
        category: _remoteControl.CATEGORY.KM_CONTROL
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [end] mouse, keyboard control',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'startLaserPointer',
    value: function startLaserPointer() {
      var x_pos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
      var y_pos = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

      var data = {
        type: _remoteControl.TYPE.START,
        category: _remoteControl.CATEGORY.LASER_POINTER,
        message: _remoteControl.LASER_POINTER_MESSAGE.CIRCLE,
        x_pos: x_pos,
        y_pos: y_pos
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [start] laser pointer',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'endLaserPointer',
    value: function endLaserPointer() {
      var data = {
        type: _remoteControl.TYPE.END,
        category: _remoteControl.CATEGORY.LASER_POINTER
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [end] laser pointer',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'moveLaserPointer',
    value: function moveLaserPointer(x_pos, y_pos, message) {
      var data = {
        type: _remoteControl.TYPE.EVENT,
        category: _remoteControl.CATEGORY.LASER_POINTER,
        message: message ? _remoteControl.BOOL.TRUE : _remoteControl.BOOL.FALSE,
        x_pos: x_pos,
        y_pos: y_pos
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [event] move laser pointer',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'controlMouse',
    value: function controlMouse(type, x_pos, y_pos, xdata) {
      var data = {
        type: _remoteControl.TYPE.EVENT,
        category: _remoteControl.CATEGORY.MOUSE,
        message: type,
        x_pos: x_pos,
        y_pos: y_pos
      };

      xdata && (data.xdata = xdata);

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [event] control mouse',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'controlKeyboard',
    value: function controlKeyboard(type, keyCode) {
      var data = {
        type: _remoteControl.TYPE.EVENT,
        category: _remoteControl.CATEGORY.KEYBOARD,
        message: type,
        keycode: keyCode
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [event] control keyboard',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'sendSpecialKey',
    value: function sendSpecialKey(message) {
      var data = {
        type: _remoteControl.TYPE.EVENT,
        category: _remoteControl.CATEGORY.SPECIAL_KEY,
        message: message
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [event] send special key',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'startDraw',
    value: function startDraw() {
      var data = {
        type: _remoteControl.TYPE.START,
        category: _remoteControl.CATEGORY.DRAW
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [start] draw',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'endDraw',
    value: function endDraw() {
      var data = {
        type: _remoteControl.TYPE.END,
        category: _remoteControl.CATEGORY.DRAW
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [end] draw',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'drawInfo',
    value: function drawInfo() {
      var _ref3 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
          _ref3$type = _ref3.type,
          type = _ref3$type === undefined ? 0 : _ref3$type,
          _ref3$color = _ref3.color,
          color = _ref3$color === undefined ? 0 : _ref3$color,
          _ref3$thickness = _ref3.thickness,
          thickness = _ref3$thickness === undefined ? 3 : _ref3$thickness;

      var data = {
        type: _remoteControl.TYPE.EVENT,
        category: _remoteControl.CATEGORY.DRAW,
        message: _remoteControl.DRAW_MESSAGE.INFO,
        drawtype: type,
        color: color,
        thickness: thickness
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [event] draw info',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'drawPaint',
    value: function drawPaint(points) {
      var data = {
        type: _remoteControl.TYPE.EVENT,
        category: _remoteControl.CATEGORY.DRAW,
        message: _remoteControl.DRAW_MESSAGE.DATA,
        count: points.length,
        points: points
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [event] draw paint',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'clearPaint',
    value: function clearPaint() {
      var data = {
        type: _remoteControl.TYPE.EVENT,
        category: _remoteControl.CATEGORY.DRAW,
        message: _remoteControl.DRAW_MESSAGE.CLEAR
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [event] draw paint',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'startScreenKeyboard',
    value: function startScreenKeyboard() {
      var data = {
        type: _remoteControl.TYPE.START,
        category: _remoteControl.CATEGORY.SCREEN_KEYBOARD
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [start] screen keyboard',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'endScreenKeyboard',
    value: function endScreenKeyboard() {
      var data = {
        type: _remoteControl.TYPE.END,
        category: _remoteControl.CATEGORY.SCREEN_KEYBOARD
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [end] exit screen keyboard',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });
    }
  }, {
    key: 'exit',
    value: function exit() {
      var data = {
        type: _remoteControl.TYPE.END,
        category: _remoteControl.CATEGORY.CONNECTION
      };

      this.publish(this.hostTopic, {
        tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
        identifier_id: this.viewerId,
        data: data
      });

      this.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
        type: 'protocol',
        message: '[viewer to host] [end] exit remote control',
        data: {
          topic: {
            viewer: this.viewerTopic,
            host: this.hostTopic
          },
          protocol: {
            tag: _remoteControl.TAG.MESSAGE_PROTOCOL,
            identifier_id: this.viewerId,
            data: data
          }
        }
      });

      this.disconnect();
    }
  }]);

  return RemoteControl;
}(_Message3.default);

exports.default = RemoteControl;

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Message = function () {
  function Message(client) {
    _classCallCheck(this, Message);

    this._mqtt = client;
  }

  _createClass(Message, [{
    key: "hasTopic",
    value: function hasTopic(topic) {
      return this._mqtt.subscribed().includes(topic);
    }
  }, {
    key: "addSubscribe",
    value: function addSubscribe(topic) {
      this._mqtt.subscribe(topic);
    }
  }, {
    key: "removeSubscribe",
    value: function removeSubscribe(topic) {
      if (this.hasTopic(topic)) this._mqtt.unsubscribe(topic);
    }
  }, {
    key: "publish",
    value: function publish(topic, payload) {
      var qos = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 2;

      this._mqtt.publish(topic, payload, { qos: qos });
    }
  }, {
    key: "disconnect",
    value: function disconnect() {
      this._mqtt.disconnect();
    }
  }]);

  return Message;
}();

exports.default = Message;

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _eventEmitter = __webpack_require__(33);

var _eventEmitter2 = _interopRequireDefault(_eventEmitter);

var _screenControl = __webpack_require__(18);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var intToByteArray = function intToByteArray(val) {
  var byteArr = [0, 0, 0, 0];
  for (var i = 0; i < byteArr.length; i++) {
    var byteVal = val & 0xff;
    byteArr[i] = byteVal;
    val = (val - byteVal) / 256;
  }
  return byteArr;
};

var getJoinBinaryData = function getJoinBinaryData(accessCode, viewerId) {
  var joinProductCode = _screenControl.PRODUCT_CODE;
  var accessCodeSize = accessCode.length;
  var viewerIdSize = viewerId.length;
  var size = 4 + 4 + viewerIdSize + 4 + accessCodeSize; // product code + viewerIdSize + 4 + accessCodeSize
  var data = new Uint8Array(size);
  var dataPos = 0;

  data.set(intToByteArray(joinProductCode), dataPos);
  dataPos += 4;

  data.set(intToByteArray(viewerIdSize), dataPos);
  dataPos += 4;

  for (var i = 0; i < viewerIdSize; ++i, ++dataPos) {
    data[dataPos] += viewerId.charCodeAt(i);
  }

  data.set(intToByteArray(accessCodeSize), dataPos);
  dataPos += 4;

  for (var j = 0; j < accessCodeSize; ++j, ++dataPos) {
    data[dataPos] += accessCode.charCodeAt(j);
  }

  return data;
};

var getPingBinaryData = function getPingBinaryData() {
  var data = new Uint8Array(4);
  data.set(intToByteArray(_screenControl.PING), 0);
  return data;
};

var ScreenControl = function () {
  function ScreenControl(webSocket) {
    _classCallCheck(this, ScreenControl);

    this._isJoin = false;
    this._keepAliveInterval = null;
    this._emitter = new _eventEmitter2.default();
    this._webSocket = webSocket;
    this._webSocket.onmessage = this._onMessage.bind(this);
    this._webSocket.onclose = this._onClose.bind(this);
  }

  _createClass(ScreenControl, [{
    key: 'setBinaryType',
    value: function setBinaryType(type) {
      this._webSocket.binaryType = type;
    }
  }, {
    key: 'send',
    value: function send(data) {
      this._webSocket.send(data);
    }
  }, {
    key: 'on',
    value: function on(eventName, listener) {
      this._emitter.on(eventName, listener);
    }
  }, {
    key: 'join',
    value: function join(accessCode, viewerId) {
      var joinData = getJoinBinaryData(accessCode, viewerId);
      this.send(joinData);
    }
  }, {
    key: '_keepAlive',
    value: function _keepAlive() {
      var _this = this;

      if (!this._isJoin) return;

      var pingData = getPingBinaryData();
      this._keepAliveInterval = setInterval(function () {
        _this.send(pingData);
      }, 3000);
    }
  }, {
    key: '_onMessage',
    value: function _onMessage(event) {
      if (!this._isJoin) {
        var dv = new DataView(event.data);
        var code = dv.getInt32(0, true);

        if (code === _screenControl.LOGIN_OK) {
          this._isJoin = true;
          this._emitter.emit(_screenControl.SCREEN_EVENT.JOIN_SUCCESS);
          this._keepAlive();
          return;
        }

        throw new Error('relay server access fail');
      }

      this._emitter.emit(_screenControl.SCREEN_EVENT.SCREEN_DATA, event.data);
    }
  }, {
    key: '_onClose',
    value: function _onClose(event) {
      clearInterval(this._keepAliveInterval);
      this._emitter.emit(_screenControl.SCREEN_EVENT.ON_CLOSE, event);
    }
  }]);

  return ScreenControl;
}();

exports.default = ScreenControl;

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var d        = __webpack_require__(34)
  , callable = __webpack_require__(48)

  , apply = Function.prototype.apply, call = Function.prototype.call
  , create = Object.create, defineProperty = Object.defineProperty
  , defineProperties = Object.defineProperties
  , hasOwnProperty = Object.prototype.hasOwnProperty
  , descriptor = { configurable: true, enumerable: false, writable: true }

  , on, once, off, emit, methods, descriptors, base;

on = function (type, listener) {
	var data;

	callable(listener);

	if (!hasOwnProperty.call(this, '__ee__')) {
		data = descriptor.value = create(null);
		defineProperty(this, '__ee__', descriptor);
		descriptor.value = null;
	} else {
		data = this.__ee__;
	}
	if (!data[type]) data[type] = listener;
	else if (typeof data[type] === 'object') data[type].push(listener);
	else data[type] = [data[type], listener];

	return this;
};

once = function (type, listener) {
	var once, self;

	callable(listener);
	self = this;
	on.call(this, type, once = function () {
		off.call(self, type, once);
		apply.call(listener, this, arguments);
	});

	once.__eeOnceListener__ = listener;
	return this;
};

off = function (type, listener) {
	var data, listeners, candidate, i;

	callable(listener);

	if (!hasOwnProperty.call(this, '__ee__')) return this;
	data = this.__ee__;
	if (!data[type]) return this;
	listeners = data[type];

	if (typeof listeners === 'object') {
		for (i = 0; (candidate = listeners[i]); ++i) {
			if ((candidate === listener) ||
					(candidate.__eeOnceListener__ === listener)) {
				if (listeners.length === 2) data[type] = listeners[i ? 0 : 1];
				else listeners.splice(i, 1);
			}
		}
	} else {
		if ((listeners === listener) ||
				(listeners.__eeOnceListener__ === listener)) {
			delete data[type];
		}
	}

	return this;
};

emit = function (type) {
	var i, l, listener, listeners, args;

	if (!hasOwnProperty.call(this, '__ee__')) return;
	listeners = this.__ee__[type];
	if (!listeners) return;

	if (typeof listeners === 'object') {
		l = arguments.length;
		args = new Array(l - 1);
		for (i = 1; i < l; ++i) args[i - 1] = arguments[i];

		listeners = listeners.slice();
		for (i = 0; (listener = listeners[i]); ++i) {
			apply.call(listener, this, args);
		}
	} else {
		switch (arguments.length) {
		case 1:
			call.call(listeners, this);
			break;
		case 2:
			call.call(listeners, this, arguments[1]);
			break;
		case 3:
			call.call(listeners, this, arguments[1], arguments[2]);
			break;
		default:
			l = arguments.length;
			args = new Array(l - 1);
			for (i = 1; i < l; ++i) {
				args[i - 1] = arguments[i];
			}
			apply.call(listeners, this, args);
		}
	}
};

methods = {
	on: on,
	once: once,
	off: off,
	emit: emit
};

descriptors = {
	on: d(on),
	once: d(once),
	off: d(off),
	emit: d(emit)
};

base = defineProperties({}, descriptors);

module.exports = exports = function (o) {
	return (o == null) ? create(base) : defineProperties(Object(o), descriptors);
};
exports.methods = methods;


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var assign        = __webpack_require__(35)
  , normalizeOpts = __webpack_require__(43)
  , isCallable    = __webpack_require__(44)
  , contains      = __webpack_require__(45)

  , d;

d = module.exports = function (dscr, value/*, options*/) {
	var c, e, w, options, desc;
	if ((arguments.length < 2) || (typeof dscr !== 'string')) {
		options = value;
		value = dscr;
		dscr = null;
	} else {
		options = arguments[2];
	}
	if (dscr == null) {
		c = w = true;
		e = false;
	} else {
		c = contains.call(dscr, 'c');
		e = contains.call(dscr, 'e');
		w = contains.call(dscr, 'w');
	}

	desc = { value: value, configurable: c, enumerable: e, writable: w };
	return !options ? desc : assign(normalizeOpts(options), desc);
};

d.gs = function (dscr, get, set/*, options*/) {
	var c, e, options, desc;
	if (typeof dscr !== 'string') {
		options = set;
		set = get;
		get = dscr;
		dscr = null;
	} else {
		options = arguments[3];
	}
	if (get == null) {
		get = undefined;
	} else if (!isCallable(get)) {
		options = get;
		get = set = undefined;
	} else if (set == null) {
		set = undefined;
	} else if (!isCallable(set)) {
		options = set;
		set = undefined;
	}
	if (dscr == null) {
		c = true;
		e = false;
	} else {
		c = contains.call(dscr, 'c');
		e = contains.call(dscr, 'e');
	}

	desc = { get: get, set: set, configurable: c, enumerable: e };
	return !options ? desc : assign(normalizeOpts(options), desc);
};


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = __webpack_require__(36)()
	? Object.assign
	: __webpack_require__(37);


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function () {
	var assign = Object.assign, obj;
	if (typeof assign !== "function") return false;
	obj = { foo: "raz" };
	assign(obj, { bar: "dwa" }, { trzy: "trzy" });
	return (obj.foo + obj.bar + obj.trzy) === "razdwatrzy";
};


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var keys  = __webpack_require__(38)
  , value = __webpack_require__(42)
  , max   = Math.max;

module.exports = function (dest, src /*, …srcn*/) {
	var error, i, length = max(arguments.length, 2), assign;
	dest = Object(value(dest));
	assign = function (key) {
		try {
			dest[key] = src[key];
		} catch (e) {
			if (!error) error = e;
		}
	};
	for (i = 1; i < length; ++i) {
		src = arguments[i];
		keys(src).forEach(assign);
	}
	if (error !== undefined) throw error;
	return dest;
};


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = __webpack_require__(39)()
	? Object.keys
	: __webpack_require__(40);


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function () {
	try {
		Object.keys("primitive");
		return true;
	} catch (e) {
 return false;
}
};


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isValue = __webpack_require__(17);

var keys = Object.keys;

module.exports = function (object) {
	return keys(isValue(object) ? Object(object) : object);
};


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// eslint-disable-next-line no-empty-function
module.exports = function () {};


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isValue = __webpack_require__(17);

module.exports = function (value) {
	if (!isValue(value)) throw new TypeError("Cannot use null or undefined");
	return value;
};


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isValue = __webpack_require__(17);

var forEach = Array.prototype.forEach, create = Object.create;

var process = function (src, obj) {
	var key;
	for (key in src) obj[key] = src[key];
};

// eslint-disable-next-line no-unused-vars
module.exports = function (opts1 /*, …options*/) {
	var result = create(null);
	forEach.call(arguments, function (options) {
		if (!isValue(options)) return;
		process(Object(options), result);
	});
	return result;
};


/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Deprecated



module.exports = function (obj) {
 return typeof obj === "function";
};


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = __webpack_require__(46)()
	? String.prototype.contains
	: __webpack_require__(47);


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var str = "razdwatrzy";

module.exports = function () {
	if (typeof str.contains !== "function") return false;
	return (str.contains("dwa") === true) && (str.contains("foo") === false);
};


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var indexOf = String.prototype.indexOf;

module.exports = function (searchString/*, position*/) {
	return indexOf.call(this, searchString, arguments[1]) > -1;
};


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (fn) {
	if (typeof fn !== "function") throw new TypeError(fn + " is not a function");
	return fn;
};


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _webviewer = __webpack_require__(5);

var _webviewer2 = _interopRequireDefault(_webviewer);

var _i18n = __webpack_require__(51);

var _i18n2 = _interopRequireDefault(_i18n);

var _utils = __webpack_require__(3);

var _validation = __webpack_require__(19);

var _webviewer3 = __webpack_require__(6);

var WEBVIEWER = _interopRequireWildcard(_webviewer3);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var withInitialize = function withInitialize() {
  return function (WrappedComponent) {
    return function (_Component) {
      _inherits(Wrapper, _Component);

      function Wrapper() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, Wrapper);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Wrapper.__proto__ || Object.getPrototypeOf(Wrapper)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
          i18n: null
        }, _temp), _possibleConstructorReturn(_this, _ret);
      }

      _createClass(Wrapper, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
          var _props = this.props,
              locale = _props.locale,
              connectInfo = _props.connectInfo,
              onReceivedEvent = _props.onReceivedEvent;


          window.MediaSource = window.MediaSource || window.WebKitMediaSource;

          if (!(0, _utils.isAvailableBrowser)()) {
            var message = 'This browser not support. (chrome, safari only)';
            onReceivedEvent(WEBVIEWER.VIEWER_EVENT.ERROR, { type: 'not support browser', message: message });
            return;
          }

          if (!window.MediaSource) {
            var _message = 'MediaSource API is not available';
            onReceivedEvent(WEBVIEWER.VIEWER_EVENT.ERROR, { type: 'not support browser', message: _message });
            return;
          }

          if (!window.WebSocket) {
            var _message2 = 'This browser does not support webSocket';
            onReceivedEvent(WEBVIEWER.VIEWER_EVENT.ERROR, { type: 'not support browser', message: _message2 });
            return;
          }

          if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
            var _message3 = 'This device(iOS) not support.';
            onReceivedEvent(WEBVIEWER.VIEWER_EVENT.ERROR, { type: 'not support browser', message: _message3 });
            return;
          }

          try {
            (0, _validation.ConnectInfoStruct)(connectInfo);
          } catch (error) {
            throw _extends({}, error);
          }

          _webviewer2.default.set('locale', locale);

          this.setState({
            i18n: _i18n2.default.setLocale(locale)
          });

          if (_webviewer2.default.get('accessCode') !== connectInfo.accessCode) {
            _webviewer2.default.set('accessCode', connectInfo.accessCode);
            _webviewer2.default.remove('activatedControlApps');
            _webviewer2.default.remove('pushServer');
            _webviewer2.default.remove('relayServer');
          }
        }
      }, {
        key: 'render',
        value: function render() {
          var i18n = this.state.i18n;


          return i18n && _react2.default.createElement(WrappedComponent, _extends({}, this.props, { i18n: i18n }));
        }
      }]);

      return Wrapper;
    }(_react.Component);
  };
};

exports.default = withInitialize;

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = '1.1.0.0';

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var I18n = function () {
  function I18n(locale, messages) {
    _classCallCheck(this, I18n);

    this._locale = locale || 'en';
    this._locales = [];
    this._messages = null;
    if (messages) this.addMessages(messages);
  }

  _createClass(I18n, [{
    key: 'addMessage',
    value: function addMessage(obj) {
      this.languageResource.push(obj.key ? obj : Object.assign({ key: this.key++ }, obj));
      return this;
    }
  }, {
    key: 'addMessages',
    value: function addMessages(messages) {
      this._messages = _extends({}, messages);
      this._locales = Object.keys(messages);
      return this;
    }
  }, {
    key: 'setLocale',
    value: function setLocale(locale) {
      if (!this._locales.includes(locale)) locale = 'en';
      this._locale = locale;
      return this;
    }
  }, {
    key: 'message',
    value: function message(key) {
      var values = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      var message = this._messages[this._locale][key];
      if (!message) return key;

      for (var _key in values) {
        if (!values.hasOwnProperty(_key)) continue;
        message = message.replace(new RegExp('\\{\\s*' + _key + '\\s*\\}', 'g'), values[_key]);
      }

      return message;
    }
  }, {
    key: 'all',
    value: function all() {
      return _extends({}, this.messages[this._locale]);
    }
  }]);

  return I18n;
}();

var i18n = new I18n(['ko', 'ja', 'en']);

i18n.addMessages({
  ko: {
    mouseAndKeyboard: '마우스 / 키보드 제어',
    laserPointer: '레이저 포인터',
    functionKey: '기능키',
    shortKey: '단축키',
    screenKeyboard: '화상 키보드',
    draw: '그리기',
    allClear: '전체 지우기',
    screenRatio: '화면 비율 조정',
    fullScreen: '전체화면',
    changeMonitor: '모니터 변경',
    screenCapture: '화면캡쳐',
    exit: '종료하기',
    fold: '접기',
    expand: '펼치기',
    notGetScreen: '원격지의 화면을 가져올 수 없습니다.',
    checkRemoteStatus: '원격지의 상태를 확인해주세요.',
    hostPause: '상대방이 화면을 일시정지 하였습니다.',
    confirmExitRemoteControl: '원격제어를 종료하시겠습니까?',
    yes: '예',
    no: '아니오',
    notSupportBrowser: '지원하지 않는 브라우저 입니다.',
    timeLeft: '남은 시간',
    min: '분',
    sec: '초',
    serverConnectFail: '서버와 연결에 실패하였습니다.',
    notSupportedOS: '지원하지 않는 OS 입니다.'
  },
  en: {
    mouseAndKeyboard: 'Mouse / keyboard control',
    laserPointer: 'Laser pointer',
    functionKey: 'Function key',
    shortKey: 'Shortcut key',
    screenKeyboard: 'Virtual keyboard',
    draw: 'Draw',
    allClear: 'Erase all',
    screenRatio: 'Fit to screen',
    fullScreen: 'Full screen',
    changeMonitor: 'Switch display',
    screenCapture: 'Capture screen',
    exit: 'End',
    fold: 'open',
    expand: 'close',
    notGetScreen: 'Remote system\',s screen could not be retrieved.',
    checkRemoteStatus: 'Please, verify the remote system\',s status.',
    hostPause: 'Customer has paused the screen.',
    confirmExitRemoteControl: 'Do you want to end the remote control?',
    yes: 'Yes',
    no: 'No',
    notSupportBrowser: 'Browser not supported.',
    timeLeft: 'Time left',
    min: 'Min',
    sec: 'Sec',
    serverConnectFail: 'Failed to connect with the server.',
    notSupportedOS: 'This OS is not supported.'
  },
  ja: {
    mouseAndKeyboard: 'マウス/キーボード制御',
    laserPointer: 'レーザーポインタ',
    functionKey: '機能キー',
    shortKey: 'ショートカットキー',
    screenKeyboard: '仮想キーボード',
    draw: '描画',
    allClear: '全体消去',
    screenRatio: '画面自動調整 ',
    fullScreen: 'フルスクリーン',
    changeMonitor: 'モニター変更',
    screenCapture: '画面キャプチャ',
    exit: '終了する',
    fold: '閉じる',
    expand: '開く',
    notGetScreen: 'お客様画面が取得できません。',
    checkRemoteStatus: 'お客様の状態を確認してください。',
    hostPause: 'お客様が画面を一時停止しました。',
    confirmExitRemoteControl: 'リモートコントロールを終了しますか？',
    yes: 'はい',
    no: 'いいえ',
    notSupportBrowser: 'このブラウザは未対応です。',
    timeLeft: '残り時間',
    min: '分',
    sec: '秒',
    serverConnectFail: 'サーバへの接続に失敗しました。',
    notSupportedOS: '対応していないOSです。'
  }
});

exports.default = i18n;

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Define a struct error.
 *
 * @type {StructError}
 */

var StructError = function (_TypeError) {
  _inherits(StructError, _TypeError);

  _createClass(StructError, null, [{
    key: 'format',
    value: function format(attrs) {
      var type = attrs.type,
          path = attrs.path,
          value = attrs.value;

      var message = 'Expected a value of type `' + type + '`' + (path.length ? ' for `' + path.join('.') + '`' : '') + ' but received `' + JSON.stringify(value) + '`.';
      return message;
    }
  }]);

  function StructError(attrs) {
    _classCallCheck(this, StructError);

    var message = StructError.format(attrs);

    var _this = _possibleConstructorReturn(this, (StructError.__proto__ || Object.getPrototypeOf(StructError)).call(this, message));

    var data = attrs.data,
        path = attrs.path,
        value = attrs.value,
        reason = attrs.reason,
        type = attrs.type,
        _attrs$errors = attrs.errors,
        errors = _attrs$errors === undefined ? [] : _attrs$errors;

    _this.data = data;
    _this.path = path;
    _this.value = value;
    _this.reason = reason;
    _this.type = type;
    _this.errors = errors;

    if (!errors.length) {
      errors.push(_this);
    }

    if (Error.captureStackTrace) {
      Error.captureStackTrace(_this, _this.constructor);
    } else {
      _this.stack = new Error().stack;
    }
    return _this;
  }

  return StructError;
}(TypeError);

var toString = Object.prototype.toString;

var kindOf = function kindOf(val) {
  if (val === void 0) return 'undefined';
  if (val === null) return 'null';

  var type = typeof val === 'undefined' ? 'undefined' : _typeof(val);
  if (type === 'boolean') return 'boolean';
  if (type === 'string') return 'string';
  if (type === 'number') return 'number';
  if (type === 'symbol') return 'symbol';
  if (type === 'function') {
    return isGeneratorFn(val) ? 'generatorfunction' : 'function';
  }

  if (isArray(val)) return 'array';
  if (isBuffer(val)) return 'buffer';
  if (isArguments(val)) return 'arguments';
  if (isDate(val)) return 'date';
  if (isError(val)) return 'error';
  if (isRegexp(val)) return 'regexp';

  switch (ctorName(val)) {
    case 'Symbol':
      return 'symbol';
    case 'Promise':
      return 'promise';

    // Set, Map, WeakSet, WeakMap
    case 'WeakMap':
      return 'weakmap';
    case 'WeakSet':
      return 'weakset';
    case 'Map':
      return 'map';
    case 'Set':
      return 'set';

    // 8-bit typed arrays
    case 'Int8Array':
      return 'int8array';
    case 'Uint8Array':
      return 'uint8array';
    case 'Uint8ClampedArray':
      return 'uint8clampedarray';

    // 16-bit typed arrays
    case 'Int16Array':
      return 'int16array';
    case 'Uint16Array':
      return 'uint16array';

    // 32-bit typed arrays
    case 'Int32Array':
      return 'int32array';
    case 'Uint32Array':
      return 'uint32array';
    case 'Float32Array':
      return 'float32array';
    case 'Float64Array':
      return 'float64array';
  }

  if (isGeneratorObj(val)) {
    return 'generator';
  }

  // Non-plain objects
  type = toString.call(val);
  switch (type) {
    case '[object Object]':
      return 'object';
    // iterators
    case '[object Map Iterator]':
      return 'mapiterator';
    case '[object Set Iterator]':
      return 'setiterator';
    case '[object String Iterator]':
      return 'stringiterator';
    case '[object Array Iterator]':
      return 'arrayiterator';
  }

  // other
  return type.slice(8, -1).toLowerCase().replace(/\s/g, '');
};

function ctorName(val) {
  return val.constructor ? val.constructor.name : null;
}

function isArray(val) {
  if (Array.isArray) return Array.isArray(val);
  return val instanceof Array;
}

function isError(val) {
  return val instanceof Error || typeof val.message === 'string' && val.constructor && typeof val.constructor.stackTraceLimit === 'number';
}

function isDate(val) {
  if (val instanceof Date) return true;
  return typeof val.toDateString === 'function' && typeof val.getDate === 'function' && typeof val.setDate === 'function';
}

function isRegexp(val) {
  if (val instanceof RegExp) return true;
  return typeof val.flags === 'string' && typeof val.ignoreCase === 'boolean' && typeof val.multiline === 'boolean' && typeof val.global === 'boolean';
}

function isGeneratorFn(name, val) {
  return ctorName(name) === 'GeneratorFunction';
}

function isGeneratorObj(val) {
  return typeof val.throw === 'function' && typeof val.return === 'function' && typeof val.next === 'function';
}

function isArguments(val) {
  try {
    if (typeof val.length === 'number' && typeof val.callee === 'function') {
      return true;
    }
  } catch (err) {
    if (err.message.indexOf('callee') !== -1) {
      return true;
    }
  }
  return false;
}

/**
 * If you need to support Safari 5-7 (8-10 yr-old browser),
 * take a look at https://github.com/feross/is-buffer
 */

function isBuffer(val) {
  if (val.constructor && typeof val.constructor.isBuffer === 'function') {
    return val.constructor.isBuffer(val);
  }
  return false;
}

/**
 * A private string to identify structs by.
 *
 * @type {String}
 */

var IS_STRUCT = '@@__STRUCT__@@';

/**
 * A private string to refer to a struct's kind.
 *
 * @type {String}
 */

var KIND = '@@__KIND__@@';

/**
 * Check if a `value` is a struct.
 *
 * @param {Any} value
 * @return {Boolean}
 */

function isStruct(value) {
  return !!(value && value[IS_STRUCT]);
}

/**
 * Resolve `defaults`, for an optional `value`.
 *
 * @param {Function|Any} defaults
 * @param {Any} value
 * @return {Any}
 */

function resolveDefaults(defaults, value) {
  return typeof defaults === 'function' ? defaults(value) : defaults;
}

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

/**
 * Kind.
 *
 * @type {Kind}
 */

var Kind = function Kind(name, type, validate) {
  _classCallCheck(this, Kind);

  this.name = name;
  this.type = type;
  this.validate = validate;
};

/**
 * Any.
 *
 * @param {Array|Function|Object|String} schema
 * @param {Any} defaults
 * @param {Object} options
 */

function any(schema, defaults$$1, options) {
  if (isStruct(schema)) {
    return schema[KIND];
  }

  if (schema instanceof Kind) {
    return schema;
  }

  switch (kindOf(schema)) {
    case 'array':
      {
        return schema.length > 1 ? tuple(schema, defaults$$1, options) : list(schema, defaults$$1, options);
      }

    case 'function':
      {
        return func(schema, defaults$$1, options);
      }

    case 'object':
      {
        return object(schema, defaults$$1, options);
      }

    case 'string':
      {
        var required = true;
        var type = void 0;

        if (schema.endsWith('?')) {
          required = false;
          schema = schema.slice(0, -1);
        }

        if (schema.includes('|')) {
          var scalars = schema.split(/\s*\|\s*/g);
          type = union(scalars, defaults$$1, options);
        } else if (schema.includes('&')) {
          var _scalars = schema.split(/\s*&\s*/g);
          type = intersection(_scalars, defaults$$1, options);
        } else {
          type = scalar(schema, defaults$$1, options);
        }

        if (!required) {
          type = optional(type, undefined, options);
        }

        return type;
      }
  }

  if (process.env.NODE_ENV !== 'production') {
    throw new Error('A schema definition must be an object, array, string or function, but you passed: ' + schema);
  } else {
    throw new Error('Invalid schema: ' + schema);
  }
}

/**
 * Dict.
 *
 * @param {Array} schema
 * @param {Object} defaults
 * @param {Object} options
 */

function dict(schema, defaults$$1, options) {
  if (kindOf(schema) !== 'array' || schema.length !== 2) {
    if (process.env.NODE_ENV !== 'production') {
      throw new Error('Dict structs must be defined as an array with two elements, but you passed: ' + schema);
    } else {
      throw new Error('Invalid schema: ' + schema);
    }
  }

  var obj = scalar('object', undefined, options);
  var keys = any(schema[0], undefined, options);
  var values = any(schema[1], undefined, options);
  var name = 'dict';
  var type = 'dict<' + keys.type + ',' + values.type + '>';
  var validate = function validate(value) {
    var resolved = resolveDefaults(defaults$$1);
    value = resolved ? _extends({}, resolved, value) : value;

    var _obj$validate = obj.validate(value),
        _obj$validate2 = _slicedToArray(_obj$validate, 1),
        error = _obj$validate2[0];

    if (error) {
      error.type = type;
      return [error];
    }

    var ret = {};
    var errors = [];

    for (var k in value) {
      var v = value[k];

      var _keys$validate = keys.validate(k),
          _keys$validate2 = _slicedToArray(_keys$validate, 2),
          e = _keys$validate2[0],
          r = _keys$validate2[1];

      if (e) {
        e.path = [k].concat(e.path);
        e.data = value;
        errors.push(e);
        continue;
      }

      k = r;

      var _values$validate = values.validate(v),
          _values$validate2 = _slicedToArray(_values$validate, 2),
          e2 = _values$validate2[0],
          r2 = _values$validate2[1];

      if (e2) {
        e2.path = [k].concat(e2.path);
        e2.data = value;
        errors.push(e2);
        continue;
      }

      ret[k] = r2;
    }

    if (errors.length) {
      var first = errors[0];
      first.errors = errors;
      return [first];
    }

    return [undefined, ret];
  };

  return new Kind(name, type, validate);
}

/**
 * Enum.
 *
 * @param {Array} schema
 * @param {Any} defaults
 * @param {Object} options
 */

function en(schema, defaults$$1, options) {
  if (kindOf(schema) !== 'array') {
    if (process.env.NODE_ENV !== 'production') {
      throw new Error('Enum structs must be defined as an array, but you passed: ' + schema);
    } else {
      throw new Error('Invalid schema: ' + schema);
    }
  }

  var name = 'enum';
  var type = schema.map(function (s) {
    try {
      return JSON.stringify(s);
    } catch (e) {
      return String(s);
    }
  }).join(' | ');

  var validate = function validate() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : resolveDefaults(defaults$$1);

    return schema.includes(value) ? [undefined, value] : [{ data: value, path: [], value: value, type: type }];
  };

  return new Kind(name, type, validate);
}

/**
 * Enums.
 *
 * @param {Array} schema
 * @param {Any} defaults
 * @param {Object} options
 */

function enums(schema, defaults$$1, options) {
  var e = en(schema, undefined, options);
  var l = list([e], defaults$$1, options);
  return l;
}

/**
 * Function.
 *
 * @param {Function} schema
 * @param {Any} defaults
 * @param {Object} options
 */

function func(schema, defaults$$1, options) {
  if (kindOf(schema) !== 'function') {
    if (process.env.NODE_ENV !== 'production') {
      throw new Error('Function structs must be defined as a function, but you passed: ' + schema);
    } else {
      throw new Error('Invalid schema: ' + schema);
    }
  }

  var name = 'function';
  var type = '<function>';
  var validate = function validate() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : resolveDefaults(defaults$$1);
    var data = arguments[1];

    var result = schema(value, data);
    var failure = { path: [], reason: null };
    var isValid = void 0;

    switch (kindOf(result)) {
      case 'boolean':
        {
          isValid = result;
          break;
        }
      case 'string':
        {
          isValid = false;
          failure.reason = result;
          break;
        }
      case 'object':
        {
          isValid = false;
          failure = _extends({}, failure, result);
          break;
        }
      default:
        {
          if (process.env.NODE_ENV !== 'production') {
            throw new Error('Validator functions must return a boolean, an error reason string or an error reason object, but you passed: ' + schema);
          } else {
            throw new Error('Invalid result: ' + result);
          }
        }
    }

    return isValid ? [undefined, value] : [_extends({ type: type, value: value, data: value }, failure)];
  };

  return new Kind(name, type, validate);
}

/**
 * Instance.
 *
 * @param {Array} schema
 * @param {Any} defaults
 * @param {Object} options
 */

function instance(schema, defaults$$1, options) {
  var name = 'instance';
  var type = 'instance<' + schema.name + '>';
  var validate = function validate() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : resolveDefaults(defaults$$1);

    return value instanceof schema ? [undefined, value] : [{ data: value, path: [], value: value, type: type }];
  };

  return new Kind(name, type, validate);
}

/**
 * Interface.
 *
 * @param {Object} schema
 * @param {Object} defaults
 * @param {Object} options
 */

function inter(schema, defaults$$1, options) {
  if (kindOf(schema) !== 'object') {
    if (process.env.NODE_ENV !== 'production') {
      throw new Error('Interface structs must be defined as an object, but you passed: ' + schema);
    } else {
      throw new Error('Invalid schema: ' + schema);
    }
  }

  var ks = [];
  var properties = {};

  for (var key in schema) {
    ks.push(key);
    var s = schema[key];
    var kind = any(s, undefined, options);
    properties[key] = kind;
  }

  var name = 'interface';
  var type = '{' + ks.join() + '}';
  var validate = function validate(value) {
    var resolved = resolveDefaults(defaults$$1);
    value = resolved ? _extends({}, resolved, value) : value;
    var errors = [];
    var ret = value;

    for (var _key in properties) {
      var v = value[_key];
      var _kind = properties[_key];

      if (v === undefined) {
        var d = defaults$$1 && defaults$$1[_key];
        v = resolveDefaults(d, value);
      }

      var _kind$validate = _kind.validate(v, value),
          _kind$validate2 = _slicedToArray(_kind$validate, 2),
          e = _kind$validate2[0],
          r = _kind$validate2[1];

      if (e) {
        e.path = [_key].concat(e.path);
        e.data = value;
        errors.push(e);
        continue;
      }

      if (_key in value || r !== undefined) {
        ret[_key] = r;
      }
    }

    if (errors.length) {
      var first = errors[0];
      first.errors = errors;
      return [first];
    }

    return [undefined, ret];
  };

  return new Kind(name, type, validate);
}

/**
 * Lazy.
 *
 * @param {Function} schema
 * @param {Any} defaults
 * @param {Object} options
 */

function lazy(schema, defaults$$1, options) {
  if (kindOf(schema) !== 'function') {
    if (process.env.NODE_ENV !== 'production') {
      throw new Error('Lazy structs must be defined as an function that returns a schema, but you passed: ' + schema);
    } else {
      throw new Error('Invalid schema: ' + schema);
    }
  }

  var kind = void 0;
  var struct = void 0;
  var name = 'lazy';
  var type = 'lazy...';
  var compile = function compile(value) {
    struct = schema();
    kind.name = struct.kind;
    kind.type = struct.type;
    kind.validate = struct.validate;
    return kind.validate(value);
  };

  kind = new Kind(name, type, compile);
  return kind;
}

/**
 * List.
 *
 * @param {Array} schema
 * @param {Array} defaults
 * @param {Object} options
 */

function list(schema, defaults$$1, options) {
  if (kindOf(schema) !== 'array' || schema.length !== 1) {
    if (process.env.NODE_ENV !== 'production') {
      throw new Error('List structs must be defined as an array with a single element, but you passed: ' + schema);
    } else {
      throw new Error('Invalid schema: ' + schema);
    }
  }

  var array = scalar('array', undefined, options);
  var element = any(schema[0], undefined, options);
  var name = 'list';
  var type = '[' + element.type + ']';
  var validate = function validate() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : resolveDefaults(defaults$$1);

    var _array$validate = array.validate(value),
        _array$validate2 = _slicedToArray(_array$validate, 2),
        error = _array$validate2[0],
        result = _array$validate2[1];

    if (error) {
      error.type = type;
      return [error];
    }

    value = result;
    var errors = [];
    var ret = [];

    for (var i = 0; i < value.length; i++) {
      var v = value[i];

      var _element$validate = element.validate(v),
          _element$validate2 = _slicedToArray(_element$validate, 2),
          e = _element$validate2[0],
          r = _element$validate2[1];

      if (e) {
        e.path = [i].concat(e.path);
        e.data = value;
        errors.push(e);
        continue;
      }

      ret[i] = r;
    }

    if (errors.length) {
      var first = errors[0];
      first.errors = errors;
      return [first];
    }

    return [undefined, ret];
  };

  return new Kind(name, type, validate);
}

/**
 * Literal.
 *
 * @param {Array} schema
 * @param {Any} defaults
 * @param {Object} options
 */

function literal(schema, defaults$$1, options) {
  var name = 'literal';
  var type = 'literal: ' + JSON.stringify(schema);
  var validate = function validate() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : resolveDefaults(defaults$$1);

    return value === schema ? [undefined, value] : [{ data: value, path: [], value: value, type: type }];
  };

  return new Kind(name, type, validate);
}

/**
 * Object.
 *
 * @param {Object} schema
 * @param {Object} defaults
 * @param {Object} options
 */

function object(schema, defaults$$1, options) {
  if (kindOf(schema) !== 'object') {
    if (process.env.NODE_ENV !== 'production') {
      throw new Error('Object structs must be defined as an object, but you passed: ' + schema);
    } else {
      throw new Error('Invalid schema: ' + schema);
    }
  }

  var obj = scalar('object', undefined, options);
  var ks = [];
  var properties = {};

  for (var key in schema) {
    ks.push(key);
    var s = schema[key];
    var kind = any(s, undefined, options);
    properties[key] = kind;
  }

  var name = 'object';
  var type = '{' + ks.join() + '}';
  var validate = function validate() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : resolveDefaults(defaults$$1);

    var _obj$validate3 = obj.validate(value),
        _obj$validate4 = _slicedToArray(_obj$validate3, 1),
        error = _obj$validate4[0];

    if (error) {
      error.type = type;
      return [error];
    }

    var errors = [];
    var ret = {};
    var valueKeys = Object.keys(value);
    var propertiesKeys = Object.keys(properties);
    var keys = new Set(valueKeys.concat(propertiesKeys));

    keys.forEach(function (key) {
      var v = value[key];
      var kind = properties[key];

      if (v === undefined) {
        var d = defaults$$1 && defaults$$1[key];
        v = resolveDefaults(d, value);
      }

      if (!kind) {
        var _e = { data: value, path: [key], value: v };
        errors.push(_e);
        return;
      }

      var _kind$validate3 = kind.validate(v, value),
          _kind$validate4 = _slicedToArray(_kind$validate3, 2),
          e = _kind$validate4[0],
          r = _kind$validate4[1];

      if (e) {
        e.path = [key].concat(e.path);
        e.data = value;
        errors.push(e);
        return;
      }

      if (key in value || r !== undefined) {
        ret[key] = r;
      }
    });

    if (errors.length) {
      var first = errors[0];
      first.errors = errors;
      return [first];
    }

    return [undefined, ret];
  };

  return new Kind(name, type, validate);
}

/**
 * Optional.
 *
 * @param {Any} schema
 * @param {Any} defaults
 * @param {Object} options
 */

function optional(schema, defaults$$1, options) {
  return union([schema, 'undefined'], defaults$$1, options);
}

/**
 * Partial.
 *
 * @param {Object} schema
 * @param {Object} defaults
 * @param {Object} options
 */

function partial(schema, defaults$$1, options) {
  if (kindOf(schema) !== 'object') {
    if (process.env.NODE_ENV !== 'production') {
      throw new Error('Partial structs must be defined as an object, but you passed: ' + schema);
    } else {
      throw new Error('Invalid schema: ' + schema);
    }
  }

  var obj = scalar('object', undefined, options);
  var ks = [];
  var properties = {};

  for (var key in schema) {
    ks.push(key);
    var s = schema[key];
    var kind = any(s, undefined, options);
    properties[key] = kind;
  }

  var name = 'partial';
  var type = '{' + ks.join() + ',...}';
  var validate = function validate() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : resolveDefaults(defaults$$1);

    var _obj$validate5 = obj.validate(value),
        _obj$validate6 = _slicedToArray(_obj$validate5, 1),
        error = _obj$validate6[0];

    if (error) {
      error.type = type;
      return [error];
    }

    var errors = [];
    var ret = {};

    for (var _key2 in properties) {
      var v = value[_key2];
      var _kind2 = properties[_key2];

      if (v === undefined) {
        var d = defaults$$1 && defaults$$1[_key2];
        v = resolveDefaults(d, value);
      }

      var _kind2$validate = _kind2.validate(v, value),
          _kind2$validate2 = _slicedToArray(_kind2$validate, 2),
          e = _kind2$validate2[0],
          r = _kind2$validate2[1];

      if (e) {
        e.path = [_key2].concat(e.path);
        e.data = value;
        errors.push(e);
        continue;
      }

      if (_key2 in value || r !== undefined) {
        ret[_key2] = r;
      }
    }

    if (errors.length) {
      var first = errors[0];
      first.errors = errors;
      return [first];
    }

    return [undefined, ret];
  };

  return new Kind(name, type, validate);
}

/**
 * Scalar.
 *
 * @param {String} schema
 * @param {Any} defaults
 * @param {Object} options
 */

function scalar(schema, defaults$$1, options) {
  if (kindOf(schema) !== 'string') {
    if (process.env.NODE_ENV !== 'production') {
      throw new Error('Scalar structs must be defined as a string, but you passed: ' + schema);
    } else {
      throw new Error('Invalid schema: ' + schema);
    }
  }

  var types = options.types;

  var fn = types[schema];

  if (kindOf(fn) !== 'function') {
    if (process.env.NODE_ENV !== 'production') {
      throw new Error('No struct validator function found for type "' + schema + '".');
    } else {
      throw new Error('Invalid type: ' + schema);
    }
  }

  var kind = func(fn, defaults$$1, options);
  var name = 'scalar';
  var type = schema;
  var validate = function validate(value) {
    var _kind$validate5 = kind.validate(value),
        _kind$validate6 = _slicedToArray(_kind$validate5, 2),
        error = _kind$validate6[0],
        result = _kind$validate6[1];

    if (error) {
      error.type = type;
      return [error];
    }

    return [undefined, result];
  };

  return new Kind(name, type, validate);
}

/**
 * Tuple.
 *
 * @param {Array} schema
 * @param {Array} defaults
 * @param {Object} options
 */

function tuple(schema, defaults$$1, options) {
  if (kindOf(schema) !== 'array') {
    if (process.env.NODE_ENV !== 'production') {
      throw new Error('Tuple structs must be defined as an array, but you passed: ' + schema);
    } else {
      throw new Error('Invalid schema: ' + schema);
    }
  }

  var kinds = schema.map(function (s) {
    return any(s, undefined, options);
  });
  var array = scalar('array', undefined, options);
  var name = 'tuple';
  var type = '[' + kinds.map(function (k) {
    return k.type;
  }).join() + ']';
  var validate = function validate() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : resolveDefaults(defaults$$1);

    var _array$validate3 = array.validate(value),
        _array$validate4 = _slicedToArray(_array$validate3, 1),
        error = _array$validate4[0];

    if (error) {
      error.type = type;
      return [error];
    }

    var ret = [];
    var errors = [];
    var length = Math.max(value.length, kinds.length);

    for (var i = 0; i < length; i++) {
      var kind = kinds[i];
      var v = value[i];

      if (!kind) {
        var _e2 = { data: value, path: [i], value: v };
        errors.push(_e2);
        continue;
      }

      var _kind$validate7 = kind.validate(v),
          _kind$validate8 = _slicedToArray(_kind$validate7, 2),
          e = _kind$validate8[0],
          r = _kind$validate8[1];

      if (e) {
        e.path = [i].concat(e.path);
        e.data = value;
        errors.push(e);
        continue;
      }

      ret[i] = r;
    }

    if (errors.length) {
      var first = errors[0];
      first.errors = errors;
      return [first];
    }

    return [undefined, ret];
  };

  return new Kind(name, type, validate);
}

/**
 * Union.
 *
 * @param {Array} schema
 * @param {Any} defaults
 * @param {Object} options
 */

function union(schema, defaults$$1, options) {
  if (kindOf(schema) !== 'array') {
    if (process.env.NODE_ENV !== 'production') {
      throw new Error('Union structs must be defined as an array, but you passed: ' + schema);
    } else {
      throw new Error('Invalid schema: ' + schema);
    }
  }

  var kinds = schema.map(function (s) {
    return any(s, undefined, options);
  });
  var name = 'union';
  var type = kinds.map(function (k) {
    return k.type;
  }).join(' | ');
  var validate = function validate() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : resolveDefaults(defaults$$1);

    var errors = [];

    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = kinds[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var k = _step.value;

        var _k$validate = k.validate(value),
            _k$validate2 = _slicedToArray(_k$validate, 2),
            e = _k$validate2[0],
            r = _k$validate2[1];

        if (!e) {
          return [undefined, r];
        }

        errors.push(e);
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }

    errors[0].type = type;
    return errors;
  };

  return new Kind(name, type, validate);
}

/**
 * Intersection.
 *
 * @param {Array} schema
 * @param {Any} defaults
 * @param {Object} options
 */

function intersection(schema, defaults$$1, options) {
  if (kindOf(schema) !== 'array') {
    if (process.env.NODE_ENV !== 'production') {
      throw new Error('Intersection structs must be defined as an array, but you passed: ' + schema);
    } else {
      throw new Error('Invalid schema: ' + schema);
    }
  }

  var types = schema.map(function (s) {
    return any(s, undefined, options);
  });
  var name = 'intersection';
  var type = types.map(function (t) {
    return t.type;
  }).join(' & ');
  var validate = function validate() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : resolveDefaults(defaults$$1);

    var v = value;

    var _iteratorNormalCompletion2 = true;
    var _didIteratorError2 = false;
    var _iteratorError2 = undefined;

    try {
      for (var _iterator2 = types[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
        var t = _step2.value;

        var _t$validate = t.validate(v),
            _t$validate2 = _slicedToArray(_t$validate, 2),
            e = _t$validate2[0],
            r = _t$validate2[1];

        if (e) {
          e.type = type;
          return [e];
        }

        v = r;
      }
    } catch (err) {
      _didIteratorError2 = true;
      _iteratorError2 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion2 && _iterator2.return) {
          _iterator2.return();
        }
      } finally {
        if (_didIteratorError2) {
          throw _iteratorError2;
        }
      }
    }

    return [undefined, v];
  };

  return new Kind(name, type, validate);
}

/**
 * Kinds.
 *
 * @type {Object}
 */

var Kinds = {
  any: any,
  dict: dict,
  enum: en,
  enums: enums,
  function: func,
  instance: instance,
  interface: inter,
  lazy: lazy,
  list: list,
  literal: literal,
  object: object,
  optional: optional,
  partial: partial,
  scalar: scalar,
  tuple: tuple,
  union: union,
  intersection: intersection

  /**
   * Export.
   *
   * @type {Object}
   */

};

/**
 * The types that `kind-of` supports.
 *
 * @type {Array}
 */

var TYPES = ['arguments', 'array', 'boolean', 'buffer', 'date', 'error', 'float32array', 'float64array', 'function', 'generatorfunction', 'int16array', 'int32array', 'int8array', 'map', 'null', 'number', 'object', 'promise', 'regexp', 'set', 'string', 'symbol', 'uint16array', 'uint32array', 'uint8array', 'uint8clampedarray', 'undefined', 'weakmap', 'weakset'];

/**
 * The default types that Superstruct ships with.
 *
 * @type {Object}
 */

var Types = {
  any: function any(value) {
    return value !== undefined;
  }
};

TYPES.forEach(function (type) {
  Types[type] = function (value) {
    return kindOf(value) === type;
  };
});

/**
 * Create a struct factory with a `config`.
 *
 * @param {Object} config
 * @return {Function}
 */

function superstruct() {
  var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  var types = _extends({}, Types, config.types || {});

  /**
   * Create a `kind` struct with `schema`, `defaults` and `options`.
   *
   * @param {Any} schema
   * @param {Any} defaults
   * @param {Object} options
   * @return {Function}
   */

  function struct(schema, defaults$$1) {
    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

    if (isStruct(schema)) {
      schema = schema.schema;
    }

    var kind = Kinds.any(schema, defaults$$1, _extends({}, options, { types: types }));

    function Struct(data) {
      if (this instanceof Struct) {
        if (process.env.NODE_ENV !== 'production') {
          throw new Error('The `Struct` creation function should not be used with the `new` keyword.');
        } else {
          throw new Error('Invalid `new` keyword!');
        }
      }

      return Struct.assert(data);
    }

    Object.defineProperty(Struct, IS_STRUCT, { value: true });
    Object.defineProperty(Struct, KIND, { value: kind });

    Struct.kind = kind.name;
    Struct.type = kind.type;
    Struct.schema = schema;
    Struct.defaults = defaults$$1;
    Struct.options = options;

    Struct.assert = function (value) {
      var _kind$validate9 = kind.validate(value),
          _kind$validate10 = _slicedToArray(_kind$validate9, 2),
          error = _kind$validate10[0],
          result = _kind$validate10[1];

      if (error) {
        throw new StructError(error);
      }

      return result;
    };

    Struct.test = function (value) {
      var _kind$validate11 = kind.validate(value),
          _kind$validate12 = _slicedToArray(_kind$validate11, 1),
          error = _kind$validate12[0];

      return !error;
    };

    Struct.validate = function (value) {
      var _kind$validate13 = kind.validate(value),
          _kind$validate14 = _slicedToArray(_kind$validate13, 2),
          error = _kind$validate14[0],
          result = _kind$validate14[1];

      if (error) {
        return [new StructError(error)];
      }

      return [undefined, result];
    };

    return Struct;
  }

  /**
   * Mix in a factory for each specific kind of struct.
   */

  Object.keys(Kinds).forEach(function (name) {
    var kind = Kinds[name];

    struct[name] = function (schema, defaults$$1, options) {
      var type = kind(schema, defaults$$1, _extends({}, options, { types: types }));
      var s = struct(type, defaults$$1, options);
      return s;
    };
  });

  /**
   * Return the struct factory.
   */

  return struct;
}

/**
 * Create a convenience `struct` factory for the default types.
 *
 * @type {Function}
 */

var struct = superstruct();

exports.struct = struct;
exports.superstruct = superstruct;
exports.isStruct = isStruct;
exports.StructError = StructError;
//# sourceMappingURL=index.es.js.map
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(53)))

/***/ }),
/* 53 */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _webviewer = __webpack_require__(6);

var WEBVIEWER = _interopRequireWildcard(_webviewer);

var _webviewer2 = __webpack_require__(5);

var _webviewer3 = _interopRequireDefault(_webviewer2);

var _utils = __webpack_require__(3);

var _MQTT = __webpack_require__(55);

var _MQTT2 = _interopRequireDefault(_MQTT);

var _webSocket = __webpack_require__(61);

var _webSocket2 = _interopRequireDefault(_webSocket);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var withConnect = function withConnect(option) {
  return function (WrappedComponent) {
    return function (_Component) {
      _inherits(_class2, _Component);

      function _class2(props) {
        _classCallCheck(this, _class2);

        var _this = _possibleConstructorReturn(this, (_class2.__proto__ || Object.getPrototypeOf(_class2)).call(this, props));

        _this.state = {
          client: null,
          webSocket: null
        };
        var _this$props$connectIn = _this.props.connectInfo,
            accessCode = _this$props$connectIn.accessCode,
            willMessage = _this$props$connectIn.willMessage,
            viewerId = _this$props$connectIn.viewerId,
            hostId = _this$props$connectIn.hostId;


        _this.viewerId = viewerId || accessCode + '_v0';
        _this.hostId = hostId || accessCode + '_h0';

        _this.option = option || {
          timeout: 2,
          useSSL: true,
          mqttVersion: 3,
          keepAliveInterval: 10,
          cleanSession: false,
          will: willMessage ? {
            topic: '/client/force_disconnect/' + accessCode,
            payload: {
              url: willMessage.url + (0, _utils.jsonToQueryString)(willMessage.params),
              identifier_id: _this.viewerId,
              data: willMessage.data || {}
            },
            qos: 0,
            retain: true
          } : null
        };

        _this.initialize(_this.option);
        return _this;
      }

      _createClass(_class2, [{
        key: 'initialize',
        value: function () {
          var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(option) {
            var _props, onReceivedEvent, connectInfo, getServers, pushServers, relayServers, concurrentTasks, _ref2, _ref3, mqttInfo, wsInfo, message;

            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _props = this.props, onReceivedEvent = _props.onReceivedEvent, connectInfo = _props.connectInfo;

                    getServers = function getServers(type) {
                      return [].concat(_webviewer3.default.get(type) || connectInfo[type + 's']);
                    };

                    pushServers = getServers('pushServer');
                    relayServers = getServers('relayServer');
                    concurrentTasks = [(0, _MQTT2.default)(pushServers, option), (0, _webSocket2.default)(relayServers)];
                    _context.next = 7;
                    return Promise.all(concurrentTasks);

                  case 7:
                    _ref2 = _context.sent;
                    _ref3 = _slicedToArray(_ref2, 2);
                    mqttInfo = _ref3[0];
                    wsInfo = _ref3[1];

                    if (!(!mqttInfo || !wsInfo)) {
                      _context.next = 15;
                      break;
                    }

                    message = !mqttInfo && !wsInfo ? 'push, relay' : !mqttInfo ? 'push' : 'relay';

                    onReceivedEvent(WEBVIEWER.VIEWER_EVENT.ERROR, { type: 'connect fail', message: message });
                    return _context.abrupt('return');

                  case 15:

                    _webviewer3.default.set('pushServer', mqttInfo.connectedServer);
                    _webviewer3.default.set('relayServer', wsInfo.connectedServer);

                    this.setState({
                      client: mqttInfo.client,
                      webSocket: wsInfo.webSocket
                    });

                  case 18:
                  case 'end':
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));

          function initialize(_x) {
            return _ref.apply(this, arguments);
          }

          return initialize;
        }()
      }, {
        key: 'render',
        value: function render() {
          var viewerId = this.viewerId,
              hostId = this.hostId;
          var _state = this.state,
              client = _state.client,
              webSocket = _state.webSocket;


          if (!client || !webSocket) return null;

          return _react2.default.createElement(WrappedComponent, _extends({}, this.props, {
            client: client,
            webSocket: webSocket,
            viewerId: viewerId,
            hostId: hostId
          }));
        }
      }]);

      return _class2;
    }(_react.Component);
  };
};

exports.default = withConnect;

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _connect = __webpack_require__(56);

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_connect).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _rsupMqtt = __webpack_require__(20);

var _Connection = __webpack_require__(59);

var _Connection2 = _interopRequireDefault(_Connection);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
  var servers = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, server, client;

  return regeneratorRuntime.wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          opts = _extends({
            timeout: 2,
            ssl: true,
            keepalive: 10,
            cleanSession: false
          }, opts);

          _iteratorNormalCompletion = true;
          _didIteratorError = false;
          _iteratorError = undefined;
          _context.prev = 4;
          _iterator = servers[Symbol.iterator]();

        case 6:
          if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
            _context.next = 15;
            break;
          }

          server = _step.value;
          _context.next = 10;
          return (0, _rsupMqtt.connect)(_extends({}, opts, { host: server.host, port: server.viewerPort }), _Connection2.default);

        case 10:
          client = _context.sent;
          return _context.abrupt('return', { client: client, connectedServer: server });

        case 12:
          _iteratorNormalCompletion = true;
          _context.next = 6;
          break;

        case 15:
          _context.next = 21;
          break;

        case 17:
          _context.prev = 17;
          _context.t0 = _context['catch'](4);
          _didIteratorError = true;
          _iteratorError = _context.t0;

        case 21:
          _context.prev = 21;
          _context.prev = 22;

          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }

        case 24:
          _context.prev = 24;

          if (!_didIteratorError) {
            _context.next = 27;
            break;
          }

          throw _iteratorError;

        case 27:
          return _context.finish(24);

        case 28:
          return _context.finish(21);

        case 29:
        case 'end':
          return _context.stop();
      }
    }
  }, _callee, this, [[4, 17, 21, 29], [22,, 24, 28]]);
}));

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*******************************************************************************
 * Copyright (c) 2013 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Andrew Banks - initial API and implementation and initial documentation
 *    Guozhen Huang - improve to umd library
 *******************************************************************************/

(function(factory) {
    var root = (typeof self == 'object' && self.self === self && self) ||
      (typeof global == 'object' && global.global === global && global);

    if (true) {
      !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {
        return factory(root)
      }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
    } else if (typeof exports === 'object') {
      module.exports = factory(root);
    } else {
      if (typeof root.Paho === 'undefined') {
        root.Paho = {};
      }
      root.Paho.MQTT = factory(root);
    }
})(function(global) {
	// Private variables below, these are only visible inside the function closure
	// which is used to define the module.

	var version = "@VERSION@";
	var buildLevel = "@BUILDLEVEL@";

	/**
	 * Unique message type identifiers, with associated
	 * associated integer values.
	 * @private
	 */
	var MESSAGE_TYPE = {
		CONNECT: 1,
		CONNACK: 2,
		PUBLISH: 3,
		PUBACK: 4,
		PUBREC: 5,
		PUBREL: 6,
		PUBCOMP: 7,
		SUBSCRIBE: 8,
		SUBACK: 9,
		UNSUBSCRIBE: 10,
		UNSUBACK: 11,
		PINGREQ: 12,
		PINGRESP: 13,
		DISCONNECT: 14
	};

	// Collection of utility methods used to simplify module code
	// and promote the DRY pattern.

	/**
	 * Validate an object's parameter names to ensure they
	 * match a list of expected variables name for this option
	 * type. Used to ensure option object passed into the API don't
	 * contain erroneous parameters.
	 * @param {Object} obj - User options object
	 * @param {Object} keys - valid keys and types that may exist in obj.
	 * @throws {Error} Invalid option parameter found.
	 * @private
	 */
	var validate = function(obj, keys) {
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				if (keys.hasOwnProperty(key)) {
					if (typeof obj[key] !== keys[key])
					   throw new Error(format(ERROR.INVALID_TYPE, [typeof obj[key], key]));
				} else {
					var errorStr = "Unknown property, " + key + ". Valid properties are:";
					for (var key in keys)
						if (keys.hasOwnProperty(key))
							errorStr = errorStr+" "+key;
					throw new Error(errorStr);
				}
			}
		}
	};

	/**
	 * Return a new function which runs the user function bound
	 * to a fixed scope.
	 * @param {function} User function
	 * @param {object} Function scope
	 * @return {function} User function bound to another scope
	 * @private
	 */
	var scope = function (f, scope) {
		return function () {
			return f.apply(scope, arguments);
		};
	};

	/**
	 * Unique message type identifiers, with associated
	 * associated integer values.
	 * @private
	 */
	var ERROR = {
		OK: {code:0, text:"AMQJSC0000I OK."},
		CONNECT_TIMEOUT: {code:1, text:"AMQJSC0001E Connect timed out."},
		SUBSCRIBE_TIMEOUT: {code:2, text:"AMQJS0002E Subscribe timed out."},
		UNSUBSCRIBE_TIMEOUT: {code:3, text:"AMQJS0003E Unsubscribe timed out."},
		PING_TIMEOUT: {code:4, text:"AMQJS0004E Ping timed out."},
		INTERNAL_ERROR: {code:5, text:"AMQJS0005E Internal error. Error Message: {0}, Stack trace: {1}"},
		CONNACK_RETURNCODE: {code:6, text:"AMQJS0006E Bad Connack return code:{0} {1}."},
		SOCKET_ERROR: {code:7, text:"AMQJS0007E Socket error:{0}."},
		SOCKET_CLOSE: {code:8, text:"AMQJS0008I Socket closed."},
		MALFORMED_UTF: {code:9, text:"AMQJS0009E Malformed UTF data:{0} {1} {2}."},
		UNSUPPORTED: {code:10, text:"AMQJS0010E {0} is not supported by this browser."},
		INVALID_STATE: {code:11, text:"AMQJS0011E Invalid state {0}."},
		INVALID_TYPE: {code:12, text:"AMQJS0012E Invalid type {0} for {1}."},
		INVALID_ARGUMENT: {code:13, text:"AMQJS0013E Invalid argument {0} for {1}."},
		UNSUPPORTED_OPERATION: {code:14, text:"AMQJS0014E Unsupported operation."},
		INVALID_STORED_DATA: {code:15, text:"AMQJS0015E Invalid data in local storage key={0} value={1}."},
		INVALID_MQTT_MESSAGE_TYPE: {code:16, text:"AMQJS0016E Invalid MQTT message type {0}."},
		MALFORMED_UNICODE: {code:17, text:"AMQJS0017E Malformed Unicode string:{0} {1}."},
	};

	/** CONNACK RC Meaning. */
	var CONNACK_RC = {
		0:"Connection Accepted",
		1:"Connection Refused: unacceptable protocol version",
		2:"Connection Refused: identifier rejected",
		3:"Connection Refused: server unavailable",
		4:"Connection Refused: bad user name or password",
		5:"Connection Refused: not authorized"
	};

	/**
	 * Format an error message text.
	 * @private
	 * @param {error} ERROR.KEY value above.
	 * @param {substitutions} [array] substituted into the text.
	 * @return the text with the substitutions made.
	 */
	var format = function(error, substitutions) {
		var text = error.text;
		if (substitutions) {
		  var field,start;
		  for (var i=0; i<substitutions.length; i++) {
			field = "{"+i+"}";
			start = text.indexOf(field);
			if(start > 0) {
				var part1 = text.substring(0,start);
				var part2 = text.substring(start+field.length);
				text = part1+substitutions[i]+part2;
			}
		  }
		}
		return text;
	};

	//MQTT protocol and version          6    M    Q    I    s    d    p    3
	var MqttProtoIdentifierv3 = [0x00,0x06,0x4d,0x51,0x49,0x73,0x64,0x70,0x03];
	//MQTT proto/version for 311         4    M    Q    T    T    4
	var MqttProtoIdentifierv4 = [0x00,0x04,0x4d,0x51,0x54,0x54,0x04];

	/**
	 * Construct an MQTT wire protocol message.
	 * @param type MQTT packet type.
	 * @param options optional wire message attributes.
	 *
	 * Optional properties
	 *
	 * messageIdentifier: message ID in the range [0..65535]
	 * payloadMessage:	Application Message - PUBLISH only
	 * connectStrings:	array of 0 or more Strings to be put into the CONNECT payload
	 * topics:			array of strings (SUBSCRIBE, UNSUBSCRIBE)
	 * requestQoS:		array of QoS values [0..2]
	 *
	 * "Flag" properties
	 * cleanSession:	true if present / false if absent (CONNECT)
	 * willMessage:  	true if present / false if absent (CONNECT)
	 * isRetained:		true if present / false if absent (CONNECT)
	 * userName:		true if present / false if absent (CONNECT)
	 * password:		true if present / false if absent (CONNECT)
	 * keepAliveInterval:	integer [0..65535]  (CONNECT)
	 *
	 * @private
	 * @ignore
	 */
	var WireMessage = function (type, options) {
		this.type = type;
		for (var name in options) {
			if (options.hasOwnProperty(name)) {
				this[name] = options[name];
			}
		}
	};

	WireMessage.prototype.encode = function() {
		// Compute the first byte of the fixed header
		var first = ((this.type & 0x0f) << 4);

		/*
		 * Now calculate the length of the variable header + payload by adding up the lengths
		 * of all the component parts
		 */

		var remLength = 0;
		var topicStrLength = new Array();
		var destinationNameLength = 0;

		// if the message contains a messageIdentifier then we need two bytes for that
		if (this.messageIdentifier != undefined)
			remLength += 2;

		switch(this.type) {
			// If this a Connect then we need to include 12 bytes for its header
			case MESSAGE_TYPE.CONNECT:
				switch(this.mqttVersion) {
					case 3:
						remLength += MqttProtoIdentifierv3.length + 3;
						break;
					case 4:
						remLength += MqttProtoIdentifierv4.length + 3;
						break;
				}

				remLength += UTF8Length(this.clientId) + 2;
				if (this.willMessage != undefined) {
					remLength += UTF8Length(this.willMessage.destinationName) + 2;
					// Will message is always a string, sent as UTF-8 characters with a preceding length.
					var willMessagePayloadBytes = this.willMessage.payloadBytes;
					if (!(willMessagePayloadBytes instanceof Uint8Array))
						willMessagePayloadBytes = new Uint8Array(payloadBytes);
					remLength += willMessagePayloadBytes.byteLength +2;
				}
				if (this.userName != undefined)
					remLength += UTF8Length(this.userName) + 2;
				if (this.password != undefined)
					remLength += UTF8Length(this.password) + 2;
			break;

			// Subscribe, Unsubscribe can both contain topic strings
			case MESSAGE_TYPE.SUBSCRIBE:
				first |= 0x02; // Qos = 1;
				for ( var i = 0; i < this.topics.length; i++) {
					topicStrLength[i] = UTF8Length(this.topics[i]);
					remLength += topicStrLength[i] + 2;
				}
				remLength += this.requestedQos.length; // 1 byte for each topic's Qos
				// QoS on Subscribe only
				break;

			case MESSAGE_TYPE.UNSUBSCRIBE:
				first |= 0x02; // Qos = 1;
				for ( var i = 0; i < this.topics.length; i++) {
					topicStrLength[i] = UTF8Length(this.topics[i]);
					remLength += topicStrLength[i] + 2;
				}
				break;

			case MESSAGE_TYPE.PUBREL:
				first |= 0x02; // Qos = 1;
				break;

			case MESSAGE_TYPE.PUBLISH:
				if (this.payloadMessage.duplicate) first |= 0x08;
				first  = first |= (this.payloadMessage.qos << 1);
				if (this.payloadMessage.retained) first |= 0x01;
				destinationNameLength = UTF8Length(this.payloadMessage.destinationName);
				remLength += destinationNameLength + 2;
				var payloadBytes = this.payloadMessage.payloadBytes;
				remLength += payloadBytes.byteLength;
				if (payloadBytes instanceof ArrayBuffer)
					payloadBytes = new Uint8Array(payloadBytes);
				else if (!(payloadBytes instanceof Uint8Array))
					payloadBytes = new Uint8Array(payloadBytes.buffer);
				break;

			case MESSAGE_TYPE.DISCONNECT:
				break;

			default:
				;
		}

		// Now we can allocate a buffer for the message

		var mbi = encodeMBI(remLength);  // Convert the length to MQTT MBI format
		var pos = mbi.length + 1;        // Offset of start of variable header
		var buffer = new ArrayBuffer(remLength + pos);
		var byteStream = new Uint8Array(buffer);    // view it as a sequence of bytes

		//Write the fixed header into the buffer
		byteStream[0] = first;
		byteStream.set(mbi,1);

		// If this is a PUBLISH then the variable header starts with a topic
		if (this.type == MESSAGE_TYPE.PUBLISH)
			pos = writeString(this.payloadMessage.destinationName, destinationNameLength, byteStream, pos);
		// If this is a CONNECT then the variable header contains the protocol name/version, flags and keepalive time

		else if (this.type == MESSAGE_TYPE.CONNECT) {
			switch (this.mqttVersion) {
				case 3:
					byteStream.set(MqttProtoIdentifierv3, pos);
					pos += MqttProtoIdentifierv3.length;
					break;
				case 4:
					byteStream.set(MqttProtoIdentifierv4, pos);
					pos += MqttProtoIdentifierv4.length;
					break;
			}
			var connectFlags = 0;
			if (this.cleanSession)
				connectFlags = 0x02;
			if (this.willMessage != undefined ) {
				connectFlags |= 0x04;
				connectFlags |= (this.willMessage.qos<<3);
				if (this.willMessage.retained) {
					connectFlags |= 0x20;
				}
			}
			if (this.userName != undefined)
				connectFlags |= 0x80;
			if (this.password != undefined)
				connectFlags |= 0x40;
			byteStream[pos++] = connectFlags;
			pos = writeUint16 (this.keepAliveInterval, byteStream, pos);
		}

		// Output the messageIdentifier - if there is one
		if (this.messageIdentifier != undefined)
			pos = writeUint16 (this.messageIdentifier, byteStream, pos);

		switch(this.type) {
			case MESSAGE_TYPE.CONNECT:
				pos = writeString(this.clientId, UTF8Length(this.clientId), byteStream, pos);
				if (this.willMessage != undefined) {
					pos = writeString(this.willMessage.destinationName, UTF8Length(this.willMessage.destinationName), byteStream, pos);
					pos = writeUint16(willMessagePayloadBytes.byteLength, byteStream, pos);
					byteStream.set(willMessagePayloadBytes, pos);
					pos += willMessagePayloadBytes.byteLength;

				}
			if (this.userName != undefined)
				pos = writeString(this.userName, UTF8Length(this.userName), byteStream, pos);
			if (this.password != undefined)
				pos = writeString(this.password, UTF8Length(this.password), byteStream, pos);
			break;

			case MESSAGE_TYPE.PUBLISH:
				// PUBLISH has a text or binary payload, if text do not add a 2 byte length field, just the UTF characters.
				byteStream.set(payloadBytes, pos);

				break;

//    	    case MESSAGE_TYPE.PUBREC:
//    	    case MESSAGE_TYPE.PUBREL:
//    	    case MESSAGE_TYPE.PUBCOMP:
//    	    	break;

			case MESSAGE_TYPE.SUBSCRIBE:
				// SUBSCRIBE has a list of topic strings and request QoS
				for (var i=0; i<this.topics.length; i++) {
					pos = writeString(this.topics[i], topicStrLength[i], byteStream, pos);
					byteStream[pos++] = this.requestedQos[i];
				}
				break;

			case MESSAGE_TYPE.UNSUBSCRIBE:
				// UNSUBSCRIBE has a list of topic strings
				for (var i=0; i<this.topics.length; i++)
					pos = writeString(this.topics[i], topicStrLength[i], byteStream, pos);
				break;

			default:
				// Do nothing.
		}

		return buffer;
	}

	function decodeMessage(input,pos) {
	    var startingPos = pos;
		var first = input[pos];
		var type = first >> 4;
		var messageInfo = first &= 0x0f;
		pos += 1;


		// Decode the remaining length (MBI format)

		var digit;
		var remLength = 0;
		var multiplier = 1;
		do {
			if (pos == input.length) {
			    return [null,startingPos];
			}
			digit = input[pos++];
			remLength += ((digit & 0x7F) * multiplier);
			multiplier *= 128;
		} while ((digit & 0x80) != 0);

		var endPos = pos+remLength;
		if (endPos > input.length) {
		    return [null,startingPos];
		}

		var wireMessage = new WireMessage(type);
		switch(type) {
			case MESSAGE_TYPE.CONNACK:
				var connectAcknowledgeFlags = input[pos++];
				if (connectAcknowledgeFlags & 0x01)
					wireMessage.sessionPresent = true;
				wireMessage.returnCode = input[pos++];
				break;

			case MESSAGE_TYPE.PUBLISH:
				var qos = (messageInfo >> 1) & 0x03;

				var len = readUint16(input, pos);
				pos += 2;
				var topicName = parseUTF8(input, pos, len);
				pos += len;
				// If QoS 1 or 2 there will be a messageIdentifier
				if (qos > 0) {
					wireMessage.messageIdentifier = readUint16(input, pos);
					pos += 2;
				}

				var message = new Message(input.subarray(pos, endPos));
				if ((messageInfo & 0x01) == 0x01)
					message.retained = true;
				if ((messageInfo & 0x08) == 0x08)
					message.duplicate =  true;
				message.qos = qos;
				message.destinationName = topicName;
				wireMessage.payloadMessage = message;
				break;

			case  MESSAGE_TYPE.PUBACK:
			case  MESSAGE_TYPE.PUBREC:
			case  MESSAGE_TYPE.PUBREL:
			case  MESSAGE_TYPE.PUBCOMP:
			case  MESSAGE_TYPE.UNSUBACK:
				wireMessage.messageIdentifier = readUint16(input, pos);
				break;

			case  MESSAGE_TYPE.SUBACK:
				wireMessage.messageIdentifier = readUint16(input, pos);
				pos += 2;
				wireMessage.returnCode = input.subarray(pos, endPos);
				break;

			default:
				;
		}

		return [wireMessage,endPos];
	}

	function writeUint16(input, buffer, offset) {
		buffer[offset++] = input >> 8;      //MSB
		buffer[offset++] = input % 256;     //LSB
		return offset;
	}

	function writeString(input, utf8Length, buffer, offset) {
		offset = writeUint16(utf8Length, buffer, offset);
		stringToUTF8(input, buffer, offset);
		return offset + utf8Length;
	}

	function readUint16(buffer, offset) {
		return 256*buffer[offset] + buffer[offset+1];
	}

	/**
	 * Encodes an MQTT Multi-Byte Integer
	 * @private
	 */
	function encodeMBI(number) {
		var output = new Array(1);
		var numBytes = 0;

		do {
			var digit = number % 128;
			number = number >> 7;
			if (number > 0) {
				digit |= 0x80;
			}
			output[numBytes++] = digit;
		} while ( (number > 0) && (numBytes<4) );

		return output;
	}

	/**
	 * Takes a String and calculates its length in bytes when encoded in UTF8.
	 * @private
	 */
	function UTF8Length(input) {
		var output = 0;
		for (var i = 0; i<input.length; i++)
		{
			var charCode = input.charCodeAt(i);
				if (charCode > 0x7FF)
				   {
					  // Surrogate pair means its a 4 byte character
					  if (0xD800 <= charCode && charCode <= 0xDBFF)
						{
						  i++;
						  output++;
						}
				   output +=3;
				   }
			else if (charCode > 0x7F)
				output +=2;
			else
				output++;
		}
		return output;
	}

	/**
	 * Takes a String and writes it into an array as UTF8 encoded bytes.
	 * @private
	 */
	function stringToUTF8(input, output, start) {
		var pos = start;
		for (var i = 0; i<input.length; i++) {
			var charCode = input.charCodeAt(i);

			// Check for a surrogate pair.
			if (0xD800 <= charCode && charCode <= 0xDBFF) {
				var lowCharCode = input.charCodeAt(++i);
				if (isNaN(lowCharCode)) {
					throw new Error(format(ERROR.MALFORMED_UNICODE, [charCode, lowCharCode]));
				}
				charCode = ((charCode - 0xD800)<<10) + (lowCharCode - 0xDC00) + 0x10000;

			}

			if (charCode <= 0x7F) {
				output[pos++] = charCode;
			} else if (charCode <= 0x7FF) {
				output[pos++] = charCode>>6  & 0x1F | 0xC0;
				output[pos++] = charCode     & 0x3F | 0x80;
			} else if (charCode <= 0xFFFF) {
				output[pos++] = charCode>>12 & 0x0F | 0xE0;
				output[pos++] = charCode>>6  & 0x3F | 0x80;
				output[pos++] = charCode     & 0x3F | 0x80;
			} else {
				output[pos++] = charCode>>18 & 0x07 | 0xF0;
				output[pos++] = charCode>>12 & 0x3F | 0x80;
				output[pos++] = charCode>>6  & 0x3F | 0x80;
				output[pos++] = charCode     & 0x3F | 0x80;
			};
		}
		return output;
	}

	function parseUTF8(input, offset, length) {
		var output = "";
		var utf16;
		var pos = offset;

		while (pos < offset+length)
		{
			var byte1 = input[pos++];
			if (byte1 < 128)
				utf16 = byte1;
			else
			{
				var byte2 = input[pos++]-128;
				if (byte2 < 0)
					throw new Error(format(ERROR.MALFORMED_UTF, [byte1.toString(16), byte2.toString(16),""]));
				if (byte1 < 0xE0)             // 2 byte character
					utf16 = 64*(byte1-0xC0) + byte2;
				else
				{
					var byte3 = input[pos++]-128;
					if (byte3 < 0)
						throw new Error(format(ERROR.MALFORMED_UTF, [byte1.toString(16), byte2.toString(16), byte3.toString(16)]));
					if (byte1 < 0xF0)        // 3 byte character
						utf16 = 4096*(byte1-0xE0) + 64*byte2 + byte3;
								else
								{
								   var byte4 = input[pos++]-128;
								   if (byte4 < 0)
						throw new Error(format(ERROR.MALFORMED_UTF, [byte1.toString(16), byte2.toString(16), byte3.toString(16), byte4.toString(16)]));
								   if (byte1 < 0xF8)        // 4 byte character
										   utf16 = 262144*(byte1-0xF0) + 4096*byte2 + 64*byte3 + byte4;
					   else                     // longer encodings are not supported
						throw new Error(format(ERROR.MALFORMED_UTF, [byte1.toString(16), byte2.toString(16), byte3.toString(16), byte4.toString(16)]));
								}
				}
			}

				if (utf16 > 0xFFFF)   // 4 byte character - express as a surrogate pair
				  {
					 utf16 -= 0x10000;
					 output += String.fromCharCode(0xD800 + (utf16 >> 10)); // lead character
					 utf16 = 0xDC00 + (utf16 & 0x3FF);  // trail character
				  }
			output += String.fromCharCode(utf16);
		}
		return output;
	}

	/**
	 * Repeat keepalive requests, monitor responses.
	 * @ignore
	 */
	var Pinger = function(client, window, keepAliveInterval) {
		this._client = client;
		this._window = window;
		this._keepAliveInterval = keepAliveInterval*1000;
		this.isReset = false;

		var pingReq = new WireMessage(MESSAGE_TYPE.PINGREQ).encode();

		var doTimeout = function (pinger) {
			return function () {
				return doPing.apply(pinger);
			};
		};

		/** @ignore */
		var doPing = function() {
			if (!this.isReset) {
				this._client._trace("Pinger.doPing", "Timed out");
				this._client._disconnected( ERROR.PING_TIMEOUT.code , format(ERROR.PING_TIMEOUT));
			} else {
				this.isReset = false;
				this._client._trace("Pinger.doPing", "send PINGREQ");
				this._client.socket.send(pingReq);
				this.timeout = this._window.setTimeout(doTimeout(this), this._keepAliveInterval);
			}
		}

		this.reset = function() {
			this.isReset = true;
			this._window.clearTimeout(this.timeout);
			if (this._keepAliveInterval > 0)
				this.timeout = setTimeout(doTimeout(this), this._keepAliveInterval);
		}

		this.cancel = function() {
			this._window.clearTimeout(this.timeout);
		}
	 };

	/**
	 * Monitor request completion.
	 * @ignore
	 */
	var Timeout = function(client, window, timeoutSeconds, action, args) {
		this._window = window;
		if (!timeoutSeconds)
			timeoutSeconds = 30;

		var doTimeout = function (action, client, args) {
			return function () {
				return action.apply(client, args);
			};
		};
		this.timeout = setTimeout(doTimeout(action, client, args), timeoutSeconds * 1000);

		this.cancel = function() {
			this._window.clearTimeout(this.timeout);
		}
	};

	/*
	 * Internal implementation of the Websockets MQTT V3.1 client.
	 *
	 * @name Paho.MQTT.ClientImpl @constructor
	 * @param {String} host the DNS nameof the webSocket host.
	 * @param {Number} port the port number for that host.
	 * @param {String} clientId the MQ client identifier.
	 */
	var ClientImpl = function (uri, host, port, path, clientId) {
		// Check dependencies are satisfied in this browser.
		if (!("WebSocket" in global && global["WebSocket"] !== null)) {
			throw new Error(format(ERROR.UNSUPPORTED, ["WebSocket"]));
		}
		if (!("localStorage" in global && global["localStorage"] !== null)) {
			throw new Error(format(ERROR.UNSUPPORTED, ["localStorage"]));
		}
		if (!("ArrayBuffer" in global && global["ArrayBuffer"] !== null)) {
			throw new Error(format(ERROR.UNSUPPORTED, ["ArrayBuffer"]));
		}
		this._trace("Paho.MQTT.Client", uri, host, port, path, clientId);

		this.host = host;
		this.port = port;
		this.path = path;
		this.uri = uri;
		this.clientId = clientId;

		// Local storagekeys are qualified with the following string.
		// The conditional inclusion of path in the key is for backward
		// compatibility to when the path was not configurable and assumed to
		// be /mqtt
		this._localKey=host+":"+port+(path!="/mqtt"?":"+path:"")+":"+clientId+":";

		// Create private instance-only message queue
		// Internal queue of messages to be sent, in sending order.
		this._msg_queue = [];

		// Messages we have sent and are expecting a response for, indexed by their respective message ids.
		this._sentMessages = {};

		// Messages we have received and acknowleged and are expecting a confirm message for
		// indexed by their respective message ids.
		this._receivedMessages = {};

		// Internal list of callbacks to be executed when messages
		// have been successfully sent over web socket, e.g. disconnect
		// when it doesn't have to wait for ACK, just message is dispatched.
		this._notify_msg_sent = {};

		// Unique identifier for SEND messages, incrementing
		// counter as messages are sent.
		this._message_identifier = 1;

		// Used to determine the transmission sequence of stored sent messages.
		this._sequence = 0;


		// Load the local state, if any, from the saved version, only restore state relevant to this client.
		for (var key in localStorage)
			if (   key.indexOf("Sent:"+this._localKey) == 0
				|| key.indexOf("Received:"+this._localKey) == 0)
			this.restore(key);
	};

	// Messaging Client public instance members.
	ClientImpl.prototype.host;
	ClientImpl.prototype.port;
	ClientImpl.prototype.path;
	ClientImpl.prototype.uri;
	ClientImpl.prototype.clientId;

	// Messaging Client private instance members.
	ClientImpl.prototype.socket;
	/* true once we have received an acknowledgement to a CONNECT packet. */
	ClientImpl.prototype.connected = false;
	/* The largest message identifier allowed, may not be larger than 2**16 but
	 * if set smaller reduces the maximum number of outbound messages allowed.
	 */
	ClientImpl.prototype.maxMessageIdentifier = 65536;
	ClientImpl.prototype.connectOptions;
	ClientImpl.prototype.hostIndex;
	ClientImpl.prototype.onConnectionLost;
	ClientImpl.prototype.onMessageDelivered;
	ClientImpl.prototype.onMessageArrived;
	ClientImpl.prototype.traceFunction;
	ClientImpl.prototype._msg_queue = null;
	ClientImpl.prototype._connectTimeout;
	/* The sendPinger monitors how long we allow before we send data to prove to the server that we are alive. */
	ClientImpl.prototype.sendPinger = null;
	/* The receivePinger monitors how long we allow before we require evidence that the server is alive. */
	ClientImpl.prototype.receivePinger = null;

	ClientImpl.prototype.receiveBuffer = null;

	ClientImpl.prototype._traceBuffer = null;
	ClientImpl.prototype._MAX_TRACE_ENTRIES = 100;

	ClientImpl.prototype.connect = function (connectOptions) {
		var connectOptionsMasked = this._traceMask(connectOptions, "password");
		this._trace("Client.connect", connectOptionsMasked, this.socket, this.connected);

		if (this.connected)
			throw new Error(format(ERROR.INVALID_STATE, ["already connected"]));
		if (this.socket)
			throw new Error(format(ERROR.INVALID_STATE, ["already connected"]));

		this.connectOptions = connectOptions;

		if (connectOptions.uris) {
			this.hostIndex = 0;
			this._doConnect(connectOptions.uris[0]);
		} else {
			this._doConnect(this.uri);
		}

	};

	ClientImpl.prototype.subscribe = function (filter, subscribeOptions) {
		this._trace("Client.subscribe", filter, subscribeOptions);

		if (!this.connected)
			throw new Error(format(ERROR.INVALID_STATE, ["not connected"]));

		var wireMessage = new WireMessage(MESSAGE_TYPE.SUBSCRIBE);
		wireMessage.topics=[filter];
		if (subscribeOptions.qos != undefined)
			wireMessage.requestedQos = [subscribeOptions.qos];
		else
			wireMessage.requestedQos = [0];

		if (subscribeOptions.onSuccess) {
			wireMessage.onSuccess = function(grantedQos) {subscribeOptions.onSuccess({invocationContext:subscribeOptions.invocationContext,grantedQos:grantedQos});};
		}

		if (subscribeOptions.onFailure) {
			wireMessage.onFailure = function(errorCode) {subscribeOptions.onFailure({invocationContext:subscribeOptions.invocationContext,errorCode:errorCode});};
		}

		if (subscribeOptions.timeout) {
			wireMessage.timeOut = new Timeout(this, window, subscribeOptions.timeout, subscribeOptions.onFailure
					, [{invocationContext:subscribeOptions.invocationContext,
						errorCode:ERROR.SUBSCRIBE_TIMEOUT.code,
						errorMessage:format(ERROR.SUBSCRIBE_TIMEOUT)}]);
		}

		// All subscriptions return a SUBACK.
		this._requires_ack(wireMessage);
		this._schedule_message(wireMessage);
	};

	/** @ignore */
	ClientImpl.prototype.unsubscribe = function(filter, unsubscribeOptions) {
		this._trace("Client.unsubscribe", filter, unsubscribeOptions);

		if (!this.connected)
		   throw new Error(format(ERROR.INVALID_STATE, ["not connected"]));

		var wireMessage = new WireMessage(MESSAGE_TYPE.UNSUBSCRIBE);
		wireMessage.topics = [filter];

		if (unsubscribeOptions.onSuccess) {
			wireMessage.callback = function() {unsubscribeOptions.onSuccess({invocationContext:unsubscribeOptions.invocationContext});};
		}
		if (unsubscribeOptions.timeout) {
			wireMessage.timeOut = new Timeout(this, window, unsubscribeOptions.timeout, unsubscribeOptions.onFailure
					, [{invocationContext:unsubscribeOptions.invocationContext,
						errorCode:ERROR.UNSUBSCRIBE_TIMEOUT.code,
						errorMessage:format(ERROR.UNSUBSCRIBE_TIMEOUT)}]);
		}

		// All unsubscribes return a SUBACK.
		this._requires_ack(wireMessage);
		this._schedule_message(wireMessage);
	};

	ClientImpl.prototype.send = function (message) {
		this._trace("Client.send", message);

		if (!this.connected)
		   throw new Error(format(ERROR.INVALID_STATE, ["not connected"]));

		wireMessage = new WireMessage(MESSAGE_TYPE.PUBLISH);
		wireMessage.payloadMessage = message;

		if (message.qos > 0)
			this._requires_ack(wireMessage);
		else if (this.onMessageDelivered)
			this._notify_msg_sent[wireMessage] = this.onMessageDelivered(wireMessage.payloadMessage);
		this._schedule_message(wireMessage);
	};

	ClientImpl.prototype.disconnect = function () {
		this._trace("Client.disconnect");

		if (!this.socket)
			throw new Error(format(ERROR.INVALID_STATE, ["not connecting or connected"]));

		wireMessage = new WireMessage(MESSAGE_TYPE.DISCONNECT);

		// Run the disconnected call back as soon as the message has been sent,
		// in case of a failure later on in the disconnect processing.
		// as a consequence, the _disconected call back may be run several times.
		this._notify_msg_sent[wireMessage] = scope(this._disconnected, this);

		this._schedule_message(wireMessage);
	};

	ClientImpl.prototype.getTraceLog = function () {
		if ( this._traceBuffer !== null ) {
			this._trace("Client.getTraceLog", new Date());
			this._trace("Client.getTraceLog in flight messages", this._sentMessages.length);
			for (var key in this._sentMessages)
				this._trace("_sentMessages ",key, this._sentMessages[key]);
			for (var key in this._receivedMessages)
				this._trace("_receivedMessages ",key, this._receivedMessages[key]);

			return this._traceBuffer;
		}
	};

	ClientImpl.prototype.startTrace = function () {
		if ( this._traceBuffer === null ) {
			this._traceBuffer = [];
		}
		this._trace("Client.startTrace", new Date(), version);
	};

	ClientImpl.prototype.stopTrace = function () {
		delete this._traceBuffer;
	};

	ClientImpl.prototype._doConnect = function (wsurl) {
		// When the socket is open, this client will send the CONNECT WireMessage using the saved parameters.
		if (this.connectOptions.useSSL) {
		    var uriParts = wsurl.split(":");
		    uriParts[0] = "wss";
		    wsurl = uriParts.join(":");
		}
		this.connected = false;
		if (this.connectOptions.mqttVersion < 4) {
			this.socket = new WebSocket(wsurl, ["mqttv3.1"]);
		} else {
			this.socket = new WebSocket(wsurl, ["mqtt"]);
		}
		this.socket.binaryType = 'arraybuffer';

		this.socket.onopen = scope(this._on_socket_open, this);
		this.socket.onmessage = scope(this._on_socket_message, this);
		this.socket.onerror = scope(this._on_socket_error, this);
		this.socket.onclose = scope(this._on_socket_close, this);

		this.sendPinger = new Pinger(this, window, this.connectOptions.keepAliveInterval);
		this.receivePinger = new Pinger(this, window, this.connectOptions.keepAliveInterval);

		this._connectTimeout = new Timeout(this, window, this.connectOptions.timeout, this._disconnected,  [ERROR.CONNECT_TIMEOUT.code, format(ERROR.CONNECT_TIMEOUT)]);
	};


	// Schedule a new message to be sent over the WebSockets
	// connection. CONNECT messages cause WebSocket connection
	// to be started. All other messages are queued internally
	// until this has happened. When WS connection starts, process
	// all outstanding messages.
	ClientImpl.prototype._schedule_message = function (message) {
		this._msg_queue.push(message);
		// Process outstanding messages in the queue if we have an  open socket, and have received CONNACK.
		if (this.connected) {
			this._process_queue();
		}
	};

	ClientImpl.prototype.store = function(prefix, wireMessage) {
		var storedMessage = {type:wireMessage.type, messageIdentifier:wireMessage.messageIdentifier, version:1};

		switch(wireMessage.type) {
		  case MESSAGE_TYPE.PUBLISH:
			  if(wireMessage.pubRecReceived)
				  storedMessage.pubRecReceived = true;

			  // Convert the payload to a hex string.
			  storedMessage.payloadMessage = {};
			  var hex = "";
			  var messageBytes = wireMessage.payloadMessage.payloadBytes;
			  for (var i=0; i<messageBytes.length; i++) {
				if (messageBytes[i] <= 0xF)
				  hex = hex+"0"+messageBytes[i].toString(16);
				else
				  hex = hex+messageBytes[i].toString(16);
			  }
			  storedMessage.payloadMessage.payloadHex = hex;

			  storedMessage.payloadMessage.qos = wireMessage.payloadMessage.qos;
			  storedMessage.payloadMessage.destinationName = wireMessage.payloadMessage.destinationName;
			  if (wireMessage.payloadMessage.duplicate)
				  storedMessage.payloadMessage.duplicate = true;
			  if (wireMessage.payloadMessage.retained)
				  storedMessage.payloadMessage.retained = true;

			  // Add a sequence number to sent messages.
			  if ( prefix.indexOf("Sent:") == 0 ) {
				  if ( wireMessage.sequence === undefined )
					  wireMessage.sequence = ++this._sequence;
				  storedMessage.sequence = wireMessage.sequence;
			  }
			  break;

			default:
				throw Error(format(ERROR.INVALID_STORED_DATA, [key, storedMessage]));
		}
		localStorage.setItem(prefix+this._localKey+wireMessage.messageIdentifier, JSON.stringify(storedMessage));
	};

	ClientImpl.prototype.restore = function(key) {
		var value = localStorage.getItem(key);
		var storedMessage = JSON.parse(value);

		var wireMessage = new WireMessage(storedMessage.type, storedMessage);

		switch(storedMessage.type) {
		  case MESSAGE_TYPE.PUBLISH:
			  // Replace the payload message with a Message object.
			  var hex = storedMessage.payloadMessage.payloadHex;
			  var buffer = new ArrayBuffer((hex.length)/2);
			  var byteStream = new Uint8Array(buffer);
			  var i = 0;
			  while (hex.length >= 2) {
				  var x = parseInt(hex.substring(0, 2), 16);
				  hex = hex.substring(2, hex.length);
				  byteStream[i++] = x;
			  }
			  var payloadMessage = new Message(byteStream);

			  payloadMessage.qos = storedMessage.payloadMessage.qos;
			  payloadMessage.destinationName = storedMessage.payloadMessage.destinationName;
			  if (storedMessage.payloadMessage.duplicate)
				  payloadMessage.duplicate = true;
			  if (storedMessage.payloadMessage.retained)
				  payloadMessage.retained = true;
			  wireMessage.payloadMessage = payloadMessage;

			  break;

			default:
			  throw Error(format(ERROR.INVALID_STORED_DATA, [key, value]));
		}

		if (key.indexOf("Sent:"+this._localKey) == 0) {
			wireMessage.payloadMessage.duplicate = true;
			this._sentMessages[wireMessage.messageIdentifier] = wireMessage;
		} else if (key.indexOf("Received:"+this._localKey) == 0) {
			this._receivedMessages[wireMessage.messageIdentifier] = wireMessage;
		}
	};

	ClientImpl.prototype._process_queue = function () {
		var message = null;
		// Process messages in order they were added
		var fifo = this._msg_queue.reverse();

		// Send all queued messages down socket connection
		while ((message = fifo.pop())) {
			this._socket_send(message);
			// Notify listeners that message was successfully sent
			if (this._notify_msg_sent[message]) {
				this._notify_msg_sent[message]();
				delete this._notify_msg_sent[message];
			}
		}
	};

	/**
	 * Expect an ACK response for this message. Add message to the set of in progress
	 * messages and set an unused identifier in this message.
	 * @ignore
	 */
	ClientImpl.prototype._requires_ack = function (wireMessage) {
		var messageCount = Object.keys(this._sentMessages).length;
		if (messageCount > this.maxMessageIdentifier)
			throw Error ("Too many messages:"+messageCount);

		while(this._sentMessages[this._message_identifier] !== undefined) {
			this._message_identifier++;
		}
		wireMessage.messageIdentifier = this._message_identifier;
		this._sentMessages[wireMessage.messageIdentifier] = wireMessage;
		if (wireMessage.type === MESSAGE_TYPE.PUBLISH) {
			this.store("Sent:", wireMessage);
		}
		if (this._message_identifier === this.maxMessageIdentifier) {
			this._message_identifier = 1;
		}
	};

	/**
	 * Called when the underlying websocket has been opened.
	 * @ignore
	 */
	ClientImpl.prototype._on_socket_open = function () {
		// Create the CONNECT message object.
		var wireMessage = new WireMessage(MESSAGE_TYPE.CONNECT, this.connectOptions);
		wireMessage.clientId = this.clientId;
		this._socket_send(wireMessage);
	};

	/**
	 * Called when the underlying websocket has received a complete packet.
	 * @ignore
	 */
	ClientImpl.prototype._on_socket_message = function (event) {
		this._trace("Client._on_socket_message", event.data);
		// Reset the receive ping timer, we now have evidence the server is alive.
		this.receivePinger.reset();
		var messages = this._deframeMessages(event.data);
		for (var i = 0; i < messages.length; i+=1) {
		    this._handleMessage(messages[i]);
		}
	}

	ClientImpl.prototype._deframeMessages = function(data) {
		var byteArray = new Uint8Array(data);
	    if (this.receiveBuffer) {
	        var newData = new Uint8Array(this.receiveBuffer.length+byteArray.length);
	        newData.set(this.receiveBuffer);
	        newData.set(byteArray,this.receiveBuffer.length);
	        byteArray = newData;
	        delete this.receiveBuffer;
	    }
		try {
		    var offset = 0;
		    var messages = [];
		    while(offset < byteArray.length) {
		        var result = decodeMessage(byteArray,offset);
		        var wireMessage = result[0];
		        offset = result[1];
		        if (wireMessage !== null) {
		            messages.push(wireMessage);
		        } else {
		            break;
		        }
		    }
		    if (offset < byteArray.length) {
		    	this.receiveBuffer = byteArray.subarray(offset);
		    }
		} catch (error) {
			this._disconnected(ERROR.INTERNAL_ERROR.code , format(ERROR.INTERNAL_ERROR, [error.message,error.stack.toString()]));
			return;
		}
		return messages;
	}

	ClientImpl.prototype._handleMessage = function(wireMessage) {

		this._trace("Client._handleMessage", wireMessage);

		try {
			switch(wireMessage.type) {
			case MESSAGE_TYPE.CONNACK:
				this._connectTimeout.cancel();

				// If we have started using clean session then clear up the local state.
				if (this.connectOptions.cleanSession) {
					for (var key in this._sentMessages) {
						var sentMessage = this._sentMessages[key];
						localStorage.removeItem("Sent:"+this._localKey+sentMessage.messageIdentifier);
					}
					this._sentMessages = {};

					for (var key in this._receivedMessages) {
						var receivedMessage = this._receivedMessages[key];
						localStorage.removeItem("Received:"+this._localKey+receivedMessage.messageIdentifier);
					}
					this._receivedMessages = {};
				}
				// Client connected and ready for business.
				if (wireMessage.returnCode === 0) {
					this.connected = true;
					// Jump to the end of the list of uris and stop looking for a good host.
					if (this.connectOptions.uris)
						this.hostIndex = this.connectOptions.uris.length;
				} else {
					this._disconnected(ERROR.CONNACK_RETURNCODE.code , format(ERROR.CONNACK_RETURNCODE, [wireMessage.returnCode, CONNACK_RC[wireMessage.returnCode]]));
					break;
				}

				// Resend messages.
				var sequencedMessages = new Array();
				for (var msgId in this._sentMessages) {
					if (this._sentMessages.hasOwnProperty(msgId))
						sequencedMessages.push(this._sentMessages[msgId]);
				}

				// Sort sentMessages into the original sent order.
				var sequencedMessages = sequencedMessages.sort(function(a,b) {return a.sequence - b.sequence;} );
				for (var i=0, len=sequencedMessages.length; i<len; i++) {
					var sentMessage = sequencedMessages[i];
					if (sentMessage.type == MESSAGE_TYPE.PUBLISH && sentMessage.pubRecReceived) {
						var pubRelMessage = new WireMessage(MESSAGE_TYPE.PUBREL, {messageIdentifier:sentMessage.messageIdentifier});
						this._schedule_message(pubRelMessage);
					} else {
						this._schedule_message(sentMessage);
					};
				}

				// Execute the connectOptions.onSuccess callback if there is one.
				if (this.connectOptions.onSuccess) {
					this.connectOptions.onSuccess({invocationContext:this.connectOptions.invocationContext});
				}

				// Process all queued messages now that the connection is established.
				this._process_queue();
				break;

			case MESSAGE_TYPE.PUBLISH:
				this._receivePublish(wireMessage);
				break;

			case MESSAGE_TYPE.PUBACK:
				var sentMessage = this._sentMessages[wireMessage.messageIdentifier];
				 // If this is a re flow of a PUBACK after we have restarted receivedMessage will not exist.
				if (sentMessage) {
					delete this._sentMessages[wireMessage.messageIdentifier];
					localStorage.removeItem("Sent:"+this._localKey+wireMessage.messageIdentifier);
					if (this.onMessageDelivered)
						this.onMessageDelivered(sentMessage.payloadMessage);
				}
				break;

			case MESSAGE_TYPE.PUBREC:
				var sentMessage = this._sentMessages[wireMessage.messageIdentifier];
				// If this is a re flow of a PUBREC after we have restarted receivedMessage will not exist.
				if (sentMessage) {
					sentMessage.pubRecReceived = true;
					var pubRelMessage = new WireMessage(MESSAGE_TYPE.PUBREL, {messageIdentifier:wireMessage.messageIdentifier});
					this.store("Sent:", sentMessage);
					this._schedule_message(pubRelMessage);
				}
				break;

			case MESSAGE_TYPE.PUBREL:
				var receivedMessage = this._receivedMessages[wireMessage.messageIdentifier];
				localStorage.removeItem("Received:"+this._localKey+wireMessage.messageIdentifier);
				// If this is a re flow of a PUBREL after we have restarted receivedMessage will not exist.
				if (receivedMessage) {
					this._receiveMessage(receivedMessage);
					delete this._receivedMessages[wireMessage.messageIdentifier];
				}
				// Always flow PubComp, we may have previously flowed PubComp but the server lost it and restarted.
				var pubCompMessage = new WireMessage(MESSAGE_TYPE.PUBCOMP, {messageIdentifier:wireMessage.messageIdentifier});
				this._schedule_message(pubCompMessage);
				break;

			case MESSAGE_TYPE.PUBCOMP:
				var sentMessage = this._sentMessages[wireMessage.messageIdentifier];
				delete this._sentMessages[wireMessage.messageIdentifier];
				localStorage.removeItem("Sent:"+this._localKey+wireMessage.messageIdentifier);
				if (this.onMessageDelivered)
					this.onMessageDelivered(sentMessage.payloadMessage);
				break;

			case MESSAGE_TYPE.SUBACK:
				var sentMessage = this._sentMessages[wireMessage.messageIdentifier];
				if (sentMessage) {
					if(sentMessage.timeOut)
						sentMessage.timeOut.cancel();
					wireMessage.returnCode.indexOf = Array.prototype.indexOf;
					if (wireMessage.returnCode.indexOf(0x80) !== -1) {
						if (sentMessage.onFailure) {
							sentMessage.onFailure(wireMessage.returnCode);
						}
					} else if (sentMessage.onSuccess) {
						sentMessage.onSuccess(wireMessage.returnCode);
					}
					delete this._sentMessages[wireMessage.messageIdentifier];
				}
				break;

			case MESSAGE_TYPE.UNSUBACK:
				var sentMessage = this._sentMessages[wireMessage.messageIdentifier];
				if (sentMessage) {
					if (sentMessage.timeOut)
						sentMessage.timeOut.cancel();
					if (sentMessage.callback) {
						sentMessage.callback();
					}
					delete this._sentMessages[wireMessage.messageIdentifier];
				}

				break;

			case MESSAGE_TYPE.PINGRESP:
				/* The sendPinger or receivePinger may have sent a ping, the receivePinger has already been reset. */
				this.sendPinger.reset();
				break;

			case MESSAGE_TYPE.DISCONNECT:
				// Clients do not expect to receive disconnect packets.
				this._disconnected(ERROR.INVALID_MQTT_MESSAGE_TYPE.code , format(ERROR.INVALID_MQTT_MESSAGE_TYPE, [wireMessage.type]));
				break;

			default:
				this._disconnected(ERROR.INVALID_MQTT_MESSAGE_TYPE.code , format(ERROR.INVALID_MQTT_MESSAGE_TYPE, [wireMessage.type]));
			};
		} catch (error) {
			this._disconnected(ERROR.INTERNAL_ERROR.code , format(ERROR.INTERNAL_ERROR, [error.message,error.stack.toString()]));
			return;
		}
	};

	/** @ignore */
	ClientImpl.prototype._on_socket_error = function (error) {
		this._disconnected(ERROR.SOCKET_ERROR.code , format(ERROR.SOCKET_ERROR, [error.data]));
	};

	/** @ignore */
	ClientImpl.prototype._on_socket_close = function () {
		this._disconnected(ERROR.SOCKET_CLOSE.code , format(ERROR.SOCKET_CLOSE));
	};

	/** @ignore */
	ClientImpl.prototype._socket_send = function (wireMessage) {

		if (wireMessage.type == 1) {
			var wireMessageMasked = this._traceMask(wireMessage, "password");
			this._trace("Client._socket_send", wireMessageMasked);
		}
		else this._trace("Client._socket_send", wireMessage);

		this.socket.send(wireMessage.encode());
		/* We have proved to the server we are alive. */
		this.sendPinger.reset();
	};

	/** @ignore */
	ClientImpl.prototype._receivePublish = function (wireMessage) {
		switch(wireMessage.payloadMessage.qos) {
			case "undefined":
			case 0:
				this._receiveMessage(wireMessage);
				break;

			case 1:
				var pubAckMessage = new WireMessage(MESSAGE_TYPE.PUBACK, {messageIdentifier:wireMessage.messageIdentifier});
				this._schedule_message(pubAckMessage);
				this._receiveMessage(wireMessage);
				break;

			case 2:
				this._receivedMessages[wireMessage.messageIdentifier] = wireMessage;
				this.store("Received:", wireMessage);
				var pubRecMessage = new WireMessage(MESSAGE_TYPE.PUBREC, {messageIdentifier:wireMessage.messageIdentifier});
				this._schedule_message(pubRecMessage);

				break;

			default:
				throw Error("Invaild qos="+wireMmessage.payloadMessage.qos);
		};
	};

	/** @ignore */
	ClientImpl.prototype._receiveMessage = function (wireMessage) {
		if (this.onMessageArrived) {
			this.onMessageArrived(wireMessage.payloadMessage);
		}
	};

	/**
	 * Client has disconnected either at its own request or because the server
	 * or network disconnected it. Remove all non-durable state.
	 * @param {errorCode} [number] the error number.
	 * @param {errorText} [string] the error text.
	 * @ignore
	 */
	ClientImpl.prototype._disconnected = function (errorCode, errorText) {
		this._trace("Client._disconnected", errorCode, errorText);

		this.sendPinger.cancel();
		this.receivePinger.cancel();
		if (this._connectTimeout)
			this._connectTimeout.cancel();
		// Clear message buffers.
		this._msg_queue = [];
		this._notify_msg_sent = {};

		if (this.socket) {
			// Cancel all socket callbacks so that they cannot be driven again by this socket.
			this.socket.onopen = null;
			this.socket.onmessage = null;
			this.socket.onerror = null;
			this.socket.onclose = null;
			if (this.socket.readyState === 1)
				this.socket.close();
			delete this.socket;
		}

		if (this.connectOptions.uris && this.hostIndex < this.connectOptions.uris.length-1) {
			// Try the next host.
			this.hostIndex++;
			this._doConnect(this.connectOptions.uris[this.hostIndex]);

		} else {

			if (errorCode === undefined) {
				errorCode = ERROR.OK.code;
				errorText = format(ERROR.OK);
			}

			// Run any application callbacks last as they may attempt to reconnect and hence create a new socket.
			if (this.connected) {
				this.connected = false;
				// Execute the connectionLostCallback if there is one, and we were connected.
				if (this.onConnectionLost)
					this.onConnectionLost({errorCode:errorCode, errorMessage:errorText});
			} else {
				// Otherwise we never had a connection, so indicate that the connect has failed.
				if (this.connectOptions.mqttVersion === 4 && this.connectOptions.mqttVersionExplicit === false) {
					this._trace("Failed to connect V4, dropping back to V3")
					this.connectOptions.mqttVersion = 3;
					if (this.connectOptions.uris) {
						this.hostIndex = 0;
						this._doConnect(this.connectOptions.uris[0]);
					} else {
						this._doConnect(this.uri);
					}
				} else if(this.connectOptions.onFailure) {
					this.connectOptions.onFailure({invocationContext:this.connectOptions.invocationContext, errorCode:errorCode, errorMessage:errorText});
				}
			}
		}
	};

	/** @ignore */
	ClientImpl.prototype._trace = function () {
		// Pass trace message back to client's callback function
		if (this.traceFunction) {
			for (var i in arguments)
			{
				if (typeof arguments[i] !== "undefined")
					arguments[i] = JSON.stringify(arguments[i]);
			}
			var record = Array.prototype.slice.call(arguments).join("");
			this.traceFunction ({severity: "Debug", message: record	});
		}

		//buffer style trace
		if ( this._traceBuffer !== null ) {
			for (var i = 0, max = arguments.length; i < max; i++) {
				if ( this._traceBuffer.length == this._MAX_TRACE_ENTRIES ) {
					this._traceBuffer.shift();
				}
				if (i === 0) this._traceBuffer.push(arguments[i]);
				else if (typeof arguments[i] === "undefined" ) this._traceBuffer.push(arguments[i]);
				else this._traceBuffer.push("  "+JSON.stringify(arguments[i]));
		   };
		};
	};

	/** @ignore */
	ClientImpl.prototype._traceMask = function (traceObject, masked) {
		var traceObjectMasked = {};
		for (var attr in traceObject) {
			if (traceObject.hasOwnProperty(attr)) {
				if (attr == masked)
					traceObjectMasked[attr] = "******";
				else
					traceObjectMasked[attr] = traceObject[attr];
			}
		}
		return traceObjectMasked;
	};

	// ------------------------------------------------------------------------
	// Public Programming interface.
	// ------------------------------------------------------------------------

	/**
	 * The JavaScript application communicates to the server using a {@link Paho.MQTT.Client} object.
	 * <p>
	 * Most applications will create just one Client object and then call its connect() method,
	 * however applications can create more than one Client object if they wish.
	 * In this case the combination of host, port and clientId attributes must be different for each Client object.
	 * <p>
	 * The send, subscribe and unsubscribe methods are implemented as asynchronous JavaScript methods
	 * (even though the underlying protocol exchange might be synchronous in nature).
	 * This means they signal their completion by calling back to the application,
	 * via Success or Failure callback functions provided by the application on the method in question.
	 * Such callbacks are called at most once per method invocation and do not persist beyond the lifetime
	 * of the script that made the invocation.
	 * <p>
	 * In contrast there are some callback functions, most notably <i>onMessageArrived</i>,
	 * that are defined on the {@link Paho.MQTT.Client} object.
	 * These may get called multiple times, and aren't directly related to specific method invocations made by the client.
	 *
	 * @name Paho.MQTT.Client
	 *
	 * @constructor
	 *
	 * @param {string} host - the address of the messaging server, as a fully qualified WebSocket URI, as a DNS name or dotted decimal IP address.
	 * @param {number} port - the port number to connect to - only required if host is not a URI
	 * @param {string} path - the path on the host to connect to - only used if host is not a URI. Default: '/mqtt'.
	 * @param {string} clientId - the Messaging client identifier, between 1 and 23 characters in length.
	 *
	 * @property {string} host - <i>read only</i> the server's DNS hostname or dotted decimal IP address.
	 * @property {number} port - <i>read only</i> the server's port.
	 * @property {string} path - <i>read only</i> the server's path.
	 * @property {string} clientId - <i>read only</i> used when connecting to the server.
	 * @property {function} onConnectionLost - called when a connection has been lost.
	 *                            after a connect() method has succeeded.
	 *                            Establish the call back used when a connection has been lost. The connection may be
	 *                            lost because the client initiates a disconnect or because the server or network
	 *                            cause the client to be disconnected. The disconnect call back may be called without
	 *                            the connectionComplete call back being invoked if, for example the client fails to
	 *                            connect.
	 *                            A single response object parameter is passed to the onConnectionLost callback containing the following fields:
	 *                            <ol>
	 *                            <li>errorCode
	 *                            <li>errorMessage
	 *                            </ol>
	 * @property {function} onMessageDelivered called when a message has been delivered.
	 *                            All processing that this Client will ever do has been completed. So, for example,
	 *                            in the case of a Qos=2 message sent by this client, the PubComp flow has been received from the server
	 *                            and the message has been removed from persistent storage before this callback is invoked.
	 *                            Parameters passed to the onMessageDelivered callback are:
	 *                            <ol>
	 *                            <li>{@link Paho.MQTT.Message} that was delivered.
	 *                            </ol>
	 * @property {function} onMessageArrived called when a message has arrived in this Paho.MQTT.client.
	 *                            Parameters passed to the onMessageArrived callback are:
	 *                            <ol>
	 *                            <li>{@link Paho.MQTT.Message} that has arrived.
	 *                            </ol>
	 */
	var Client = function (host, port, path, clientId) {

	    var uri;

		if (typeof host !== "string")
			throw new Error(format(ERROR.INVALID_TYPE, [typeof host, "host"]));

	    if (arguments.length == 2) {
	        // host: must be full ws:// uri
	        // port: clientId
	        clientId = port;
	        uri = host;
	        var match = uri.match(/^(wss?):\/\/((\[(.+)\])|([^\/]+?))(:(\d+))?(\/.*)$/);
	        if (match) {
	            host = match[4]||match[2];
	            port = parseInt(match[7]);
	            path = match[8];
	        } else {
	            throw new Error(format(ERROR.INVALID_ARGUMENT,[host,"host"]));
	        }
	    } else {
	        if (arguments.length == 3) {
				clientId = path;
				path = "/mqtt";
			}
			if (typeof port !== "number" || port < 0)
				throw new Error(format(ERROR.INVALID_TYPE, [typeof port, "port"]));
			if (typeof path !== "string")
				throw new Error(format(ERROR.INVALID_TYPE, [typeof path, "path"]));

			var ipv6AddSBracket = (host.indexOf(":") != -1 && host.slice(0,1) != "[" && host.slice(-1) != "]");
			uri = "ws://"+(ipv6AddSBracket?"["+host+"]":host)+":"+port+path;
		}

		var clientIdLength = 0;
		for (var i = 0; i<clientId.length; i++) {
			var charCode = clientId.charCodeAt(i);
			if (0xD800 <= charCode && charCode <= 0xDBFF)  {
				 i++; // Surrogate pair.
			}
			clientIdLength++;
		}
		if (typeof clientId !== "string" || clientIdLength > 65535)
			throw new Error(format(ERROR.INVALID_ARGUMENT, [clientId, "clientId"]));

		var client = new ClientImpl(uri, host, port, path, clientId);
		this._getHost =  function() { return host; };
		this._setHost = function() { throw new Error(format(ERROR.UNSUPPORTED_OPERATION)); };

		this._getPort = function() { return port; };
		this._setPort = function() { throw new Error(format(ERROR.UNSUPPORTED_OPERATION)); };

		this._getPath = function() { return path; };
		this._setPath = function() { throw new Error(format(ERROR.UNSUPPORTED_OPERATION)); };

		this._getURI = function() { return uri; };
		this._setURI = function() { throw new Error(format(ERROR.UNSUPPORTED_OPERATION)); };

		this._getClientId = function() { return client.clientId; };
		this._setClientId = function() { throw new Error(format(ERROR.UNSUPPORTED_OPERATION)); };

		this._getOnConnectionLost = function() { return client.onConnectionLost; };
		this._setOnConnectionLost = function(newOnConnectionLost) {
			if (typeof newOnConnectionLost === "function")
				client.onConnectionLost = newOnConnectionLost;
			else
				throw new Error(format(ERROR.INVALID_TYPE, [typeof newOnConnectionLost, "onConnectionLost"]));
		};

		this._getOnMessageDelivered = function() { return client.onMessageDelivered; };
		this._setOnMessageDelivered = function(newOnMessageDelivered) {
			if (typeof newOnMessageDelivered === "function")
				client.onMessageDelivered = newOnMessageDelivered;
			else
				throw new Error(format(ERROR.INVALID_TYPE, [typeof newOnMessageDelivered, "onMessageDelivered"]));
		};

		this._getOnMessageArrived = function() { return client.onMessageArrived; };
		this._setOnMessageArrived = function(newOnMessageArrived) {
			if (typeof newOnMessageArrived === "function")
				client.onMessageArrived = newOnMessageArrived;
			else
				throw new Error(format(ERROR.INVALID_TYPE, [typeof newOnMessageArrived, "onMessageArrived"]));
		};

		this._getTrace = function() { return client.traceFunction; };
		this._setTrace = function(trace) {
			if(typeof trace === "function"){
				client.traceFunction = trace;
			}else{
				throw new Error(format(ERROR.INVALID_TYPE, [typeof trace, "onTrace"]));
			}
		};

		/**
		 * Connect this Messaging client to its server.
		 *
		 * @name Paho.MQTT.Client#connect
		 * @function
		 * @param {Object} connectOptions - attributes used with the connection.
		 * @param {number} connectOptions.timeout - If the connect has not succeeded within this
		 *                    number of seconds, it is deemed to have failed.
		 *                    The default is 30 seconds.
		 * @param {string} connectOptions.userName - Authentication username for this connection.
		 * @param {string} connectOptions.password - Authentication password for this connection.
		 * @param {Paho.MQTT.Message} connectOptions.willMessage - sent by the server when the client
		 *                    disconnects abnormally.
		 * @param {Number} connectOptions.keepAliveInterval - the server disconnects this client if
		 *                    there is no activity for this number of seconds.
		 *                    The default value of 60 seconds is assumed if not set.
		 * @param {boolean} connectOptions.cleanSession - if true(default) the client and server
		 *                    persistent state is deleted on successful connect.
		 * @param {boolean} connectOptions.useSSL - if present and true, use an SSL Websocket connection.
		 * @param {object} connectOptions.invocationContext - passed to the onSuccess callback or onFailure callback.
		 * @param {function} connectOptions.onSuccess - called when the connect acknowledgement
		 *                    has been received from the server.
		 * A single response object parameter is passed to the onSuccess callback containing the following fields:
		 * <ol>
		 * <li>invocationContext as passed in to the onSuccess method in the connectOptions.
		 * </ol>
		 * @config {function} [onFailure] called when the connect request has failed or timed out.
		 * A single response object parameter is passed to the onFailure callback containing the following fields:
		 * <ol>
		 * <li>invocationContext as passed in to the onFailure method in the connectOptions.
		 * <li>errorCode a number indicating the nature of the error.
		 * <li>errorMessage text describing the error.
		 * </ol>
		 * @config {Array} [hosts] If present this contains either a set of hostnames or fully qualified
		 * WebSocket URIs (ws://example.com:1883/mqtt), that are tried in order in place
		 * of the host and port paramater on the construtor. The hosts are tried one at at time in order until
		 * one of then succeeds.
		 * @config {Array} [ports] If present the set of ports matching the hosts. If hosts contains URIs, this property
		 * is not used.
		 * @throws {InvalidState} if the client is not in disconnected state. The client must have received connectionLost
		 * or disconnected before calling connect for a second or subsequent time.
		 */
		this.connect = function (connectOptions) {
			connectOptions = connectOptions || {} ;
			validate(connectOptions,  {timeout:"number",
									   userName:"string",
									   password:"string",
									   willMessage:"object",
									   keepAliveInterval:"number",
									   cleanSession:"boolean",
									   useSSL:"boolean",
									   invocationContext:"object",
									   onSuccess:"function",
									   onFailure:"function",
									   hosts:"object",
									   ports:"object",
									   mqttVersion:"number"});

			// If no keep alive interval is set, assume 60 seconds.
			if (connectOptions.keepAliveInterval === undefined)
				connectOptions.keepAliveInterval = 60;

			if (connectOptions.mqttVersion > 4 || connectOptions.mqttVersion < 3) {
				throw new Error(format(ERROR.INVALID_ARGUMENT, [connectOptions.mqttVersion, "connectOptions.mqttVersion"]));
			}

			if (connectOptions.mqttVersion === undefined) {
				connectOptions.mqttVersionExplicit = false;
				connectOptions.mqttVersion = 4;
			} else {
				connectOptions.mqttVersionExplicit = true;
			}

			//Check that if password is set, so is username
			if (connectOptions.password === undefined && connectOptions.userName !== undefined)
				throw new Error(format(ERROR.INVALID_ARGUMENT, [connectOptions.password, "connectOptions.password"]))

			if (connectOptions.willMessage) {
				if (!(connectOptions.willMessage instanceof Message))
					throw new Error(format(ERROR.INVALID_TYPE, [connectOptions.willMessage, "connectOptions.willMessage"]));
				// The will message must have a payload that can be represented as a string.
				// Cause the willMessage to throw an exception if this is not the case.
				connectOptions.willMessage.stringPayload;

				if (typeof connectOptions.willMessage.destinationName === "undefined")
					throw new Error(format(ERROR.INVALID_TYPE, [typeof connectOptions.willMessage.destinationName, "connectOptions.willMessage.destinationName"]));
			}
			if (typeof connectOptions.cleanSession === "undefined")
				connectOptions.cleanSession = true;
			if (connectOptions.hosts) {

				if (!(connectOptions.hosts instanceof Array) )
					throw new Error(format(ERROR.INVALID_ARGUMENT, [connectOptions.hosts, "connectOptions.hosts"]));
				if (connectOptions.hosts.length <1 )
					throw new Error(format(ERROR.INVALID_ARGUMENT, [connectOptions.hosts, "connectOptions.hosts"]));

				var usingURIs = false;
				for (var i = 0; i<connectOptions.hosts.length; i++) {
					if (typeof connectOptions.hosts[i] !== "string")
						throw new Error(format(ERROR.INVALID_TYPE, [typeof connectOptions.hosts[i], "connectOptions.hosts["+i+"]"]));
					if (/^(wss?):\/\/((\[(.+)\])|([^\/]+?))(:(\d+))?(\/.*)$/.test(connectOptions.hosts[i])) {
						if (i == 0) {
							usingURIs = true;
						} else if (!usingURIs) {
							throw new Error(format(ERROR.INVALID_ARGUMENT, [connectOptions.hosts[i], "connectOptions.hosts["+i+"]"]));
						}
					} else if (usingURIs) {
						throw new Error(format(ERROR.INVALID_ARGUMENT, [connectOptions.hosts[i], "connectOptions.hosts["+i+"]"]));
					}
				}

				if (!usingURIs) {
					if (!connectOptions.ports)
						throw new Error(format(ERROR.INVALID_ARGUMENT, [connectOptions.ports, "connectOptions.ports"]));
					if (!(connectOptions.ports instanceof Array) )
						throw new Error(format(ERROR.INVALID_ARGUMENT, [connectOptions.ports, "connectOptions.ports"]));
					if (connectOptions.hosts.length != connectOptions.ports.length)
						throw new Error(format(ERROR.INVALID_ARGUMENT, [connectOptions.ports, "connectOptions.ports"]));

					connectOptions.uris = [];

					for (var i = 0; i<connectOptions.hosts.length; i++) {
						if (typeof connectOptions.ports[i] !== "number" || connectOptions.ports[i] < 0)
							throw new Error(format(ERROR.INVALID_TYPE, [typeof connectOptions.ports[i], "connectOptions.ports["+i+"]"]));
						var host = connectOptions.hosts[i];
						var port = connectOptions.ports[i];

						var ipv6 = (host.indexOf(":") != -1);
						uri = "ws://"+(ipv6?"["+host+"]":host)+":"+port+path;
						connectOptions.uris.push(uri);
					}
				} else {
					connectOptions.uris = connectOptions.hosts;
				}
			}

			client.connect(connectOptions);
		};

		/**
		 * Subscribe for messages, request receipt of a copy of messages sent to the destinations described by the filter.
		 *
		 * @name Paho.MQTT.Client#subscribe
		 * @function
		 * @param {string} filter describing the destinations to receive messages from.
		 * <br>
		 * @param {object} subscribeOptions - used to control the subscription
		 *
		 * @param {number} subscribeOptions.qos - the maiximum qos of any publications sent
		 *                                  as a result of making this subscription.
		 * @param {object} subscribeOptions.invocationContext - passed to the onSuccess callback
		 *                                  or onFailure callback.
		 * @param {function} subscribeOptions.onSuccess - called when the subscribe acknowledgement
		 *                                  has been received from the server.
		 *                                  A single response object parameter is passed to the onSuccess callback containing the following fields:
		 *                                  <ol>
		 *                                  <li>invocationContext if set in the subscribeOptions.
		 *                                  </ol>
		 * @param {function} subscribeOptions.onFailure - called when the subscribe request has failed or timed out.
		 *                                  A single response object parameter is passed to the onFailure callback containing the following fields:
		 *                                  <ol>
		 *                                  <li>invocationContext - if set in the subscribeOptions.
		 *                                  <li>errorCode - a number indicating the nature of the error.
		 *                                  <li>errorMessage - text describing the error.
		 *                                  </ol>
		 * @param {number} subscribeOptions.timeout - which, if present, determines the number of
		 *                                  seconds after which the onFailure calback is called.
		 *                                  The presence of a timeout does not prevent the onSuccess
		 *                                  callback from being called when the subscribe completes.
		 * @throws {InvalidState} if the client is not in connected state.
		 */
		this.subscribe = function (filter, subscribeOptions) {
			if (typeof filter !== "string")
				throw new Error("Invalid argument:"+filter);
			subscribeOptions = subscribeOptions || {} ;
			validate(subscribeOptions,  {qos:"number",
										 invocationContext:"object",
										 onSuccess:"function",
										 onFailure:"function",
										 timeout:"number"
										});
			if (subscribeOptions.timeout && !subscribeOptions.onFailure)
				throw new Error("subscribeOptions.timeout specified with no onFailure callback.");
			if (typeof subscribeOptions.qos !== "undefined"
				&& !(subscribeOptions.qos === 0 || subscribeOptions.qos === 1 || subscribeOptions.qos === 2 ))
				throw new Error(format(ERROR.INVALID_ARGUMENT, [subscribeOptions.qos, "subscribeOptions.qos"]));
			client.subscribe(filter, subscribeOptions);
		};

		/**
		 * Unsubscribe for messages, stop receiving messages sent to destinations described by the filter.
		 *
		 * @name Paho.MQTT.Client#unsubscribe
		 * @function
		 * @param {string} filter - describing the destinations to receive messages from.
		 * @param {object} unsubscribeOptions - used to control the subscription
		 * @param {object} unsubscribeOptions.invocationContext - passed to the onSuccess callback
		                                      or onFailure callback.
		 * @param {function} unsubscribeOptions.onSuccess - called when the unsubscribe acknowledgement has been received from the server.
		 *                                    A single response object parameter is passed to the
		 *                                    onSuccess callback containing the following fields:
		 *                                    <ol>
		 *                                    <li>invocationContext - if set in the unsubscribeOptions.
		 *                                    </ol>
		 * @param {function} unsubscribeOptions.onFailure called when the unsubscribe request has failed or timed out.
		 *                                    A single response object parameter is passed to the onFailure callback containing the following fields:
		 *                                    <ol>
		 *                                    <li>invocationContext - if set in the unsubscribeOptions.
		 *                                    <li>errorCode - a number indicating the nature of the error.
		 *                                    <li>errorMessage - text describing the error.
		 *                                    </ol>
		 * @param {number} unsubscribeOptions.timeout - which, if present, determines the number of seconds
		 *                                    after which the onFailure callback is called. The presence of
		 *                                    a timeout does not prevent the onSuccess callback from being
		 *                                    called when the unsubscribe completes
		 * @throws {InvalidState} if the client is not in connected state.
		 */
		this.unsubscribe = function (filter, unsubscribeOptions) {
			if (typeof filter !== "string")
				throw new Error("Invalid argument:"+filter);
			unsubscribeOptions = unsubscribeOptions || {} ;
			validate(unsubscribeOptions,  {invocationContext:"object",
										   onSuccess:"function",
										   onFailure:"function",
										   timeout:"number"
										  });
			if (unsubscribeOptions.timeout && !unsubscribeOptions.onFailure)
				throw new Error("unsubscribeOptions.timeout specified with no onFailure callback.");
			client.unsubscribe(filter, unsubscribeOptions);
		};

		/**
		 * Send a message to the consumers of the destination in the Message.
		 *
		 * @name Paho.MQTT.Client#send
		 * @function
		 * @param {string|Paho.MQTT.Message} topic - <b>mandatory</b> The name of the destination to which the message is to be sent.
		 * 					   - If it is the only parameter, used as Paho.MQTT.Message object.
		 * @param {String|ArrayBuffer} payload - The message data to be sent.
		 * @param {number} qos The Quality of Service used to deliver the message.
		 * 		<dl>
		 * 			<dt>0 Best effort (default).
		 *     			<dt>1 At least once.
		 *     			<dt>2 Exactly once.
		 * 		</dl>
		 * @param {Boolean} retained If true, the message is to be retained by the server and delivered
		 *                     to both current and future subscriptions.
		 *                     If false the server only delivers the message to current subscribers, this is the default for new Messages.
		 *                     A received message has the retained boolean set to true if the message was published
		 *                     with the retained boolean set to true
		 *                     and the subscrption was made after the message has been published.
		 * @throws {InvalidState} if the client is not connected.
		 */
		this.send = function (topic,payload,qos,retained) {
			var message ;

			if(arguments.length == 0){
				throw new Error("Invalid argument."+"length");

			}else if(arguments.length == 1) {

				if (!(topic instanceof Message) && (typeof topic !== "string"))
					throw new Error("Invalid argument:"+ typeof topic);

				message = topic;
				if (typeof message.destinationName === "undefined")
					throw new Error(format(ERROR.INVALID_ARGUMENT,[message.destinationName,"Message.destinationName"]));
				client.send(message);

			}else {
				//parameter checking in Message object
				message = new Message(payload);
				message.destinationName = topic;
				if(arguments.length >= 3)
					message.qos = qos;
				if(arguments.length >= 4)
					message.retained = retained;
				client.send(message);
			}
		};

		/**
		 * Normal disconnect of this Messaging client from its server.
		 *
		 * @name Paho.MQTT.Client#disconnect
		 * @function
		 * @throws {InvalidState} if the client is already disconnected.
		 */
		this.disconnect = function () {
			client.disconnect();
		};

		/**
		 * Get the contents of the trace log.
		 *
		 * @name Paho.MQTT.Client#getTraceLog
		 * @function
		 * @return {Object[]} tracebuffer containing the time ordered trace records.
		 */
		this.getTraceLog = function () {
			return client.getTraceLog();
		}

		/**
		 * Start tracing.
		 *
		 * @name Paho.MQTT.Client#startTrace
		 * @function
		 */
		this.startTrace = function () {
			client.startTrace();
		};

		/**
		 * Stop tracing.
		 *
		 * @name Paho.MQTT.Client#stopTrace
		 * @function
		 */
		this.stopTrace = function () {
			client.stopTrace();
		};

		this.isConnected = function() {
			return client.connected;
		};
	};

	Client.prototype = {
		get host() { return this._getHost(); },
		set host(newHost) { this._setHost(newHost); },

		get port() { return this._getPort(); },
		set port(newPort) { this._setPort(newPort); },

		get path() { return this._getPath(); },
		set path(newPath) { this._setPath(newPath); },

		get clientId() { return this._getClientId(); },
		set clientId(newClientId) { this._setClientId(newClientId); },

		get onConnectionLost() { return this._getOnConnectionLost(); },
		set onConnectionLost(newOnConnectionLost) { this._setOnConnectionLost(newOnConnectionLost); },

		get onMessageDelivered() { return this._getOnMessageDelivered(); },
		set onMessageDelivered(newOnMessageDelivered) { this._setOnMessageDelivered(newOnMessageDelivered); },

		get onMessageArrived() { return this._getOnMessageArrived(); },
		set onMessageArrived(newOnMessageArrived) { this._setOnMessageArrived(newOnMessageArrived); },

		get trace() { return this._getTrace(); },
		set trace(newTraceFunction) { this._setTrace(newTraceFunction); }

	};

	/**
	 * An application message, sent or received.
	 * <p>
	 * All attributes may be null, which implies the default values.
	 *
	 * @name Paho.MQTT.Message
	 * @constructor
	 * @param {String|ArrayBuffer} payload The message data to be sent.
	 * <p>
	 * @property {string} payloadString <i>read only</i> The payload as a string if the payload consists of valid UTF-8 characters.
	 * @property {ArrayBuffer} payloadBytes <i>read only</i> The payload as an ArrayBuffer.
	 * <p>
	 * @property {string} destinationName <b>mandatory</b> The name of the destination to which the message is to be sent
	 *                    (for messages about to be sent) or the name of the destination from which the message has been received.
	 *                    (for messages received by the onMessage function).
	 * <p>
	 * @property {number} qos The Quality of Service used to deliver the message.
	 * <dl>
	 *     <dt>0 Best effort (default).
	 *     <dt>1 At least once.
	 *     <dt>2 Exactly once.
	 * </dl>
	 * <p>
	 * @property {Boolean} retained If true, the message is to be retained by the server and delivered
	 *                     to both current and future subscriptions.
	 *                     If false the server only delivers the message to current subscribers, this is the default for new Messages.
	 *                     A received message has the retained boolean set to true if the message was published
	 *                     with the retained boolean set to true
	 *                     and the subscrption was made after the message has been published.
	 * <p>
	 * @property {Boolean} duplicate <i>read only</i> If true, this message might be a duplicate of one which has already been received.
	 *                     This is only set on messages received from the server.
	 *
	 */
	var Message = function (newPayload) {
		var payload;
		if (   typeof newPayload === "string"
			|| newPayload instanceof ArrayBuffer
			|| newPayload instanceof Int8Array
			|| newPayload instanceof Uint8Array
			|| newPayload instanceof Int16Array
			|| newPayload instanceof Uint16Array
			|| newPayload instanceof Int32Array
			|| newPayload instanceof Uint32Array
			|| newPayload instanceof Float32Array
			|| newPayload instanceof Float64Array
		   ) {
			payload = newPayload;
		} else {
			throw (format(ERROR.INVALID_ARGUMENT, [newPayload, "newPayload"]));
		}

		this._getPayloadString = function () {
			if (typeof payload === "string")
				return payload;
			else
				return parseUTF8(payload, 0, payload.length);
		};

		this._getPayloadBytes = function() {
			if (typeof payload === "string") {
				var buffer = new ArrayBuffer(UTF8Length(payload));
				var byteStream = new Uint8Array(buffer);
				stringToUTF8(payload, byteStream, 0);

				return byteStream;
			} else {
				return payload;
			};
		};

		var destinationName = undefined;
		this._getDestinationName = function() { return destinationName; };
		this._setDestinationName = function(newDestinationName) {
			if (typeof newDestinationName === "string")
				destinationName = newDestinationName;
			else
				throw new Error(format(ERROR.INVALID_ARGUMENT, [newDestinationName, "newDestinationName"]));
		};

		var qos = 0;
		this._getQos = function() { return qos; };
		this._setQos = function(newQos) {
			if (newQos === 0 || newQos === 1 || newQos === 2 )
				qos = newQos;
			else
				throw new Error("Invalid argument:"+newQos);
		};

		var retained = false;
		this._getRetained = function() { return retained; };
		this._setRetained = function(newRetained) {
			if (typeof newRetained === "boolean")
				retained = newRetained;
			else
				throw new Error(format(ERROR.INVALID_ARGUMENT, [newRetained, "newRetained"]));
		};

		var duplicate = false;
		this._getDuplicate = function() { return duplicate; };
		this._setDuplicate = function(newDuplicate) { duplicate = newDuplicate; };
	};

	Message.prototype = {
		get payloadString() { return this._getPayloadString(); },
		get payloadBytes() { return this._getPayloadBytes(); },

		get destinationName() { return this._getDestinationName(); },
		set destinationName(newDestinationName) { this._setDestinationName(newDestinationName); },

		get qos() { return this._getQos(); },
		set qos(newQos) { this._setQos(newQos); },

		get retained() { return this._getRetained(); },
		set retained(newRetained) { this._setRetained(newRetained); },

		get duplicate() { return this._getDuplicate(); },
		set duplicate(newDuplicate) { this._setDuplicate(newDuplicate); }
	};

	// Module contents.
	return {
		Client: Client,
		Message: Message
	};

});

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(58)))

/***/ }),
/* 58 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _rsupMqtt = __webpack_require__(20);

var _eventTypes = __webpack_require__(60);

var _eventTypes2 = _interopRequireDefault(_eventTypes);

var _webviewer = __webpack_require__(5);

var _webviewer2 = _interopRequireDefault(_webviewer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Connection = function (_Client) {
  _inherits(Connection, _Client);

  function Connection() {
    var _ref;

    _classCallCheck(this, Connection);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Connection.__proto__ || Object.getPrototypeOf(Connection)).call.apply(_ref, [this].concat(args)));

    _this._disconnected = false;

    _this._setReconnect();
    _this._setEvents();
    return _this;
  }

  _createClass(Connection, [{
    key: 'publish',
    value: function publish() {
      var _get2;

      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      if (!this._disconnected) (_get2 = _get(Connection.prototype.__proto__ || Object.getPrototypeOf(Connection.prototype), 'publish', this)).call.apply(_get2, [this].concat(args));
    }
  }, {
    key: '_setReconnect',
    value: function _setReconnect() {
      var _this2 = this;

      var retries = 0;
      var span = timespan();

      this.on('close', _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        var currentTime, reconnectedTime;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this2._disconnected = true;

                // 5초이내 짧은 주기의 재시도가 3회이상 반복되면 reload 합니다.
                retries = span() < 5 ? retries + 1 : 0;

                if (!(retries > 3)) {
                  _context.next = 11;
                  break;
                }

                currentTime = new Date().getTime();
                reconnectedTime = _webviewer2.default.get('reconnectedTime');

                // 15초 이내 reload 재 발생 시 stop 합니다.

                if (!(currentTime - reconnectedTime <= 15000)) {
                  _context.next = 9;
                  break;
                }

                _webviewer2.default.remove('reconnectedTime');
                alert('연결 실패');
                return _context.abrupt('return');

              case 9:

                _webviewer2.default.set('reconnectedTime', currentTime);
                return _context.abrupt('return', location.reload(true));

              case 11:
                _context.next = 13;
                return _this2.reconnect();

              case 13:

                // after reconnected
                span = timespan();
                _this2._disconnected = false;

              case 15:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, _this2);
      })));
    }
  }, {
    key: '_setEvents',
    value: function _setEvents() {
      var _this3 = this;

      this.on('message', function (topic, message) {
        window.mqttLog && console.log({ topic: topic, message: message.json });

        for (var name in _eventTypes2.default) {
          var _emitter;

          var match = _eventTypes2.default[name];
          var matched = match(message.json);

          if (matched) return (_emitter = _this3._emitter).emit.apply(_emitter, [name].concat(_toConsumableArray([].concat(matched))));
        }
      });
    }
  }]);

  return Connection;
}(_rsupMqtt.Client);

exports.default = Connection;


function timespan() {
  var start = Date.now();
  return function () {
    return (Date.now() - start) / 1000;
  };
}

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _HOST_EVENT$RELAY_INF;

var _remoteControl = __webpack_require__(7);

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

exports.default = (_HOST_EVENT$RELAY_INF = {}, _defineProperty(_HOST_EVENT$RELAY_INF, _remoteControl.HOST_EVENT.RELAY_INFO, function (_ref) {
  var tag = _ref.tag;

  return tag === _remoteControl.TAG.RELAY_INFO;
}), _defineProperty(_HOST_EVENT$RELAY_INF, _remoteControl.HOST_EVENT.READY, function (_ref2) {
  var tag = _ref2.tag,
      _ref2$data = _ref2.data,
      type = _ref2$data.type,
      category = _ref2$data.category,
      status = _ref2$data.status;

  return tag === _remoteControl.TAG.MESSAGE_PROTOCOL && type === _remoteControl.TYPE.EVENT && category === _remoteControl.CATEGORY.STATUS && status === _remoteControl.HOST_STATUS.HOST_READY;
}), _defineProperty(_HOST_EVENT$RELAY_INF, _remoteControl.HOST_EVENT.ENGINE_START, function (_ref3) {
  var tag = _ref3.tag,
      _ref3$data = _ref3.data,
      type = _ref3$data.type,
      category = _ref3$data.category,
      status = _ref3$data.status;

  return tag === _remoteControl.TAG.MESSAGE_PROTOCOL && type === _remoteControl.TYPE.EVENT && category === _remoteControl.CATEGORY.STATUS && status === _remoteControl.HOST_STATUS.ENGINE_START;
}), _defineProperty(_HOST_EVENT$RELAY_INF, _remoteControl.HOST_EVENT.SESSION_CHANGE_ON, function (_ref4) {
  var tag = _ref4.tag,
      _ref4$data = _ref4.data,
      type = _ref4$data.type,
      category = _ref4$data.category,
      status = _ref4$data.status,
      message = _ref4$data.message,
      change = _ref4$data.change;

  return tag === _remoteControl.TAG.MESSAGE_PROTOCOL && type === _remoteControl.TYPE.EVENT && category === _remoteControl.CATEGORY.STATUS && status === _remoteControl.HOST_STATUS.SESSION_CHANGED && message === _remoteControl.STATUS_MESSAGE.HOST_STATUS && change === _remoteControl.BOOL.TRUE;
}), _defineProperty(_HOST_EVENT$RELAY_INF, _remoteControl.HOST_EVENT.SESSION_CHANGE_OFF, function (_ref5) {
  var tag = _ref5.tag,
      _ref5$data = _ref5.data,
      type = _ref5$data.type,
      category = _ref5$data.category,
      status = _ref5$data.status,
      message = _ref5$data.message,
      change = _ref5$data.change;

  return tag === _remoteControl.TAG.MESSAGE_PROTOCOL && type === _remoteControl.TYPE.EVENT && category === _remoteControl.CATEGORY.STATUS && status === _remoteControl.HOST_STATUS.SESSION_CHANGED && message === _remoteControl.STATUS_MESSAGE.HOST_STATUS && change === _remoteControl.BOOL.FALSE;
}), _defineProperty(_HOST_EVENT$RELAY_INF, _remoteControl.HOST_EVENT.SCREEN_PAUSE, function (_ref6) {
  var tag = _ref6.tag,
      _ref6$data = _ref6.data,
      type = _ref6$data.type,
      category = _ref6$data.category,
      message = _ref6$data.message;

  return tag === _remoteControl.TAG.MESSAGE_PROTOCOL && type === _remoteControl.TYPE.EVENT && category === _remoteControl.CATEGORY.SCREEN && message === _remoteControl.BOOL.FALSE;
}), _defineProperty(_HOST_EVENT$RELAY_INF, _remoteControl.HOST_EVENT.SCREEN_RESUME, function (_ref7) {
  var tag = _ref7.tag,
      _ref7$data = _ref7.data,
      type = _ref7$data.type,
      category = _ref7$data.category,
      message = _ref7$data.message;

  return tag === _remoteControl.TAG.MESSAGE_PROTOCOL && type === _remoteControl.TYPE.EVENT && category === _remoteControl.CATEGORY.SCREEN && message === _remoteControl.BOOL.TRUE;
}), _defineProperty(_HOST_EVENT$RELAY_INF, _remoteControl.HOST_EVENT.MONITOR_INFO, function (_ref8) {
  var tag = _ref8.tag,
      _ref8$data = _ref8.data,
      type = _ref8$data.type,
      category = _ref8$data.category,
      message = _ref8$data.message,
      data = _objectWithoutProperties(_ref8$data, ['type', 'category', 'message']);

  if (tag === _remoteControl.TAG.MESSAGE_PROTOCOL && category === _remoteControl.CATEGORY.MONITOR && (type === _remoteControl.TYPE.RESPONSE || type === _remoteControl.TYPE.EVENT) && message === _remoteControl.MONITOR_MESSAGE.MONITORS) {
    return {
      count: data.count,
      cx: data.cx,
      cy: data.cy,
      monitors: data.monitors
    };
  }
}), _defineProperty(_HOST_EVENT$RELAY_INF, _remoteControl.HOST_EVENT.SELECTED_MONITOR_INDEX, function (_ref9) {
  var tag = _ref9.tag,
      _ref9$data = _ref9.data,
      type = _ref9$data.type,
      category = _ref9$data.category,
      message = _ref9$data.message,
      index = _ref9$data.index;

  if (tag === _remoteControl.TAG.MESSAGE_PROTOCOL && category === _remoteControl.CATEGORY.MONITOR && (type === _remoteControl.TYPE.RESPONSE || type === _remoteControl.TYPE.EVENT) && message === _remoteControl.MONITOR_MESSAGE.SELECTED_MONITOR) {
    return [index];
  }
}), _defineProperty(_HOST_EVENT$RELAY_INF, _remoteControl.HOST_EVENT.DISPLAY_CHANGE, function (_ref10) {
  var tag = _ref10.tag,
      data = _ref10.data;
  var type = data.type,
      category = data.category,
      message = data.message;


  if (tag === _remoteControl.TAG.MESSAGE_PROTOCOL && category === _remoteControl.CATEGORY.MONITOR && (type === _remoteControl.TYPE.RESPONSE || type === _remoteControl.TYPE.EVENT) && message === _remoteControl.MONITOR_MESSAGE.DISPLAY_CHANGE) {
    return data;
  }
}), _defineProperty(_HOST_EVENT$RELAY_INF, _remoteControl.HOST_EVENT.KM_CONTROL, function (_ref11) {
  var tag = _ref11.tag,
      _ref11$data = _ref11.data,
      type = _ref11$data.type,
      category = _ref11$data.category,
      message = _ref11$data.message;

  if (tag === _remoteControl.TAG.MESSAGE_PROTOCOL && category === _remoteControl.CATEGORY.KM_CONTROL && type === _remoteControl.TYPE.RESPONSE) {
    return [message === _remoteControl.RESPONSE_VALUE.YES];
  }
}), _defineProperty(_HOST_EVENT$RELAY_INF, _remoteControl.HOST_EVENT.SCREEN_KEYBOARD_ON, function (_ref12) {
  var tag = _ref12.tag,
      _ref12$data = _ref12.data,
      type = _ref12$data.type,
      category = _ref12$data.category,
      message = _ref12$data.message;

  return tag === _remoteControl.TAG.MESSAGE_PROTOCOL && type === _remoteControl.TYPE.EVENT && category === _remoteControl.CATEGORY.SCREEN_KEYBOARD && message === _remoteControl.BOOL.TRUE;
}), _defineProperty(_HOST_EVENT$RELAY_INF, _remoteControl.HOST_EVENT.SCREEN_KEYBOARD_OFF, function (_ref13) {
  var tag = _ref13.tag,
      _ref13$data = _ref13.data,
      type = _ref13$data.type,
      category = _ref13$data.category,
      message = _ref13$data.message;

  return tag === _remoteControl.TAG.MESSAGE_PROTOCOL && type === _remoteControl.TYPE.EVENT && category === _remoteControl.CATEGORY.SCREEN_KEYBOARD && message === _remoteControl.BOOL.FALSE;
}), _defineProperty(_HOST_EVENT$RELAY_INF, _remoteControl.HOST_EVENT.DISCONNECT, function (_ref14) {
  var _ref14$data = _ref14.data,
      type = _ref14$data.type,
      category = _ref14$data.category;

  return type === _remoteControl.TYPE.END && category === _remoteControl.CATEGORY.CONNECTION;
}), _defineProperty(_HOST_EVENT$RELAY_INF, _remoteControl.HOST_EVENT.FORCE_DISCONNECT, function (_ref15) {
  var _ref15$data = _ref15.data,
      type = _ref15$data.type,
      category = _ref15$data.category,
      message = _ref15$data.message;

  return type === _remoteControl.TYPE.EVENT && category === _remoteControl.CATEGORY.CONNECTION && message === _remoteControl.CONNECTION_MESSAGE.DISCONNECT;
}), _HOST_EVENT$RELAY_INF);

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _connect = __webpack_require__(62);

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_connect).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var connect = function connect(address) {
  return new Promise(function (resolve, reject) {
    var webSocket = new WebSocket('wss://' + address.host + ':' + address.port);
    webSocket.onopen = function () {
      return resolve(webSocket);
    };
    webSocket.onerror = function () {
      return reject(new Error('faild connect!'));
    };
  });
};

exports.default = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(relayServers) {
    var _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, server, webSocket;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _iteratorNormalCompletion = true;
            _didIteratorError = false;
            _iteratorError = undefined;
            _context.prev = 3;
            _iterator = relayServers[Symbol.iterator]();

          case 5:
            if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
              _context.next = 14;
              break;
            }

            server = _step.value;
            _context.next = 9;
            return connect(server);

          case 9:
            webSocket = _context.sent;
            return _context.abrupt('return', { webSocket: webSocket, connectedServer: server });

          case 11:
            _iteratorNormalCompletion = true;
            _context.next = 5;
            break;

          case 14:
            _context.next = 20;
            break;

          case 16:
            _context.prev = 16;
            _context.t0 = _context['catch'](3);
            _didIteratorError = true;
            _iteratorError = _context.t0;

          case 20:
            _context.prev = 20;
            _context.prev = 21;

            if (!_iteratorNormalCompletion && _iterator.return) {
              _iterator.return();
            }

          case 23:
            _context.prev = 23;

            if (!_didIteratorError) {
              _context.next = 26;
              break;
            }

            throw _iteratorError;

          case 26:
            return _context.finish(23);

          case 27:
            return _context.finish(20);

          case 28:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[3, 16, 20, 28], [21,, 23, 27]]);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}();

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _utils = __webpack_require__(3);

var _status = __webpack_require__(4);

var STATUS = _interopRequireWildcard(_status);

var _remoteControl = __webpack_require__(7);

var REMOTE_CONTROL = _interopRequireWildcard(_remoteControl);

var _webviewer = __webpack_require__(6);

var WEBVIEWER = _interopRequireWildcard(_webviewer);

var _webviewer2 = __webpack_require__(5);

var _webviewer3 = _interopRequireDefault(_webviewer2);

var _base = __webpack_require__(8);

var _host = __webpack_require__(10);

var _viewer = __webpack_require__(9);

var _Viewer = __webpack_require__(64);

var _Viewer2 = _interopRequireDefault(_Viewer);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ViewerContainer = function (_Component) {
  _inherits(ViewerContainer, _Component);

  function ViewerContainer(props) {
    _classCallCheck(this, ViewerContainer);

    var _this = _possibleConstructorReturn(this, (ViewerContainer.__proto__ || Object.getPrototypeOf(ViewerContainer)).call(this, props));

    _this.updateViewerStatus = function (status) {
      var viewerActions = _this.props.viewerActions;

      viewerActions.updateOperatingStatus(status);
    };

    _this.addActivatedControlApp = function (selectedApp) {
      var _this$props = _this.props,
          viewer = _this$props.viewer,
          viewerActions = _this$props.viewerActions;

      var filteredActivatedControlApps = viewer.activatedControlApps.filter(function (app) {
        return app.groupId !== selectedApp.groupId;
      });

      viewerActions.updateActivatedControlApp([].concat(_toConsumableArray(filteredActivatedControlApps), [{ appId: selectedApp.id, groupId: selectedApp.groupId }]));
    };

    _this.removeActivatedControlApp = function (selectedApp) {
      var _this$props2 = _this.props,
          viewer = _this$props2.viewer,
          viewerActions = _this$props2.viewerActions;

      var filteredActivatedControlApps = viewer.activatedControlApps.filter(function (app) {
        return app.appId !== selectedApp.id;
      });

      viewerActions.updateActivatedControlApp([].concat(_toConsumableArray(filteredActivatedControlApps)));
    };

    var config = props.config,
        remoteControl = props.remoteControl,
        baseActions = props.baseActions;


    remoteControl.set({
      accessCode: config.accessCode,
      viewerId: config.viewerId,
      hostId: config.hostId,
      onReceivedEvent: baseActions.onReceivedEvent
    });

    _this.prevHostStatus = null;
    return _this;
  }

  _createClass(ViewerContainer, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.runRemoteControl();
    }
  }, {
    key: 'runRemoteControl',
    value: function runRemoteControl() {
      var _this2 = this;

      var _props = this.props,
          client = _props.client,
          config = _props.config,
          remoteControl = _props.remoteControl,
          baseActions = _props.baseActions,
          hostActions = _props.hostActions;

      var topic = remoteControl.getTopic();

      client.on(REMOTE_CONTROL.HOST_EVENT.RELAY_INFO, function () {
        var relayServer = _webviewer3.default.get('relayServer');

        hostActions.updateOperatingStatus(STATUS.HOST.RUN);

        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
          type: 'protocol',
          message: '[host to viewer] [request] relay info',
          data: {
            topic: topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.RELAY_INFO,
              identifier_id: config.hostId
            }
          }
        });

        remoteControl.relayInfo({
          relay: {
            ip: relayServer.host,
            port: relayServer.port.toString(),
            domain: relayServer.host
          },
          roomid: config.accessCode
        });
      });

      client.on(REMOTE_CONTROL.HOST_EVENT.READY, function () {
        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
          type: 'protocol',
          message: '[host to viewer] [event] host ready',
          data: {
            topic: topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        });

        remoteControl.requestMonitorsInfo();
      });

      client.on(REMOTE_CONTROL.HOST_EVENT.MONITOR_INFO, function (data) {
        var monitorIndex = 0; // 첫 번째 모니터 선택

        hostActions.updateMonitorInfo(_extends({}, data, {
          selectedMonitor: monitorIndex
        }));

        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
          type: 'protocol',
          message: '[host to viewer] [response] host monitor info',
          data: {
            topic: topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId,
              data: data
            }
          }
        });

        remoteControl.requestSelectedMonitorInfo(monitorIndex);
      });

      client.on(REMOTE_CONTROL.HOST_EVENT.SELECTED_MONITOR_INDEX, function (index) {
        var host = _this2.props.host;


        if (host.status === STATUS.HOST.DISPLAY_CHANGE) {
          hostActions.updateOperatingStatus(_this2.prevHostStatus);
          _this2.prevHostStatus = null;
        }

        hostActions.updateMonitorInfo(_extends({}, host.monitorInfo, {
          selectedMonitor: index
        }));

        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
          type: 'protocol',
          message: '[host to viewer] [response] selected host monitor info',
          data: {
            topic: topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId,
              data: index
            }
          }
        });

        remoteControl.viewerStatus(REMOTE_CONTROL.VIEWER_STATUS.SCREEN_READY);
      });

      client.on(REMOTE_CONTROL.HOST_EVENT.ENGINE_START, function () {
        _this2.updateViewerStatus(STATUS.VIEWER.RUN);

        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.JOIN_OK);
        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
          type: 'protocol',
          message: '[host to viewer] [event] engine start (join ok)',
          data: {
            topic: topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        });
      });

      client.on(REMOTE_CONTROL.HOST_EVENT.KM_CONTROL, function (available) {
        var kmControl = _this2.hasActivatedApp(0);
        var laserPointer = _this2.hasActivatedApp(1);
        var draw = _this2.hasActivatedApp(2);

        kmControl && remoteControl.startKmControl();
        laserPointer && remoteControl.startLaserPointer();
        draw && remoteControl.startDraw();

        hostActions.updateControlStatus(available);
        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
          type: 'protocol',
          message: '[host to viewer] [response] mouse, keyboard control',
          data: {
            topic: topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId,
              data: available
            }
          }
        });
      });

      client.on(REMOTE_CONTROL.HOST_EVENT.SCREEN_PAUSE, function () {
        var host = _this2.props.host;


        _this2.prevHostStatus = host.status;
        hostActions.updateOperatingStatus(STATUS.HOST.PAUSE);
        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
          type: 'protocol',
          message: '[host to viewer] [event] screen pause',
          data: {
            topic: topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        });
      });

      client.on(REMOTE_CONTROL.HOST_EVENT.SCREEN_RESUME, function () {
        hostActions.updateOperatingStatus(_this2.prevHostStatus);
        _this2.prevHostStatus = null;
        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
          type: 'protocol',
          message: '[host to viewer] [event] screen resume',
          data: {
            topic: topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        });
      });

      client.on(REMOTE_CONTROL.HOST_EVENT.DISPLAY_CHANGE, function (data) {
        var host = _this2.props.host;


        _this2.prevHostStatus = host.status;
        hostActions.updateOperatingStatus(STATUS.HOST.DISPLAY_CHANGE);
        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
          type: 'protocol',
          message: '[host to viewer] [event] display change',
          data: {
            topic: topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId,
              data: data
            }
          }
        });

        remoteControl.requestMonitorsInfo();
      });

      client.on(REMOTE_CONTROL.HOST_EVENT.SESSION_CHANGE_ON, function () {
        var host = _this2.props.host;


        _this2.prevHostStatus = host.status;
        hostActions.updateOperatingStatus(STATUS.HOST.SESSION_CHANGE);
        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
          type: 'protocol',
          message: '[host to viewer] [event] session change on',
          data: {
            topic: topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        });
      });

      client.on(REMOTE_CONTROL.HOST_EVENT.SESSION_CHANGE_OFF, function () {
        hostActions.updateOperatingStatus(_this2.prevHostStatus);
        _this2.prevHostStatus = null;
        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
          type: 'protocol',
          message: '[host to viewer] [event] session change off',
          data: {
            topic: topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        });
      });

      client.on(REMOTE_CONTROL.HOST_EVENT.SCREEN_KEYBOARD_ON, function () {
        var screenKeyboard = _this2.findControlApp(4);
        _this2.addActivatedControlApp(screenKeyboard);

        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
          type: 'protocol',
          message: '[host to viewer] [event] screen keyboard on',
          data: {
            topic: topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        });
      });

      client.on(REMOTE_CONTROL.HOST_EVENT.SCREEN_KEYBOARD_OFF, function () {
        var screenKeyboard = _this2.findControlApp(4);
        _this2.removeActivatedControlApp(screenKeyboard);

        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.LOG, {
          type: 'protocol',
          message: '[host to viewer] [event] screen keyboard off',
          data: {
            topic: topic,
            protocol: {
              tag: REMOTE_CONTROL.TAG.MESSAGE_PROTOCOL,
              identifier_id: config.hostId
            }
          }
        });
      });

      client.on(REMOTE_CONTROL.HOST_EVENT.DISCONNECT, function () {
        hostActions.updateOperatingStatus(STATUS.HOST.EXIT);
        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.CLOSE, {
          target: 'host',
          server: 'mqtt',
          normal: true
        });

        client.disconnect();
      });

      client.on(REMOTE_CONTROL.HOST_EVENT.FORCE_DISCONNECT, function () {
        hostActions.updateOperatingStatus(STATUS.HOST.EXIT);
        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.CLOSE, {
          target: 'host',
          server: 'mqtt',
          normal: false
        });

        client.disconnect();
      });
    }
  }, {
    key: 'hasActivatedApp',
    value: function hasActivatedApp(id) {
      var activatedControlApps = this.props.viewer.activatedControlApps;

      return !!activatedControlApps.find(function (app) {
        return app.appId === id;
      });
    }
  }, {
    key: 'findControlApp',
    value: function findControlApp(id) {
      var controlApps = this.props.config.controlApps;

      return controlApps.find(function (app) {
        return app.id === id;
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          viewer = _props2.viewer,
          host = _props2.host;


      return _react2.default.createElement(_Viewer2.default, { viewer: viewer, host: host });
    }
  }]);

  return ViewerContainer;
}(_react.Component);

var enhance = (0, _utils.compose)(_base.useBase, _host.useHost, _viewer.useViewer);

exports.default = enhance(ViewerContainer);

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Viewer = __webpack_require__(65);

var _Viewer2 = _interopRequireDefault(_Viewer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Viewer2.default;

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _Viewer = __webpack_require__(66);

var _Viewer2 = _interopRequireDefault(_Viewer);

var _status = __webpack_require__(4);

var STATUS = _interopRequireWildcard(_status);

var _ScreenContainer = __webpack_require__(68);

var _ScreenContainer2 = _interopRequireDefault(_ScreenContainer);

var _ControllerContainer = __webpack_require__(73);

var _ControllerContainer2 = _interopRequireDefault(_ControllerContainer);

var _webviewer = __webpack_require__(5);

var _webviewer2 = _interopRequireDefault(_webviewer);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Viewer = function (_Component) {
  _inherits(Viewer, _Component);

  function Viewer() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Viewer);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Viewer.__proto__ || Object.getPrototypeOf(Viewer)).call.apply(_ref, [this].concat(args))), _this), _this.handleLeave = function () {
      var _this$props = _this.props,
          host = _this$props.host,
          viewer = _this$props.viewer;


      if (host.status === STATUS.HOST.EXIT || viewer.status === STATUS.VIEWER.EXIT) return;

      // 전체화면은 제외
      var activatedControlApps = viewer.activatedControlApps.filter(function (app) {
        return app.groupId !== 2;
      });

      _webviewer2.default.set('activatedControlApps', activatedControlApps);
      _webviewer2.default.set('screenCaptureNumber', viewer.screenCaptureNumber);
      _webviewer2.default.set('screenRatio', viewer.screenRatio);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Viewer, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      window.addEventListener('beforeunload', this.handleLeave);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.removeEventListener('beforeunload', this.handleLeave);
    }
  }, {
    key: 'render',
    value: function render() {
      var viewer = this.props.viewer;

      var viewerStyle = 'viewer' + (viewer.status === STATUS.VIEWER.RUN ? '' : '--disable');

      return _react2.default.createElement(
        'div',
        { className: _Viewer2.default['viewer-wrap'] },
        _react2.default.createElement(
          'div',
          { className: _Viewer2.default[viewerStyle] },
          _react2.default.createElement(
            'div',
            {
              className: _Viewer2.default['screen-wrap'],
              onContextMenu: function onContextMenu(e) {
                e.preventDefault();
                return false;
              }
            },
            _react2.default.createElement(_ScreenContainer2.default, null),
            _react2.default.createElement(_ControllerContainer2.default, null)
          )
        )
      );
    }
  }]);

  return Viewer;
}(_react.Component);

exports.default = Viewer;

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(67);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(2)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./Viewer.scss", function() {
		var newContent = require("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./Viewer.scss");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "._2wSXr8_Y2KfP::after {\n  content: ' ';\n  display: block;\n  clear: both; }\n\n._3C7AI-KHeAve {\n  float: left;\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: #2f2f2f;\n  min-width: 100%;\n  min-height: 100vh; }\n  ._1zD3OjE8KcG2 {\n    display: none; }\n\n._18jW7brvpduR {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 100%;\n  height: 100%;\n  background: white; }\n\n._P7_UcWHp3_dD {\n  font-size: 40px; }\n\n._2ONgsLMYtkry {\n  position: relative;\n  display: inline-block;\n  -webkit-box-shadow: 0 0 30px 10px rgba(0, 0, 0, 0.3);\n          box-shadow: 0 0 30px 10px rgba(0, 0, 0, 0.3);\n  font-size: 0; }\n", ""]);

// exports
exports.locals = {
	"viewer-wrap": "_2wSXr8_Y2KfP",
	"viewer": "_3C7AI-KHeAve",
	"viewer--disable": "_1zD3OjE8KcG2",
	"waiting": "_18jW7brvpduR",
	"access-code": "_P7_UcWHp3_dD",
	"screen-wrap": "_2ONgsLMYtkry"
};

/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _utils = __webpack_require__(3);

var _status = __webpack_require__(4);

var STATUS = _interopRequireWildcard(_status);

var _webviewer = __webpack_require__(6);

var WEBVIEWER = _interopRequireWildcard(_webviewer);

var _base = __webpack_require__(8);

var _host = __webpack_require__(10);

var _viewer = __webpack_require__(9);

var _Screen = __webpack_require__(69);

var _Screen2 = _interopRequireDefault(_Screen);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ScreenContainer = function (_Component) {
  _inherits(ScreenContainer, _Component);

  function ScreenContainer() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, ScreenContainer);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = ScreenContainer.__proto__ || Object.getPrototypeOf(ScreenContainer)).call.apply(_ref, [this].concat(args))), _this), _this.addActivatedControlApp = function (selectedApp) {
      var _this$props = _this.props,
          viewer = _this$props.viewer,
          viewerActions = _this$props.viewerActions;

      var filteredActivatedControlApps = viewer.activatedControlApps.filter(function (app) {
        return app.groupId !== selectedApp.groupId;
      });

      viewerActions.updateActivatedControlApp([].concat(_toConsumableArray(filteredActivatedControlApps), [{ appId: selectedApp.id, groupId: selectedApp.groupId }]));
    }, _this.removeActivatedControlApp = function (selectedApp) {
      var _this$props2 = _this.props,
          viewer = _this$props2.viewer,
          viewerActions = _this$props2.viewerActions;

      var filteredActivatedControlApps = viewer.activatedControlApps.filter(function (app) {
        return app.appId !== selectedApp.id;
      });

      viewerActions.updateActivatedControlApp([].concat(_toConsumableArray(filteredActivatedControlApps)));
    }, _this.updateViewerStatus = function (status) {
      var viewerActions = _this.props.viewerActions;

      viewerActions.updateOperatingStatus(status);
    }, _this.updatePressedFunctionKeys = function (pressedFunctionKeys) {
      var viewerActions = _this.props.viewerActions;

      viewerActions.updatePressedFunctionKeys(pressedFunctionKeys);
    }, _this.changeScreenSize = function (_ref2) {
      var width = _ref2.width,
          height = _ref2.height,
          overflow = _ref2.overflow;
      var viewerActions = _this.props.viewerActions;

      viewerActions.changeScreenSize({ width: width, height: height, isScreenOverflow: overflow });
    }, _this.exitRemoteControl = function () {
      var _this$props3 = _this.props,
          baseActions = _this$props3.baseActions,
          hostActions = _this$props3.hostActions;

      hostActions.updateOperatingStatus(STATUS.HOST.EXIT);
      baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.CLOSE, {
        target: 'host',
        server: 'relay',
        normal: true
      });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(ScreenContainer, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          i18n = _props.i18n,
          config = _props.config,
          host = _props.host,
          viewer = _props.viewer,
          remoteControl = _props.remoteControl,
          screenControl = _props.screenControl,
          baseActions = _props.baseActions;


      return _react2.default.createElement(_Screen2.default, {
        i18n: i18n,
        host: host,
        viewer: viewer,
        config: config,
        screenControl: screenControl,
        remoteControl: remoteControl,
        baseActions: baseActions,
        addActivatedControlApp: this.addActivatedControlApp,
        removeActivatedControlApp: this.removeActivatedControlApp,
        updateViewerStatus: this.updateViewerStatus,
        updatePressedFunctionKeys: this.updatePressedFunctionKeys,
        changeScreenSize: this.changeScreenSize,
        exitRemoteControl: this.exitRemoteControl
      });
    }
  }]);

  return ScreenContainer;
}(_react.Component);

var enhance = (0, _utils.compose)(_base.useBase, _host.useHost, _viewer.useViewer);

exports.default = enhance(ScreenContainer);

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Screen = __webpack_require__(70);

var _Screen2 = _interopRequireDefault(_Screen);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Screen2.default;

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _Screen = __webpack_require__(71);

var _Screen2 = _interopRequireDefault(_Screen);

var _webviewer = __webpack_require__(5);

var _webviewer2 = _interopRequireDefault(_webviewer);

var _utils = __webpack_require__(3);

var _remoteControl = __webpack_require__(7);

var REMOTE_CONTROL = _interopRequireWildcard(_remoteControl);

var _screenControl = __webpack_require__(18);

var SCREEN_CONTROL = _interopRequireWildcard(_screenControl);

var _webviewer3 = __webpack_require__(6);

var WEBVIEWER = _interopRequireWildcard(_webviewer3);

var _status = __webpack_require__(4);

var STATUS = _interopRequireWildcard(_status);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Screen = function (_Component) {
  _inherits(Screen, _Component);

  function Screen(props) {
    _classCallCheck(this, Screen);

    var _this = _possibleConstructorReturn(this, (Screen.__proto__ || Object.getPrototypeOf(Screen)).call(this, props));

    _this.convertCoordinates = function (clientX, clientY) {
      var screen = _this.screen;
      var monitorInfo = _this.props.host.monitorInfo;

      var hostMonitor = monitorInfo.monitors[monitorInfo.selectedMonitor];
      var windowWidth = screen.getAttribute('width');
      var windowHeight = screen.getAttribute('height');
      var screenRect = screen.getBoundingClientRect();
      var convertedX = Math.round((clientX - screenRect.x) * (hostMonitor.width / windowWidth));
      var convertedY = Math.round((clientY - screenRect.y) * (hostMonitor.height / windowHeight));

      return { x: convertedX, y: convertedY };
    };

    _this.initializeDraw = function (e) {
      var _this$getPosition = _this.getPosition(e),
          x = _this$getPosition.x,
          y = _this$getPosition.y;

      var _this$props = _this.props,
          config = _this$props.config,
          viewer = _this$props.viewer;
      var canvasTool = config.canvasTool;
      var paintColor = viewer.paintColor;

      var selected = canvasTool.colors.find(function (color) {
        return color.id === paintColor;
      });
      canvasTool.isDrawing = true;
      _this.ctx.beginPath();
      _this.ctx.lineWidth = canvasTool.thickness;
      _this.ctx.strokeStyle = selected.hex;
      canvasTool.x = x;
      canvasTool.y = y;
      _this.ctx.moveTo(canvasTool.x, canvasTool.y);

      _this.setDrawInfo({ color: paintColor });
    };

    _this.drawPaint = function (e) {
      var canvasTool = _this.props.config.canvasTool;


      if (!canvasTool.isDrawing) return;

      var _this$getPosition2 = _this.getPosition(e),
          x = _this$getPosition2.x,
          y = _this$getPosition2.y;

      _this.ctx.lineTo(x, y);
      canvasTool.x = x;
      canvasTool.y = y;
      _this.ctx.stroke();

      _this.gatherDrawPoints(e);
    };

    _this.finishDraw = function () {
      var screen = _this.screen;
      var _this$props2 = _this.props,
          config = _this$props2.config,
          remoteControl = _this$props2.remoteControl;
      var canvasTool = config.canvasTool;

      canvasTool.isDrawing = false;
      canvasTool.x = -1;
      canvasTool.y = -1;
      _this.ctx.clearRect(0, 0, screen.width, screen.height);
      remoteControl.drawPaint(canvasTool.drawPoints);
      canvasTool.drawPoints = [];
    };

    _this.toggleFullScreen = function () {
      var _this$props3 = _this.props,
          addActivatedControlApp = _this$props3.addActivatedControlApp,
          removeActivatedControlApp = _this$props3.removeActivatedControlApp;

      var fullScreen = _this.findControlApp(5);
      var isActivated = !_this.findActivatedControlApp(fullScreen);

      if (isActivated) {
        addActivatedControlApp(fullScreen);
      } else {
        removeActivatedControlApp(fullScreen);
      }
    };

    _this.openMediaSource = function () {
      if (_this.buffer) return;

      _this.buffer = _this.mediaSource.addSourceBuffer('video/mp4; codecs="avc1.64002A"');
      _this.mediaSource.duration = 'Infinity';
      _this.buffer.mode = 'sequence';
      _this.buffer.addEventListener('updateend', function () {
        if (_this.queue.length > 0 && !_this.buffer.updating) {
          _this.buffer.appendBuffer(_this.queue.shift());
        }
      });

      _this.runScreen();
    };

    _this.errorMediaSource = function () {
      console.log('mediaSource error: ' + _this.mediaSource.readyState);
    };

    _this.mouseMoveListener = function (e) {
      var kmControl = _this.hasActivatedApp(0);
      var laserPointer = _this.hasActivatedApp(1);
      var isMouseDown = _this.isMouseDown;


      if (kmControl) {
        _this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.MOVE, e);
      }

      if (laserPointer) {
        _this.handleLaserPointer({ xPos: e.clientX, yPos: e.clientY, message: isMouseDown });
      }
    };

    _this.mouseDownListener = function (e) {
      var kmControl = _this.hasActivatedApp(0);
      var laserPointer = _this.hasActivatedApp(1);

      switch (e.which) {
        case 1:
          _this.onPressMouse(true);

          if (kmControl) {
            _this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.L_BTN_DOWN, e);
          }

          if (laserPointer) {
            _this.handleLaserPointer({ xPos: e.clientX, yPos: e.clientY, message: true });
          }
          break;
        case 2:
          _this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.M_BTN_DOWN, e);
          break;
        case 3:
          _this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.R_BTN_DOWN, e);
          break;
        default:
          break;
      }
    };

    _this.mouseUpListener = function (e) {
      var kmControl = _this.hasActivatedApp(0);
      var laserPointer = _this.hasActivatedApp(1);

      switch (e.which) {
        case 1:
          _this.onPressMouse(false);

          if (kmControl) {
            _this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.L_BTN_UP, e);
          }

          if (laserPointer) {
            _this.handleLaserPointer({ xPos: e.clientX, yPos: e.clientY });
          }
          break;
        case 2:
          _this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.M_BTN_UP, e);
          break;
        case 3:
          _this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.R_BTN_UP, e);
          break;
        default:
          break;
      }
    };

    _this.mouseWheelListener = function (e) {
      _this.handleMouseControl(e.wheelDelta > 0 ? REMOTE_CONTROL.MOUSE_MESSAGE.WHEEL_UP : REMOTE_CONTROL.MOUSE_MESSAGE.WHEEL_DOWN, e);
    };

    _this.touchStartListener = function (_ref) {
      var _ref$touches = _slicedToArray(_ref.touches, 1),
          e = _ref$touches[0];

      var kmControl = _this.hasActivatedApp(0);
      var laserPointer = _this.hasActivatedApp(1);

      _this.onPressMouse(true);

      if (kmControl) {
        _this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.L_BTN_DOWN, e);
      }

      if (laserPointer) {
        _this.handleLaserPointer({ xPos: e.clientX, yPos: e.clientY, message: true });
      }
    };

    _this.touchMoveListener = function (_ref2) {
      var _ref2$touches = _slicedToArray(_ref2.touches, 1),
          e = _ref2$touches[0];

      var kmControl = _this.hasActivatedApp(0);
      var laserPointer = _this.hasActivatedApp(1);
      var isMouseDown = _this.isMouseDown;


      if (kmControl) {
        _this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.MOVE, e);
      }

      if (laserPointer) {
        _this.handleLaserPointer({ xPos: e.clientX, yPos: e.clientY, message: isMouseDown });
      }
    };

    _this.touchEndListener = function (_ref3) {
      var _ref3$touches = _slicedToArray(_ref3.touches, 1),
          e = _ref3$touches[0];

      var kmControl = _this.hasActivatedApp(0);
      var laserPointer = _this.hasActivatedApp(1);

      if (e && kmControl) {
        _this.handleMouseControl(REMOTE_CONTROL.MOUSE_MESSAGE.L_BTN_UP, e);
      }

      if (e && laserPointer) {
        _this.handleLaserPointer({ xPos: e.clientX, yPos: e.clientY });
      }
    };

    _this.touchStartDraw = function (_ref4) {
      var _ref4$touches = _slicedToArray(_ref4.touches, 1),
          e = _ref4$touches[0];

      _this.initializeDraw(e);
    };

    _this.touchMoveDraw = function (_ref5) {
      var _ref5$touches = _slicedToArray(_ref5.touches, 1),
          e = _ref5$touches[0];

      _this.drawPaint(e);
    };

    _this.touchEndDraw = function () {
      _this.finishDraw();
    };

    _this.resizeListener = function () {
      var _this$props4 = _this.props,
          viewer = _this$props4.viewer,
          changeScreenSize = _this$props4.changeScreenSize;

      if (viewer.status !== STATUS.VIEWER.RUN) return;

      changeScreenSize(_this.getScreenSize());
    };

    _this.focusListener = function () {
      var _this$props5 = _this.props,
          viewer = _this$props5.viewer,
          remoteControl = _this$props5.remoteControl;

      if (viewer.status !== STATUS.VIEWER.RUN) return;

      remoteControl.activeChange(true);
    };

    _this.blurListener = function () {
      var _this$props6 = _this.props,
          viewer = _this$props6.viewer,
          remoteControl = _this$props6.remoteControl;

      if (viewer.status !== STATUS.VIEWER.RUN) return;

      remoteControl.activeChange(false);
    };

    _this.keyDownListener = function (e) {
      _this.handleKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.DOWN, e.keyCode);
    };

    _this.keyUpListener = function (e) {
      _this.handleKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.UP, e.keyCode);
      _this.clearPressedFunctionKeys();
    };

    _this.visibilityChangeListener = function () {
      var viewer = _this.props.viewer;

      if (viewer.status === STATUS.VIEWER.RUN) _this.changeScreenLock();
    };

    _this.mediaSource = new MediaSource();
    _this.buffer = null;
    _this.queue = [];
    _this.ctx = null;
    _this.isMouseDown = false;
    _this.fullScreenPrevRatio = 0;
    return _this;
  }

  _createClass(Screen, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var screen = this.screen,
          canvas = this.canvas,
          mediaSource = this.mediaSource;

      screen.src = window.URL.createObjectURL(mediaSource);

      mediaSource.addEventListener('sourceopen', this.openMediaSource, false);
      mediaSource.addEventListener('error', this.errorMediaSource);

      this.ctx = canvas.getContext('2d');

      screen.addEventListener('mousemove', this.mouseMoveListener);
      screen.addEventListener('mousedown', this.mouseDownListener);
      screen.addEventListener('mouseup', this.mouseUpListener);
      screen.addEventListener('mousewheel', this.mouseWheelListener);
      screen.addEventListener('touchstart', this.touchStartListener);
      screen.addEventListener('touchmove', this.touchMoveListener);
      screen.addEventListener('touchend', this.touchEndListener);

      canvas.addEventListener('mousedown', this.initializeDraw);
      canvas.addEventListener('mousemove', this.drawPaint);
      canvas.addEventListener('mouseup', this.finishDraw);
      canvas.addEventListener('touchstart', this.touchStartDraw);
      canvas.addEventListener('touchmove', this.touchMoveDraw);
      canvas.addEventListener('touchend', this.touchEndDraw);

      window.addEventListener('resize', this.resizeListener);
      window.addEventListener('focus', this.focusListener);
      window.addEventListener('blur', this.blurListener);

      document.addEventListener('keydown', this.keyDownListener);
      document.addEventListener('keyup', this.keyUpListener);
      document.addEventListener('visibilitychange', this.visibilityChangeListener);
      document.addEventListener('fullscreenchange', this.toggleFullScreen);
      document.addEventListener('mozfullscreenchange', this.toggleFullScreen);
      document.addEventListener('webkitfullscreenchange', this.toggleFullScreen);
      document.addEventListener('MSFullscreenChange', this.toggleFullScreen);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      var screen = this.screen,
          canvas = this.canvas,
          mediaSource = this.mediaSource;


      mediaSource.removeEventListener('sourceopen', this.openMediaSource, false);
      mediaSource.removeEventListener('error', this.errorMediaSource);

      screen.removeEventListener('mousemove', this.mouseMoveListener);
      screen.removeEventListener('mousedown', this.mouseDownListener);
      screen.removeEventListener('mouseup', this.mouseUpListener);
      screen.removeEventListener('mousewheel', this.mouseWheelListener);
      screen.removeEventListener('touchstart', this.touchStartListener);
      screen.removeEventListener('touchmove', this.touchMoveListener);
      screen.removeEventListener('touchend', this.touchEndListener);

      canvas.removeEventListener('mousedown', this.initializeDraw);
      canvas.removeEventListener('mousemove', this.drawPaint);
      canvas.removeEventListener('mouseup', this.finishDraw);
      canvas.removeEventListener('touchstart', this.touchStartDraw);
      canvas.removeEventListener('touchmove', this.touchMoveDraw);
      canvas.removeEventListener('touchend', this.touchEndDraw);

      window.removeEventListener('resize', this.resizeListener);
      window.removeEventListener('focus', this.focusListener);
      window.removeEventListener('blur', this.blurListener);

      document.removeEventListener('keydown', this.keyDownListener);
      document.removeEventListener('keyup', this.keyUpListener);
      document.removeEventListener('visibilitychange', this.visibilityChangeListener);
      document.removeEventListener('fullscreenchange', this.toggleFullScreen);
      document.removeEventListener('mozfullscreenchange', this.toggleFullScreen);
      document.removeEventListener('webkitfullscreenchange', this.toggleFullScreen);
      document.removeEventListener('MSFullscreenChange', this.toggleFullScreen);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      // TODO: 수정해야함.
      var draw = this.hasActivatedApp(2);
      document.querySelector("meta[name=viewport]").setAttribute('content', 'user-scalable=' + (_utils.mobileDetect.mobile() && draw ? '0' : '1'));

      this.updateScreen(prevProps);
      this.screenCapture(prevProps);
    }
  }, {
    key: 'runScreen',
    value: function runScreen() {
      var _this2 = this;

      var _props = this.props,
          config = _props.config,
          screenControl = _props.screenControl,
          updateViewerStatus = _props.updateViewerStatus,
          baseActions = _props.baseActions;


      screenControl.setBinaryType('arraybuffer');
      screenControl.join(config.accessCode, config.viewerId);
      screenControl.on(SCREEN_CONTROL.SCREEN_EVENT.JOIN_SUCCESS, function () {
        var pushServer = _webviewer2.default.get('pushServer');
        var relayServer = _webviewer2.default.get('relayServer');

        updateViewerStatus(STATUS.VIEWER.WAITING);
        baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.WAIT_OK, { pushServer: pushServer, relayServer: relayServer });
      });

      screenControl.on(SCREEN_CONTROL.SCREEN_EVENT.SCREEN_DATA, function (data) {
        if (_this2.buffer.updating || _this2.queue.length > 0) _this2.queue.push(data);else _this2.buffer.appendBuffer(data);
      });

      screenControl.on(SCREEN_CONTROL.SCREEN_EVENT.ON_CLOSE, function () {
        var exitRemoteControl = _this2.props.exitRemoteControl;

        exitRemoteControl();
      });
    }
  }, {
    key: 'updateScreen',
    value: function updateScreen(prevProps) {
      var _props2 = this.props,
          viewer = _props2.viewer,
          changeScreenSize = _props2.changeScreenSize;


      if (viewer.status === STATUS.VIEWER.RUN && this.isChangedViewerStatus(prevProps)) {
        this.changeScreenLock();
        changeScreenSize(this.getScreenSize());
        return;
      }

      if (this.isChangedHostMonitorInfo(prevProps)) {
        var monitorInfo = this.props.host.monitorInfo;

        var monitor = monitorInfo.monitors[monitorInfo.selectedMonitor];
        changeScreenSize(this.getScreenSize({ monitor: monitor }));
        return;
      }

      if (this.isChangedViewerScreenRatio(prevProps)) {
        var _props3 = this.props,
            config = _props3.config,
            _viewer = _props3.viewer;

        var ratio = config.screenRatios[_viewer.screenRatio];
        changeScreenSize(this.getScreenSize({ ratio: ratio }));
      }
    }
  }, {
    key: 'screenCapture',
    value: function screenCapture(prevProps) {
      if (this.isChangedScreenCaptureNumber(prevProps)) {
        this.canvas.getContext('2d').drawImage(this.screen, 0, 0, this.canvas.width, this.canvas.height);
        var dataURL = this.canvas.toDataURL('image/jpeg');
        var screenCaptureNumber = this.props.viewer.screenCaptureNumber;

        (0, _utils.fileDownload)({ dataURL: dataURL, filename: 'screen-capture-' + screenCaptureNumber });
      }
    }
  }, {
    key: 'isChangedViewerStatus',
    value: function isChangedViewerStatus(_ref6) {
      var prevViewer = _ref6.viewer;

      return this.props.viewer.status !== prevViewer.status;
    }
  }, {
    key: 'isChangedHostMonitorInfo',
    value: function isChangedHostMonitorInfo(_ref7) {
      var prevHost = _ref7.host;

      return this.props.host.monitorInfo !== prevHost.monitorInfo;
    }
  }, {
    key: 'isChangedViewerScreenRatio',
    value: function isChangedViewerScreenRatio(_ref8) {
      var prevViewer = _ref8.viewer;

      return this.props.viewer.screenRatio !== prevViewer.screenRatio;
    }
  }, {
    key: 'isChangedScreenCaptureNumber',
    value: function isChangedScreenCaptureNumber(_ref9) {
      var prevViewer = _ref9.viewer;

      return this.props.viewer.screenCaptureNumber !== prevViewer.screenCaptureNumber;
    }
  }, {
    key: 'hasActivatedApp',
    value: function hasActivatedApp(id) {
      var activatedControlApps = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.props.viewer.activatedControlApps;

      return !!activatedControlApps.find(function (app) {
        return app.appId === id;
      });
    }
  }, {
    key: 'isUseKmControl',
    value: function isUseKmControl() {
      var _props$host = this.props.host,
          useKmControl = _props$host.useKmControl,
          sessionChange = _props$host.sessionChange,
          pause = _props$host.pause;

      return useKmControl && !sessionChange && !pause;
    }
  }, {
    key: 'findControlApp',
    value: function findControlApp(id) {
      var controlApps = this.props.config.controlApps;

      return controlApps.find(function (app) {
        return app.id === id;
      });
    }
  }, {
    key: 'findActivatedControlApp',
    value: function findActivatedControlApp(selectedApp) {
      var activatedControlApps = this.props.viewer.activatedControlApps;

      return !(activatedControlApps.findIndex(function (app) {
        return app.appId === selectedApp.id;
      }) === -1);
    }
  }, {
    key: 'getHostMonitor',
    value: function getHostMonitor() {
      var host = this.props.host;
      var monitorInfo = host.monitorInfo;

      return monitorInfo.monitors[monitorInfo.selectedMonitor];
    }
  }, {
    key: 'getScreenRatio',
    value: function getScreenRatio() {
      var _props4 = this.props,
          viewer = _props4.viewer,
          config = _props4.config;

      var screenRatios = config.screenRatios;
      return screenRatios[viewer.screenRatio];
    }
  }, {
    key: 'getScreenSize',
    value: function getScreenSize() {
      var _ref10 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
          _ref10$monitor = _ref10.monitor,
          monitor = _ref10$monitor === undefined ? this.getHostMonitor() : _ref10$monitor,
          _ref10$ratio = _ref10.ratio,
          ratio = _ref10$ratio === undefined ? this.getScreenRatio() : _ref10$ratio;

      if (ratio === 'fit') {
        return this.getScreenFitSize(monitor);
      }

      if (ratio === '100') {
        var _toEnlargeScreen = this.toEnlargeScreen({ monitor: monitor, ratio: 1 }),
            width = _toEnlargeScreen.width,
            height = _toEnlargeScreen.height;

        var overflow = this.isScreenOverflow({ width: width, height: height });
        return { width: width, height: height, overflow: overflow };
      }

      if (ratio === '125') {
        var _toEnlargeScreen2 = this.toEnlargeScreen({ monitor: monitor, ratio: 1.25 }),
            _width = _toEnlargeScreen2.width,
            _height = _toEnlargeScreen2.height;

        var _overflow = this.isScreenOverflow({ width: _width, height: _height });
        return { width: _width, height: _height, overflow: _overflow };
      }

      if (ratio === '150') {
        var _toEnlargeScreen3 = this.toEnlargeScreen({ monitor: monitor, ratio: 1.5 }),
            _width2 = _toEnlargeScreen3.width,
            _height2 = _toEnlargeScreen3.height;

        var _overflow2 = this.isScreenOverflow({ width: _width2, height: _height2 });
        return { width: _width2, height: _height2, overflow: _overflow2 };
      }

      return false;
    }
  }, {
    key: 'getScreenFitSize',
    value: function getScreenFitSize(hostMonitor) {
      var xRate = hostMonitor.height / hostMonitor.width;
      var yRate = hostMonitor.width / hostMonitor.height;
      var windowWidth = window.innerWidth;
      var windowHeight = window.innerHeight;

      if (windowWidth < windowHeight * yRate) {
        var width = windowWidth;
        var height = windowWidth * xRate;
        var overflow = this.isScreenOverflow({ width: width, height: height });
        return { width: width, height: height, overflow: overflow };
      } else {
        var _width3 = windowHeight * yRate;
        var _height3 = windowHeight;
        var _overflow3 = this.isScreenOverflow({ width: _width3, height: _height3 });
        return { width: _width3, height: _height3, overflow: _overflow3 };
      }
    }
  }, {
    key: 'isScreenOverflow',
    value: function isScreenOverflow() {
      var _ref11 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
          width = _ref11.width,
          height = _ref11.height;

      return width > window.innerWidth || height > window.innerHeight;
    }
  }, {
    key: 'toEnlargeScreen',
    value: function toEnlargeScreen() {
      var _ref12 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
          monitor = _ref12.monitor,
          ratio = _ref12.ratio;

      return { width: monitor.width * ratio, height: monitor.height * ratio };
    }
  }, {
    key: 'handleMouseControl',
    value: function handleMouseControl(type, e) {
      if (!this.isUseKmControl()) return;

      var kmControlApp = this.findControlApp(0);
      var isActivated = this.findActivatedControlApp(kmControlApp);

      if (!isActivated) return;

      var remoteControl = this.props.remoteControl;

      var _convertCoordinates = this.convertCoordinates(e.clientX, e.clientY),
          x = _convertCoordinates.x,
          y = _convertCoordinates.y;

      remoteControl.controlMouse(type, x, y);
    }
  }, {
    key: 'handleLaserPointer',
    value: function handleLaserPointer(_ref13) {
      var xPos = _ref13.xPos,
          yPos = _ref13.yPos,
          _ref13$message = _ref13.message,
          message = _ref13$message === undefined ? false : _ref13$message;

      if (!this.isUseKmControl()) return;

      var laserPointerApp = this.findControlApp(1);
      var isActivated = this.findActivatedControlApp(laserPointerApp);

      if (!isActivated) return;

      var remoteControl = this.props.remoteControl;

      var _convertCoordinates2 = this.convertCoordinates(xPos, yPos),
          x = _convertCoordinates2.x,
          y = _convertCoordinates2.y;

      remoteControl.moveLaserPointer(x, y, message);
    }
  }, {
    key: 'handleKeyboardControl',
    value: function handleKeyboardControl(type, keyCode) {
      if (!this.isUseKmControl()) return;
      var remoteControl = this.props.remoteControl;

      remoteControl.controlKeyboard(type, keyCode);
    }
  }, {
    key: 'clearPressedFunctionKeys',
    value: function clearPressedFunctionKeys() {
      var _this3 = this;

      var _props5 = this.props,
          config = _props5.config,
          viewer = _props5.viewer,
          updatePressedFunctionKeys = _props5.updatePressedFunctionKeys;


      config.functionKeys.filter(function (key) {
        return viewer.pressedFunctionKeys.includes(key.id);
      }).map(function (key) {
        return key.keyCode;
      }).forEach(function (keyCode) {
        return _this3.handleKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.UP, keyCode);
      });

      updatePressedFunctionKeys([]);
    }
  }, {
    key: 'onPressMouse',
    value: function onPressMouse(press) {
      this.isMouseDown = press;
    }
  }, {
    key: 'getPosition',
    value: function getPosition(e) {
      var canvasRect = this.canvas.getBoundingClientRect();
      var x = e.clientX - canvasRect.x;
      var y = e.clientY - canvasRect.y;
      return { x: x, y: y };
    }
  }, {
    key: 'gatherDrawPoints',
    value: function gatherDrawPoints(e) {
      var canvasTool = this.props.config.canvasTool;

      var _convertCoordinates3 = this.convertCoordinates(e.clientX, e.clientY),
          x = _convertCoordinates3.x,
          y = _convertCoordinates3.y;

      canvasTool.drawPoints.push({ x_pos: x, y_pos: y });
    }
  }, {
    key: 'setDrawInfo',
    value: function setDrawInfo(options) {
      var remoteControl = this.props.remoteControl;

      remoteControl.drawInfo(options);
    }
  }, {
    key: 'changeScreenLock',
    value: function changeScreenLock() {
      var remoteControl = this.props.remoteControl;


      var visible = document.visibilityState === 'visible';
      visible ? this.screen.play() : this.screen.pause();
      remoteControl.screenLock(!visible);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this4 = this;

      var viewer = this.props.viewer;

      var draw = this.hasActivatedApp(2);
      var canvasStyle = 'canvas' + (draw ? '--active' : '');

      return _react2.default.createElement(
        _react.Fragment,
        null,
        _react2.default.createElement(
          'video',
          {
            id: 'screen',
            className: _Screen2.default['screen'],
            ref: function ref(_ref14) {
              return _this4.screen = _ref14;
            },
            width: viewer.width,
            height: viewer.height,
            muted: true,
            autoPlay: 'autoplay'
          },
          _react2.default.createElement('source', { src: '', type: 'video/mp4' })
        ),
        _react2.default.createElement('canvas', {
          ref: function ref(_ref15) {
            return _this4.canvas = _ref15;
          },
          className: _Screen2.default[canvasStyle],
          width: viewer.width,
          height: viewer.height
        })
      );
    }
  }]);

  return Screen;
}(_react.Component);

exports.default = Screen;

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(72);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(2)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./Screen.scss", function() {
		var newContent = require("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./Screen.scss");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "._2clUtm4WXT_k {\n  border: 1px solid #167acd;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box; }\n\n._2Uqots9_FdlF {\n  display: none;\n  position: absolute;\n  top: 0;\n  left: 0; }\n  ._131QYHSveJCL {\n    display: none;\n    position: absolute;\n    top: 0;\n    left: 0;\n    display: inline-block; }\n", ""]);

// exports
exports.locals = {
	"screen": "_2clUtm4WXT_k",
	"canvas": "_2Uqots9_FdlF",
	"canvas--active": "_131QYHSveJCL"
};

/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _utils = __webpack_require__(3);

var _base = __webpack_require__(8);

var _host = __webpack_require__(10);

var _viewer = __webpack_require__(9);

var _Controller = __webpack_require__(74);

var _Controller2 = _interopRequireDefault(_Controller);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ControllerContainer = function (_Component) {
  _inherits(ControllerContainer, _Component);

  function ControllerContainer() {
    _classCallCheck(this, ControllerContainer);

    return _possibleConstructorReturn(this, (ControllerContainer.__proto__ || Object.getPrototypeOf(ControllerContainer)).apply(this, arguments));
  }

  _createClass(ControllerContainer, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          i18n = _props.i18n,
          host = _props.host,
          viewer = _props.viewer;


      return _react2.default.createElement(_Controller2.default, { i18n: i18n, host: host, viewer: viewer });
    }
  }]);

  return ControllerContainer;
}(_react.Component);

var enhance = (0, _utils.compose)(_base.useBase, _host.useHost, _viewer.useViewer);

exports.default = enhance(ControllerContainer);

/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Controller = __webpack_require__(75);

var _Controller2 = _interopRequireDefault(_Controller);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Controller2.default;

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _status = __webpack_require__(4);

var STATUS = _interopRequireWildcard(_status);

var _ToolbarContainer = __webpack_require__(76);

var _ToolbarContainer2 = _interopRequireDefault(_ToolbarContainer);

var _ConfirmModalContainer = __webpack_require__(106);

var _ConfirmModalContainer2 = _interopRequireDefault(_ConfirmModalContainer);

var _FunctionKeyboardContainer = __webpack_require__(119);

var _FunctionKeyboardContainer2 = _interopRequireDefault(_FunctionKeyboardContainer);

var _HostMessageOverlay = __webpack_require__(127);

var _HostMessageOverlay2 = _interopRequireDefault(_HostMessageOverlay);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Controller = function (_Component) {
  _inherits(Controller, _Component);

  function Controller() {
    _classCallCheck(this, Controller);

    return _possibleConstructorReturn(this, (Controller.__proto__ || Object.getPrototypeOf(Controller)).apply(this, arguments));
  }

  _createClass(Controller, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          i18n = _props.i18n,
          host = _props.host,
          viewer = _props.viewer;

      if (viewer.status !== STATUS.VIEWER.RUN) return null;

      return _react2.default.createElement(
        _react.Fragment,
        null,
        _react2.default.createElement(_ToolbarContainer2.default, null),
        _react2.default.createElement(_FunctionKeyboardContainer2.default, null),
        _react2.default.createElement(_ConfirmModalContainer2.default, null),
        _react2.default.createElement(_HostMessageOverlay2.default, {
          i18n: i18n,
          status: host.status,
          fixed: viewer.isScreenOverflow
        })
      );
    }
  }]);

  return Controller;
}(_react.Component);

exports.default = Controller;

/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _utils = __webpack_require__(3);

var _viewer = __webpack_require__(9);

var _base = __webpack_require__(8);

var _host = __webpack_require__(10);

var _ToolBar = __webpack_require__(77);

var _ToolBar2 = _interopRequireDefault(_ToolBar);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ToolbarContainer = function (_Component) {
  _inherits(ToolbarContainer, _Component);

  function ToolbarContainer() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, ToolbarContainer);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = ToolbarContainer.__proto__ || Object.getPrototypeOf(ToolbarContainer)).call.apply(_ref, [this].concat(args))), _this), _this.addActivatedControlApp = function (selectedApp) {
      var _this$props = _this.props,
          viewer = _this$props.viewer,
          viewerActions = _this$props.viewerActions;

      var filteredActivatedControlApps = viewer.activatedControlApps.filter(function (app) {
        return app.groupId !== selectedApp.groupId;
      });

      viewerActions.updateActivatedControlApp([].concat(_toConsumableArray(filteredActivatedControlApps), [{ appId: selectedApp.id, groupId: selectedApp.groupId }]));
    }, _this.removeActivatedControlApp = function (selectedApp) {
      var _this$props2 = _this.props,
          viewer = _this$props2.viewer,
          viewerActions = _this$props2.viewerActions;

      var filteredActivatedControlApps = viewer.activatedControlApps.filter(function (app) {
        return app.appId !== selectedApp.id;
      });

      viewerActions.updateActivatedControlApp([].concat(_toConsumableArray(filteredActivatedControlApps)));
    }, _this.changePaintColor = function (paintColorIndex) {
      var viewer = _this.props.viewer;

      if (viewer.paintColor === paintColorIndex) return;

      var viewerActions = _this.props.viewerActions;

      viewerActions.changePaintColor(paintColorIndex);
    }, _this.handleScreenCapture = function () {
      var viewerActions = _this.props.viewerActions;

      viewerActions.updateScreenCaptureCount();
    }, _this.changeScreenRatio = function () {
      var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
          ratio = _ref2.ratio;

      var _this$props3 = _this.props,
          viewer = _this$props3.viewer,
          config = _this$props3.config;

      var lastScreenRatio = config.screenRatios.length - 1;
      var nextScreenRatio = viewer.screenRatio === lastScreenRatio ? 0 : viewer.screenRatio + 1;

      if (ratio) nextScreenRatio = config.screenRatios.indexOf(ratio);

      var viewerActions = _this.props.viewerActions;

      viewerActions.changeScreenRatio(nextScreenRatio);
    }, _this.handleConfirmModal = function () {
      var baseActions = _this.props.baseActions;

      baseActions.showModal('exitViewer');
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(ToolbarContainer, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          config = _props.config,
          i18n = _props.i18n,
          host = _props.host,
          viewer = _props.viewer,
          remoteControl = _props.remoteControl;


      return _react2.default.createElement(_ToolBar2.default, {
        config: config,
        i18n: i18n,
        host: host,
        viewer: viewer,
        remoteControl: remoteControl,
        addActivatedControlApp: this.addActivatedControlApp,
        removeActivatedControlApp: this.removeActivatedControlApp,
        changePaintColor: this.changePaintColor,
        onScreenCapture: this.handleScreenCapture,
        changeScreenRatio: this.changeScreenRatio,
        onConfirmModal: this.handleConfirmModal
      });
    }
  }]);

  return ToolbarContainer;
}(_react.Component);

var enhance = (0, _utils.compose)(_base.useBase, _host.useHost, _viewer.useViewer);

exports.default = enhance(ToolbarContainer);

/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ToolBar = __webpack_require__(78);

var _ToolBar2 = _interopRequireDefault(_ToolBar);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _ToolBar2.default;

/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _ToolBar = __webpack_require__(79);

var _ToolBar2 = _interopRequireDefault(_ToolBar);

var _webviewer = __webpack_require__(5);

var _webviewer2 = _interopRequireDefault(_webviewer);

var _utils = __webpack_require__(3);

var _ScreenNav = __webpack_require__(89);

var _ScreenNav2 = _interopRequireDefault(_ScreenNav);

var _ToolBarBaseDropdown = __webpack_require__(93);

var _ToolBarBaseDropdown2 = _interopRequireDefault(_ToolBarBaseDropdown);

var _ToolBarColorPickerDropdown = __webpack_require__(98);

var _ToolBarColorPickerDropdown2 = _interopRequireDefault(_ToolBarColorPickerDropdown);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ToolBar = function (_Component) {
  _inherits(ToolBar, _Component);

  function ToolBar(props) {
    _classCallCheck(this, ToolBar);

    var _this = _possibleConstructorReturn(this, (ToolBar.__proto__ || Object.getPrototypeOf(ToolBar)).call(this, props));

    _this.state = {
      isOpenToolbar: true,
      isOpenDropdown: false,
      activatedDropdown: null,
      selectedMouseEvent: _this.hasActivatedApp(1) ? 'laser-pointer' : 'km-control',
      selectedKeyboardEvent: _this.hasActivatedApp(4) ? 'screen-keyboard' : 'function-keyboard'
    };

    _this.hideDropdown = function () {
      _this.setState({ isOpenDropdown: false });
    };

    _this.onToggle = function () {
      _this.setState({
        isOpenToolbar: !_this.state.isOpenToolbar
      });
    };

    _this.toggleMouseMenu = function (target) {
      var isOpenDropdown = _this.state.isOpenDropdown;


      if (isOpenDropdown || !target) {
        _this.toggleDropdown('mouseGroup');
      }

      if (target) {
        _this.setState({
          selectedMouseEvent: target
        });
      }
    };

    _this.toggleKeyboardMenu = function (target) {
      var isOpenDropdown = _this.state.isOpenDropdown;


      if (isOpenDropdown || !target) {
        _this.toggleDropdown('virtualKeyGroup');
      }

      if (target) {
        target === 'function-keyboard' && _this.toggleFunctionKeyboard();
        target === 'screen-keyboard' && _this.toggleScreenKeyboard();

        _this.setState({
          selectedKeyboardEvent: target
        });
      }
    };

    _this.changeMouseEvent = function (target) {
      // app 활성화
      if (target === 'km-control') {
        _this.toggleKmControl();
        _this.toggleMouseMenu(target);
        return;
      }

      if (target === 'laser-pointer') {
        _this.toggleLaserPointer();
        _this.toggleMouseMenu(target);
        return;
      }

      if (target === 'draw') _this.toggleDraw();
    };

    _this.changeKeyboardEvent = function (target) {
      // app 비활성화
      _this.clearKeyboardEvent();

      // app 활성화
      _this.toggleKeyboardMenu(target);
    };

    _this.toggleKmControl = function () {
      var _this$props = _this.props,
          addActivatedControlApp = _this$props.addActivatedControlApp,
          removeActivatedControlApp = _this$props.removeActivatedControlApp;

      var kmControlApp = _this.findControlApp(0);
      var isActivated = !_this.findActivatedControlApp(kmControlApp);

      if (isActivated) {
        addActivatedControlApp(kmControlApp);
      } else {
        removeActivatedControlApp(kmControlApp);
      }
    };

    _this.toggleLaserPointer = function () {
      var _this$props2 = _this.props,
          addActivatedControlApp = _this$props2.addActivatedControlApp,
          removeActivatedControlApp = _this$props2.removeActivatedControlApp;

      var laserPointerApp = _this.findControlApp(1);
      var isActivated = !_this.findActivatedControlApp(laserPointerApp);

      if (isActivated) {
        addActivatedControlApp(laserPointerApp);
      } else {
        removeActivatedControlApp(laserPointerApp);
      }
    };

    _this.toggleDraw = function () {
      var _this$props3 = _this.props,
          addActivatedControlApp = _this$props3.addActivatedControlApp,
          removeActivatedControlApp = _this$props3.removeActivatedControlApp;

      var drawApp = _this.findControlApp(2);
      var isActivated = !_this.findActivatedControlApp(drawApp);

      if (isActivated) {
        addActivatedControlApp(drawApp);
      } else {
        removeActivatedControlApp(drawApp);
      }
    };

    _this.toggleFunctionKeyboard = function () {
      var kmControl = _this.hasActivatedApp(0);
      if (!kmControl) return;

      var _this$props4 = _this.props,
          addActivatedControlApp = _this$props4.addActivatedControlApp,
          removeActivatedControlApp = _this$props4.removeActivatedControlApp;

      var functionKeyboardApp = _this.findControlApp(3);
      var isActivated = !_this.findActivatedControlApp(functionKeyboardApp);

      if (isActivated) {
        addActivatedControlApp(functionKeyboardApp);
      } else {
        removeActivatedControlApp(functionKeyboardApp);
      }
    };

    _this.toggleScreenKeyboard = function () {
      var remoteControl = _this.props.remoteControl;

      var screenKeyboard = _this.findControlApp(4);
      var isActivated = !_this.findActivatedControlApp(screenKeyboard);

      if (isActivated) {
        remoteControl.startScreenKeyboard();
      } else {
        remoteControl.endScreenKeyboard();
      }
    };

    _this.clearPaint = function () {
      var remoteControl = _this.props.remoteControl;

      remoteControl.clearPaint();
      _this.toggleDropdown('drawGroup');
    };

    _this.handleFullScreen = function () {
      var fullScreen = _this.findControlApp(5);
      var isActivated = !_this.findActivatedControlApp(fullScreen);

      if (isActivated) {
        document.documentElement.requestFullScreen && document.documentElement.requestFullScreen();
        document.documentElement.webkitRequestFullScreen && document.documentElement.webkitRequestFullScreen();
        document.documentElement.mozRequestFullScreen && document.documentElement.mozRequestFullScreen();
        document.documentElement.msRequestFullscreen && document.documentElement.msRequestFullscreen();
      } else {
        document.exitFullscreen && document.exitFullscreen();
        document.webkitExitFullscreen && document.webkitExitFullscreen();
        document.mozCancelFullScreen && document.mozCancelFullScreen();
        document.msExitFullscreen && document.msExitFullscreen();
      }
    };

    return _this;
  }

  _createClass(ToolBar, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var remoteControl = this.props.remoteControl;

      var hasActivatedControlApps = _webviewer2.default.get('activatedControlApps') || false;
      var isActivatedControlApps = hasActivatedControlApps && hasActivatedControlApps.length;
      isActivatedControlApps ? remoteControl.requestKmControl() : this.toggleKmControl();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      var prevActivatedControlApps = prevProps.viewer.activatedControlApps;
      var activatedControlApps = this.props.viewer.activatedControlApps;

      if (prevActivatedControlApps !== activatedControlApps) {
        this.updateActiveRemoteControl(prevActivatedControlApps, activatedControlApps);
      }
    }
  }, {
    key: 'findControlApp',
    value: function findControlApp(id) {
      var controlApps = this.props.config.controlApps;

      return controlApps.find(function (app) {
        return app.id === id;
      });
    }
  }, {
    key: 'findActivatedControlApp',
    value: function findActivatedControlApp(selectedApp) {
      var activatedControlApps = this.props.viewer.activatedControlApps;

      return !(activatedControlApps.findIndex(function (app) {
        return app.appId === selectedApp.id;
      }) === -1);
    }
  }, {
    key: 'hasActivatedApp',
    value: function hasActivatedApp(id) {
      var activatedControlApps = this.props.viewer.activatedControlApps;

      return !!activatedControlApps.find(function (app) {
        return app.appId === id;
      });
    }
  }, {
    key: 'hasActivatedDropdown',
    value: function hasActivatedDropdown(target) {
      var _state = this.state,
          isOpenDropdown = _state.isOpenDropdown,
          activatedDropdown = _state.activatedDropdown;

      return isOpenDropdown && activatedDropdown === target;
    }
  }, {
    key: 'toggleDropdown',
    value: function toggleDropdown(target) {
      var activatedDropdown = this.state.activatedDropdown;


      this.setState({
        activatedDropdown: target,
        isOpenDropdown: activatedDropdown !== target ? true : !this.state.isOpenDropdown
      });
    }
  }, {
    key: 'getMouseGroupAppName',
    value: function getMouseGroupAppName(activatedControlApps) {
      var item = activatedControlApps.find(function (app) {
        return app.groupId === 0;
      });

      if (!item) return false;
      if (item.appId === 0) return 'kmControl';
      if (item.appId === 1) return 'laserPointer';
      if (item.appId === 2) return 'draw';
    }
  }, {
    key: 'updateActiveRemoteControl',
    value: function updateActiveRemoteControl(prevActivatedControlApps, activatedControlApps) {
      var remoteControl = this.props.remoteControl;

      var prevActivatedApp = this.getMouseGroupAppName(prevActivatedControlApps);
      var activatedApp = this.getMouseGroupAppName(activatedControlApps);

      if (activatedApp === prevActivatedApp) return;

      // 비활성화
      this.clearKeyboardEvent();
      if (prevActivatedApp === 'kmControl') remoteControl.endKmControl();
      if (prevActivatedApp === 'laserPointer') remoteControl.endLaserPointer();
      if (prevActivatedApp === 'draw') remoteControl.endDraw();

      // 활성화
      if (activatedApp === 'kmControl') remoteControl.requestKmControl();
      if (activatedApp === 'laserPointer') remoteControl.startLaserPointer();
      if (activatedApp === 'draw') remoteControl.startDraw();
    }
  }, {
    key: 'clearKeyboardEvent',
    value: function clearKeyboardEvent() {
      var toggleFunctionKeyboard = this.toggleFunctionKeyboard,
          toggleScreenKeyboard = this.toggleScreenKeyboard;
      var activatedControlApps = this.props.viewer.activatedControlApps;

      var activatedApp = activatedControlApps.find(function (app) {
        return app.groupId === 1;
      });
      var activatedFunctionKeyboard = activatedApp && activatedApp.appId === 3;
      var activatedScreenKeyboard = activatedApp && activatedApp.appId === 4;

      // app 비활성화
      if (activatedFunctionKeyboard) toggleFunctionKeyboard('function-keyboard');
      if (activatedScreenKeyboard) toggleScreenKeyboard('screen-keyboard');
    }
  }, {
    key: 'render',
    value: function render() {
      var _mouseEventGroup,
          _keyboardEventGroup,
          _this2 = this;

      var _state2 = this.state,
          isOpenToolbar = _state2.isOpenToolbar,
          selectedMouseEvent = _state2.selectedMouseEvent,
          selectedKeyboardEvent = _state2.selectedKeyboardEvent;
      var onToggle = this.onToggle,
          toggleMouseMenu = this.toggleMouseMenu,
          toggleKeyboardMenu = this.toggleKeyboardMenu,
          clearPaint = this.clearPaint,
          hideDropdown = this.hideDropdown,
          changeMouseEvent = this.changeMouseEvent,
          changeKeyboardEvent = this.changeKeyboardEvent,
          toggleFunctionKeyboard = this.toggleFunctionKeyboard,
          toggleScreenKeyboard = this.toggleScreenKeyboard;
      var _props = this.props,
          i18n = _props.i18n,
          host = _props.host,
          viewer = _props.viewer,
          config = _props.config,
          remoteControl = _props.remoteControl,
          changePaintColor = _props.changePaintColor,
          onScreenCapture = _props.onScreenCapture,
          changeScreenRatio = _props.changeScreenRatio,
          onConfirmModal = _props.onConfirmModal;
      var monitorInfo = host.monitorInfo;

      var kmControl = this.hasActivatedApp(0);
      var laserPointer = this.hasActivatedApp(1);
      var draw = this.hasActivatedApp(2);
      var functionKeyboard = this.hasActivatedApp(3);
      var screenKeyboard = this.hasActivatedApp(4);
      var fullScreen = this.hasActivatedApp(5);
      var screenRatio = config.screenRatios[viewer.screenRatio];
      var monitorSize = monitorInfo.monitors.length;
      var mouseMenu = this.hasActivatedDropdown('mouseGroup');
      var keyboardMenu = this.hasActivatedDropdown('virtualKeyGroup');
      var drawMenu = this.hasActivatedDropdown('drawGroup');

      var mouseMenuItems = [{ id: 0, target: 'km-control', value: i18n.message('mouseAndKeyboard'), active: kmControl }, { id: 1, target: 'laser-pointer', value: i18n.message('laserPointer'), active: laserPointer }];

      var keyboardMenuItems = [{ id: 0, target: 'function-keyboard', value: i18n.message('functionKey'), active: functionKeyboard }, { id: 1, target: 'screen-keyboard', value: i18n.message('screenKeyboard'), active: screenKeyboard }];

      var kmControlStyle = 'toolbar-open__item--km-control' + (kmControl ? '--active' : '');
      var laserPointerStyle = 'toolbar-open__item--laser-pointer' + (laserPointer ? '--active' : '');
      var functionKeyStyle = 'toolbar-open__item--function-keyboard' + (kmControl ? functionKeyboard ? '--active' : '' : '--disable');
      var screenKeyStyle = 'toolbar-open__item--screen-keyboard' + (kmControl ? screenKeyboard ? '--active' : '' : '--disable');
      var toolbarOpenStyle = 'toolbar-open' + (viewer.isScreenOverflow ? '--fixed' : '');
      var keyboardSelectStyle = 'toolbar-open__select' + (!kmControl ? '--disable' : '');
      var drawOpenStyle = 'toolbar-open__item--draw' + (draw ? '--active' : '');
      var drawSelectStyle = 'toolbar-open__select' + (!draw ? '--disable' : '');
      var fullScreenStyle = 'toolbar-open__item--full-screen-' + (fullScreen ? 'minimize' : 'expand');
      var changeMonitorStyle = 'toolbar-open__item--change-monitor' + (monitorSize <= 1 ? '--none' : '');
      var toolbarCloseStyle = 'toolbar-close' + (viewer.isScreenOverflow ? '--fixed' : '');

      var mouseEventGroup = (_mouseEventGroup = {}, _defineProperty(_mouseEventGroup, 'km-control', _react2.default.createElement(
        'div',
        {
          className: _ToolBar2.default[kmControlStyle],
          onClick: function onClick() {
            return changeMouseEvent('km-control');
          }
        },
        _react2.default.createElement(
          'div',
          { className: _ToolBar2.default['toolbar-open__tooltip-wrap'] },
          _react2.default.createElement(
            'span',
            { className: _ToolBar2.default['toolbar-open__tooltip'] },
            i18n.message('mouseAndKeyboard')
          )
        )
      )), _defineProperty(_mouseEventGroup, 'laser-pointer', _react2.default.createElement(
        'div',
        {
          className: _ToolBar2.default[laserPointerStyle],
          onClick: function onClick() {
            return changeMouseEvent('laser-pointer');
          }
        },
        _react2.default.createElement(
          'div',
          { className: _ToolBar2.default['toolbar-open__tooltip-wrap'] },
          _react2.default.createElement(
            'span',
            { className: _ToolBar2.default['toolbar-open__tooltip'] },
            i18n.message('laserPointer')
          )
        )
      )), _mouseEventGroup);

      var keyboardEventGroup = (_keyboardEventGroup = {}, _defineProperty(_keyboardEventGroup, 'function-keyboard', _react2.default.createElement(
        'div',
        {
          className: _ToolBar2.default[functionKeyStyle],
          onClick: kmControl ? toggleFunctionKeyboard : null
        },
        _react2.default.createElement(
          'div',
          { className: _ToolBar2.default['toolbar-open__tooltip-wrap'] },
          _react2.default.createElement(
            'span',
            { className: _ToolBar2.default['toolbar-open__tooltip'] },
            i18n.message('functionKey')
          )
        )
      )), _defineProperty(_keyboardEventGroup, 'screen-keyboard', _react2.default.createElement(
        'div',
        {
          className: _ToolBar2.default[screenKeyStyle],
          onClick: kmControl ? toggleScreenKeyboard : null
        },
        _react2.default.createElement(
          'div',
          { className: _ToolBar2.default['toolbar-open__tooltip-wrap'] },
          _react2.default.createElement(
            'span',
            { className: _ToolBar2.default['toolbar-open__tooltip'] },
            i18n.message('screenKeyboard')
          )
        )
      )), _keyboardEventGroup);

      var openedToolbar = _react2.default.createElement(
        'div',
        { className: _ToolBar2.default[toolbarOpenStyle] },
        _react2.default.createElement(
          'div',
          { className: _ToolBar2.default['toolbar-open__group'] },
          mouseEventGroup[selectedMouseEvent],
          _react2.default.createElement('div', {
            className: _ToolBar2.default['toolbar-open__select'],
            onClick: function onClick() {
              return toggleMouseMenu();
            }
          }),
          _react2.default.createElement(_ToolBarBaseDropdown2.default, {
            visible: mouseMenu,
            items: mouseMenuItems,
            onSelectItem: changeMouseEvent,
            onClose: hideDropdown
          }),
          keyboardEventGroup[_utils.mobileDetect.mobile() ? 'screen-keyboard' : selectedKeyboardEvent],
          !_utils.mobileDetect.mobile() && _react2.default.createElement('div', {
            className: _ToolBar2.default[keyboardSelectStyle],
            onClick: kmControl ? function () {
              return toggleKeyboardMenu();
            } : null
          }),
          _react2.default.createElement(_ToolBarBaseDropdown2.default, {
            visible: keyboardMenu,
            items: keyboardMenuItems,
            onSelectItem: changeKeyboardEvent,
            onClose: hideDropdown
          }),
          _react2.default.createElement(
            'div',
            {
              className: _ToolBar2.default[drawOpenStyle],
              onClick: function onClick() {
                return changeMouseEvent('draw');
              }
            },
            _react2.default.createElement(
              'div',
              { className: _ToolBar2.default['toolbar-open__tooltip-wrap'] },
              _react2.default.createElement(
                'span',
                { className: _ToolBar2.default['toolbar-open__tooltip'] },
                i18n.message('draw')
              )
            )
          ),
          _react2.default.createElement('div', {
            className: _ToolBar2.default[drawSelectStyle],
            onClick: draw ? function () {
              return _this2.toggleDropdown('drawGroup');
            } : null
          }),
          _react2.default.createElement(_ToolBarColorPickerDropdown2.default, {
            i18n: i18n,
            visible: drawMenu && draw,
            colors: config.canvasTool.colors,
            paintColor: viewer.paintColor,
            changePaintColor: changePaintColor,
            clearPaint: clearPaint,
            onClose: hideDropdown
          })
        ),
        _react2.default.createElement('div', { className: _ToolBar2.default['toolbar-open__item-divide'] }),
        _react2.default.createElement(
          'div',
          { className: _ToolBar2.default['toolbar-open__group'] },
          _react2.default.createElement(
            'div',
            {
              className: _ToolBar2.default['toolbar-open__item--screen-capture'],
              onClick: onScreenCapture
            },
            viewer.screenCaptureNumber > 0 && _react2.default.createElement(
              'div',
              { className: _ToolBar2.default['toolbar-open__item--event-number-notice'] },
              viewer.screenCaptureNumber
            ),
            _react2.default.createElement(
              'div',
              { className: _ToolBar2.default['toolbar-open__tooltip-wrap'] },
              _react2.default.createElement(
                'span',
                { className: _ToolBar2.default['toolbar-open__tooltip'] },
                i18n.message('screenCapture')
              )
            )
          ),
          _react2.default.createElement(
            'div',
            {
              className: _ToolBar2.default['toolbar-open__item--screen-ratio-' + screenRatio],
              onClick: changeScreenRatio
            },
            _react2.default.createElement(
              'div',
              { className: _ToolBar2.default['toolbar-open__tooltip-wrap'] },
              _react2.default.createElement(
                'span',
                { className: _ToolBar2.default['toolbar-open__tooltip'] },
                i18n.message('screenRatio')
              )
            )
          ),
          _react2.default.createElement(
            'div',
            {
              className: _ToolBar2.default[fullScreenStyle],
              onClick: this.handleFullScreen
            },
            _react2.default.createElement(
              'div',
              { className: _ToolBar2.default['toolbar-open__tooltip-wrap'] },
              _react2.default.createElement(
                'span',
                { className: _ToolBar2.default['toolbar-open__tooltip'] },
                i18n.message('fullScreen')
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: _ToolBar2.default[changeMonitorStyle] },
            _react2.default.createElement(
              'div',
              { className: _ToolBar2.default['toolbar-open__screen-nav-wrap'] },
              _react2.default.createElement(_ScreenNav2.default, {
                monitorInfo: monitorInfo,
                remoteControl: remoteControl
              })
            ),
            _react2.default.createElement(
              'div',
              { className: _ToolBar2.default['toolbar-open__tooltip-wrap'] },
              _react2.default.createElement(
                'span',
                { className: _ToolBar2.default['toolbar-open__tooltip'] },
                i18n.message('changeMonitor')
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          {
            className: _ToolBar2.default['toolbar-open__item--close'],
            onClick: onConfirmModal
          },
          _react2.default.createElement(
            'div',
            { className: _ToolBar2.default['toolbar-open__tooltip-wrap'] },
            _react2.default.createElement(
              'span',
              { className: _ToolBar2.default['toolbar-open__tooltip'] },
              i18n.message('exit')
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: _ToolBar2.default['toolbar-open__item--fold'], onClick: onToggle },
          _react2.default.createElement(
            'div',
            { className: _ToolBar2.default['toolbar-open__tooltip-wrap'] },
            _react2.default.createElement(
              'span',
              { className: _ToolBar2.default['toolbar-open__tooltip'] },
              i18n.message('fold')
            )
          )
        )
      );

      var closedToolbar = _react2.default.createElement(
        'div',
        { className: _ToolBar2.default[toolbarCloseStyle], onClick: onToggle },
        _react2.default.createElement('div', { className: _ToolBar2.default['toolbar-close--toolbar-expand'] }),
        _react2.default.createElement(
          'div',
          { className: _ToolBar2.default['toolbar-close__tooltip-wrap'] },
          _react2.default.createElement(
            'span',
            { className: _ToolBar2.default['toolbar-close__tooltip'] },
            i18n.message('expand')
          )
        )
      );

      return isOpenToolbar ? openedToolbar : closedToolbar;
    }
  }]);

  return ToolBar;
}(_react.Component);

exports.default = ToolBar;

/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(80);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(2)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./ToolBar.scss", function() {
		var newContent = require("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./ToolBar.scss");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__(16);
exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "._1lwuxjckyjYt {\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n  position: absolute;\n  top: 0;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n          transform: translateX(-50%);\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  background: #2185DA;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  -webkit-box-shadow: 0 2px 10px 1px rgba(0, 0, 0, 0.3);\n          box-shadow: 0 2px 10px 1px rgba(0, 0, 0, 0.3); }\n  ._2-ep5kJGdNdR {\n    display: -webkit-inline-box;\n    display: -ms-inline-flexbox;\n    display: inline-flex;\n    position: absolute;\n    top: 0;\n    left: 50%;\n    -webkit-transform: translateX(-50%);\n            transform: translateX(-50%);\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    background: #2185DA;\n    border-bottom-left-radius: 10px;\n    border-bottom-right-radius: 10px;\n    -webkit-box-shadow: 0 2px 10px 1px rgba(0, 0, 0, 0.3);\n            box-shadow: 0 2px 10px 1px rgba(0, 0, 0, 0.3);\n    position: fixed; }\n\n._Q30jRxAdWBKL {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 4px 6px; }\n\n._22UKXhGDEIrI {\n  position: relative;\n  top: 6px; }\n\n._3XYTuzVZ1Iih {\n  position: absolute;\n  top: 25px;\n  left: 45px; }\n\n._13CQSmsntPyo {\n  width: 12px;\n  height: 40px;\n  border-radius: 4px;\n  position: relative; }\n  ._13CQSmsntPyo:hover {\n    background: #0762AB;\n    cursor: pointer; }\n  ._13CQSmsntPyo::before {\n    content: \" \";\n    position: absolute;\n    display: block;\n    bottom: 8px;\n    left: calc(50% - 4px);\n    width: 0;\n    height: 0;\n    border-top: 4px solid white;\n    border-left: 4px solid transparent;\n    border-right: 4px solid transparent; }\n  ._NK6dUmuq3p0E {\n    width: 12px;\n    height: 40px;\n    border-radius: 4px;\n    position: relative;\n    opacity: 0.2; }\n    ._NK6dUmuq3p0E:hover {\n      background: #0762AB;\n      cursor: pointer; }\n    ._NK6dUmuq3p0E::before {\n      content: \" \";\n      position: absolute;\n      display: block;\n      bottom: 8px;\n      left: calc(50% - 4px);\n      width: 0;\n      height: 0;\n      border-top: 4px solid white;\n      border-left: 4px solid transparent;\n      border-right: 4px solid transparent; }\n    ._NK6dUmuq3p0E:hover {\n      background: transparent;\n      cursor: default; }\n\n._1Vs1z-kI7_ie {\n  position: absolute;\n  background: #003764;\n  color: white;\n  font-size: 0.6em;\n  letter-spacing: -0.01em;\n  padding: 2px 5px 4px;\n  border-radius: 8px;\n  right: -2px;\n  top: 0; }\n\n._1MWZ6zcMsOBX {\n  background: #1d77c3;\n  width: 1px;\n  height: 25px; }\n\n._iixqKaBeyxXN {\n  margin: 6px; }\n\n._2gUn220csnrb, ._2SXixVB57RSr {\n  opacity: 0;\n  visibility: hidden;\n  width: auto;\n  min-width: 24px;\n  font-size: 12px;\n  font-weight: bold;\n  white-space: nowrap;\n  background-color: #ffffff;\n  color: #333333;\n  text-align: left;\n  border: 1px solid #444444;\n  border-radius: 2px;\n  -webkit-box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 3px 10px 0 rgba(0, 0, 0, 0.19);\n          box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 3px 10px 0 rgba(0, 0, 0, 0.19);\n  padding: 4px 7px;\n  position: absolute;\n  z-index: 1000;\n  bottom: -20px;\n  -webkit-transform: translateX(-50%);\n          transform: translateX(-50%); }\n  ._2gUn220csnrb::after, ._2SXixVB57RSr::after {\n    content: \"\";\n    position: absolute;\n    top: -10px;\n    left: calc(50% - 5px);\n    border-width: 5px;\n    border-style: solid;\n    border-color: transparent transparent #ffffff transparent; }\n\n._3bi3IAel9wM7 {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 42px;\n  height: 40px;\n  border-radius: 4px;\n  margin: 0; }\n  ._3bi3IAel9wM7:first-child {\n    margin-left: 6px; }\n  ._3bi3IAel9wM7::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 36px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._3bi3IAel9wM7:hover {\n    cursor: pointer;\n    background-color: #096DBB; }\n    ._3bi3IAel9wM7:hover ._2gUn220csnrb {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._3bi3IAel9wM7::before {\n    background: url(" + escape(__webpack_require__(11)) + ") no-repeat; }\n  ._2l-9wEmYXpET {\n    position: relative;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    width: 42px;\n    height: 40px;\n    border-radius: 4px;\n    margin: 0;\n    background-color: #013a67; }\n    ._2l-9wEmYXpET:first-child {\n      margin-left: 6px; }\n    ._2l-9wEmYXpET::before {\n      content: '';\n      display: block;\n      width: 36px;\n      height: 36px;\n      margin: 0 auto;\n      -ms-flex-item-align: center;\n          align-self: center; }\n    ._2l-9wEmYXpET:hover {\n      cursor: pointer;\n      background-color: #096DBB; }\n      ._2l-9wEmYXpET:hover ._2gUn220csnrb {\n        opacity: 1;\n        visibility: visible;\n        -webkit-transition: opacity 0.3s 0.5s;\n        transition: opacity 0.3s 0.5s; }\n    ._2l-9wEmYXpET::before {\n      background: url(" + escape(__webpack_require__(11)) + ") no-repeat; }\n  ._B8EmaoqqYQIG {\n    position: relative;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    width: 42px;\n    height: 40px;\n    border-radius: 4px;\n    margin: 0;\n    opacity: 0.2; }\n    ._B8EmaoqqYQIG:first-child {\n      margin-left: 6px; }\n    ._B8EmaoqqYQIG::before {\n      content: '';\n      display: block;\n      width: 36px;\n      height: 36px;\n      margin: 0 auto;\n      -ms-flex-item-align: center;\n          align-self: center; }\n    ._B8EmaoqqYQIG:hover {\n      cursor: pointer;\n      background-color: #096DBB; }\n      ._B8EmaoqqYQIG:hover ._2gUn220csnrb {\n        opacity: 1;\n        visibility: visible;\n        -webkit-transition: opacity 0.3s 0.5s;\n        transition: opacity 0.3s 0.5s; }\n    ._B8EmaoqqYQIG::before {\n      background: url(" + escape(__webpack_require__(11)) + ") no-repeat; }\n    ._B8EmaoqqYQIG:hover {\n      cursor: default;\n      background: transparent; }\n      ._B8EmaoqqYQIG:hover ._2gUn220csnrb {\n        opacity: 0;\n        visibility: hidden; }\n\n._2z4vcaprrgSf {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 42px;\n  height: 40px;\n  border-radius: 4px;\n  margin: 0; }\n  ._2z4vcaprrgSf:first-child {\n    margin-left: 6px; }\n  ._2z4vcaprrgSf::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 36px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._2z4vcaprrgSf:hover {\n    cursor: pointer;\n    background-color: #096DBB; }\n    ._2z4vcaprrgSf:hover ._2gUn220csnrb {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._2z4vcaprrgSf::before {\n    background: url(" + escape(__webpack_require__(12)) + ") no-repeat; }\n  ._1P7spIVSQHW2 {\n    position: relative;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    width: 42px;\n    height: 40px;\n    border-radius: 4px;\n    margin: 0;\n    background-color: #013a67; }\n    ._1P7spIVSQHW2:first-child {\n      margin-left: 6px; }\n    ._1P7spIVSQHW2::before {\n      content: '';\n      display: block;\n      width: 36px;\n      height: 36px;\n      margin: 0 auto;\n      -ms-flex-item-align: center;\n          align-self: center; }\n    ._1P7spIVSQHW2:hover {\n      cursor: pointer;\n      background-color: #096DBB; }\n      ._1P7spIVSQHW2:hover ._2gUn220csnrb {\n        opacity: 1;\n        visibility: visible;\n        -webkit-transition: opacity 0.3s 0.5s;\n        transition: opacity 0.3s 0.5s; }\n    ._1P7spIVSQHW2::before {\n      background: url(" + escape(__webpack_require__(12)) + ") no-repeat; }\n  ._2zlnV4Z4Afgb {\n    position: relative;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    width: 42px;\n    height: 40px;\n    border-radius: 4px;\n    margin: 0;\n    opacity: 0.2; }\n    ._2zlnV4Z4Afgb:first-child {\n      margin-left: 6px; }\n    ._2zlnV4Z4Afgb::before {\n      content: '';\n      display: block;\n      width: 36px;\n      height: 36px;\n      margin: 0 auto;\n      -ms-flex-item-align: center;\n          align-self: center; }\n    ._2zlnV4Z4Afgb:hover {\n      cursor: pointer;\n      background-color: #096DBB; }\n      ._2zlnV4Z4Afgb:hover ._2gUn220csnrb {\n        opacity: 1;\n        visibility: visible;\n        -webkit-transition: opacity 0.3s 0.5s;\n        transition: opacity 0.3s 0.5s; }\n    ._2zlnV4Z4Afgb::before {\n      background: url(" + escape(__webpack_require__(12)) + ") no-repeat; }\n    ._2zlnV4Z4Afgb:hover {\n      cursor: default;\n      background: transparent; }\n      ._2zlnV4Z4Afgb:hover ._2gUn220csnrb {\n        opacity: 0;\n        visibility: hidden; }\n\n._tAZxXgcQZDGK {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 42px;\n  height: 40px;\n  border-radius: 4px;\n  margin: 0; }\n  ._tAZxXgcQZDGK:first-child {\n    margin-left: 6px; }\n  ._tAZxXgcQZDGK::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 36px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._tAZxXgcQZDGK:hover {\n    cursor: pointer;\n    background-color: #096DBB; }\n    ._tAZxXgcQZDGK:hover ._2gUn220csnrb {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._tAZxXgcQZDGK::before {\n    background: url(" + escape(__webpack_require__(13)) + ") no-repeat; }\n  ._yYm7fXtIuITv {\n    position: relative;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    width: 42px;\n    height: 40px;\n    border-radius: 4px;\n    margin: 0;\n    background-color: #013a67; }\n    ._yYm7fXtIuITv:first-child {\n      margin-left: 6px; }\n    ._yYm7fXtIuITv::before {\n      content: '';\n      display: block;\n      width: 36px;\n      height: 36px;\n      margin: 0 auto;\n      -ms-flex-item-align: center;\n          align-self: center; }\n    ._yYm7fXtIuITv:hover {\n      cursor: pointer;\n      background-color: #096DBB; }\n      ._yYm7fXtIuITv:hover ._2gUn220csnrb {\n        opacity: 1;\n        visibility: visible;\n        -webkit-transition: opacity 0.3s 0.5s;\n        transition: opacity 0.3s 0.5s; }\n    ._yYm7fXtIuITv::before {\n      background: url(" + escape(__webpack_require__(13)) + ") no-repeat; }\n  ._1sR66InqQ0lV {\n    position: relative;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    width: 42px;\n    height: 40px;\n    border-radius: 4px;\n    margin: 0;\n    opacity: 0.2; }\n    ._1sR66InqQ0lV:first-child {\n      margin-left: 6px; }\n    ._1sR66InqQ0lV::before {\n      content: '';\n      display: block;\n      width: 36px;\n      height: 36px;\n      margin: 0 auto;\n      -ms-flex-item-align: center;\n          align-self: center; }\n    ._1sR66InqQ0lV:hover {\n      cursor: pointer;\n      background-color: #096DBB; }\n      ._1sR66InqQ0lV:hover ._2gUn220csnrb {\n        opacity: 1;\n        visibility: visible;\n        -webkit-transition: opacity 0.3s 0.5s;\n        transition: opacity 0.3s 0.5s; }\n    ._1sR66InqQ0lV::before {\n      background: url(" + escape(__webpack_require__(13)) + ") no-repeat; }\n    ._1sR66InqQ0lV:hover {\n      cursor: default;\n      background: transparent; }\n      ._1sR66InqQ0lV:hover ._2gUn220csnrb {\n        opacity: 0;\n        visibility: hidden; }\n\n._VVNBz9K_TcJl {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 42px;\n  height: 40px;\n  border-radius: 4px;\n  margin: 0; }\n  ._VVNBz9K_TcJl:first-child {\n    margin-left: 6px; }\n  ._VVNBz9K_TcJl::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 36px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._VVNBz9K_TcJl:hover {\n    cursor: pointer;\n    background-color: #096DBB; }\n    ._VVNBz9K_TcJl:hover ._2gUn220csnrb {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._VVNBz9K_TcJl::before {\n    background: url(" + escape(__webpack_require__(14)) + ") no-repeat; }\n  ._2JjKioN6Vcc5 {\n    position: relative;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    width: 42px;\n    height: 40px;\n    border-radius: 4px;\n    margin: 0;\n    background-color: #013a67; }\n    ._2JjKioN6Vcc5:first-child {\n      margin-left: 6px; }\n    ._2JjKioN6Vcc5::before {\n      content: '';\n      display: block;\n      width: 36px;\n      height: 36px;\n      margin: 0 auto;\n      -ms-flex-item-align: center;\n          align-self: center; }\n    ._2JjKioN6Vcc5:hover {\n      cursor: pointer;\n      background-color: #096DBB; }\n      ._2JjKioN6Vcc5:hover ._2gUn220csnrb {\n        opacity: 1;\n        visibility: visible;\n        -webkit-transition: opacity 0.3s 0.5s;\n        transition: opacity 0.3s 0.5s; }\n    ._2JjKioN6Vcc5::before {\n      background: url(" + escape(__webpack_require__(14)) + ") no-repeat; }\n  ._pwub9KCIv9ol {\n    position: relative;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    width: 42px;\n    height: 40px;\n    border-radius: 4px;\n    margin: 0;\n    opacity: 0.2; }\n    ._pwub9KCIv9ol:first-child {\n      margin-left: 6px; }\n    ._pwub9KCIv9ol::before {\n      content: '';\n      display: block;\n      width: 36px;\n      height: 36px;\n      margin: 0 auto;\n      -ms-flex-item-align: center;\n          align-self: center; }\n    ._pwub9KCIv9ol:hover {\n      cursor: pointer;\n      background-color: #096DBB; }\n      ._pwub9KCIv9ol:hover ._2gUn220csnrb {\n        opacity: 1;\n        visibility: visible;\n        -webkit-transition: opacity 0.3s 0.5s;\n        transition: opacity 0.3s 0.5s; }\n    ._pwub9KCIv9ol::before {\n      background: url(" + escape(__webpack_require__(14)) + ") no-repeat; }\n    ._pwub9KCIv9ol:hover {\n      cursor: default;\n      background: transparent; }\n      ._pwub9KCIv9ol:hover ._2gUn220csnrb {\n        opacity: 0;\n        visibility: hidden; }\n\n._3E9jtidXQv4G {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 42px;\n  height: 40px;\n  border-radius: 4px;\n  margin: 0; }\n  ._3E9jtidXQv4G:first-child {\n    margin-left: 6px; }\n  ._3E9jtidXQv4G::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 36px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._3E9jtidXQv4G:hover {\n    cursor: pointer;\n    background-color: #096DBB; }\n    ._3E9jtidXQv4G:hover ._2gUn220csnrb {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._3E9jtidXQv4G::before {\n    background: url(" + escape(__webpack_require__(15)) + ") no-repeat; }\n  ._II1eeeHeOlU_ {\n    position: relative;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    width: 42px;\n    height: 40px;\n    border-radius: 4px;\n    margin: 0;\n    background-color: #013a67; }\n    ._II1eeeHeOlU_:first-child {\n      margin-left: 6px; }\n    ._II1eeeHeOlU_::before {\n      content: '';\n      display: block;\n      width: 36px;\n      height: 36px;\n      margin: 0 auto;\n      -ms-flex-item-align: center;\n          align-self: center; }\n    ._II1eeeHeOlU_:hover {\n      cursor: pointer;\n      background-color: #096DBB; }\n      ._II1eeeHeOlU_:hover ._2gUn220csnrb {\n        opacity: 1;\n        visibility: visible;\n        -webkit-transition: opacity 0.3s 0.5s;\n        transition: opacity 0.3s 0.5s; }\n    ._II1eeeHeOlU_::before {\n      background: url(" + escape(__webpack_require__(15)) + ") no-repeat; }\n  ._D7jXdvgxF7A2 {\n    position: relative;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    width: 42px;\n    height: 40px;\n    border-radius: 4px;\n    margin: 0;\n    opacity: 0.2; }\n    ._D7jXdvgxF7A2:first-child {\n      margin-left: 6px; }\n    ._D7jXdvgxF7A2::before {\n      content: '';\n      display: block;\n      width: 36px;\n      height: 36px;\n      margin: 0 auto;\n      -ms-flex-item-align: center;\n          align-self: center; }\n    ._D7jXdvgxF7A2:hover {\n      cursor: pointer;\n      background-color: #096DBB; }\n      ._D7jXdvgxF7A2:hover ._2gUn220csnrb {\n        opacity: 1;\n        visibility: visible;\n        -webkit-transition: opacity 0.3s 0.5s;\n        transition: opacity 0.3s 0.5s; }\n    ._D7jXdvgxF7A2::before {\n      background: url(" + escape(__webpack_require__(15)) + ") no-repeat; }\n    ._D7jXdvgxF7A2:hover {\n      cursor: default;\n      background: transparent; }\n      ._D7jXdvgxF7A2:hover ._2gUn220csnrb {\n        opacity: 0;\n        visibility: hidden; }\n\n._2ZMuuSbDGTmR {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 42px;\n  height: 40px;\n  border-radius: 4px;\n  margin: 0; }\n  ._2ZMuuSbDGTmR:first-child {\n    margin-left: 6px; }\n  ._2ZMuuSbDGTmR::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 36px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._2ZMuuSbDGTmR:hover {\n    cursor: pointer;\n    background-color: #096DBB; }\n    ._2ZMuuSbDGTmR:hover ._2gUn220csnrb {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._2ZMuuSbDGTmR::before {\n    background: url(" + escape(__webpack_require__(81)) + ") no-repeat; }\n\n._3l5Syr9ryIXM {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  position: absolute;\n  top: 1px;\n  left: 22px;\n  min-width: 22px;\n  height: 16px;\n  border-radius: 8px;\n  text-align: center;\n  vertical-align: middle;\n  font-size: 10px;\n  color: #ffffff;\n  background: #00345d; }\n\n._ycVkiBzsRqQg {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 42px;\n  height: 40px;\n  border-radius: 4px;\n  margin: 0; }\n  ._ycVkiBzsRqQg:first-child {\n    margin-left: 6px; }\n  ._ycVkiBzsRqQg::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 36px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._ycVkiBzsRqQg:hover {\n    cursor: pointer;\n    background-color: #096DBB; }\n    ._ycVkiBzsRqQg:hover ._2gUn220csnrb {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._ycVkiBzsRqQg::before {\n    background: url(" + escape(__webpack_require__(82)) + ") no-repeat; }\n\n._22ziUpZB3Nfv {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 42px;\n  height: 40px;\n  border-radius: 4px;\n  margin: 0; }\n  ._22ziUpZB3Nfv:first-child {\n    margin-left: 6px; }\n  ._22ziUpZB3Nfv::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 36px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._22ziUpZB3Nfv:hover {\n    cursor: pointer;\n    background-color: #096DBB; }\n    ._22ziUpZB3Nfv:hover ._2gUn220csnrb {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._22ziUpZB3Nfv::before {\n    background: url(" + escape(__webpack_require__(83)) + ") no-repeat; }\n\n._QZQ7M1E359Tj {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 42px;\n  height: 40px;\n  border-radius: 4px;\n  margin: 0; }\n  ._QZQ7M1E359Tj:first-child {\n    margin-left: 6px; }\n  ._QZQ7M1E359Tj::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 36px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._QZQ7M1E359Tj:hover {\n    cursor: pointer;\n    background-color: #096DBB; }\n    ._QZQ7M1E359Tj:hover ._2gUn220csnrb {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._QZQ7M1E359Tj::before {\n    background: url(" + escape(__webpack_require__(84)) + ") no-repeat; }\n\n._VdKlgAwRJzpo {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 42px;\n  height: 40px;\n  border-radius: 4px;\n  margin: 0; }\n  ._VdKlgAwRJzpo:first-child {\n    margin-left: 6px; }\n  ._VdKlgAwRJzpo::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 36px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._VdKlgAwRJzpo:hover {\n    cursor: pointer;\n    background-color: #096DBB; }\n    ._VdKlgAwRJzpo:hover ._2gUn220csnrb {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._VdKlgAwRJzpo::before {\n    background: url(" + escape(__webpack_require__(85)) + ") no-repeat; }\n\n._1hVyujT8bFxy {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 42px;\n  height: 40px;\n  border-radius: 4px;\n  margin: 0; }\n  ._1hVyujT8bFxy:first-child {\n    margin-left: 6px; }\n  ._1hVyujT8bFxy::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 36px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._1hVyujT8bFxy:hover {\n    cursor: pointer;\n    background-color: #096DBB; }\n    ._1hVyujT8bFxy:hover ._2gUn220csnrb {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._1hVyujT8bFxy::before {\n    background: url(" + escape(__webpack_require__(86)) + ") no-repeat; }\n\n._2iiB5lwT742_ {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 42px;\n  height: 40px;\n  border-radius: 4px;\n  margin: 0; }\n  ._2iiB5lwT742_:first-child {\n    margin-left: 6px; }\n  ._2iiB5lwT742_::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 36px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._2iiB5lwT742_:hover {\n    cursor: pointer;\n    background-color: #096DBB; }\n    ._2iiB5lwT742_:hover ._2gUn220csnrb {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._2iiB5lwT742_::before {\n    background: url(" + escape(__webpack_require__(87)) + ") no-repeat; }\n\n._2MOpJOX0hDZJ {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 42px;\n  height: 40px;\n  border-radius: 4px;\n  margin: 0;\n  width: auto; }\n  ._2MOpJOX0hDZJ:first-child {\n    margin-left: 6px; }\n  ._2MOpJOX0hDZJ::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 36px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._2MOpJOX0hDZJ:hover {\n    cursor: pointer;\n    background-color: #096DBB; }\n    ._2MOpJOX0hDZJ:hover ._2gUn220csnrb {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._2MOpJOX0hDZJ:hover {\n    background: transparent; }\n  ._2MOpJOX0hDZJ::before {\n    content: none; }\n  ._21svvfsGjhHR {\n    display: none; }\n\n._16Ex4gN04g9U {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 42px;\n  height: 40px;\n  border-radius: 4px;\n  margin: 0;\n  width: 50px;\n  margin: 0;\n  height: inherit;\n  -ms-flex-item-align: stretch;\n      align-self: stretch;\n  border-radius: 0;\n  border-left: 1px solid rgba(50, 50, 50, 0.1);\n  margin-left: 10px;\n  align-items: center;\n  flex-direction: column;\n  justify-content: center;\n  background: #167ACF; }\n  ._16Ex4gN04g9U:first-child {\n    margin-left: 6px; }\n  ._16Ex4gN04g9U::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 36px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._16Ex4gN04g9U:hover {\n    cursor: pointer;\n    background-color: #096DBB; }\n    ._16Ex4gN04g9U:hover ._2gUn220csnrb {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._16Ex4gN04g9U::before {\n    background: url(" + escape(__webpack_require__(88)) + ") no-repeat; }\n  ._16Ex4gN04g9U:hover {\n    background: #ED430E; }\n\n._13M6Buj7rCRx {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 42px;\n  height: 40px;\n  border-radius: 4px;\n  margin: 0;\n  width: 50px;\n  margin: 0;\n  height: inherit;\n  -ms-flex-item-align: stretch;\n      align-self: stretch;\n  border-radius: 0;\n  border-left: 1px solid rgba(50, 50, 50, 0.1);\n  background: #0C68B3;\n  border-bottom-right-radius: 10px; }\n  ._13M6Buj7rCRx:first-child {\n    margin-left: 6px; }\n  ._13M6Buj7rCRx::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 36px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._13M6Buj7rCRx:hover {\n    cursor: pointer;\n    background-color: #096DBB; }\n    ._13M6Buj7rCRx:hover ._2gUn220csnrb {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._13M6Buj7rCRx::before {\n    background: url(" + escape(__webpack_require__(21)) + ") no-repeat; }\n  ._13M6Buj7rCRx:hover {\n    background: #F56A15; }\n\n._3Zc65AxNvAMU {\n  z-index: 70;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  position: absolute;\n  width: 88px;\n  height: 18px;\n  top: 0;\n  left: 50%;\n  -webkit-transform: translateX(-50%);\n          transform: translateX(-50%);\n  background: #2185d9;\n  border-radius: 0 0 8px 8px;\n  -webkit-box-shadow: 0 2px 10px 1px rgba(0, 0, 0, 0.3);\n          box-shadow: 0 2px 10px 1px rgba(0, 0, 0, 0.3);\n  cursor: pointer; }\n  ._3Zc65AxNvAMU:hover {\n    background: #f46a16; }\n    ._3Zc65AxNvAMU:hover ._2SXixVB57RSr {\n      opacity: 1;\n      visibility: visible;\n      -webkit-transition: opacity 0.3s 0.5s;\n      transition: opacity 0.3s 0.5s; }\n  ._3C-ScvyGv5dD {\n    z-index: 70;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    position: absolute;\n    width: 88px;\n    height: 18px;\n    top: 0;\n    left: 50%;\n    -webkit-transform: translateX(-50%);\n            transform: translateX(-50%);\n    background: #2185d9;\n    border-radius: 0 0 8px 8px;\n    -webkit-box-shadow: 0 2px 10px 1px rgba(0, 0, 0, 0.3);\n            box-shadow: 0 2px 10px 1px rgba(0, 0, 0, 0.3);\n    cursor: pointer;\n    position: fixed; }\n    ._3C-ScvyGv5dD:hover {\n      background: #f46a16; }\n      ._3C-ScvyGv5dD:hover ._2SXixVB57RSr {\n        opacity: 1;\n        visibility: visible;\n        -webkit-transition: opacity 0.3s 0.5s;\n        transition: opacity 0.3s 0.5s; }\n\n._3Fmc1bqQYIKl {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 36px;\n  height: 18px; }\n  ._3Fmc1bqQYIKl::before {\n    content: '';\n    display: block;\n    width: 36px;\n    height: 18px;\n    margin: 0 auto;\n    -ms-flex-item-align: center;\n        align-self: center; }\n  ._3Fmc1bqQYIKl::before {\n    background: url(" + escape(__webpack_require__(21)) + ") no-repeat center center;\n    -webkit-transform: rotate(-180deg);\n            transform: rotate(-180deg);\n    background-size: cover; }\n", ""]);

// exports
exports.locals = {
	"toolbar-open": "_1lwuxjckyjYt",
	"toolbar-open--fixed": "_2-ep5kJGdNdR",
	"toolbar-open__group": "_Q30jRxAdWBKL",
	"toolbar-open__tooltip-wrap": "_22UKXhGDEIrI",
	"toolbar-close__tooltip-wrap": "_3XYTuzVZ1Iih",
	"toolbar-open__select": "_13CQSmsntPyo",
	"toolbar-open__select--disable": "_NK6dUmuq3p0E",
	"toolbar-open__item-number": "_1Vs1z-kI7_ie",
	"toolbar-open__item-divide": "_1MWZ6zcMsOBX",
	"toolbar-open__screen-nav-wrap": "_iixqKaBeyxXN",
	"toolbar-open__tooltip": "_2gUn220csnrb",
	"toolbar-close__tooltip": "_2SXixVB57RSr",
	"toolbar-open__item--km-control": "_3bi3IAel9wM7",
	"toolbar-open__item--km-control--active": "_2l-9wEmYXpET",
	"toolbar-open__item--km-control--disable": "_B8EmaoqqYQIG",
	"toolbar-open__item--laser-pointer": "_2z4vcaprrgSf",
	"toolbar-open__item--laser-pointer--active": "_1P7spIVSQHW2",
	"toolbar-open__item--laser-pointer--disable": "_2zlnV4Z4Afgb",
	"toolbar-open__item--function-keyboard": "_tAZxXgcQZDGK",
	"toolbar-open__item--function-keyboard--active": "_yYm7fXtIuITv",
	"toolbar-open__item--function-keyboard--disable": "_1sR66InqQ0lV",
	"toolbar-open__item--screen-keyboard": "_VVNBz9K_TcJl",
	"toolbar-open__item--screen-keyboard--active": "_2JjKioN6Vcc5",
	"toolbar-open__item--screen-keyboard--disable": "_pwub9KCIv9ol",
	"toolbar-open__item--draw": "_3E9jtidXQv4G",
	"toolbar-open__item--draw--active": "_II1eeeHeOlU_",
	"toolbar-open__item--draw--disable": "_D7jXdvgxF7A2",
	"toolbar-open__item--screen-capture": "_2ZMuuSbDGTmR",
	"toolbar-open__item--event-number-notice": "_3l5Syr9ryIXM",
	"toolbar-open__item--screen-ratio-fit": "_ycVkiBzsRqQg",
	"toolbar-open__item--screen-ratio-100": "_22ziUpZB3Nfv",
	"toolbar-open__item--screen-ratio-125": "_QZQ7M1E359Tj",
	"toolbar-open__item--screen-ratio-150": "_VdKlgAwRJzpo",
	"toolbar-open__item--full-screen-expand": "_1hVyujT8bFxy",
	"toolbar-open__item--full-screen-minimize": "_2iiB5lwT742_",
	"toolbar-open__item--change-monitor": "_2MOpJOX0hDZJ",
	"toolbar-open__item--change-monitor--none": "_21svvfsGjhHR",
	"toolbar-open__item--close": "_16Ex4gN04g9U",
	"toolbar-open__item--fold": "_13M6Buj7rCRx",
	"toolbar-close": "_3Zc65AxNvAMU",
	"toolbar-close--fixed": "_3C-ScvyGv5dD",
	"toolbar-close--toolbar-expand": "_3Fmc1bqQYIKl"
};

/***/ }),
/* 81 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAzNjAgMzYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzNjAgMzYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7b3BhY2l0eTowLjI7fQ0KCS5zdDF7ZmlsbDojRkZGRkZGO30NCjwvc3R5bGU+DQo8Zz4NCgk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMTc5LjMsMTQxLjJjLTI2LjgsMC00OC41LDIxLjgtNDguNSw0OC41YzAsMjYuOCwyMS44LDQ4LjUsNDguNSw0OC41YzI2LjgsMCw0OC41LTIxLjgsNDguNS00OC41DQoJCUMyMjcuOCwxNjMsMjA2LDE0MS4yLDE3OS4zLDE0MS4yeiIvPg0KCTxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik0yNzAuMiwxMDEuOGgtNDIuMWMtNC43LTEyLTE2LjQtMjAuMi0yOS43LTIwLjJoLTM0LjZjLTEzLjMsMC0yNC45LDguMi0yOS43LDIwLjJIODcuNw0KCQljLTE4LjgsMC0zNC4xLDE1LjMtMzQuMSwzNC4xdjEwNy43YzAsMTguOCwxNS4zLDM0LjEsMzQuMSwzNC4xaDE4Mi41YzIxLjIsMCwzOC40LTE3LjIsMzguNC0zOC40di05OQ0KCQlDMzA4LjYsMTE5LjEsMjkxLjQsMTAxLjgsMjcwLjIsMTAxLjh6IE0yNzIuMSwxNDMuN2MwLDIuOC0yLjIsNS01LDVzLTUtMi4yLTUtNXMyLjItNSw1LTVTMjcyLjEsMTQwLjksMjcyLjEsMTQzLjd6IE0xNzkuMywxMjguNg0KCQljMzQsMCw2MC42LDI2LjksNjAuNiw2MS4xYzAsMzQuNC0yNi4xLDYwLjQtNjAuNiw2MC40Yy0zNC43LDAtNjAuOS0yNi02MC45LTYwLjRDMTE4LjMsMTU1LjUsMTQ1LjEsMTI4LjYsMTc5LjMsMTI4LjZ6Ii8+DQoJPGNpcmNsZSBjbGFzcz0ic3QxIiBjeD0iMTc5LjMiIGN5PSIxODkuOCIgcj0iNDEiLz4NCgk8cGF0aCBjbGFzcz0ic3QxIiBkPSJNMjcwLjIsMTA5LjNoLTQ3LjdjLTItMTEuNC0xMi0yMC4yLTI0LTIwLjJoLTM0LjZjLTEyLDAtMjIsOC44LTI0LDIwLjJIODcuN2MtMTQuNiwwLTI2LjYsMTItMjYuNiwyNi42djEwNy43DQoJCWMwLDE0LjYsMTIsMjYuNiwyNi42LDI2LjZoMTgyLjVjMTcsMCwzMC45LTEzLjksMzAuOS0zMC45di05OUMzMDEuMSwxMjMuMywyODcuMiwxMDkuMywyNzAuMiwxMDkuM3ogTTE3OS4zLDI1Ny43DQoJCWMtMzguNSwwLTY4LjQtMjkuNC02OC40LTY3LjlzMjkuOS02OC42LDY4LjQtNjguNnM2OC4xLDMwLjIsNjguMSw2OC42UzIxNy44LDI1Ny43LDE3OS4zLDI1Ny43eiBNMjY3LjEsMTU2LjINCgkJYy02LjksMC0xMi41LTUuNi0xMi41LTEyLjVzNS42LTEyLjUsMTIuNS0xMi41czEyLjUsNS42LDEyLjUsMTIuNVMyNzQsMTU2LjIsMjY3LjEsMTU2LjJ6Ii8+DQo8L2c+DQo8L3N2Zz4NCg=="

/***/ }),
/* 82 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAzNjAgMzYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzNjAgMzYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7b3BhY2l0eTowLjI7fQ0KCS5zdDF7ZmlsbDojRkZGRkZGO30NCjwvc3R5bGU+DQo8Zz4NCgk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMjc1LjksMzA2LjVIODNjLTcuNywwLTE0LTYuMy0xNC0xNGwwLDBjMC03LjcsNi4zLTE0LDE0LTE0aDE5Mi45YzcuNywwLDE0LDYuMywxNCwxNGwwLDANCgkJQzI4OS45LDMwMC4yLDI4My42LDMwNi41LDI3NS45LDMwNi41eiIvPg0KCTxnPg0KCQk8Zz4NCgkJCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik0xNjIuMSwxMjcuMmgtMzEuNFYxNTVoMjkuMXY4LjVoLTI5LjF2MzUuN2gtOS40di04MC41aDQwLjlWMTI3LjJ6Ii8+DQoJCQk8cGF0aCBjbGFzcz0ic3QxIiBkPSJNMTMxLDE5OS41aC0xMHYtODEuMWg0MS41djkuMUgxMzF2MjcuM2gyOS4xdjkuMUgxMzFWMTk5LjV6IE0xMjEuNSwxOTguOWg4Ljh2LTM1LjdoMjkuMXYtNy45aC0yOS4xdi0yOC41DQoJCQkJaDMxLjR2LTcuOWgtNDAuM1YxOTguOXoiLz4NCgkJPC9nPg0KCQk8Zz4NCgkJCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik0xODYuOCwxOTkuMmgtOS40di04MC41aDkuNFYxOTkuMnoiLz4NCgkJCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik0xODcuMSwxOTkuNWgtMTB2LTgxLjFoMTBWMTk5LjV6IE0xNzcuNywxOTguOWg4Ljh2LTc5LjloLTguOFYxOTguOXoiLz4NCgkJPC9nPg0KCQk8Zz4NCgkJCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik0yNTUuNiwxMjcuMmgtMjMuMnY3MmgtOS40di03MmgtMjMuMnYtOC41aDU1LjlWMTI3LjJ6Ii8+DQoJCQk8cGF0aCBjbGFzcz0ic3QxIiBkPSJNMjMyLjcsMTk5LjVoLTEwdi03MmgtMjMuMnYtOS4xaDU2LjV2OS4xaC0yMy4yVjE5OS41eiBNMjIzLjIsMTk4LjloOC44di03MmgyMy4ydi03LjlIMjAwdjcuOWgyMy4yVjE5OC45eg0KCQkJCSIvPg0KCQk8L2c+DQoJPC9nPg0KCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik04MywzMDYuNWg0MS4zdi0yNy45SDgzYy03LjcsMC0xNCw2LjMtMTQsMTRTNzUuMywzMDYuNSw4MywzMDYuNXoiLz4NCgk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMjcxLjksNjBIOTAuM2MtMjAuMywwLTM2LjksMTYuNS0zNi45LDM2Ljl2MTMxLjNjMCwyMC4zLDE2LjUsMzYuOSwzNi45LDM2LjloMTgxLjcNCgkJYzIwLjMsMCwzNi45LTE2LjUsMzYuOS0zNi45Vjk2LjlDMzA4LjgsNzYuNSwyOTIuMyw2MCwyNzEuOSw2MHogTTI3Myw5OS40djEyNi4zYzAsMi4xLTEuOCw0LTQsNEg5My4yYy0yLjEsMC00LTEuOC00LTRWOTkuNA0KCQljMC0yLjEsMS44LTQsNC00SDI2OUMyNzEuMiw5NS40LDI3Myw5Ny4yLDI3Myw5OS40eiIvPg0KCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik0yNzEuOSw2Ny41SDkwLjNjLTE2LjIsMC0yOS40LDEzLjItMjkuNCwyOS40djEzMS4zYzAsMTYuMiwxMy4yLDI5LjQsMjkuNCwyOS40aDE4MS43DQoJCWMxNi4yLDAsMjkuNC0xMy4yLDI5LjQtMjkuNFY5Ni45QzMwMS4zLDgwLjcsMjg4LjEsNjcuNSwyNzEuOSw2Ny41eiBNMjgwLjUsMjI1LjZjMCw2LjMtNS4yLDExLjUtMTEuNSwxMS41SDkzLjINCgkJYy02LjMsMC0xMS41LTUuMi0xMS41LTExLjVWOTkuNGMwLTYuMyw1LjItMTEuNSwxMS41LTExLjVIMjY5YzYuMywwLDExLjUsNS4yLDExLjUsMTEuNVYyMjUuNnoiLz4NCjwvZz4NCjwvc3ZnPg0K"

/***/ }),
/* 83 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAzNjAgMzYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzNjAgMzYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7b3BhY2l0eTowLjI7fQ0KCS5zdDF7ZmlsbDojRkZGRkZGO3N0cm9rZTojRkZGRkZGO3N0cm9rZS13aWR0aDowLjQ7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjEwO30NCgkuc3Qye2ZpbGw6I0ZGRkZGRjt9DQo8L3N0eWxlPg0KPGc+DQoJPHBhdGggY2xhc3M9InN0MCIgZD0iTTI3NS45LDMwNS41SDgzYy03LjcsMC0xNC02LjMtMTQtMTRsMCwwYzAtNy43LDYuMy0xNCwxNC0xNGgxOTIuOWM3LjcsMCwxNCw2LjMsMTQsMTRsMCwwDQoJCUMyODkuOSwyOTkuMiwyODMuNiwzMDUuNSwyNzUuOSwzMDUuNXoiLz4NCgk8Zz4NCgkJPHBhdGggY2xhc3M9InN0MSIgZD0iTTEzMCwyMDEuNGgtMTAuNHYtNjkuMmwtMjAuOSw3Ljd2LTkuNGwyOS44LTExLjJoMS42VjIwMS40eiIvPg0KCQk8cGF0aCBjbGFzcz0ic3QxIiBkPSJNMTk0LjQsMTY2LjZjMCwxMi4yLTIuMSwyMS4yLTYuMiwyNy4xYy00LjIsNS45LTEwLjYsOC45LTE5LjUsOC45Yy04LjcsMC0xNS4yLTIuOS0xOS40LTguNw0KCQkJYy00LjItNS44LTYuNC0xNC40LTYuNS0yNS45di0xMy45YzAtMTIsMi4xLTIwLjksNi4yLTI2LjhjNC4yLTUuOCwxMC43LTguOCwxOS42LTguOGM4LjgsMCwxNS4zLDIuOCwxOS40LDguNXM2LjMsMTQuMyw2LjQsMjYuMQ0KCQkJVjE2Ni42eiBNMTg0LDE1Mi40YzAtOC44LTEuMi0xNS4yLTMuNy0xOS4ycy02LjQtNi0xMS43LTZjLTUuMywwLTkuMiwyLTExLjYsNmMtMi40LDQtMy43LDEwLjItMy44LDE4LjV2MTYuNg0KCQkJYzAsOC44LDEuMywxNS40LDMuOCwxOS42YzIuNiw0LjIsNi40LDYuMywxMS43LDYuM2M1LjEsMCw4LjktMiwxMS40LTZjMi41LTQsMy44LTEwLjIsMy45LTE4LjhWMTUyLjR6Ii8+DQoJCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik0yNTMuMywxNjYuNmMwLDEyLjItMi4xLDIxLjItNi4yLDI3LjFjLTQuMiw1LjktMTAuNyw4LjktMTkuNSw4LjljLTguNywwLTE1LjItMi45LTE5LjQtOC43DQoJCQljLTQuMi01LjgtNi40LTE0LjQtNi41LTI1Ljl2LTEzLjljMC0xMiwyLjEtMjAuOSw2LjItMjYuOGM0LjItNS44LDEwLjctOC44LDE5LjUtOC44YzguOCwwLDE1LjMsMi44LDE5LjQsOC41czYuMywxNC4zLDYuNCwyNi4xDQoJCQlWMTY2LjZ6IE0yNDIuOSwxNTIuNGMwLTguOC0xLjItMTUuMi0zLjctMTkuMnMtNi40LTYtMTEuNy02Yy01LjMsMC05LjIsMi0xMS42LDZjLTIuNCw0LTMuNywxMC4yLTMuOCwxOC41djE2LjYNCgkJCWMwLDguOCwxLjMsMTUuNCwzLjgsMTkuNmMyLjYsNC4yLDYuNCw2LjMsMTEuNyw2LjNjNS4xLDAsOC45LTIsMTEuNC02YzIuNS00LDMuOC0xMC4yLDMuOS0xOC44VjE1Mi40eiIvPg0KCTwvZz4NCgk8cmVjdCB4PSIxMjQuMiIgeT0iMjc3LjUiIGNsYXNzPSJzdDIiIHdpZHRoPSI1NS4yIiBoZWlnaHQ9IjI3LjkiLz4NCgk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMjcxLjksNTlIOTAuM2MtMjAuMywwLTM2LjksMTYuNS0zNi45LDM2Ljl2MTMxLjNjMCwyMC4zLDE2LjUsMzYuOSwzNi45LDM2LjloMTgxLjcNCgkJYzIwLjMsMCwzNi45LTE2LjUsMzYuOS0zNi45Vjk1LjlDMzA4LjgsNzUuNSwyOTIuMyw1OSwyNzEuOSw1OXogTTI3Myw5OC40djEyNi4zYzAsMi4xLTEuOCw0LTQsNEg5My4yYy0yLjEsMC00LTEuOC00LTRWOTguNA0KCQljMC0yLjEsMS44LTQsNC00SDI2OUMyNzEuMiw5NC40LDI3Myw5Ni4yLDI3Myw5OC40eiIvPg0KCTxwYXRoIGNsYXNzPSJzdDIiIGQ9Ik0yNzEuOSw2Ni41SDkwLjNjLTE2LjIsMC0yOS40LDEzLjItMjkuNCwyOS40djEzMS4zYzAsMTYuMiwxMy4yLDI5LjQsMjkuNCwyOS40aDE4MS43DQoJCWMxNi4yLDAsMjkuNC0xMy4yLDI5LjQtMjkuNFY5NS45QzMwMS4zLDc5LjcsMjg4LjEsNjYuNSwyNzEuOSw2Ni41eiBNMjgwLjUsMjI0LjZjMCw2LjMtNS4yLDExLjUtMTEuNSwxMS41SDkzLjINCgkJYy02LjMsMC0xMS41LTUuMi0xMS41LTExLjVWOTguNGMwLTYuMyw1LjItMTEuNSwxMS41LTExLjVIMjY5YzYuMywwLDExLjUsNS4yLDExLjUsMTEuNVYyMjQuNnoiLz4NCjwvZz4NCjwvc3ZnPg0K"

/***/ }),
/* 84 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAzNjAgMzYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzNjAgMzYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7b3BhY2l0eTowLjI7fQ0KCS5zdDF7ZmlsbDojRkZGRkZGO3N0cm9rZTojRkZGRkZGO3N0cm9rZS13aWR0aDowLjQ7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjEwO30NCgkuc3Qye2ZpbGw6I0ZGRkZGRjt9DQo8L3N0eWxlPg0KPGc+DQoJPHBhdGggY2xhc3M9InN0MCIgZD0iTTI3NS45LDMwNi41SDgzYy03LjcsMC0xNC02LjMtMTQtMTRsMCwwYzAtNy43LDYuMy0xNCwxNC0xNGgxOTIuOWM3LjcsMCwxNCw2LjMsMTQsMTRsMCwwDQoJCUMyODkuOSwzMDAuMiwyODMuNiwzMDYuNSwyNzUuOSwzMDYuNXoiLz4NCgk8Zz4NCgkJPHBhdGggY2xhc3M9InN0MSIgZD0iTTEzMS41LDIwMi40aC0xMC40di02OS4ybC0yMC45LDcuN3YtOS40bDI5LjgtMTEuMmgxLjZWMjAyLjR6Ii8+DQoJCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik0yMDAuMSwyMDIuNGgtNTMuNlYxOTVsMjguMy0zMS40YzQuMi00LjgsNy4xLTguNiw4LjctMTEuNmMxLjYtMywyLjQtNi4xLDIuNC05LjJjMC00LjMtMS4zLTcuOC0zLjktMTAuNQ0KCQkJYy0yLjYtMi43LTYtNC4xLTEwLjMtNC4xYy01LjIsMC05LjIsMS41LTEyLDQuNGMtMi45LDIuOS00LjMsNy00LjMsMTIuM2gtMTAuNGMwLTcuNSwyLjQtMTMuNiw3LjMtMTguM2M0LjgtNC42LDExLjMtNywxOS41LTcNCgkJCWM3LjYsMCwxMy42LDIsMTgsNmM0LjQsNCw2LjYsOS4zLDYuNiwxNS45YzAsOC01LjEsMTcuNi0xNS40LDI4LjhMMTU5LDE5NGg0MVYyMDIuNHoiLz4NCgkJPHBhdGggY2xhc3M9InN0MSIgZD0iTTIwOC4zLDE2My4zbDQuMi00MC44aDQxLjl2OS42aC0zMy4xbC0yLjUsMjIuM2M0LTIuNCw4LjYtMy41LDEzLjYtMy41YzcuNSwwLDEzLjQsMi41LDE3LjcsNy40DQoJCQljNC40LDQuOSw2LjYsMTEuNiw2LjYsMjBjMCw4LjQtMi4zLDE1LjEtNi44LDE5LjljLTQuNSw0LjgtMTAuOSw3LjMtMTkuMSw3LjNjLTcuMiwwLTEzLjEtMi0xNy43LTZjLTQuNi00LTcuMi05LjYtNy44LTE2LjZoOS44DQoJCQljMC42LDQuNywyLjMsOC4yLDUsMTAuNnM2LjMsMy42LDEwLjcsMy42YzQuOCwwLDguNi0xLjcsMTEuNC00LjljMi43LTMuMyw0LjEtNy44LDQuMS0xMy42YzAtNS41LTEuNS05LjktNC41LTEzLjINCgkJCWMtMy0zLjMtNi45LTUtMTEuOS01Yy00LjUsMC04LjEsMS0xMC43LDNsLTIuNywyLjJMMjA4LjMsMTYzLjN6Ii8+DQoJPC9nPg0KCTxyZWN0IHg9IjE3OS40IiB5PSIyNzguNSIgY2xhc3M9InN0MiIgd2lkdGg9IjU1LjIiIGhlaWdodD0iMjcuOSIvPg0KCTxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik0yNzEuOSw2MEg5MC4zYy0yMC4zLDAtMzYuOSwxNi41LTM2LjksMzYuOXYxMzEuM2MwLDIwLjMsMTYuNSwzNi45LDM2LjksMzYuOWgxODEuNw0KCQljMjAuMywwLDM2LjktMTYuNSwzNi45LTM2LjlWOTYuOUMzMDguOCw3Ni41LDI5Mi4zLDYwLDI3MS45LDYweiBNMjczLDk5LjR2MTI2LjNjMCwyLjEtMS44LDQtNCw0SDkzLjJjLTIuMSwwLTQtMS44LTQtNFY5OS40DQoJCWMwLTIuMSwxLjgtNCw0LTRIMjY5QzI3MS4yLDk1LjQsMjczLDk3LjIsMjczLDk5LjR6Ii8+DQoJPHBhdGggY2xhc3M9InN0MiIgZD0iTTI3MS45LDY3LjVIOTAuM2MtMTYuMiwwLTI5LjQsMTMuMi0yOS40LDI5LjR2MTMxLjNjMCwxNi4yLDEzLjIsMjkuNCwyOS40LDI5LjRoMTgxLjcNCgkJYzE2LjIsMCwyOS40LTEzLjIsMjkuNC0yOS40Vjk2LjlDMzAxLjMsODAuNywyODguMSw2Ny41LDI3MS45LDY3LjV6IE0yODAuNSwyMjUuNmMwLDYuMy01LjIsMTEuNS0xMS41LDExLjVIOTMuMg0KCQljLTYuMywwLTExLjUtNS4yLTExLjUtMTEuNVY5OS40YzAtNi4zLDUuMi0xMS41LDExLjUtMTEuNUgyNjljNi4zLDAsMTEuNSw1LjIsMTEuNSwxMS41VjIyNS42eiIvPg0KPC9nPg0KPC9zdmc+DQo="

/***/ }),
/* 85 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAzNjAgMzYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzNjAgMzYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7b3BhY2l0eTowLjI7fQ0KCS5zdDF7ZmlsbDojRkZGRkZGO3N0cm9rZTojRkZGRkZGO3N0cm9rZS13aWR0aDowLjQ7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjEwO30NCgkuc3Qye2ZpbGw6I0ZGRkZGRjt9DQo8L3N0eWxlPg0KPGc+DQoJPHBhdGggY2xhc3M9InN0MCIgZD0iTTI3MS45LDYwSDkwLjNjLTIwLjMsMC0zNi45LDE2LjUtMzYuOSwzNi45djEzMS4zYzAsMjAuMywxNi41LDM2LjksMzYuOSwzNi45aDE4MS43DQoJCWMyMC4zLDAsMzYuOS0xNi41LDM2LjktMzYuOVY5Ni45QzMwOC44LDc2LjUsMjkyLjMsNjAsMjcxLjksNjB6IE0yNzMsOTkuNHYxMjYuM2MwLDIuMS0xLjgsNC00LDRIOTMuMmMtMi4xLDAtNC0xLjgtNC00Vjk5LjQNCgkJYzAtMi4xLDEuOC00LDQtNEgyNjlDMjcxLjIsOTUuNCwyNzMsOTcuMiwyNzMsOTkuNHoiLz4NCgk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMjc1LjksMzA2LjVIODNjLTcuNywwLTE0LTYuMy0xNC0xNGwwLDBjMC03LjcsNi4zLTE0LDE0LTE0aDE5Mi45YzcuNywwLDE0LDYuMywxNCwxNGwwLDANCgkJQzI4OS45LDMwMC4yLDI4My42LDMwNi41LDI3NS45LDMwNi41eiIvPg0KCTxnPg0KCQk8cGF0aCBjbGFzcz0ic3QxIiBkPSJNMTMxLjUsMjAyLjRoLTEwLjR2LTY5LjJsLTIwLjksNy43di05LjRsMjkuOC0xMS4yaDEuNlYyMDIuNHoiLz4NCgkJPHBhdGggY2xhc3M9InN0MSIgZD0iTTE0Ny45LDE2MS41bDQuMi00MC44SDE5NHY5LjZoLTMzLjFsLTIuNSwyMi4zYzQtMi40LDguNS0zLjUsMTMuNi0zLjVjNy41LDAsMTMuNCwyLjUsMTcuNyw3LjQNCgkJCWM0LjQsNC45LDYuNiwxMS42LDYuNiwyMGMwLDguNC0yLjMsMTUuMS02LjgsMTkuOXMtMTAuOSw3LjMtMTkuMSw3LjNjLTcuMiwwLTEzLjEtMi0xNy43LTZjLTQuNi00LTcuMi05LjYtNy44LTE2LjZoOS44DQoJCQljMC42LDQuNywyLjMsOC4yLDUsMTAuNnM2LjMsMy42LDEwLjcsMy42YzQuOCwwLDguNi0xLjcsMTEuNC00LjljMi44LTMuMyw0LjEtNy44LDQuMS0xMy42YzAtNS41LTEuNS05LjktNC41LTEzLjINCgkJCWMtMy0zLjMtNi45LTUtMTEuOS01Yy00LjUsMC04LjEsMS0xMC43LDNsLTIuOCwyLjJMMTQ3LjksMTYxLjV6Ii8+DQoJCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik0yNTQuOCwxNjcuNmMwLDEyLjItMi4xLDIxLjItNi4yLDI3LjFjLTQuMiw1LjktMTAuNyw4LjktMTkuNSw4LjljLTguNywwLTE1LjItMi45LTE5LjQtOC43DQoJCQljLTQuMi01LjgtNi40LTE0LjQtNi41LTI1Ljl2LTEzLjljMC0xMiwyLjEtMjAuOSw2LjItMjYuOGM0LjItNS44LDEwLjctOC44LDE5LjUtOC44YzguOCwwLDE1LjMsMi44LDE5LjQsOC41DQoJCQljNC4yLDUuNiw2LjMsMTQuMyw2LjQsMjYuMVYxNjcuNnogTTI0NC40LDE1My40YzAtOC44LTEuMi0xNS4yLTMuNy0xOS4ycy02LjQtNi0xMS43LTZjLTUuMywwLTkuMiwyLTExLjYsNg0KCQkJYy0yLjQsNC0zLjcsMTAuMi0zLjgsMTguNXYxNi42YzAsOC44LDEuMywxNS40LDMuOCwxOS42YzIuNiw0LjIsNi40LDYuMywxMS43LDYuM2M1LjEsMCw4LjktMiwxMS40LTZjMi41LTQsMy44LTEwLjIsMy45LTE4LjgNCgkJCVYxNTMuNHoiLz4NCgk8L2c+DQoJPHBhdGggY2xhc3M9InN0MiIgZD0iTTI3NS45LDI3OC41aC00MS4ydjI3LjloNDEuMmM3LjcsMCwxNC02LjMsMTQtMTRTMjgzLjYsMjc4LjUsMjc1LjksMjc4LjV6Ii8+DQoJPHBhdGggY2xhc3M9InN0MiIgZD0iTTI3MS45LDY3LjVIOTAuM2MtMTYuMiwwLTI5LjQsMTMuMi0yOS40LDI5LjR2MTMxLjNjMCwxNi4yLDEzLjIsMjkuNCwyOS40LDI5LjRoMTgxLjcNCgkJYzE2LjIsMCwyOS40LTEzLjIsMjkuNC0yOS40Vjk2LjlDMzAxLjMsODAuNywyODguMSw2Ny41LDI3MS45LDY3LjV6IE0yODAuNSwyMjUuNmMwLDYuMy01LjIsMTEuNS0xMS41LDExLjVIOTMuMg0KCQljLTYuMywwLTExLjUtNS4yLTExLjUtMTEuNVY5OS40YzAtNi4zLDUuMi0xMS41LDExLjUtMTEuNUgyNjljNi4zLDAsMTEuNSw1LjIsMTEuNSwxMS41VjIyNS42eiIvPg0KPC9nPg0KPC9zdmc+DQo="

/***/ }),
/* 86 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAzNjAgMzYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzNjAgMzYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7b3BhY2l0eTowLjI7fQ0KCS5zdDF7ZmlsbDojRkZGRkZGO30NCjwvc3R5bGU+DQo8Zz4NCgk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMjQwLjksMTE3LjhIMTIxLjJjLTE1LjgsMC0yOC42LDEyLjgtMjguNiwyOC42djY3LjhjMCwxNS44LDEyLjgsMjguNiwyOC42LDI4LjZoMTE5LjcNCgkJYzE1LjgsMCwyOC42LTEyLjgsMjguNi0yOC42di02Ny44QzI2OS41LDEzMC43LDI1Ni43LDExNy44LDI0MC45LDExNy44eiIvPg0KCTxnIGNsYXNzPSJzdDAiPg0KCQk8cGF0aCBkPSJNMTEwLjEsNzcuNWwtMjkuMywwYy0yLTAuMS0xMi4xLTAuMS0yMC4yLDcuN2MtMy43LDMuNS04LDkuOC04LDE5Ljh2MjguOGMwLDQuMSwzLjQsNy41LDcuNSw3LjVoMjANCgkJCWM0LjEsMCw3LjUtMy4zLDcuNS03LjVsMC0yMS4zaDIyLjVjNC4xLDAsNy41LTMuNCw3LjUtNy41Vjg1YzAtMi0wLjgtMy45LTIuMi01LjNTMTEyLjEsNzcuNSwxMTAuMSw3Ny41eiIvPg0KCQk8cGF0aCBkPSJNMjk5LjksODQuOWMtMy41LTMuNy05LjktOC0xOS44LThoLTI4LjdjLTQuMSwwLTcuNSwzLjQtNy41LDcuNXYyMGMwLDQuMSwzLjMsNy41LDcuNSw3LjVsMjEuMywwdjIyLjUNCgkJCWMwLDQuMSwzLjQsNy41LDcuNSw3LjVoMjBjNC4xLDAsNy41LTMuNCw3LjUtNy41di0yOS41QzMwNy43LDEwMi4xLDMwNy40LDkyLjUsMjk5LjksODQuOXoiLz4NCgkJPHBhdGggZD0iTTMwMC4xLDIxOC4xaC0yMGMtNC4xLDAtNy41LDMuMy03LjUsNy41bDAsMjEuM2gtMjIuNWMtNC4xLDAtNy41LDMuNC03LjUsNy41djIwYzAsMiwwLjgsMy45LDIuMiw1LjNzMy4zLDIuMiw1LjMsMi4yDQoJCQloMzAuMWMzLjQsMCwxMi4zLTAuNywxOS40LTcuN2MzLjctMy41LDgtOS44LDgtMTkuOHYtMjguOEMzMDcuNiwyMjEuNSwzMDQuMiwyMTguMSwzMDAuMSwyMTguMXoiLz4NCgkJPHBhdGggZD0iTTEwOC45LDI0Ny41bC0yMS4zLDBWMjI1YzAtNC4xLTMuNC03LjUtNy41LTcuNWgtMjBjLTQuMSwwLTcuNSwzLjQtNy41LDcuNXYyOS41Yy0wLjEsMi44LDAuMywxMi40LDcuNywyMA0KCQkJYzMuNSwzLjcsOS45LDgsMTkuOCw4aDI4LjhjNC4xLDAsNy41LTMuNCw3LjUtNy41di0yMEMxMTYuNCwyNTAuOSwxMTMsMjQ3LjUsMTA4LjksMjQ3LjV6Ii8+DQoJPC9nPg0KCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik0yMzkuOCwyMzUuM0gxMjIuM2MtMTIuMiwwLTIyLjItMTAtMjIuMi0yMi4ydi02NS42YzAtMTIuMiwxMC0yMi4yLDIyLjItMjIuMmgxMTcuNWMxMi4yLDAsMjIuMiwxMCwyMi4yLDIyLjINCgkJdjY1LjZDMjYyLDIyNS4zLDI1MiwyMzUuMywyMzkuOCwyMzUuM3oiLz4NCgk8Zz4NCgkJPHBhdGggY2xhc3M9InN0MSIgZD0iTTY1LjgsOTAuNmMtMi42LDIuNS01LjcsNy4xLTUuNywxNC40djI4LjhoMjBsMC4xLTI4LjhoMjkuOVY4NWwtMjkuNywwQzc5LDg1LDcxLjUsODUsNjUuOCw5MC42eiIvPg0KCQk8cGF0aCBjbGFzcz0ic3QxIiBkPSJNMjgwLjEsMTM0LjRoMjBsMC0yOS43YzAtMS40LDAtOC45LTUuNi0xNC42Yy0yLjUtMi42LTcuMS01LjctMTQuNC01LjdoLTI4Ljd2MjBsMjguNywwLjFWMTM0LjR6Ii8+DQoJCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik0yODAsMjU0LjRoLTI5Ljl2MjBsMjkuNywwYzAuMSwwLDAuMiwwLDAuNCwwYzIuMSwwLDguOS0wLjQsMTQuMi01LjZjMi42LTIuNSw1LjctNy4xLDUuNy0xNC40di0yOC44aC0yMA0KCQkJTDI4MCwyNTQuNHoiLz4NCgkJPHBhdGggY2xhc3M9InN0MSIgZD0iTTgwLjEsMjI1aC0yMGwwLDI5LjdjMCwxLjQsMCw4LjksNS42LDE0LjZjMi41LDIuNiw3LjEsNS43LDE0LjQsNS43aDI4Ljh2LTIwTDgwLjEsMjU1VjIyNXoiLz4NCgk8L2c+DQo8L2c+DQo8L3N2Zz4NCg=="

/***/ }),
/* 87 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAzNjAgMzYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzNjAgMzYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7b3BhY2l0eTowLjI7fQ0KCS5zdDF7ZmlsbDojRkZGRkZGO30NCjwvc3R5bGU+DQo8Zz4NCgk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMjI1LDEzNC40aC04Ny45Yy0xMS42LDAtMjEsOS40LTIxLDIxdjQ5LjhjMCwxMS42LDkuNCwyMSwyMSwyMUgyMjVjMTEuNiwwLDIxLTkuNCwyMS0yMXYtNDkuOA0KCQlDMjQ2LDE0My44LDIzNi42LDEzNC40LDIyNSwxMzQuNHoiLz4NCgk8cGF0aCBjbGFzcz0ic3QxIiBkPSJNMjIzLjUsMjE3LjdoLTg0LjljLTcuNywwLTE0LTYuMy0xNC0xNFYxNTdjMC03LjcsNi4zLTE0LDE0LTE0aDg0LjljNy43LDAsMTQsNi4zLDE0LDE0djQ2LjcNCgkJQzIzNy41LDIxMS40LDIzMS4yLDIxNy43LDIyMy41LDIxNy43eiIvPg0KCTxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik02MC4xLDE0MS4zbDI5LjMsMGMyLDAuMSwxMi4xLDAuMSwyMC4yLTcuN2MzLjctMy41LDgtOS44LDgtMTkuOFY4NWMwLTQuMS0zLjQtNy41LTcuNS03LjVoLTIwDQoJCWMtNC4xLDAtNy41LDMuMy03LjUsNy41bDAsMjEuM0g2MC4xYy00LjEsMC03LjUsMy40LTcuNSw3LjV2MjBjMCwyLDAuOCwzLjksMi4yLDUuM1M1OC4xLDE0MS4zLDYwLjEsMTQxLjN6Ii8+DQoJPHBhdGggY2xhc3M9InN0MSIgZD0iTTEwNC40LDEyOC4yYzIuNi0yLjUsNS43LTcuMSw1LjctMTQuNFY4NWgtMjBMOTAsMTEzLjhINjAuMXYyMGwyOS43LDBDOTEuMiwxMzMuOCw5OC43LDEzMy43LDEwNC40LDEyOC4yeiIvPg0KCTxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik0yNTEuNSwxMzMuOWMzLjUsMy43LDkuOSw4LDE5LjgsOGgyOC44YzQuMSwwLDcuNS0zLjQsNy41LTcuNXYtMjBjMC00LjEtMy4zLTcuNS03LjUtNy41bC0yMS4zLDBWODQuNA0KCQljMC00LjEtMy40LTcuNS03LjUtNy41aC0yMGMtNC4xLDAtNy41LDMuNC03LjUsNy41djI5LjVDMjQzLjgsMTE2LjYsMjQ0LjEsMTI2LjIsMjUxLjUsMTMzLjl6Ii8+DQoJPHBhdGggY2xhc3M9InN0MSIgZD0iTTI3MS40LDg0LjRoLTIwbDAsMjkuN2MwLDEuNCwwLDguOSw1LjYsMTQuNmMyLjUsMi42LDcuMSw1LjcsMTQuNCw1LjdoMjguOHYtMjBsLTI4LjgtMC4xVjg0LjR6Ii8+DQoJPHBhdGggY2xhc3M9InN0MCIgZD0iTTI1MC4xLDI4MS45aDIwYzQuMSwwLDcuNS0zLjMsNy41LTcuNWwwLTIxLjNoMjIuNWM0LjEsMCw3LjUtMy40LDcuNS03LjV2LTIwYzAtMi0wLjgtMy45LTIuMi01LjMNCgkJcy0zLjMtMi4yLTUuMy0yLjJIMjcwYy0zLjQsMC0xMi4zLDAuNy0xOS40LDcuN2MtMy43LDMuNS04LDkuOC04LDE5Ljh2MjguOEMyNDIuNiwyNzguNSwyNDYsMjgxLjksMjUwLjEsMjgxLjl6Ii8+DQoJPHBhdGggY2xhc3M9InN0MSIgZD0iTTI3MC4yLDI0NS42aDI5Ljl2LTIwbC0yOS43LDBjLTAuMSwwLTAuMiwwLTAuNCwwYy0yLjEsMC04LjksMC40LTE0LjIsNS42Yy0yLjYsMi41LTUuNyw3LjEtNS43LDE0LjR2MjguOGgyMA0KCQlMMjcwLjIsMjQ1LjZ6Ii8+DQoJPHBhdGggY2xhc3M9InN0MCIgZD0iTTYwLjEsMjUyLjVsMjEuMywwVjI3NWMwLDQuMSwzLjQsNy41LDcuNSw3LjVoMjBjNC4xLDAsNy41LTMuNCw3LjUtNy41di0yOS41YzAuMS0yLjgtMC4zLTEyLjQtNy43LTIwDQoJCWMtMy41LTMuNy05LjktOC0xOS44LThINjAuMWMtNC4xLDAtNy41LDMuNC03LjUsNy41djIwQzUyLjYsMjQ5LjIsNTUuOSwyNTIuNSw2MC4xLDI1Mi41eiIvPg0KCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik04OC44LDI3NWgyMGwwLTI5LjdjMC0xLjQsMC04LjktNS42LTE0LjZjLTIuNS0yLjYtNy4xLTUuNy0xNC40LTUuN0g2MC4xdjIwbDI4LjgsMC4xVjI3NXoiLz4NCjwvZz4NCjwvc3ZnPg0K"

/***/ }),
/* 88 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAzNjAgMzYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzNjAgMzYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7b3BhY2l0eTowLjI7fQ0KCS5zdDF7ZmlsbDojRkZGRkZGO30NCjwvc3R5bGU+DQo8Zz4NCgk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMjMzLjgsMjAyLjVMMjExLjMsMTgwbDIyLjUtMjIuNWMzLjgtMy44LDUuOS04LjksNS45LTE0LjRzLTIuMS0xMC41LTUuOS0xNC40bC0xLjUtMS41DQoJCWMtNy45LTcuOS0yMC44LTcuOS0yOC43LDBsLTIyLjUsMjIuNWwtMjIuNS0yMi41Yy03LjctNy43LTIxLjEtNy43LTI4LjcsMGwtMS41LDEuNWMtMy44LDMuOC01LjksOC45LTUuOSwxNC40czIuMSwxMC41LDUuOSwxNC40DQoJCWwyMi41LDIyLjVsLTIyLjUsMjIuNWMtNy45LDcuOS03LjksMjAuOCwwLDI4LjdsMS41LDEuNWM3LjcsNy43LDIxLjEsNy43LDI4LjcsMGwyMi41LTIyLjVsMjIuNSwyMi41YzQsNCw5LjIsNS45LDE0LjQsNS45DQoJCWM1LjIsMCwxMC40LTIsMTQuNC01LjlsMS41LTEuNUMyNDEuOCwyMjMuMywyNDEuOCwyMTAuNCwyMzMuOCwyMDIuNXoiLz4NCgk8cGF0aCBjbGFzcz0ic3QxIiBkPSJNMjAwLjcsMTgwbDI3LjgtMjcuOGM1LTUsNS0xMy4xLDAtMTguMWwtMS41LTEuNWMtNS01LTEzLjEtNS0xOC4xLDBsLTI3LjgsMjcuOGwtMjcuOC0yNy44DQoJCWMtNS01LTEzLjEtNS0xOC4xLDBsLTEuNSwxLjVjLTUsNS01LDEzLjEsMCwxOC4xbDI3LjgsMjcuOGwtMjcuOCwyNy44Yy01LDUtNSwxMy4xLDAsMTguMWwxLjUsMS41YzUsNSwxMy4xLDUsMTguMSwwbDI3LjgtMjcuOA0KCQlsMjcuOCwyNy44YzUsNSwxMy4xLDUsMTguMSwwbDEuNS0xLjVjNS01LDUtMTMuMSwwLTE4LjFMMjAwLjcsMTgweiIvPg0KPC9nPg0KPC9zdmc+DQo="

/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ScreenNav = __webpack_require__(90);

var _ScreenNav2 = _interopRequireDefault(_ScreenNav);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _ScreenNav2.default;

/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _ScreenNav = __webpack_require__(91);

var _ScreenNav2 = _interopRequireDefault(_ScreenNav);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ScreenNav = function ScreenNav(_ref) {
  var monitorInfo = _ref.monitorInfo,
      remoteControl = _ref.remoteControl;

  var changeMonitor = function changeMonitor(selectedIndex) {
    remoteControl.requestSelectedMonitorInfo(selectedIndex);
  };

  return _react2.default.createElement(
    'div',
    { className: _ScreenNav2.default['screen-nav'] },
    monitorInfo.monitors.map(function (monitor, i) {
      var monitorStyle = 'monitor' + (monitorInfo.selectedMonitor === monitor.index ? '--active' : '');

      return _react2.default.createElement(
        'div',
        {
          key: monitor.index,
          className: _ScreenNav2.default[monitorStyle],
          onClick: function onClick() {
            return changeMonitor(monitor.index);
          }
        },
        i + 1
      );
    })
  );
};

exports.default = ScreenNav;

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(92);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(2)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./ScreenNav.scss", function() {
		var newContent = require("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./ScreenNav.scss");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "._2jEl3mZ0iDe3 {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: auto;\n  height: 20px;\n  border: 2px solid #0a6dbc;\n  border-radius: 4px; }\n\n._3XiF_n7U3i0G {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 20px;\n  height: 20px;\n  background: #0a6dbc;\n  font-size: 12px;\n  color: rgba(255, 255, 255, 0.3);\n  border-radius: 0; }\n  ._3XiF_n7U3i0G:hover {\n    cursor: pointer;\n    background: #013a67;\n    border-radius: 2px; }\n\n._2q8iALT5lYiH {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 20px;\n  height: 20px;\n  background: #0a6dbc;\n  font-size: 12px;\n  color: rgba(255, 255, 255, 0.3);\n  border-radius: 0;\n  background: #003967;\n  color: #ffffff;\n  border-radius: 2px; }\n  ._2q8iALT5lYiH:hover {\n    cursor: pointer;\n    background: #013a67;\n    border-radius: 2px; }\n", ""]);

// exports
exports.locals = {
	"screen-nav": "_2jEl3mZ0iDe3",
	"monitor": "_3XiF_n7U3i0G",
	"monitor--active": "_2q8iALT5lYiH"
};

/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ToolBarBaseDropdown = __webpack_require__(94);

var _ToolBarBaseDropdown2 = _interopRequireDefault(_ToolBarBaseDropdown);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _ToolBarBaseDropdown2.default;

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _ToolBarBaseDropdown = __webpack_require__(95);

var _ToolBarBaseDropdown2 = _interopRequireDefault(_ToolBarBaseDropdown);

var _DropdownWrapper = __webpack_require__(22);

var _DropdownWrapper2 = _interopRequireDefault(_DropdownWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ToolBarBaseDropdown = function ToolBarBaseDropdown(_ref) {
  var visible = _ref.visible,
      items = _ref.items,
      onSelectItem = _ref.onSelectItem,
      onClose = _ref.onClose;

  return _react2.default.createElement(
    _DropdownWrapper2.default,
    { visible: visible, onClose: onClose },
    _react2.default.createElement(
      'div',
      { className: _ToolBarBaseDropdown2.default['dropdown'] },
      items.map(function (item) {
        var dropdownStyle = 'dropdown-item--' + item.target + (item.active ? '--active' : '');

        return _react2.default.createElement(
          'div',
          {
            key: item.id,
            className: _ToolBarBaseDropdown2.default[dropdownStyle],
            onClick: function onClick() {
              return onSelectItem(item.target);
            }
          },
          item.value
        );
      })
    )
  );
};

exports.default = ToolBarBaseDropdown;

/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(96);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(2)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./ToolBarBaseDropdown.scss", function() {
		var newContent = require("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./ToolBarBaseDropdown.scss");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__(16);
exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "._2KPXQYtiE49P {\n  display: inline-block;\n  -webkit-transform: translate(-55px, 24px);\n          transform: translate(-55px, 24px);\n  position: absolute;\n  -webkit-box-shadow: inset 0 8px 5px -5px rgba(0, 0, 0, 0.3);\n          box-shadow: inset 0 8px 5px -5px rgba(0, 0, 0, 0.3);\n  text-align: left;\n  background: #2e434e;\n  color: white;\n  font-size: 0.85em;\n  border-bottom-left-radius: 5px;\n  border-bottom-right-radius: 5px;\n  overflow: hidden;\n  padding: 8px 0; }\n\n._2qu6irs34hpX {\n  padding-left: 54px;\n  padding-right: 30px;\n  height: 36px;\n  line-height: 36px;\n  font-size: 13px;\n  background: url(" + escape(__webpack_require__(11)) + ") no-repeat;\n  background-position-x: 8px; }\n  ._2qu6irs34hpX:hover {\n    cursor: pointer;\n    background-color: #1b2c35; }\n  ._3_Ycw2NGFB6L {\n    padding-left: 54px;\n    padding-right: 30px;\n    height: 36px;\n    line-height: 36px;\n    font-size: 13px;\n    background: url(" + escape(__webpack_require__(11)) + ") no-repeat;\n    background-position-x: 8px;\n    color: #34affc; }\n    ._3_Ycw2NGFB6L:hover {\n      cursor: pointer;\n      background-color: #1b2c35; }\n\n._3j05BDAaBFXS {\n  padding-left: 54px;\n  padding-right: 30px;\n  height: 36px;\n  line-height: 36px;\n  font-size: 13px;\n  background: url(" + escape(__webpack_require__(12)) + ") no-repeat;\n  background-position-x: 8px; }\n  ._3j05BDAaBFXS:hover {\n    cursor: pointer;\n    background-color: #1b2c35; }\n  ._3mnG1nwtOBuQ {\n    padding-left: 54px;\n    padding-right: 30px;\n    height: 36px;\n    line-height: 36px;\n    font-size: 13px;\n    background: url(" + escape(__webpack_require__(12)) + ") no-repeat;\n    background-position-x: 8px;\n    color: #34affc; }\n    ._3mnG1nwtOBuQ:hover {\n      cursor: pointer;\n      background-color: #1b2c35; }\n\n._3iPdHfzKCoCJ {\n  padding-left: 54px;\n  padding-right: 30px;\n  height: 36px;\n  line-height: 36px;\n  font-size: 13px;\n  background: url(" + escape(__webpack_require__(13)) + ") no-repeat;\n  background-position-x: 8px; }\n  ._3iPdHfzKCoCJ:hover {\n    cursor: pointer;\n    background-color: #1b2c35; }\n  ._1PYv6jZEMDZT {\n    padding-left: 54px;\n    padding-right: 30px;\n    height: 36px;\n    line-height: 36px;\n    font-size: 13px;\n    background: url(" + escape(__webpack_require__(13)) + ") no-repeat;\n    background-position-x: 8px;\n    color: #34affc; }\n    ._1PYv6jZEMDZT:hover {\n      cursor: pointer;\n      background-color: #1b2c35; }\n\n._2ek9xIgayS4t {\n  padding-left: 54px;\n  padding-right: 30px;\n  height: 36px;\n  line-height: 36px;\n  font-size: 13px;\n  background: url(" + escape(__webpack_require__(14)) + ") no-repeat;\n  background-position-x: 8px; }\n  ._2ek9xIgayS4t:hover {\n    cursor: pointer;\n    background-color: #1b2c35; }\n  ._ln__JgJ8JPTf {\n    padding-left: 54px;\n    padding-right: 30px;\n    height: 36px;\n    line-height: 36px;\n    font-size: 13px;\n    background: url(" + escape(__webpack_require__(14)) + ") no-repeat;\n    background-position-x: 8px;\n    color: #34affc; }\n    ._ln__JgJ8JPTf:hover {\n      cursor: pointer;\n      background-color: #1b2c35; }\n\n._1FO9d3WmbiQ4 {\n  padding-left: 54px;\n  padding-right: 30px;\n  height: 36px;\n  line-height: 36px;\n  font-size: 13px;\n  background: url(" + escape(__webpack_require__(15)) + ") no-repeat;\n  background-position-x: 8px; }\n  ._1FO9d3WmbiQ4:hover {\n    cursor: pointer;\n    background-color: #1b2c35; }\n  ._2Jq499wP49_J {\n    padding-left: 54px;\n    padding-right: 30px;\n    height: 36px;\n    line-height: 36px;\n    font-size: 13px;\n    background: url(" + escape(__webpack_require__(15)) + ") no-repeat;\n    background-position-x: 8px;\n    color: #34affc; }\n    ._2Jq499wP49_J:hover {\n      cursor: pointer;\n      background-color: #1b2c35; }\n", ""]);

// exports
exports.locals = {
	"dropdown": "_2KPXQYtiE49P",
	"dropdown-item--km-control": "_2qu6irs34hpX",
	"dropdown-item--km-control--active": "_3_Ycw2NGFB6L",
	"dropdown-item--laser-pointer": "_3j05BDAaBFXS",
	"dropdown-item--laser-pointer--active": "_3mnG1nwtOBuQ",
	"dropdown-item--function-keyboard": "_3iPdHfzKCoCJ",
	"dropdown-item--function-keyboard--active": "_1PYv6jZEMDZT",
	"dropdown-item--screen-keyboard": "_2ek9xIgayS4t",
	"dropdown-item--screen-keyboard--active": "_ln__JgJ8JPTf",
	"dropdown-item--draw": "_1FO9d3WmbiQ4",
	"dropdown-item--draw--active": "_2Jq499wP49_J"
};

/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DropdownWrapper = function (_Component) {
  _inherits(DropdownWrapper, _Component);

  function DropdownWrapper(props) {
    _classCallCheck(this, DropdownWrapper);

    var _this = _possibleConstructorReturn(this, (DropdownWrapper.__proto__ || Object.getPrototypeOf(DropdownWrapper)).call(this, props));

    _this.el = null;
    return _this;
  }

  _createClass(DropdownWrapper, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _this2 = this;

      document.body.addEventListener('click', function (evt) {
        var el = _this2.el;
        var onClose = _this2.props.onClose;


        if (el && !_this2.el.contains(evt.target)) {
          evt.stopImmediatePropagation();
          onClose();
        }
      }, { passive: true, capture: true });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props = this.props,
          visible = _props.visible,
          children = _props.children;

      if (!visible) return null;

      return _react2.default.createElement(
        'div',
        { ref: function ref(_ref) {
            return _this3.el = _ref;
          } },
        children
      );
    }
  }]);

  return DropdownWrapper;
}(_react.Component);

exports.default = DropdownWrapper;

/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ToolBarColorPickerDropdown = __webpack_require__(99);

var _ToolBarColorPickerDropdown2 = _interopRequireDefault(_ToolBarColorPickerDropdown);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _ToolBarColorPickerDropdown2.default;

/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _ToolBarColorPickerDropdown = __webpack_require__(100);

var _ToolBarColorPickerDropdown2 = _interopRequireDefault(_ToolBarColorPickerDropdown);

var _DropdownWrapper = __webpack_require__(22);

var _DropdownWrapper2 = _interopRequireDefault(_DropdownWrapper);

var _ColorPicker = __webpack_require__(102);

var _ColorPicker2 = _interopRequireDefault(_ColorPicker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ToolBarColorPickerDropdown = function ToolBarColorPickerDropdown(_ref) {
  var i18n = _ref.i18n,
      visible = _ref.visible,
      colors = _ref.colors,
      paintColor = _ref.paintColor,
      changePaintColor = _ref.changePaintColor,
      clearPaint = _ref.clearPaint,
      onClose = _ref.onClose;

  return _react2.default.createElement(
    _DropdownWrapper2.default,
    { visible: visible, onClose: onClose },
    _react2.default.createElement(
      'div',
      { className: _ToolBarColorPickerDropdown2.default['dropdown'] },
      _react2.default.createElement(
        'div',
        { className: _ToolBarColorPickerDropdown2.default['dropdown-item--color-picker'] },
        _react2.default.createElement(_ColorPicker2.default, {
          paintColor: paintColor,
          colors: colors,
          changePaintColor: changePaintColor,
          onCloseDrawMenu: onClose
        })
      ),
      _react2.default.createElement(
        'div',
        {
          className: _ToolBarColorPickerDropdown2.default['dropdown-item--clear-paint'],
          onClick: clearPaint
        },
        i18n.message('allClear')
      )
    )
  );
};

exports.default = ToolBarColorPickerDropdown;

/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(101);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(2)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./ToolBarColorPickerDropdown.scss", function() {
		var newContent = require("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./ToolBarColorPickerDropdown.scss");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "._3X56P9Ba-Q3b {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-transform: translate(-55px, 24px);\n          transform: translate(-55px, 24px);\n  position: absolute;\n  -webkit-box-shadow: inset 0 8px 5px -5px rgba(0, 0, 0, 0.3);\n          box-shadow: inset 0 8px 5px -5px rgba(0, 0, 0, 0.3);\n  text-align: left;\n  background: #2e434e;\n  color: white;\n  font-size: 0.85em;\n  border-bottom-left-radius: 5px;\n  border-bottom-right-radius: 5px;\n  overflow: hidden;\n  padding: 0; }\n\n._NG8_HFA-Szp5 {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  height: auto;\n  line-height: 36px;\n  font-size: 13px;\n  letter-spacing: 0.1em;\n  padding: 0; }\n\n._2ay8G0dWYMTk {\n  padding: 0 14px;\n  line-height: 48px;\n  background: #2e434e;\n  white-space: nowrap;\n  font-size: 13px; }\n  ._2ay8G0dWYMTk:hover {\n    cursor: pointer;\n    background: #1b2c35; }\n", ""]);

// exports
exports.locals = {
	"dropdown": "_3X56P9Ba-Q3b",
	"dropdown-item--color-picker": "_NG8_HFA-Szp5",
	"dropdown-item--clear-paint": "_2ay8G0dWYMTk"
};

/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ColorPicker = __webpack_require__(103);

var _ColorPicker2 = _interopRequireDefault(_ColorPicker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _ColorPicker2.default;

/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _ColorPicker = __webpack_require__(104);

var _ColorPicker2 = _interopRequireDefault(_ColorPicker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ColorPicker = function ColorPicker(_ref) {
  var paintColor = _ref.paintColor,
      colors = _ref.colors,
      changePaintColor = _ref.changePaintColor,
      onCloseDrawMenu = _ref.onCloseDrawMenu;

  var changeColor = function changeColor(id) {
    changePaintColor(id);
    onCloseDrawMenu();
  };

  return _react2.default.createElement(
    'div',
    { className: _ColorPicker2.default['colors'] },
    colors.map(function (color) {
      var colorStyle = 'color-wrap' + (color.id === paintColor ? '--active' : '');

      return _react2.default.createElement(
        'div',
        {
          key: color.id,
          className: _ColorPicker2.default[colorStyle],
          onClick: function onClick() {
            return changeColor(color.id);
          }
        },
        _react2.default.createElement('div', { className: _ColorPicker2.default['color-item--' + color.name] })
      );
    })
  );
};

exports.default = ColorPicker;

/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(105);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(2)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./ColorPicker.scss", function() {
		var newContent = require("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./ColorPicker.scss");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "._2w_4xceYTAYl {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 100%;\n  height: 100%;\n  padding: 8px 14px;\n  background: #263943; }\n\n._1czI-XUNrULs {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  width: 32px;\n  height: 32px;\n  border-radius: 16px; }\n  ._1czI-XUNrULs:hover {\n    cursor: pointer; }\n  ._1czI-XUNrULs:hover {\n    background: #142128; }\n  ._1YapypncyrK2 {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    width: 32px;\n    height: 32px;\n    border-radius: 16px;\n    background: #142128; }\n    ._1YapypncyrK2:hover {\n      cursor: pointer; }\n\n._3d9KkuGcXa2x {\n  width: 18px;\n  height: 18px;\n  border-radius: 9px;\n  background: #fe0002; }\n\n._1yxtN71Dnktb {\n  width: 18px;\n  height: 18px;\n  border-radius: 9px;\n  background: #fffe03; }\n\n._7SNW0-fUe5se {\n  width: 18px;\n  height: 18px;\n  border-radius: 9px;\n  background: #00fe00; }\n\n._1aqiiesK_g02 {\n  width: 18px;\n  height: 18px;\n  border-radius: 9px;\n  background: #00ffff; }\n\n._R30dOiTkkgUw {\n  width: 18px;\n  height: 18px;\n  border-radius: 9px;\n  background: #ff00ff; }\n", ""]);

// exports
exports.locals = {
	"colors": "_2w_4xceYTAYl",
	"color-wrap": "_1czI-XUNrULs",
	"color-wrap--active": "_1YapypncyrK2",
	"color-item--red": "_3d9KkuGcXa2x",
	"color-item--yellow": "_1yxtN71Dnktb",
	"color-item--green": "_7SNW0-fUe5se",
	"color-item--sky": "_1aqiiesK_g02",
	"color-item--pink": "_R30dOiTkkgUw"
};

/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _utils = __webpack_require__(3);

var _status = __webpack_require__(4);

var STATUS = _interopRequireWildcard(_status);

var _webviewer = __webpack_require__(6);

var WEBVIEWER = _interopRequireWildcard(_webviewer);

var _viewer = __webpack_require__(9);

var _base = __webpack_require__(8);

var _ConfirmModal = __webpack_require__(107);

var _ConfirmModal2 = _interopRequireDefault(_ConfirmModal);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ConfirmModalContainer = function (_Component) {
  _inherits(ConfirmModalContainer, _Component);

  function ConfirmModalContainer() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, ConfirmModalContainer);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = ConfirmModalContainer.__proto__ || Object.getPrototypeOf(ConfirmModalContainer)).call.apply(_ref, [this].concat(args))), _this), _this.handleCancel = function () {
      var baseActions = _this.props.baseActions;

      baseActions.hideModal('exitViewer');
    }, _this.handleConfirm = function () {
      var _this$props = _this.props,
          baseActions = _this$props.baseActions,
          viewerActions = _this$props.viewerActions,
          remoteControl = _this$props.remoteControl;

      baseActions.hideModal('exitViewer');
      remoteControl.exit();
      viewerActions.updateOperatingStatus(STATUS.VIEWER.EXIT);
      baseActions.onReceivedEvent(WEBVIEWER.VIEWER_EVENT.CLOSE, {
        target: 'viewer',
        normal: true
      });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(ConfirmModalContainer, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          i18n = _props.i18n,
          modal = _props.modal,
          viewer = _props.viewer;
      var handleCancel = this.handleCancel,
          handleConfirm = this.handleConfirm;


      return _react2.default.createElement(_ConfirmModal2.default, {
        i18n: i18n,
        visible: modal.exitViewer,
        fixed: viewer.isScreenOverflow,
        description: i18n.message('confirmExitRemoteControl'),
        onCancel: handleCancel,
        onConfirm: handleConfirm
      });
    }
  }]);

  return ConfirmModalContainer;
}(_react.Component);

var enhance = (0, _utils.compose)(_base.useBase, _viewer.useViewer);

exports.default = enhance(ConfirmModalContainer);

/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ConfirmModal = __webpack_require__(108);

var _ConfirmModal2 = _interopRequireDefault(_ConfirmModal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _ConfirmModal2.default;

/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _ConfirmModal = __webpack_require__(109);

var _ConfirmModal2 = _interopRequireDefault(_ConfirmModal);

var _ModalWrapper = __webpack_require__(111);

var _ModalWrapper2 = _interopRequireDefault(_ModalWrapper);

var _Button = __webpack_require__(115);

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ConfirmModal = function ConfirmModal(_ref) {
  var i18n = _ref.i18n,
      visible = _ref.visible,
      fixed = _ref.fixed,
      onConfirm = _ref.onConfirm,
      onCancel = _ref.onCancel,
      description = _ref.description;

  return _react2.default.createElement(
    _ModalWrapper2.default,
    { visible: visible, fixed: fixed },
    _react2.default.createElement(
      'div',
      { className: _ConfirmModal2.default['content'] },
      _react2.default.createElement(
        'div',
        { className: _ConfirmModal2.default['body'] },
        description
      ),
      _react2.default.createElement(
        'div',
        { className: _ConfirmModal2.default['footer'] },
        _react2.default.createElement(
          _Button2.default,
          { theme: 'modal-confirm', onClick: onConfirm },
          i18n.message('yes')
        ),
        _react2.default.createElement(
          _Button2.default,
          { theme: 'modal-cancel', onClick: onCancel },
          i18n.message('no')
        )
      )
    )
  );
};

exports.default = ConfirmModal;

/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(110);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(2)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./ConfirmModal.scss", function() {
		var newContent = require("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./ConfirmModal.scss");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "._3eWhm-BlGq2S {\n  background: white;\n  min-width: 330px;\n  border: 1px solid #a0a0a0;\n  border-radius: 4px;\n  -webkit-box-shadow: 0 0 8px rgba(49, 49, 49, 0.3);\n          box-shadow: 0 0 8px rgba(49, 49, 49, 0.3);\n  overflow: hidden; }\n\n._2Jqxori8u0Dd {\n  padding: 10px;\n  color: #333333;\n  font-size: 12px; }\n\n._2cSM8at5WzTZ {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 20px;\n  font-size: 14px;\n  min-height: 74px;\n  max-width: 360px;\n  color: #555555;\n  background: #f7f8f9; }\n\n._16hF_BNqXFkR {\n  padding: 10px;\n  background: #f7f8f9;\n  text-align: right; }\n", ""]);

// exports
exports.locals = {
	"content": "_3eWhm-BlGq2S",
	"header": "_2Jqxori8u0Dd",
	"body": "_2cSM8at5WzTZ",
	"footer": "_16hF_BNqXFkR"
};

/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ModalWrapper = __webpack_require__(112);

var _ModalWrapper2 = _interopRequireDefault(_ModalWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _ModalWrapper2.default;

/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _ModalWrapper = __webpack_require__(113);

var _ModalWrapper2 = _interopRequireDefault(_ModalWrapper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ModalWrapper = function (_Component) {
  _inherits(ModalWrapper, _Component);

  function ModalWrapper() {
    _classCallCheck(this, ModalWrapper);

    return _possibleConstructorReturn(this, (ModalWrapper.__proto__ || Object.getPrototypeOf(ModalWrapper)).apply(this, arguments));
  }

  _createClass(ModalWrapper, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          visible = _props.visible,
          fixed = _props.fixed;

      if (!visible) return null;

      var backgroundStyle = 'background' + (fixed ? '--fixed' : '');
      var wrapperStyle = 'wrapper' + (fixed ? '--fixed' : '');

      return _react2.default.createElement(
        _react.Fragment,
        null,
        _react2.default.createElement('div', { className: _ModalWrapper2.default[backgroundStyle] }),
        _react2.default.createElement(
          'div',
          { className: _ModalWrapper2.default[wrapperStyle] },
          children
        )
      );
    }
  }]);

  return ModalWrapper;
}(_react.Component);

exports.default = ModalWrapper;

/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(114);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(2)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./ModalWrapper.scss", function() {
		var newContent = require("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./ModalWrapper.scss");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "._BpJcmImuSPO- {\n  background: rgba(97, 97, 97, 0.6);\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  z-index: 100; }\n  ._2x43lgr-4hBQ {\n    background: rgba(97, 97, 97, 0.6);\n    position: absolute;\n    top: 0;\n    left: 0;\n    right: 0;\n    bottom: 0;\n    z-index: 100;\n    position: fixed; }\n\n._gFEQyziwzjcZ {\n  z-index: 110;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%); }\n  ._2Mz8t5772OLS {\n    z-index: 110;\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n    position: fixed; }\n", ""]);

// exports
exports.locals = {
	"background": "_BpJcmImuSPO-",
	"background--fixed": "_2x43lgr-4hBQ",
	"wrapper": "_gFEQyziwzjcZ",
	"wrapper--fixed": "_2Mz8t5772OLS"
};

/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Button = __webpack_require__(116);

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Button2.default;

/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _Button = __webpack_require__(117);

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Button = function (_Component) {
  _inherits(Button, _Component);

  function Button() {
    _classCallCheck(this, Button);

    return _possibleConstructorReturn(this, (Button.__proto__ || Object.getPrototypeOf(Button)).apply(this, arguments));
  }

  _createClass(Button, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          onClick = _props.onClick,
          theme = _props.theme,
          disabled = _props.disabled;

      return _react2.default.createElement(
        'button',
        {
          className: _Button2.default[theme],
          onClick: disabled ? function () {
            return null;
          } : onClick },
        children
      );
    }
  }]);

  return Button;
}(_react.Component);

exports.default = Button;

/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(118);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(2)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./Button.scss", function() {
		var newContent = require("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./Button.scss");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "._3RshP0yS_HZv {\n  display: inline-block;\n  padding: 14px 80px;\n  text-align: center;\n  vertical-align: middle;\n  color: #fff;\n  font-size: 14px;\n  background: #b1b1b1; }\n  ._3RshP0yS_HZv + ._3RshP0yS_HZv {\n    margin-left: 8px; }\n\n._7CoObsHXJHpZ {\n  display: inline-block;\n  padding: 14px 80px;\n  text-align: center;\n  vertical-align: middle;\n  color: #fff;\n  font-size: 14px;\n  background-color: #f17979; }\n  ._7CoObsHXJHpZ + ._7CoObsHXJHpZ {\n    margin-left: 8px; }\n  ._7CoObsHXJHpZ:hover {\n    background-color: #ec4b4b; }\n\n._pVHAy8Q-xRI3 {\n  display: inline-block;\n  padding: 14px 80px;\n  text-align: center;\n  vertical-align: middle;\n  color: #fff;\n  font-size: 14px;\n  background-color: #7a98bf; }\n  ._pVHAy8Q-xRI3 + ._pVHAy8Q-xRI3 {\n    margin-left: 8px; }\n  ._pVHAy8Q-xRI3:hover {\n    background-color: #587dae; }\n\n._1IfS8atjwvbo {\n  font-size: 14px;\n  padding-top: 0.5rem;\n  padding-bottom: 0.5rem;\n  padding-left: 1rem;\n  padding-right: 1rem;\n  border: 0;\n  color: white;\n  background-color: #7a98bf; }\n  ._1IfS8atjwvbo + ._1IfS8atjwvbo {\n    margin-left: 8px; }\n  ._1IfS8atjwvbo:hover {\n    background-color: #587dae; }\n\n._3VIp7ZYjeagQ {\n  line-height: 30px;\n  min-width: 80px;\n  height: 30px;\n  border-radius: 4px;\n  cursor: pointer;\n  background: #1c9cf3;\n  color: #ffffff;\n  border: 1px solid #1c9cf3;\n  font-size: 13px;\n  margin-right: 8px; }\n  ._3VIp7ZYjeagQ:hover {\n    background: #1986cf;\n    border-color: #1986cf; }\n\n._28B4_JJAm5Uu {\n  line-height: 30px;\n  min-width: 80px;\n  height: 30px;\n  border-radius: 4px;\n  cursor: pointer;\n  background: #f7f8f9;\n  font-size: 13px;\n  color: #666666;\n  border: 1px solid #d5d5d5; }\n  ._28B4_JJAm5Uu:hover {\n    background: rgba(228, 228, 228, 0.5); }\n", ""]);

// exports
exports.locals = {
	"default-button": "_3RshP0yS_HZv",
	"create-button": "_7CoObsHXJHpZ",
	"join-button": "_pVHAy8Q-xRI3",
	"join-small-button": "_1IfS8atjwvbo",
	"modal-confirm": "_3VIp7ZYjeagQ",
	"modal-cancel": "_28B4_JJAm5Uu"
};

/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _remoteControl = __webpack_require__(7);

var REMOTE_CONTROL = _interopRequireWildcard(_remoteControl);

var _status = __webpack_require__(4);

var STATUS = _interopRequireWildcard(_status);

var _utils = __webpack_require__(3);

var _viewer = __webpack_require__(9);

var _base = __webpack_require__(8);

var _host = __webpack_require__(10);

var _FunctionKeyboard = __webpack_require__(120);

var _FunctionKeyboard2 = _interopRequireDefault(_FunctionKeyboard);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FunctionKeyboardContainer = function (_Component) {
  _inherits(FunctionKeyboardContainer, _Component);

  function FunctionKeyboardContainer() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, FunctionKeyboardContainer);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = FunctionKeyboardContainer.__proto__ || Object.getPrototypeOf(FunctionKeyboardContainer)).call.apply(_ref, [this].concat(args))), _this), _this.handleKeyboardControl = function (type, keyCode) {
      if (!_this.isUseKmControl()) return;

      var remoteControl = _this.props.remoteControl;

      remoteControl.controlKeyboard(type, keyCode);
    }, _this.updatePressedFunctionKeys = function (pressedFunctionKeys) {
      var viewerActions = _this.props.viewerActions;

      viewerActions.updatePressedFunctionKeys(pressedFunctionKeys);
    }, _this.changeFunctionKeyStatus = function (id) {
      var viewer = _this.props.viewer;
      var pressedFunctionKeys = viewer.pressedFunctionKeys;

      var findKeyId = pressedFunctionKeys.includes(id);
      var nextPressedFunctionKeys = findKeyId ? pressedFunctionKeys.filter(function (keyId) {
        return keyId !== id;
      }) : pressedFunctionKeys.concat(id);

      _this.updatePressedFunctionKeys(nextPressedFunctionKeys);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(FunctionKeyboardContainer, [{
    key: 'hasActivatedApp',
    value: function hasActivatedApp(id) {
      var activatedControlApps = this.props.viewer.activatedControlApps;

      return !!activatedControlApps.find(function (app) {
        return app.appId === id;
      });
    }
  }, {
    key: 'isUseKmControl',
    value: function isUseKmControl() {
      var _props = this.props,
          host = _props.host,
          viewer = _props.viewer;

      return host.useKmControl && viewer.status === STATUS.VIEWER.RUN;
    }
  }, {
    key: 'clearPressedFunctionKeys',
    value: function clearPressedFunctionKeys() {
      var _this2 = this;

      var _props2 = this.props,
          config = _props2.config,
          viewer = _props2.viewer;


      if (!viewer.pressedFunctionKeys.length) return;

      config.functionKeys.filter(function (key) {
        return viewer.pressedFunctionKeys.includes(key.id);
      }).map(function (key) {
        return key.keyCode;
      }).forEach(function (keyCode) {
        return _this2.handleKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.UP, keyCode);
      });

      this.updatePressedFunctionKeys([]);
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      var kmControl = this.hasActivatedApp(0);
      var functionKeyboard = this.hasActivatedApp(3);

      if (!kmControl || !functionKeyboard) {
        this.clearPressedFunctionKeys();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props3 = this.props,
          config = _props3.config,
          host = _props3.host,
          viewer = _props3.viewer,
          remoteControl = _props3.remoteControl;

      var kmControl = this.hasActivatedApp(0);
      var functionKeyboard = this.hasActivatedApp(3);

      if (!kmControl || !functionKeyboard) return null;

      return _react2.default.createElement(_FunctionKeyboard2.default, {
        config: config,
        host: host,
        viewer: viewer,
        remoteControl: remoteControl,
        updatePressedFunctionKeys: this.updatePressedFunctionKeys,
        changeFunctionKeyStatus: this.changeFunctionKeyStatus,
        onKeyboardControl: this.handleKeyboardControl,
        clearPressedFunctionKeys: this.clearPressedFunctionKeys
      });
    }
  }]);

  return FunctionKeyboardContainer;
}(_react.Component);

var enhance = (0, _utils.compose)(_base.useBase, _host.useHost, _viewer.useViewer);

exports.default = enhance(FunctionKeyboardContainer);

/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _FunctionKeyboard = __webpack_require__(121);

var _FunctionKeyboard2 = _interopRequireDefault(_FunctionKeyboard);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _FunctionKeyboard2.default;

/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactDraggable = __webpack_require__(122);

var _reactDraggable2 = _interopRequireDefault(_reactDraggable);

var _FunctionKeyboard = __webpack_require__(124);

var _FunctionKeyboard2 = _interopRequireDefault(_FunctionKeyboard);

var _remoteControl = __webpack_require__(7);

var REMOTE_CONTROL = _interopRequireWildcard(_remoteControl);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FunctionKeyboard = function (_Component) {
  _inherits(FunctionKeyboard, _Component);

  function FunctionKeyboard() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, FunctionKeyboard);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = FunctionKeyboard.__proto__ || Object.getPrototypeOf(FunctionKeyboard)).call.apply(_ref, [this].concat(args))), _this), _this.hasPressedKey = function (id) {
      var pressedFunctionKeys = _this.props.viewer.pressedFunctionKeys;

      return pressedFunctionKeys.includes(id);
    }, _this.sendKeyBoardPressEvent = function (keyCode) {
      var onKeyboardControl = _this.props.onKeyboardControl;

      onKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.DOWN, keyCode);
      onKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.UP, keyCode);
    }, _this.sendSpecialKey = function (keyCode) {
      var remoteControl = _this.props.remoteControl;

      remoteControl.sendSpecialKey(keyCode);
    }, _this.sendKeyBoardDownEvent = function (id, keyCode) {
      var _this$props = _this.props,
          onKeyboardControl = _this$props.onKeyboardControl,
          changeFunctionKeyStatus = _this$props.changeFunctionKeyStatus;

      onKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.DOWN, keyCode);
      changeFunctionKeyStatus(id);
    }, _this.sendKeyBoardUpEvent = function (id, keyCode) {
      var _this$props2 = _this.props,
          onKeyboardControl = _this$props2.onKeyboardControl,
          changeFunctionKeyStatus = _this$props2.changeFunctionKeyStatus;

      onKeyboardControl(REMOTE_CONTROL.KEYBOARD_MESSAGE.UP, keyCode);
      changeFunctionKeyStatus(id);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(FunctionKeyboard, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      var _props = this.props,
          viewer = _props.viewer,
          clearPressedFunctionKeys = _props.clearPressedFunctionKeys;

      var isPressedFunctionKeys = !!viewer.pressedFunctionKeys.length;
      var functionKeyboard = this.hasActivatedApp(3);

      if (isPressedFunctionKeys && !functionKeyboard) {
        clearPressedFunctionKeys();
      }
    }
  }, {
    key: 'hasActivatedApp',
    value: function hasActivatedApp(id) {
      var activatedControlApps = this.props.viewer.activatedControlApps;

      return !!activatedControlApps.find(function (app) {
        return app.appId === id;
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          viewer = _props2.viewer,
          config = _props2.config;
      var sendKeyBoardPressEvent = this.sendKeyBoardPressEvent,
          sendSpecialKey = this.sendSpecialKey,
          sendKeyBoardDownEvent = this.sendKeyBoardDownEvent,
          sendKeyBoardUpEvent = this.sendKeyBoardUpEvent,
          hasPressedKey = this.hasPressedKey;


      var keys = config.functionKeys.filter(function (item) {
        return !item.pressable;
      });
      var pressableKeys = config.functionKeys.filter(function (item) {
        return item.pressable;
      });
      var defaultPosition = {
        x: Math.min(window.innerWidth, viewer.width) / 2 - 425 / 2,
        y: 140
      };

      return _react2.default.createElement(
        _reactDraggable2.default,
        { handle: '.' + _FunctionKeyboard2.default['handle'], bounds: 'parent', defaultPosition: defaultPosition },
        _react2.default.createElement(
          'div',
          { className: _FunctionKeyboard2.default['function-keyboard'] },
          _react2.default.createElement('img', { className: _FunctionKeyboard2.default['handle'], alt: '' }),
          _react2.default.createElement(
            'div',
            { className: _FunctionKeyboard2.default['function-keyboard__item-wrap'] },
            keys.map(function (key) {
              var keyStyle = 'function-keyboard__item' + (hasPressedKey(key.id) ? '--active' : '');

              return _react2.default.createElement(
                'div',
                {
                  key: key.id,
                  className: _FunctionKeyboard2.default[keyStyle],
                  onClick: function onClick() {
                    return (key.specialKey ? sendSpecialKey : sendKeyBoardPressEvent)(key.keyCode);
                  }
                },
                key.name
              );
            })
          ),
          _react2.default.createElement(
            'div',
            { className: _FunctionKeyboard2.default['function-keyboard__item--plus-wrap'] },
            pressableKeys.map(function (key) {
              var plusKeyStyle = 'function-keyboard__item--plus' + (hasPressedKey(key.id) ? '--active' : '');
              var pressedKeyStyle = 'plus' + (hasPressedKey(key.id) ? '--active' : '');

              return _react2.default.createElement(
                'div',
                {
                  key: key.id,
                  className: _FunctionKeyboard2.default[plusKeyStyle],
                  onClick: function onClick() {
                    return (hasPressedKey(key.id) ? sendKeyBoardUpEvent : sendKeyBoardDownEvent)(key.id, key.keyCode);
                  }
                },
                key.name,
                _react2.default.createElement(
                  'span',
                  { className: _FunctionKeyboard2.default[pressedKeyStyle] },
                  '+'
                )
              );
            })
          ),
          _react2.default.createElement('img', { className: _FunctionKeyboard2.default['handle'], alt: '' })
        )
      );
    }
  }]);

  return FunctionKeyboard;
}(_react.Component);

exports.default = FunctionKeyboard;

/***/ }),
/* 122 */
/***/ (function(module, exports, __webpack_require__) {

(function webpackUniversalModuleDefinition(root, factory) {
	if(true)
		module.exports = factory(__webpack_require__(123), __webpack_require__(0));
	else if(typeof define === 'function' && define.amd)
		define(["react-dom", "react"], factory);
	else if(typeof exports === 'object')
		exports["ReactDraggable"] = factory(require("react-dom"), require("react"));
	else
		root["ReactDraggable"] = factory(root["ReactDOM"], root["React"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_4__, __WEBPACK_EXTERNAL_MODULE_6__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 12);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.findInArray = findInArray;
exports.isFunction = isFunction;
exports.isNum = isNum;
exports.int = int;
exports.dontSetMe = dontSetMe;

// @credits https://gist.github.com/rogozhnikoff/a43cfed27c41e4e68cdc
function findInArray(array /*: Array<any> | TouchList*/, callback /*: Function*/) /*: any*/ {
  for (var i = 0, length = array.length; i < length; i++) {
    if (callback.apply(callback, [array[i], i, array])) return array[i];
  }
}

function isFunction(func /*: any*/) /*: boolean*/ {
  return typeof func === 'function' || Object.prototype.toString.call(func) === '[object Function]';
}

function isNum(num /*: any*/) /*: boolean*/ {
  return typeof num === 'number' && !isNaN(num);
}

function int(a /*: string*/) /*: number*/ {
  return parseInt(a, 10);
}

function dontSetMe(props /*: Object*/, propName /*: string*/, componentName /*: string*/) {
  if (props[propName]) {
    return new Error('Invalid prop ' + propName + ' passed to ' + componentName + ' - do not set this, set it on the child.');
  }
}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * 
 */

function makeEmptyFunction(arg) {
  return function () {
    return arg;
  };
}

/**
 * This function accepts and discards inputs; it has no side effects. This is
 * primarily useful idiomatically for overridable function endpoints which
 * always need to be callable, since JS lacks a null-call idiom ala Cocoa.
 */
var emptyFunction = function emptyFunction() {};

emptyFunction.thatReturns = makeEmptyFunction;
emptyFunction.thatReturnsFalse = makeEmptyFunction(false);
emptyFunction.thatReturnsTrue = makeEmptyFunction(true);
emptyFunction.thatReturnsNull = makeEmptyFunction(null);
emptyFunction.thatReturnsThis = function () {
  return this;
};
emptyFunction.thatReturnsArgument = function (arg) {
  return arg;
};

module.exports = emptyFunction;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */



/**
 * Use invariant() to assert state which your program assumes to be true.
 *
 * Provide sprintf-style format (only %s is supported) and arguments
 * to provide information about what broke and what you were
 * expecting.
 *
 * The invariant message will be stripped in production, but the invariant
 * will remain to ensure logic does not differ in production.
 */

var validateFormat = function validateFormat(format) {};

if (Object({"DRAGGABLE_DEBUG":undefined}).NODE_ENV !== 'production') {
  validateFormat = function validateFormat(format) {
    if (format === undefined) {
      throw new Error('invariant requires an error message argument');
    }
  };
}

function invariant(condition, format, a, b, c, d, e, f) {
  validateFormat(format);

  if (!condition) {
    var error;
    if (format === undefined) {
      error = new Error('Minified exception occurred; use the non-minified dev environment ' + 'for the full error message and additional helpful warnings.');
    } else {
      var args = [a, b, c, d, e, f];
      var argIndex = 0;
      error = new Error(format.replace(/%s/g, function () {
        return args[argIndex++];
      }));
      error.name = 'Invariant Violation';
    }

    error.framesToPop = 1; // we don't care about invariant's own frame
    throw error;
  }
}

module.exports = invariant;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var ReactPropTypesSecret = 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED';

module.exports = ReactPropTypesSecret;


/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.matchesSelector = matchesSelector;
exports.matchesSelectorAndParentsTo = matchesSelectorAndParentsTo;
exports.addEvent = addEvent;
exports.removeEvent = removeEvent;
exports.outerHeight = outerHeight;
exports.outerWidth = outerWidth;
exports.innerHeight = innerHeight;
exports.innerWidth = innerWidth;
exports.offsetXYFromParent = offsetXYFromParent;
exports.createCSSTransform = createCSSTransform;
exports.createSVGTransform = createSVGTransform;
exports.getTouch = getTouch;
exports.getTouchIdentifier = getTouchIdentifier;
exports.addUserSelectStyles = addUserSelectStyles;
exports.removeUserSelectStyles = removeUserSelectStyles;
exports.styleHacks = styleHacks;
exports.addClassName = addClassName;
exports.removeClassName = removeClassName;

var _shims = __webpack_require__(0);

var _getPrefix = __webpack_require__(19);

var _getPrefix2 = _interopRequireDefault(_getPrefix);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*:: import type {ControlPosition, MouseTouchEvent} from './types';*/


var matchesSelectorFunc = '';
function matchesSelector(el /*: Node*/, selector /*: string*/) /*: boolean*/ {
  if (!matchesSelectorFunc) {
    matchesSelectorFunc = (0, _shims.findInArray)(['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'], function (method) {
      // $FlowIgnore: Doesn't think elements are indexable
      return (0, _shims.isFunction)(el[method]);
    });
  }

  // Might not be found entirely (not an Element?) - in that case, bail
  // $FlowIgnore: Doesn't think elements are indexable
  if (!(0, _shims.isFunction)(el[matchesSelectorFunc])) return false;

  // $FlowIgnore: Doesn't think elements are indexable
  return el[matchesSelectorFunc](selector);
}

// Works up the tree to the draggable itself attempting to match selector.
function matchesSelectorAndParentsTo(el /*: Node*/, selector /*: string*/, baseNode /*: Node*/) /*: boolean*/ {
  var node = el;
  do {
    if (matchesSelector(node, selector)) return true;
    if (node === baseNode) return false;
    node = node.parentNode;
  } while (node);

  return false;
}

function addEvent(el /*: ?Node*/, event /*: string*/, handler /*: Function*/) /*: void*/ {
  if (!el) {
    return;
  }
  if (el.attachEvent) {
    el.attachEvent('on' + event, handler);
  } else if (el.addEventListener) {
    el.addEventListener(event, handler, true);
  } else {
    // $FlowIgnore: Doesn't think elements are indexable
    el['on' + event] = handler;
  }
}

function removeEvent(el /*: ?Node*/, event /*: string*/, handler /*: Function*/) /*: void*/ {
  if (!el) {
    return;
  }
  if (el.detachEvent) {
    el.detachEvent('on' + event, handler);
  } else if (el.removeEventListener) {
    el.removeEventListener(event, handler, true);
  } else {
    // $FlowIgnore: Doesn't think elements are indexable
    el['on' + event] = null;
  }
}

function outerHeight(node /*: HTMLElement*/) /*: number*/ {
  // This is deliberately excluding margin for our calculations, since we are using
  // offsetTop which is including margin. See getBoundPosition
  var height = node.clientHeight;
  var computedStyle = node.ownerDocument.defaultView.getComputedStyle(node);
  height += (0, _shims.int)(computedStyle.borderTopWidth);
  height += (0, _shims.int)(computedStyle.borderBottomWidth);
  return height;
}

function outerWidth(node /*: HTMLElement*/) /*: number*/ {
  // This is deliberately excluding margin for our calculations, since we are using
  // offsetLeft which is including margin. See getBoundPosition
  var width = node.clientWidth;
  var computedStyle = node.ownerDocument.defaultView.getComputedStyle(node);
  width += (0, _shims.int)(computedStyle.borderLeftWidth);
  width += (0, _shims.int)(computedStyle.borderRightWidth);
  return width;
}
function innerHeight(node /*: HTMLElement*/) /*: number*/ {
  var height = node.clientHeight;
  var computedStyle = node.ownerDocument.defaultView.getComputedStyle(node);
  height -= (0, _shims.int)(computedStyle.paddingTop);
  height -= (0, _shims.int)(computedStyle.paddingBottom);
  return height;
}

function innerWidth(node /*: HTMLElement*/) /*: number*/ {
  var width = node.clientWidth;
  var computedStyle = node.ownerDocument.defaultView.getComputedStyle(node);
  width -= (0, _shims.int)(computedStyle.paddingLeft);
  width -= (0, _shims.int)(computedStyle.paddingRight);
  return width;
}

// Get from offsetParent
function offsetXYFromParent(evt /*: {clientX: number, clientY: number}*/, offsetParent /*: HTMLElement*/) /*: ControlPosition*/ {
  var isBody = offsetParent === offsetParent.ownerDocument.body;
  var offsetParentRect = isBody ? { left: 0, top: 0 } : offsetParent.getBoundingClientRect();

  var x = evt.clientX + offsetParent.scrollLeft - offsetParentRect.left;
  var y = evt.clientY + offsetParent.scrollTop - offsetParentRect.top;

  return { x: x, y: y };
}

function createCSSTransform(_ref) /*: Object*/ {
  var x = _ref.x,
      y = _ref.y;

  // Replace unitless items with px
  return _defineProperty({}, (0, _getPrefix.browserPrefixToKey)('transform', _getPrefix2.default), 'translate(' + x + 'px,' + y + 'px)');
}

function createSVGTransform(_ref3) /*: string*/ {
  var x = _ref3.x,
      y = _ref3.y;

  return 'translate(' + x + ',' + y + ')';
}

function getTouch(e /*: MouseTouchEvent*/, identifier /*: number*/) /*: ?{clientX: number, clientY: number}*/ {
  return e.targetTouches && (0, _shims.findInArray)(e.targetTouches, function (t) {
    return identifier === t.identifier;
  }) || e.changedTouches && (0, _shims.findInArray)(e.changedTouches, function (t) {
    return identifier === t.identifier;
  });
}

function getTouchIdentifier(e /*: MouseTouchEvent*/) /*: ?number*/ {
  if (e.targetTouches && e.targetTouches[0]) return e.targetTouches[0].identifier;
  if (e.changedTouches && e.changedTouches[0]) return e.changedTouches[0].identifier;
}

// User-select Hacks:
//
// Useful for preventing blue highlights all over everything when dragging.

// Note we're passing `document` b/c we could be iframed
function addUserSelectStyles(doc /*: ?Document*/) {
  if (!doc) return;
  var styleEl = doc.getElementById('react-draggable-style-el');
  if (!styleEl) {
    styleEl = doc.createElement('style');
    styleEl.type = 'text/css';
    styleEl.id = 'react-draggable-style-el';
    styleEl.innerHTML = '.react-draggable-transparent-selection *::-moz-selection {background: transparent;}\n';
    styleEl.innerHTML += '.react-draggable-transparent-selection *::selection {background: transparent;}\n';
    doc.getElementsByTagName('head')[0].appendChild(styleEl);
  }
  if (doc.body) addClassName(doc.body, 'react-draggable-transparent-selection');
}

function removeUserSelectStyles(doc /*: ?Document*/) {
  try {
    if (doc && doc.body) removeClassName(doc.body, 'react-draggable-transparent-selection');
    window.getSelection().removeAllRanges(); // remove selection caused by scroll
  } catch (e) {
    // probably IE
  }
}

function styleHacks() /*: Object*/ {
  var childStyle /*: Object*/ = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  // Workaround IE pointer events; see #51
  // https://github.com/mzabriskie/react-draggable/issues/51#issuecomment-103488278
  return _extends({
    touchAction: 'none'
  }, childStyle);
}

function addClassName(el /*: HTMLElement*/, className /*: string*/) {
  if (el.classList) {
    el.classList.add(className);
  } else {
    if (!el.className.match(new RegExp('(?:^|\\s)' + className + '(?!\\S)'))) {
      el.className += ' ' + className;
    }
  }
}

function removeClassName(el /*: HTMLElement*/, className /*: string*/) {
  if (el.classList) {
    el.classList.remove(className);
  } else {
    el.className = el.className.replace(new RegExp('(?:^|\\s)' + className + '(?!\\S)', 'g'), '');
  }
}

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_6__;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

if (Object({"DRAGGABLE_DEBUG":undefined}).NODE_ENV !== 'production') {
  var REACT_ELEMENT_TYPE = (typeof Symbol === 'function' &&
    Symbol.for &&
    Symbol.for('react.element')) ||
    0xeac7;

  var isValidElement = function(object) {
    return typeof object === 'object' &&
      object !== null &&
      object.$$typeof === REACT_ELEMENT_TYPE;
  };

  // By explicitly using `prop-types` you are opting into new development behavior.
  // http://fb.me/prop-types-in-prod
  var throwOnDirectAccess = true;
  module.exports = __webpack_require__(14)(isValidElement, throwOnDirectAccess);
} else {
  // By explicitly using `prop-types` you are opting into new production behavior.
  // http://fb.me/prop-types-in-prod
  module.exports = __webpack_require__(17)();
}


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */



var emptyFunction = __webpack_require__(1);

/**
 * Similar to invariant but only logs a warning if the condition is not met.
 * This can be used to log issues in development environments in critical
 * paths. Removing the logging code for production environments will keep the
 * same logic and follow the same code paths.
 */

var warning = emptyFunction;

if (Object({"DRAGGABLE_DEBUG":undefined}).NODE_ENV !== 'production') {
  var printWarning = function printWarning(format) {
    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    var argIndex = 0;
    var message = 'Warning: ' + format.replace(/%s/g, function () {
      return args[argIndex++];
    });
    if (typeof console !== 'undefined') {
      console.error(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };

  warning = function warning(condition, format) {
    if (format === undefined) {
      throw new Error('`warning(condition, format, ...args)` requires a warning ' + 'message argument');
    }

    if (format.indexOf('Failed Composite propType: ') === 0) {
      return; // Ignore CompositeComponent proptype check.
    }

    if (!condition) {
      for (var _len2 = arguments.length, args = Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
        args[_key2 - 2] = arguments[_key2];
      }

      printWarning.apply(undefined, [format].concat(args));
    }
  };
}

module.exports = warning;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getBoundPosition = getBoundPosition;
exports.snapToGrid = snapToGrid;
exports.canDragX = canDragX;
exports.canDragY = canDragY;
exports.getControlPosition = getControlPosition;
exports.createCoreData = createCoreData;
exports.createDraggableData = createDraggableData;

var _shims = __webpack_require__(0);

var _reactDom = __webpack_require__(4);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _domFns = __webpack_require__(5);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*:: import type Draggable from '../Draggable';*/
/*:: import type {Bounds, ControlPosition, DraggableData, MouseTouchEvent} from './types';*/
/*:: import type DraggableCore from '../DraggableCore';*/
function getBoundPosition(draggable /*: Draggable*/, x /*: number*/, y /*: number*/) /*: [number, number]*/ {
  // If no bounds, short-circuit and move on
  if (!draggable.props.bounds) return [x, y];

  // Clone new bounds
  var bounds = draggable.props.bounds;

  bounds = typeof bounds === 'string' ? bounds : cloneBounds(bounds);
  var node = findDOMNode(draggable);

  if (typeof bounds === 'string') {
    var ownerDocument = node.ownerDocument;

    var ownerWindow = ownerDocument.defaultView;
    var boundNode = void 0;
    if (bounds === 'parent') {
      boundNode = node.parentNode;
    } else {
      boundNode = ownerDocument.querySelector(bounds);
    }
    if (!(boundNode instanceof HTMLElement)) {
      throw new Error('Bounds selector "' + bounds + '" could not find an element.');
    }
    var nodeStyle = ownerWindow.getComputedStyle(node);
    var boundNodeStyle = ownerWindow.getComputedStyle(boundNode);
    // Compute bounds. This is a pain with padding and offsets but this gets it exactly right.
    bounds = {
      left: -node.offsetLeft + (0, _shims.int)(boundNodeStyle.paddingLeft) + (0, _shims.int)(nodeStyle.marginLeft),
      top: -node.offsetTop + (0, _shims.int)(boundNodeStyle.paddingTop) + (0, _shims.int)(nodeStyle.marginTop),
      right: (0, _domFns.innerWidth)(boundNode) - (0, _domFns.outerWidth)(node) - node.offsetLeft + (0, _shims.int)(boundNodeStyle.paddingRight) - (0, _shims.int)(nodeStyle.marginRight),
      bottom: (0, _domFns.innerHeight)(boundNode) - (0, _domFns.outerHeight)(node) - node.offsetTop + (0, _shims.int)(boundNodeStyle.paddingBottom) - (0, _shims.int)(nodeStyle.marginBottom)
    };
  }

  // Keep x and y below right and bottom limits...
  if ((0, _shims.isNum)(bounds.right)) x = Math.min(x, bounds.right);
  if ((0, _shims.isNum)(bounds.bottom)) y = Math.min(y, bounds.bottom);

  // But above left and top limits.
  if ((0, _shims.isNum)(bounds.left)) x = Math.max(x, bounds.left);
  if ((0, _shims.isNum)(bounds.top)) y = Math.max(y, bounds.top);

  return [x, y];
}

function snapToGrid(grid /*: [number, number]*/, pendingX /*: number*/, pendingY /*: number*/) /*: [number, number]*/ {
  var x = Math.round(pendingX / grid[0]) * grid[0];
  var y = Math.round(pendingY / grid[1]) * grid[1];
  return [x, y];
}

function canDragX(draggable /*: Draggable*/) /*: boolean*/ {
  return draggable.props.axis === 'both' || draggable.props.axis === 'x';
}

function canDragY(draggable /*: Draggable*/) /*: boolean*/ {
  return draggable.props.axis === 'both' || draggable.props.axis === 'y';
}

// Get {x, y} positions from event.
function getControlPosition(e /*: MouseTouchEvent*/, touchIdentifier /*: ?number*/, draggableCore /*: DraggableCore*/) /*: ?ControlPosition*/ {
  var touchObj = typeof touchIdentifier === 'number' ? (0, _domFns.getTouch)(e, touchIdentifier) : null;
  if (typeof touchIdentifier === 'number' && !touchObj) return null; // not the right touch
  var node = findDOMNode(draggableCore);
  // User can provide an offsetParent if desired.
  var offsetParent = draggableCore.props.offsetParent || node.offsetParent || node.ownerDocument.body;
  return (0, _domFns.offsetXYFromParent)(touchObj || e, offsetParent);
}

// Create an data object exposed by <DraggableCore>'s events
function createCoreData(draggable /*: DraggableCore*/, x /*: number*/, y /*: number*/) /*: DraggableData*/ {
  var state = draggable.state;
  var isStart = !(0, _shims.isNum)(state.lastX);
  var node = findDOMNode(draggable);

  if (isStart) {
    // If this is our first move, use the x and y as last coords.
    return {
      node: node,
      deltaX: 0, deltaY: 0,
      lastX: x, lastY: y,
      x: x, y: y
    };
  } else {
    // Otherwise calculate proper values.
    return {
      node: node,
      deltaX: x - state.lastX, deltaY: y - state.lastY,
      lastX: state.lastX, lastY: state.lastY,
      x: x, y: y
    };
  }
}

// Create an data exposed by <Draggable>'s events
function createDraggableData(draggable /*: Draggable*/, coreData /*: DraggableData*/) /*: DraggableData*/ {
  return {
    node: coreData.node,
    x: draggable.state.x + coreData.deltaX,
    y: draggable.state.y + coreData.deltaY,
    deltaX: coreData.deltaX,
    deltaY: coreData.deltaY,
    lastX: draggable.state.x,
    lastY: draggable.state.y
  };
}

// A lot faster than stringify/parse
function cloneBounds(bounds /*: Bounds*/) /*: Bounds*/ {
  return {
    left: bounds.left,
    top: bounds.top,
    right: bounds.right,
    bottom: bounds.bottom
  };
}

function findDOMNode(draggable /*: Draggable | DraggableCore*/) /*: HTMLElement*/ {
  var node = _reactDom2.default.findDOMNode(draggable);
  if (!node) {
    throw new Error('<DraggableCore>: Unmounted during event!');
  }
  // $FlowIgnore we can't assert on HTMLElement due to tests... FIXME
  return node;
}

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(6);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(7);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDom = __webpack_require__(4);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _domFns = __webpack_require__(5);

var _positionFns = __webpack_require__(9);

var _shims = __webpack_require__(0);

var _log = __webpack_require__(11);

var _log2 = _interopRequireDefault(_log);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/*:: import type {EventHandler, MouseTouchEvent} from './utils/types';*/


// Simple abstraction for dragging events names.
/*:: import type {Element as ReactElement} from 'react';*/
var eventsFor = {
  touch: {
    start: 'touchstart',
    move: 'touchmove',
    stop: 'touchend'
  },
  mouse: {
    start: 'mousedown',
    move: 'mousemove',
    stop: 'mouseup'
  }
};

// Default to mouse events.
var dragEventFor = eventsFor.mouse;

/*:: type DraggableCoreState = {
  dragging: boolean,
  lastX: number,
  lastY: number,
  touchIdentifier: ?number
};*/
/*:: export type DraggableBounds = {
  left: number,
  right: number,
  top: number,
  bottom: number,
};*/
/*:: export type DraggableData = {
  node: HTMLElement,
  x: number, y: number,
  deltaX: number, deltaY: number,
  lastX: number, lastY: number,
};*/
/*:: export type DraggableEventHandler = (e: MouseEvent, data: DraggableData) => void;*/
/*:: export type ControlPosition = {x: number, y: number};*/


//
// Define <DraggableCore>.
//
// <DraggableCore> is for advanced usage of <Draggable>. It maintains minimal internal state so it can
// work well with libraries that require more control over the element.
//

/*:: export type DraggableCoreProps = {
  allowAnyClick: boolean,
  cancel: string,
  children: ReactElement<any>,
  disabled: boolean,
  enableUserSelectHack: boolean,
  offsetParent: HTMLElement,
  grid: [number, number],
  handle: string,
  onStart: DraggableEventHandler,
  onDrag: DraggableEventHandler,
  onStop: DraggableEventHandler,
  onMouseDown: (e: MouseEvent) => void,
};*/

var DraggableCore = function (_React$Component) {
  _inherits(DraggableCore, _React$Component);

  function DraggableCore() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, DraggableCore);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = DraggableCore.__proto__ || Object.getPrototypeOf(DraggableCore)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      dragging: false,
      // Used while dragging to determine deltas.
      lastX: NaN, lastY: NaN,
      touchIdentifier: null
    }, _this.handleDragStart = function (e) {
      // Make it possible to attach event handlers on top of this one.
      _this.props.onMouseDown(e);

      // Only accept left-clicks.
      if (!_this.props.allowAnyClick && typeof e.button === 'number' && e.button !== 0) return false;

      // Get nodes. Be sure to grab relative document (could be iframed)
      var thisNode = _reactDom2.default.findDOMNode(_this);
      if (!thisNode || !thisNode.ownerDocument || !thisNode.ownerDocument.body) {
        throw new Error('<DraggableCore> not mounted on DragStart!');
      }
      var ownerDocument = thisNode.ownerDocument;

      // Short circuit if handle or cancel prop was provided and selector doesn't match.

      if (_this.props.disabled || !(e.target instanceof ownerDocument.defaultView.Node) || _this.props.handle && !(0, _domFns.matchesSelectorAndParentsTo)(e.target, _this.props.handle, thisNode) || _this.props.cancel && (0, _domFns.matchesSelectorAndParentsTo)(e.target, _this.props.cancel, thisNode)) {
        return;
      }

      // Set touch identifier in component state if this is a touch event. This allows us to
      // distinguish between individual touches on multitouch screens by identifying which
      // touchpoint was set to this element.
      var touchIdentifier = (0, _domFns.getTouchIdentifier)(e);
      _this.setState({ touchIdentifier: touchIdentifier });

      // Get the current drag point from the event. This is used as the offset.
      var position = (0, _positionFns.getControlPosition)(e, touchIdentifier, _this);
      if (position == null) return; // not possible but satisfies flow
      var x = position.x,
          y = position.y;

      // Create an event object with all the data parents need to make a decision here.

      var coreEvent = (0, _positionFns.createCoreData)(_this, x, y);

      (0, _log2.default)('DraggableCore: handleDragStart: %j', coreEvent);

      // Call event handler. If it returns explicit false, cancel.
      (0, _log2.default)('calling', _this.props.onStart);
      var shouldUpdate = _this.props.onStart(e, coreEvent);
      if (shouldUpdate === false) return;

      // Add a style to the body to disable user-select. This prevents text from
      // being selected all over the page.
      if (_this.props.enableUserSelectHack) (0, _domFns.addUserSelectStyles)(ownerDocument);

      // Initiate dragging. Set the current x and y as offsets
      // so we know how much we've moved during the drag. This allows us
      // to drag elements around even if they have been moved, without issue.
      _this.setState({
        dragging: true,

        lastX: x,
        lastY: y
      });

      // Add events to the document directly so we catch when the user's mouse/touch moves outside of
      // this element. We use different events depending on whether or not we have detected that this
      // is a touch-capable device.
      (0, _domFns.addEvent)(ownerDocument, dragEventFor.move, _this.handleDrag);
      (0, _domFns.addEvent)(ownerDocument, dragEventFor.stop, _this.handleDragStop);
    }, _this.handleDrag = function (e) {

      // Prevent scrolling on mobile devices, like ipad/iphone.
      if (e.type === 'touchmove') e.preventDefault();

      // Get the current drag point from the event. This is used as the offset.
      var position = (0, _positionFns.getControlPosition)(e, _this.state.touchIdentifier, _this);
      if (position == null) return;
      var x = position.x,
          y = position.y;

      // Snap to grid if prop has been provided

      if (Array.isArray(_this.props.grid)) {
        var _deltaX = x - _this.state.lastX,
            _deltaY = y - _this.state.lastY;

        var _snapToGrid = (0, _positionFns.snapToGrid)(_this.props.grid, _deltaX, _deltaY);

        var _snapToGrid2 = _slicedToArray(_snapToGrid, 2);

        _deltaX = _snapToGrid2[0];
        _deltaY = _snapToGrid2[1];

        if (!_deltaX && !_deltaY) return; // skip useless drag
        x = _this.state.lastX + _deltaX, y = _this.state.lastY + _deltaY;
      }

      var coreEvent = (0, _positionFns.createCoreData)(_this, x, y);

      (0, _log2.default)('DraggableCore: handleDrag: %j', coreEvent);

      // Call event handler. If it returns explicit false, trigger end.
      var shouldUpdate = _this.props.onDrag(e, coreEvent);
      if (shouldUpdate === false) {
        try {
          // $FlowIgnore
          _this.handleDragStop(new MouseEvent('mouseup'));
        } catch (err) {
          // Old browsers
          var event = ((document.createEvent('MouseEvents') /*: any*/) /*: MouseTouchEvent*/);
          // I see why this insanity was deprecated
          // $FlowIgnore
          event.initMouseEvent('mouseup', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
          _this.handleDragStop(event);
        }
        return;
      }

      _this.setState({
        lastX: x,
        lastY: y
      });
    }, _this.handleDragStop = function (e) {
      if (!_this.state.dragging) return;

      var position = (0, _positionFns.getControlPosition)(e, _this.state.touchIdentifier, _this);
      if (position == null) return;
      var x = position.x,
          y = position.y;

      var coreEvent = (0, _positionFns.createCoreData)(_this, x, y);

      var thisNode = _reactDom2.default.findDOMNode(_this);
      if (thisNode) {
        // Remove user-select hack
        if (_this.props.enableUserSelectHack) (0, _domFns.removeUserSelectStyles)(thisNode.ownerDocument);
      }

      (0, _log2.default)('DraggableCore: handleDragStop: %j', coreEvent);

      // Reset the el.
      _this.setState({
        dragging: false,
        lastX: NaN,
        lastY: NaN
      });

      // Call event handler
      _this.props.onStop(e, coreEvent);

      if (thisNode) {
        // Remove event handlers
        (0, _log2.default)('DraggableCore: Removing handlers');
        (0, _domFns.removeEvent)(thisNode.ownerDocument, dragEventFor.move, _this.handleDrag);
        (0, _domFns.removeEvent)(thisNode.ownerDocument, dragEventFor.stop, _this.handleDragStop);
      }
    }, _this.onMouseDown = function (e) {
      dragEventFor = eventsFor.mouse; // on touchscreen laptops we could switch back to mouse

      return _this.handleDragStart(e);
    }, _this.onMouseUp = function (e) {
      dragEventFor = eventsFor.mouse;

      return _this.handleDragStop(e);
    }, _this.onTouchStart = function (e) {
      // We're on a touch device now, so change the event handlers
      dragEventFor = eventsFor.touch;

      return _this.handleDragStart(e);
    }, _this.onTouchEnd = function (e) {
      // We're on a touch device now, so change the event handlers
      dragEventFor = eventsFor.touch;

      return _this.handleDragStop(e);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(DraggableCore, [{
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      // Remove any leftover event handlers. Remove both touch and mouse handlers in case
      // some browser quirk caused a touch event to fire during a mouse move, or vice versa.
      var thisNode = _reactDom2.default.findDOMNode(this);
      if (thisNode) {
        var ownerDocument = thisNode.ownerDocument;

        (0, _domFns.removeEvent)(ownerDocument, eventsFor.mouse.move, this.handleDrag);
        (0, _domFns.removeEvent)(ownerDocument, eventsFor.touch.move, this.handleDrag);
        (0, _domFns.removeEvent)(ownerDocument, eventsFor.mouse.stop, this.handleDragStop);
        (0, _domFns.removeEvent)(ownerDocument, eventsFor.touch.stop, this.handleDragStop);
        if (this.props.enableUserSelectHack) (0, _domFns.removeUserSelectStyles)(ownerDocument);
      }
    }

    // Same as onMouseDown (start drag), but now consider this a touch device.

  }, {
    key: 'render',
    value: function render() {
      // Reuse the child provided
      // This makes it flexible to use whatever element is wanted (div, ul, etc)
      return _react2.default.cloneElement(_react2.default.Children.only(this.props.children), {
        style: (0, _domFns.styleHacks)(this.props.children.props.style),

        // Note: mouseMove handler is attached to document so it will still function
        // when the user drags quickly and leaves the bounds of the element.
        onMouseDown: this.onMouseDown,
        onTouchStart: this.onTouchStart,
        onMouseUp: this.onMouseUp,
        onTouchEnd: this.onTouchEnd
      });
    }
  }]);

  return DraggableCore;
}(_react2.default.Component);

DraggableCore.displayName = 'DraggableCore';
DraggableCore.propTypes = {
  /**
   * `allowAnyClick` allows dragging using any mouse button.
   * By default, we only accept the left button.
   *
   * Defaults to `false`.
   */
  allowAnyClick: _propTypes2.default.bool,

  /**
   * `disabled`, if true, stops the <Draggable> from dragging. All handlers,
   * with the exception of `onMouseDown`, will not fire.
   */
  disabled: _propTypes2.default.bool,

  /**
   * By default, we add 'user-select:none' attributes to the document body
   * to prevent ugly text selection during drag. If this is causing problems
   * for your app, set this to `false`.
   */
  enableUserSelectHack: _propTypes2.default.bool,

  /**
   * `offsetParent`, if set, uses the passed DOM node to compute drag offsets
   * instead of using the parent node.
   */
  offsetParent: function offsetParent(props /*: DraggableCoreProps*/, propName /*: $Keys<DraggableCoreProps>*/) {
    if (process.browser === true && props[propName] && props[propName].nodeType !== 1) {
      throw new Error('Draggable\'s offsetParent must be a DOM Node.');
    }
  },

  /**
   * `grid` specifies the x and y that dragging should snap to.
   */
  grid: _propTypes2.default.arrayOf(_propTypes2.default.number),

  /**
   * `handle` specifies a selector to be used as the handle that initiates drag.
   *
   * Example:
   *
   * ```jsx
   *   let App = React.createClass({
   *       render: function () {
   *         return (
   *            <Draggable handle=".handle">
   *              <div>
   *                  <div className="handle">Click me to drag</div>
   *                  <div>This is some other content</div>
   *              </div>
   *           </Draggable>
   *         );
   *       }
   *   });
   * ```
   */
  handle: _propTypes2.default.string,

  /**
   * `cancel` specifies a selector to be used to prevent drag initialization.
   *
   * Example:
   *
   * ```jsx
   *   let App = React.createClass({
   *       render: function () {
   *           return(
   *               <Draggable cancel=".cancel">
   *                   <div>
   *                     <div className="cancel">You can't drag from here</div>
   *                     <div>Dragging here works fine</div>
   *                   </div>
   *               </Draggable>
   *           );
   *       }
   *   });
   * ```
   */
  cancel: _propTypes2.default.string,

  /**
   * Called when dragging starts.
   * If this function returns the boolean false, dragging will be canceled.
   */
  onStart: _propTypes2.default.func,

  /**
   * Called while dragging.
   * If this function returns the boolean false, dragging will be canceled.
   */
  onDrag: _propTypes2.default.func,

  /**
   * Called when dragging stops.
   * If this function returns the boolean false, the drag will remain active.
   */
  onStop: _propTypes2.default.func,

  /**
   * A workaround option which can be passed if onMouseDown needs to be accessed,
   * since it'll always be blocked (as there is internal use of onMouseDown)
   */
  onMouseDown: _propTypes2.default.func,

  /**
   * These properties should be defined on the child, not here.
   */
  className: _shims.dontSetMe,
  style: _shims.dontSetMe,
  transform: _shims.dontSetMe
};
DraggableCore.defaultProps = {
  allowAnyClick: false, // by default only accept left click
  cancel: null,
  disabled: false,
  enableUserSelectHack: true,
  offsetParent: null,
  handle: null,
  grid: null,
  transform: null,
  onStart: function onStart() {},
  onDrag: function onDrag() {},
  onStop: function onStop() {},
  onMouseDown: function onMouseDown() {}
};
exports.default = DraggableCore;
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(20)))

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = log;

/*eslint no-console:0*/
function log() {
  var _console;

  if (undefined) (_console = console).log.apply(_console, arguments);
}

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Draggable = __webpack_require__(13).default;

// Previous versions of this lib exported <Draggable> as the root export. As to not break
// them, or TypeScript, we export *both* as the root and as 'default'.
// See https://github.com/mzabriskie/react-draggable/pull/254
// and https://github.com/mzabriskie/react-draggable/issues/266
module.exports = Draggable;
module.exports.default = Draggable;
module.exports.DraggableCore = __webpack_require__(10).default;

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(6);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(7);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDom = __webpack_require__(4);

var _reactDom2 = _interopRequireDefault(_reactDom);

var _classnames = __webpack_require__(18);

var _classnames2 = _interopRequireDefault(_classnames);

var _domFns = __webpack_require__(5);

var _positionFns = __webpack_require__(9);

var _shims = __webpack_require__(0);

var _DraggableCore = __webpack_require__(10);

var _DraggableCore2 = _interopRequireDefault(_DraggableCore);

var _log = __webpack_require__(11);

var _log2 = _interopRequireDefault(_log);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/*:: import type {ControlPosition, DraggableBounds, DraggableCoreProps} from './DraggableCore';*/
/*:: import type {DraggableEventHandler} from './utils/types';*/
/*:: import type {Element as ReactElement} from 'react';*/
/*:: type DraggableState = {
  dragging: boolean,
  dragged: boolean,
  x: number, y: number,
  slackX: number, slackY: number,
  isElementSVG: boolean
};*/


//
// Define <Draggable>
//

/*:: export type DraggableProps = {
  ...$Exact<DraggableCoreProps>,
  axis: 'both' | 'x' | 'y' | 'none',
  bounds: DraggableBounds | string | false,
  defaultClassName: string,
  defaultClassNameDragging: string,
  defaultClassNameDragged: string,
  defaultPosition: ControlPosition,
  position: ControlPosition,
};*/

var Draggable = function (_React$Component) {
  _inherits(Draggable, _React$Component);

  function Draggable(props /*: DraggableProps*/) {
    _classCallCheck(this, Draggable);

    var _this = _possibleConstructorReturn(this, (Draggable.__proto__ || Object.getPrototypeOf(Draggable)).call(this, props));

    _this.onDragStart = function (e, coreData) {
      (0, _log2.default)('Draggable: onDragStart: %j', coreData);

      // Short-circuit if user's callback killed it.
      var shouldStart = _this.props.onStart(e, (0, _positionFns.createDraggableData)(_this, coreData));
      // Kills start event on core as well, so move handlers are never bound.
      if (shouldStart === false) return false;

      _this.setState({ dragging: true, dragged: true });
    };

    _this.onDrag = function (e, coreData) {
      if (!_this.state.dragging) return false;
      (0, _log2.default)('Draggable: onDrag: %j', coreData);

      var uiData = (0, _positionFns.createDraggableData)(_this, coreData);

      var newState /*: $Shape<DraggableState>*/ = {
        x: uiData.x,
        y: uiData.y
      };

      // Keep within bounds.
      if (_this.props.bounds) {
        // Save original x and y.
        var _x = newState.x,
            _y = newState.y;

        // Add slack to the values used to calculate bound position. This will ensure that if
        // we start removing slack, the element won't react to it right away until it's been
        // completely removed.

        newState.x += _this.state.slackX;
        newState.y += _this.state.slackY;

        // Get bound position. This will ceil/floor the x and y within the boundaries.

        var _getBoundPosition = (0, _positionFns.getBoundPosition)(_this, newState.x, newState.y),
            _getBoundPosition2 = _slicedToArray(_getBoundPosition, 2),
            newStateX = _getBoundPosition2[0],
            newStateY = _getBoundPosition2[1];

        newState.x = newStateX;
        newState.y = newStateY;

        // Recalculate slack by noting how much was shaved by the boundPosition handler.
        newState.slackX = _this.state.slackX + (_x - newState.x);
        newState.slackY = _this.state.slackY + (_y - newState.y);

        // Update the event we fire to reflect what really happened after bounds took effect.
        uiData.x = newState.x;
        uiData.y = newState.y;
        uiData.deltaX = newState.x - _this.state.x;
        uiData.deltaY = newState.y - _this.state.y;
      }

      // Short-circuit if user's callback killed it.
      var shouldUpdate = _this.props.onDrag(e, uiData);
      if (shouldUpdate === false) return false;

      _this.setState(newState);
    };

    _this.onDragStop = function (e, coreData) {
      if (!_this.state.dragging) return false;

      // Short-circuit if user's callback killed it.
      var shouldStop = _this.props.onStop(e, (0, _positionFns.createDraggableData)(_this, coreData));
      if (shouldStop === false) return false;

      (0, _log2.default)('Draggable: onDragStop: %j', coreData);

      var newState /*: $Shape<DraggableState>*/ = {
        dragging: false,
        slackX: 0,
        slackY: 0
      };

      // If this is a controlled component, the result of this operation will be to
      // revert back to the old position. We expect a handler on `onDragStop`, at the least.
      var controlled = Boolean(_this.props.position);
      if (controlled) {
        var _this$props$position = _this.props.position,
            _x2 = _this$props$position.x,
            _y2 = _this$props$position.y;

        newState.x = _x2;
        newState.y = _y2;
      }

      _this.setState(newState);
    };

    _this.state = {
      // Whether or not we are currently dragging.
      dragging: false,

      // Whether or not we have been dragged before.
      dragged: false,

      // Current transform x and y.
      x: props.position ? props.position.x : props.defaultPosition.x,
      y: props.position ? props.position.y : props.defaultPosition.y,

      // Used for compensating for out-of-bounds drags
      slackX: 0, slackY: 0,

      // Can only determine if SVG after mounting
      isElementSVG: false
    };
    return _this;
  }

  _createClass(Draggable, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      if (this.props.position && !(this.props.onDrag || this.props.onStop)) {
        // eslint-disable-next-line
        console.warn('A `position` was applied to this <Draggable>, without drag handlers. This will make this ' + 'component effectively undraggable. Please attach `onDrag` or `onStop` handlers so you can adjust the ' + '`position` of this element.');
      }
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      // Check to see if the element passed is an instanceof SVGElement
      if (typeof window.SVGElement !== 'undefined' && _reactDom2.default.findDOMNode(this) instanceof window.SVGElement) {
        this.setState({ isElementSVG: true });
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps /*: Object*/) {
      // Set x/y if position has changed
      if (nextProps.position && (!this.props.position || nextProps.position.x !== this.props.position.x || nextProps.position.y !== this.props.position.y)) {
        this.setState({ x: nextProps.position.x, y: nextProps.position.y });
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.setState({ dragging: false }); // prevents invariant if unmounted while dragging
    }
  }, {
    key: 'render',
    value: function render() /*: ReactElement<any>*/ {
      var _classNames;

      var style = {},
          svgTransform = null;

      // If this is controlled, we don't want to move it - unless it's dragging.
      var controlled = Boolean(this.props.position);
      var draggable = !controlled || this.state.dragging;

      var position = this.props.position || this.props.defaultPosition;
      var transformOpts = {
        // Set left if horizontal drag is enabled
        x: (0, _positionFns.canDragX)(this) && draggable ? this.state.x : position.x,

        // Set top if vertical drag is enabled
        y: (0, _positionFns.canDragY)(this) && draggable ? this.state.y : position.y
      };

      // If this element was SVG, we use the `transform` attribute.
      if (this.state.isElementSVG) {
        svgTransform = (0, _domFns.createSVGTransform)(transformOpts);
      } else {
        // Add a CSS transform to move the element around. This allows us to move the element around
        // without worrying about whether or not it is relatively or absolutely positioned.
        // If the item you are dragging already has a transform set, wrap it in a <span> so <Draggable>
        // has a clean slate.
        style = (0, _domFns.createCSSTransform)(transformOpts);
      }

      var _props = this.props,
          defaultClassName = _props.defaultClassName,
          defaultClassNameDragging = _props.defaultClassNameDragging,
          defaultClassNameDragged = _props.defaultClassNameDragged;


      var children = _react2.default.Children.only(this.props.children);

      // Mark with class while dragging
      var className = (0, _classnames2.default)(children.props.className || '', defaultClassName, (_classNames = {}, _defineProperty(_classNames, defaultClassNameDragging, this.state.dragging), _defineProperty(_classNames, defaultClassNameDragged, this.state.dragged), _classNames));

      // Reuse the child provided
      // This makes it flexible to use whatever element is wanted (div, ul, etc)
      return _react2.default.createElement(
        _DraggableCore2.default,
        _extends({}, this.props, { onStart: this.onDragStart, onDrag: this.onDrag, onStop: this.onDragStop }),
        _react2.default.cloneElement(children, {
          className: className,
          style: _extends({}, children.props.style, style),
          transform: svgTransform
        })
      );
    }
  }]);

  return Draggable;
}(_react2.default.Component);

Draggable.displayName = 'Draggable';
Draggable.propTypes = _extends({}, _DraggableCore2.default.propTypes, {

  /**
   * `axis` determines which axis the draggable can move.
   *
   *  Note that all callbacks will still return data as normal. This only
   *  controls flushing to the DOM.
   *
   * 'both' allows movement horizontally and vertically.
   * 'x' limits movement to horizontal axis.
   * 'y' limits movement to vertical axis.
   * 'none' limits all movement.
   *
   * Defaults to 'both'.
   */
  axis: _propTypes2.default.oneOf(['both', 'x', 'y', 'none']),

  /**
   * `bounds` determines the range of movement available to the element.
   * Available values are:
   *
   * 'parent' restricts movement within the Draggable's parent node.
   *
   * Alternatively, pass an object with the following properties, all of which are optional:
   *
   * {left: LEFT_BOUND, right: RIGHT_BOUND, bottom: BOTTOM_BOUND, top: TOP_BOUND}
   *
   * All values are in px.
   *
   * Example:
   *
   * ```jsx
   *   let App = React.createClass({
   *       render: function () {
   *         return (
   *            <Draggable bounds={{right: 300, bottom: 300}}>
   *              <div>Content</div>
   *           </Draggable>
   *         );
   *       }
   *   });
   * ```
   */
  bounds: _propTypes2.default.oneOfType([_propTypes2.default.shape({
    left: _propTypes2.default.number,
    right: _propTypes2.default.number,
    top: _propTypes2.default.number,
    bottom: _propTypes2.default.number
  }), _propTypes2.default.string, _propTypes2.default.oneOf([false])]),

  defaultClassName: _propTypes2.default.string,
  defaultClassNameDragging: _propTypes2.default.string,
  defaultClassNameDragged: _propTypes2.default.string,

  /**
   * `defaultPosition` specifies the x and y that the dragged item should start at
   *
   * Example:
   *
   * ```jsx
   *      let App = React.createClass({
   *          render: function () {
   *              return (
   *                  <Draggable defaultPosition={{x: 25, y: 25}}>
   *                      <div>I start with transformX: 25px and transformY: 25px;</div>
   *                  </Draggable>
   *              );
   *          }
   *      });
   * ```
   */
  defaultPosition: _propTypes2.default.shape({
    x: _propTypes2.default.number,
    y: _propTypes2.default.number
  }),

  /**
   * `position`, if present, defines the current position of the element.
   *
   *  This is similar to how form elements in React work - if no `position` is supplied, the component
   *  is uncontrolled.
   *
   * Example:
   *
   * ```jsx
   *      let App = React.createClass({
   *          render: function () {
   *              return (
   *                  <Draggable position={{x: 25, y: 25}}>
   *                      <div>I start with transformX: 25px and transformY: 25px;</div>
   *                  </Draggable>
   *              );
   *          }
   *      });
   * ```
   */
  position: _propTypes2.default.shape({
    x: _propTypes2.default.number,
    y: _propTypes2.default.number
  }),

  /**
   * These properties should be defined on the child, not here.
   */
  className: _shims.dontSetMe,
  style: _shims.dontSetMe,
  transform: _shims.dontSetMe
});
Draggable.defaultProps = _extends({}, _DraggableCore2.default.defaultProps, {
  axis: 'both',
  bounds: false,
  defaultClassName: 'react-draggable',
  defaultClassNameDragging: 'react-draggable-dragging',
  defaultClassNameDragged: 'react-draggable-dragged',
  defaultPosition: { x: 0, y: 0 },
  position: null
});
exports.default = Draggable;

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var emptyFunction = __webpack_require__(1);
var invariant = __webpack_require__(2);
var warning = __webpack_require__(8);
var assign = __webpack_require__(15);

var ReactPropTypesSecret = __webpack_require__(3);
var checkPropTypes = __webpack_require__(16);

module.exports = function(isValidElement, throwOnDirectAccess) {
  /* global Symbol */
  var ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
  var FAUX_ITERATOR_SYMBOL = '@@iterator'; // Before Symbol spec.

  /**
   * Returns the iterator method function contained on the iterable object.
   *
   * Be sure to invoke the function with the iterable as context:
   *
   *     var iteratorFn = getIteratorFn(myIterable);
   *     if (iteratorFn) {
   *       var iterator = iteratorFn.call(myIterable);
   *       ...
   *     }
   *
   * @param {?object} maybeIterable
   * @return {?function}
   */
  function getIteratorFn(maybeIterable) {
    var iteratorFn = maybeIterable && (ITERATOR_SYMBOL && maybeIterable[ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL]);
    if (typeof iteratorFn === 'function') {
      return iteratorFn;
    }
  }

  /**
   * Collection of methods that allow declaration and validation of props that are
   * supplied to React components. Example usage:
   *
   *   var Props = require('ReactPropTypes');
   *   var MyArticle = React.createClass({
   *     propTypes: {
   *       // An optional string prop named "description".
   *       description: Props.string,
   *
   *       // A required enum prop named "category".
   *       category: Props.oneOf(['News','Photos']).isRequired,
   *
   *       // A prop named "dialog" that requires an instance of Dialog.
   *       dialog: Props.instanceOf(Dialog).isRequired
   *     },
   *     render: function() { ... }
   *   });
   *
   * A more formal specification of how these methods are used:
   *
   *   type := array|bool|func|object|number|string|oneOf([...])|instanceOf(...)
   *   decl := ReactPropTypes.{type}(.isRequired)?
   *
   * Each and every declaration produces a function with the same signature. This
   * allows the creation of custom validation functions. For example:
   *
   *  var MyLink = React.createClass({
   *    propTypes: {
   *      // An optional string or URI prop named "href".
   *      href: function(props, propName, componentName) {
   *        var propValue = props[propName];
   *        if (propValue != null && typeof propValue !== 'string' &&
   *            !(propValue instanceof URI)) {
   *          return new Error(
   *            'Expected a string or an URI for ' + propName + ' in ' +
   *            componentName
   *          );
   *        }
   *      }
   *    },
   *    render: function() {...}
   *  });
   *
   * @internal
   */

  var ANONYMOUS = '<<anonymous>>';

  // Important!
  // Keep this list in sync with production version in `./factoryWithThrowingShims.js`.
  var ReactPropTypes = {
    array: createPrimitiveTypeChecker('array'),
    bool: createPrimitiveTypeChecker('boolean'),
    func: createPrimitiveTypeChecker('function'),
    number: createPrimitiveTypeChecker('number'),
    object: createPrimitiveTypeChecker('object'),
    string: createPrimitiveTypeChecker('string'),
    symbol: createPrimitiveTypeChecker('symbol'),

    any: createAnyTypeChecker(),
    arrayOf: createArrayOfTypeChecker,
    element: createElementTypeChecker(),
    instanceOf: createInstanceTypeChecker,
    node: createNodeChecker(),
    objectOf: createObjectOfTypeChecker,
    oneOf: createEnumTypeChecker,
    oneOfType: createUnionTypeChecker,
    shape: createShapeTypeChecker,
    exact: createStrictShapeTypeChecker,
  };

  /**
   * inlined Object.is polyfill to avoid requiring consumers ship their own
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
   */
  /*eslint-disable no-self-compare*/
  function is(x, y) {
    // SameValue algorithm
    if (x === y) {
      // Steps 1-5, 7-10
      // Steps 6.b-6.e: +0 != -0
      return x !== 0 || 1 / x === 1 / y;
    } else {
      // Step 6.a: NaN == NaN
      return x !== x && y !== y;
    }
  }
  /*eslint-enable no-self-compare*/

  /**
   * We use an Error-like object for backward compatibility as people may call
   * PropTypes directly and inspect their output. However, we don't use real
   * Errors anymore. We don't inspect their stack anyway, and creating them
   * is prohibitively expensive if they are created too often, such as what
   * happens in oneOfType() for any type before the one that matched.
   */
  function PropTypeError(message) {
    this.message = message;
    this.stack = '';
  }
  // Make `instanceof Error` still work for returned errors.
  PropTypeError.prototype = Error.prototype;

  function createChainableTypeChecker(validate) {
    if (Object({"DRAGGABLE_DEBUG":undefined}).NODE_ENV !== 'production') {
      var manualPropTypeCallCache = {};
      var manualPropTypeWarningCount = 0;
    }
    function checkType(isRequired, props, propName, componentName, location, propFullName, secret) {
      componentName = componentName || ANONYMOUS;
      propFullName = propFullName || propName;

      if (secret !== ReactPropTypesSecret) {
        if (throwOnDirectAccess) {
          // New behavior only for users of `prop-types` package
          invariant(
            false,
            'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
            'Use `PropTypes.checkPropTypes()` to call them. ' +
            'Read more at http://fb.me/use-check-prop-types'
          );
        } else if (Object({"DRAGGABLE_DEBUG":undefined}).NODE_ENV !== 'production' && typeof console !== 'undefined') {
          // Old behavior for people using React.PropTypes
          var cacheKey = componentName + ':' + propName;
          if (
            !manualPropTypeCallCache[cacheKey] &&
            // Avoid spamming the console because they are often not actionable except for lib authors
            manualPropTypeWarningCount < 3
          ) {
            warning(
              false,
              'You are manually calling a React.PropTypes validation ' +
              'function for the `%s` prop on `%s`. This is deprecated ' +
              'and will throw in the standalone `prop-types` package. ' +
              'You may be seeing this warning due to a third-party PropTypes ' +
              'library. See https://fb.me/react-warning-dont-call-proptypes ' + 'for details.',
              propFullName,
              componentName
            );
            manualPropTypeCallCache[cacheKey] = true;
            manualPropTypeWarningCount++;
          }
        }
      }
      if (props[propName] == null) {
        if (isRequired) {
          if (props[propName] === null) {
            return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required ' + ('in `' + componentName + '`, but its value is `null`.'));
          }
          return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required in ' + ('`' + componentName + '`, but its value is `undefined`.'));
        }
        return null;
      } else {
        return validate(props, propName, componentName, location, propFullName);
      }
    }

    var chainedCheckType = checkType.bind(null, false);
    chainedCheckType.isRequired = checkType.bind(null, true);

    return chainedCheckType;
  }

  function createPrimitiveTypeChecker(expectedType) {
    function validate(props, propName, componentName, location, propFullName, secret) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== expectedType) {
        // `propValue` being instance of, say, date/regexp, pass the 'object'
        // check, but we can offer a more precise error message here rather than
        // 'of type `object`'.
        var preciseType = getPreciseType(propValue);

        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + preciseType + '` supplied to `' + componentName + '`, expected ') + ('`' + expectedType + '`.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createAnyTypeChecker() {
    return createChainableTypeChecker(emptyFunction.thatReturnsNull);
  }

  function createArrayOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside arrayOf.');
      }
      var propValue = props[propName];
      if (!Array.isArray(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an array.'));
      }
      for (var i = 0; i < propValue.length; i++) {
        var error = typeChecker(propValue, i, componentName, location, propFullName + '[' + i + ']', ReactPropTypesSecret);
        if (error instanceof Error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createElementTypeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      if (!isValidElement(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createInstanceTypeChecker(expectedClass) {
    function validate(props, propName, componentName, location, propFullName) {
      if (!(props[propName] instanceof expectedClass)) {
        var expectedClassName = expectedClass.name || ANONYMOUS;
        var actualClassName = getClassName(props[propName]);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + actualClassName + '` supplied to `' + componentName + '`, expected ') + ('instance of `' + expectedClassName + '`.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createEnumTypeChecker(expectedValues) {
    if (!Array.isArray(expectedValues)) {
      Object({"DRAGGABLE_DEBUG":undefined}).NODE_ENV !== 'production' ? warning(false, 'Invalid argument supplied to oneOf, expected an instance of array.') : void 0;
      return emptyFunction.thatReturnsNull;
    }

    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      for (var i = 0; i < expectedValues.length; i++) {
        if (is(propValue, expectedValues[i])) {
          return null;
        }
      }

      var valuesString = JSON.stringify(expectedValues);
      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of value `' + propValue + '` ' + ('supplied to `' + componentName + '`, expected one of ' + valuesString + '.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createObjectOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside objectOf.');
      }
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an object.'));
      }
      for (var key in propValue) {
        if (propValue.hasOwnProperty(key)) {
          var error = typeChecker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
          if (error instanceof Error) {
            return error;
          }
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createUnionTypeChecker(arrayOfTypeCheckers) {
    if (!Array.isArray(arrayOfTypeCheckers)) {
      Object({"DRAGGABLE_DEBUG":undefined}).NODE_ENV !== 'production' ? warning(false, 'Invalid argument supplied to oneOfType, expected an instance of array.') : void 0;
      return emptyFunction.thatReturnsNull;
    }

    for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
      var checker = arrayOfTypeCheckers[i];
      if (typeof checker !== 'function') {
        warning(
          false,
          'Invalid argument supplied to oneOfType. Expected an array of check functions, but ' +
          'received %s at index %s.',
          getPostfixForTypeWarning(checker),
          i
        );
        return emptyFunction.thatReturnsNull;
      }
    }

    function validate(props, propName, componentName, location, propFullName) {
      for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
        var checker = arrayOfTypeCheckers[i];
        if (checker(props, propName, componentName, location, propFullName, ReactPropTypesSecret) == null) {
          return null;
        }
      }

      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createNodeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      if (!isNode(props[propName])) {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`, expected a ReactNode.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      for (var key in shapeTypes) {
        var checker = shapeTypes[key];
        if (!checker) {
          continue;
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
        if (error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createStrictShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      // We need to check all keys in case some are required but missing from
      // props.
      var allKeys = assign({}, props[propName], shapeTypes);
      for (var key in allKeys) {
        var checker = shapeTypes[key];
        if (!checker) {
          return new PropTypeError(
            'Invalid ' + location + ' `' + propFullName + '` key `' + key + '` supplied to `' + componentName + '`.' +
            '\nBad object: ' + JSON.stringify(props[propName], null, '  ') +
            '\nValid keys: ' +  JSON.stringify(Object.keys(shapeTypes), null, '  ')
          );
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
        if (error) {
          return error;
        }
      }
      return null;
    }

    return createChainableTypeChecker(validate);
  }

  function isNode(propValue) {
    switch (typeof propValue) {
      case 'number':
      case 'string':
      case 'undefined':
        return true;
      case 'boolean':
        return !propValue;
      case 'object':
        if (Array.isArray(propValue)) {
          return propValue.every(isNode);
        }
        if (propValue === null || isValidElement(propValue)) {
          return true;
        }

        var iteratorFn = getIteratorFn(propValue);
        if (iteratorFn) {
          var iterator = iteratorFn.call(propValue);
          var step;
          if (iteratorFn !== propValue.entries) {
            while (!(step = iterator.next()).done) {
              if (!isNode(step.value)) {
                return false;
              }
            }
          } else {
            // Iterator will provide entry [k,v] tuples rather than values.
            while (!(step = iterator.next()).done) {
              var entry = step.value;
              if (entry) {
                if (!isNode(entry[1])) {
                  return false;
                }
              }
            }
          }
        } else {
          return false;
        }

        return true;
      default:
        return false;
    }
  }

  function isSymbol(propType, propValue) {
    // Native Symbol.
    if (propType === 'symbol') {
      return true;
    }

    // 19.4.3.5 Symbol.prototype[@@toStringTag] === 'Symbol'
    if (propValue['@@toStringTag'] === 'Symbol') {
      return true;
    }

    // Fallback for non-spec compliant Symbols which are polyfilled.
    if (typeof Symbol === 'function' && propValue instanceof Symbol) {
      return true;
    }

    return false;
  }

  // Equivalent of `typeof` but with special handling for array and regexp.
  function getPropType(propValue) {
    var propType = typeof propValue;
    if (Array.isArray(propValue)) {
      return 'array';
    }
    if (propValue instanceof RegExp) {
      // Old webkits (at least until Android 4.0) return 'function' rather than
      // 'object' for typeof a RegExp. We'll normalize this here so that /bla/
      // passes PropTypes.object.
      return 'object';
    }
    if (isSymbol(propType, propValue)) {
      return 'symbol';
    }
    return propType;
  }

  // This handles more types than `getPropType`. Only used for error messages.
  // See `createPrimitiveTypeChecker`.
  function getPreciseType(propValue) {
    if (typeof propValue === 'undefined' || propValue === null) {
      return '' + propValue;
    }
    var propType = getPropType(propValue);
    if (propType === 'object') {
      if (propValue instanceof Date) {
        return 'date';
      } else if (propValue instanceof RegExp) {
        return 'regexp';
      }
    }
    return propType;
  }

  // Returns a string that is postfixed to a warning about an invalid type.
  // For example, "undefined" or "of type array"
  function getPostfixForTypeWarning(value) {
    var type = getPreciseType(value);
    switch (type) {
      case 'array':
      case 'object':
        return 'an ' + type;
      case 'boolean':
      case 'date':
      case 'regexp':
        return 'a ' + type;
      default:
        return type;
    }
  }

  // Returns class name of the object, if any.
  function getClassName(propValue) {
    if (!propValue.constructor || !propValue.constructor.name) {
      return ANONYMOUS;
    }
    return propValue.constructor.name;
  }

  ReactPropTypes.checkPropTypes = checkPropTypes;
  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*
object-assign
(c) Sindre Sorhus
@license MIT
*/


/* eslint-disable no-unused-vars */
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var propIsEnumerable = Object.prototype.propertyIsEnumerable;

function toObject(val) {
	if (val === null || val === undefined) {
		throw new TypeError('Object.assign cannot be called with null or undefined');
	}

	return Object(val);
}

function shouldUseNative() {
	try {
		if (!Object.assign) {
			return false;
		}

		// Detect buggy property enumeration order in older V8 versions.

		// https://bugs.chromium.org/p/v8/issues/detail?id=4118
		var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
		test1[5] = 'de';
		if (Object.getOwnPropertyNames(test1)[0] === '5') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test2 = {};
		for (var i = 0; i < 10; i++) {
			test2['_' + String.fromCharCode(i)] = i;
		}
		var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
			return test2[n];
		});
		if (order2.join('') !== '0123456789') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test3 = {};
		'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
			test3[letter] = letter;
		});
		if (Object.keys(Object.assign({}, test3)).join('') !==
				'abcdefghijklmnopqrst') {
			return false;
		}

		return true;
	} catch (err) {
		// We don't expect any of the above to throw, but better to be safe.
		return false;
	}
}

module.exports = shouldUseNative() ? Object.assign : function (target, source) {
	var from;
	var to = toObject(target);
	var symbols;

	for (var s = 1; s < arguments.length; s++) {
		from = Object(arguments[s]);

		for (var key in from) {
			if (hasOwnProperty.call(from, key)) {
				to[key] = from[key];
			}
		}

		if (getOwnPropertySymbols) {
			symbols = getOwnPropertySymbols(from);
			for (var i = 0; i < symbols.length; i++) {
				if (propIsEnumerable.call(from, symbols[i])) {
					to[symbols[i]] = from[symbols[i]];
				}
			}
		}
	}

	return to;
};


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



if (Object({"DRAGGABLE_DEBUG":undefined}).NODE_ENV !== 'production') {
  var invariant = __webpack_require__(2);
  var warning = __webpack_require__(8);
  var ReactPropTypesSecret = __webpack_require__(3);
  var loggedTypeFailures = {};
}

/**
 * Assert that the values match with the type specs.
 * Error messages are memorized and will only be shown once.
 *
 * @param {object} typeSpecs Map of name to a ReactPropType
 * @param {object} values Runtime values that need to be type-checked
 * @param {string} location e.g. "prop", "context", "child context"
 * @param {string} componentName Name of the component for error messages.
 * @param {?Function} getStack Returns the component stack.
 * @private
 */
function checkPropTypes(typeSpecs, values, location, componentName, getStack) {
  if (Object({"DRAGGABLE_DEBUG":undefined}).NODE_ENV !== 'production') {
    for (var typeSpecName in typeSpecs) {
      if (typeSpecs.hasOwnProperty(typeSpecName)) {
        var error;
        // Prop type validation may throw. In case they do, we don't want to
        // fail the render phase where it didn't fail before. So we log it.
        // After these have been cleaned up, we'll let them throw.
        try {
          // This is intentionally an invariant that gets caught. It's the same
          // behavior as without this statement except with a better message.
          invariant(typeof typeSpecs[typeSpecName] === 'function', '%s: %s type `%s` is invalid; it must be a function, usually from ' + 'the `prop-types` package, but received `%s`.', componentName || 'React class', location, typeSpecName, typeof typeSpecs[typeSpecName]);
          error = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, ReactPropTypesSecret);
        } catch (ex) {
          error = ex;
        }
        warning(!error || error instanceof Error, '%s: type specification of %s `%s` is invalid; the type checker ' + 'function must return `null` or an `Error` but returned a %s. ' + 'You may have forgotten to pass an argument to the type checker ' + 'creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and ' + 'shape all require an argument).', componentName || 'React class', location, typeSpecName, typeof error);
        if (error instanceof Error && !(error.message in loggedTypeFailures)) {
          // Only monitor this failure once because there tends to be a lot of the
          // same error.
          loggedTypeFailures[error.message] = true;

          var stack = getStack ? getStack() : '';

          warning(false, 'Failed %s type: %s%s', location, error.message, stack != null ? stack : '');
        }
      }
    }
  }
}

module.exports = checkPropTypes;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var emptyFunction = __webpack_require__(1);
var invariant = __webpack_require__(2);
var ReactPropTypesSecret = __webpack_require__(3);

module.exports = function() {
  function shim(props, propName, componentName, location, propFullName, secret) {
    if (secret === ReactPropTypesSecret) {
      // It is still safe when called from React.
      return;
    }
    invariant(
      false,
      'Calling PropTypes validators directly is not supported by the `prop-types` package. ' +
      'Use PropTypes.checkPropTypes() to call them. ' +
      'Read more at http://fb.me/use-check-prop-types'
    );
  };
  shim.isRequired = shim;
  function getShim() {
    return shim;
  };
  // Important!
  // Keep this list in sync with production version in `./factoryWithTypeCheckers.js`.
  var ReactPropTypes = {
    array: shim,
    bool: shim,
    func: shim,
    number: shim,
    object: shim,
    string: shim,
    symbol: shim,

    any: shim,
    arrayOf: getShim,
    element: shim,
    instanceOf: getShim,
    node: shim,
    objectOf: getShim,
    oneOf: getShim,
    oneOfType: getShim,
    shape: getShim,
    exact: getShim
  };

  ReactPropTypes.checkPropTypes = emptyFunction;
  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
  Copyright (c) 2016 Jed Watson.
  Licensed under the MIT License (MIT), see
  http://jedwatson.github.io/classnames
*/
/* global define */

(function () {
	'use strict';

	var hasOwn = {}.hasOwnProperty;

	function classNames () {
		var classes = [];

		for (var i = 0; i < arguments.length; i++) {
			var arg = arguments[i];
			if (!arg) continue;

			var argType = typeof arg;

			if (argType === 'string' || argType === 'number') {
				classes.push(arg);
			} else if (Array.isArray(arg)) {
				classes.push(classNames.apply(null, arg));
			} else if (argType === 'object') {
				for (var key in arg) {
					if (hasOwn.call(arg, key) && arg[key]) {
						classes.push(key);
					}
				}
			}
		}

		return classes.join(' ');
	}

	if (typeof module !== 'undefined' && module.exports) {
		module.exports = classNames;
	} else if (true) {
		// register as 'classnames', consistent with npm package name
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function () {
			return classNames;
		}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {
		window.classNames = classNames;
	}
}());


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPrefix = getPrefix;
exports.browserPrefixToKey = browserPrefixToKey;
exports.browserPrefixToStyle = browserPrefixToStyle;
var prefixes = ['Moz', 'Webkit', 'O', 'ms'];
function getPrefix() /*: string*/ {
  var prop /*: string*/ = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'transform';

  // Checking specifically for 'window.document' is for pseudo-browser server-side
  // environments that define 'window' as the global context.
  // E.g. React-rails (see https://github.com/reactjs/react-rails/pull/84)
  if (typeof window === 'undefined' || typeof window.document === 'undefined') return '';

  var style = window.document.documentElement.style;

  if (prop in style) return '';

  for (var i = 0; i < prefixes.length; i++) {
    if (browserPrefixToKey(prop, prefixes[i]) in style) return prefixes[i];
  }

  return '';
}

function browserPrefixToKey(prop /*: string*/, prefix /*: string*/) /*: string*/ {
  return prefix ? '' + prefix + kebabToTitleCase(prop) : prop;
}

function browserPrefixToStyle(prop /*: string*/, prefix /*: string*/) /*: string*/ {
  return prefix ? '-' + prefix.toLowerCase() + '-' + prop : prop;
}

function kebabToTitleCase(str /*: string*/) /*: string*/ {
  var out = '';
  var shouldCapitalize = true;
  for (var i = 0; i < str.length; i++) {
    if (shouldCapitalize) {
      out += str[i].toUpperCase();
      shouldCapitalize = false;
    } else if (str[i] === '-') {
      shouldCapitalize = true;
    } else {
      out += str[i];
    }
  }
  return out;
}

// Default export is the prefix itself, like 'Moz', 'Webkit', etc
// Note that you may have to re-test for certain things; for instance, Chrome 50
// can handle unprefixed `transform`, but not unprefixed `user-select`
exports.default = getPrefix();

/***/ }),
/* 20 */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ })
/******/ ]);
});
//# sourceMappingURL=react-draggable.js.map

/***/ }),
/* 123 */
/***/ (function(module, exports) {

module.exports = require("react-dom");

/***/ }),
/* 124 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(125);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(2)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./FunctionKeyboard.scss", function() {
		var newContent = require("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./FunctionKeyboard.scss");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__(16);
exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "._2GmTpNLXHa5I {\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n  position: absolute;\n  top: 0;\n  left: 0;\n  background: #2e434e;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  height: 44px;\n  border-radius: 6px;\n  padding-left: 3px;\n  padding-right: 3px; }\n\n._216vB8qv-fD7 {\n  content: url(" + escape(__webpack_require__(126)) + ");\n  padding: 0 6px;\n  width: 13px; }\n  ._216vB8qv-fD7:hover {\n    cursor: pointer; }\n\n._2JYJRo_6AfNf {\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n  margin-right: 5px; }\n\n._1fhmu_g582eR {\n  position: relative;\n  background: #314854;\n  border: 2px solid #1b2c35;\n  font-size: 13px;\n  min-width: 34px;\n  text-align: center;\n  height: 30px;\n  border-radius: 4px;\n  padding: 0 10px;\n  margin-right: 2px;\n  line-height: 31px;\n  color: white; }\n  ._1fhmu_g582eR:hover {\n    cursor: pointer;\n    background: #445e6b;\n    border-color: #1b2c35; }\n  ._1fhmu_g582eR:last-child {\n    margin-right: 0; }\n  ._3AmoTNocqLvE {\n    position: relative;\n    background: #314854;\n    border: 2px solid #1b2c35;\n    font-size: 13px;\n    min-width: 34px;\n    text-align: center;\n    height: 30px;\n    border-radius: 4px;\n    padding: 0 10px;\n    margin-right: 2px;\n    line-height: 31px;\n    color: white;\n    background: #121e25;\n    border-color: #121e25; }\n    ._3AmoTNocqLvE:hover {\n      cursor: pointer;\n      background: #445e6b;\n      border-color: #1b2c35; }\n    ._3AmoTNocqLvE:last-child {\n      margin-right: 0; }\n\n._1Zt15NlUg5A5 {\n  display: -webkit-inline-box;\n  display: -ms-inline-flexbox;\n  display: inline-flex; }\n\n._2T7tW0yZHRoP {\n  position: relative;\n  background: #314854;\n  border: 2px solid #1b2c35;\n  font-size: 13px;\n  min-width: 34px;\n  text-align: center;\n  height: 30px;\n  border-radius: 4px;\n  padding: 0 10px;\n  margin-right: 2px;\n  line-height: 31px;\n  color: white;\n  background: #273c47;\n  border-color: #121e25;\n  padding: 0 16px 0 6px; }\n  ._2T7tW0yZHRoP:hover {\n    cursor: pointer;\n    background: #445e6b;\n    border-color: #1b2c35; }\n  ._2T7tW0yZHRoP:last-child {\n    margin-right: 0; }\n  ._2T7tW0yZHRoP:hover {\n    border-color: #121e25; }\n  ._mPJjvQUubTVA {\n    position: relative;\n    background: #314854;\n    border: 2px solid #1b2c35;\n    font-size: 13px;\n    min-width: 34px;\n    text-align: center;\n    height: 30px;\n    border-radius: 4px;\n    padding: 0 10px;\n    margin-right: 2px;\n    line-height: 31px;\n    color: white;\n    background: #273c47;\n    border-color: #121e25;\n    padding: 0 16px 0 6px;\n    background: #121e25;\n    border-color: #121e25; }\n    ._mPJjvQUubTVA:hover {\n      cursor: pointer;\n      background: #445e6b;\n      border-color: #1b2c35; }\n    ._mPJjvQUubTVA:last-child {\n      margin-right: 0; }\n    ._mPJjvQUubTVA:hover {\n      border-color: #121e25; }\n\n._-gHJW2BHQcT4 {\n  position: absolute;\n  top: 11px;\n  right: 7px;\n  font-size: 16px;\n  color: #071016;\n  font-weight: bold;\n  line-height: 10px; }\n  ._2k8Z4pG7hHmE {\n    position: absolute;\n    top: 11px;\n    right: 7px;\n    font-size: 16px;\n    color: #071016;\n    font-weight: bold;\n    line-height: 10px;\n    color: #2185d9; }\n", ""]);

// exports
exports.locals = {
	"function-keyboard": "_2GmTpNLXHa5I",
	"handle": "_216vB8qv-fD7",
	"function-keyboard__item-wrap": "_2JYJRo_6AfNf",
	"function-keyboard__item": "_1fhmu_g582eR",
	"function-keyboard__item--active": "_3AmoTNocqLvE",
	"function-keyboard__item--plus-wrap": "_1Zt15NlUg5A5",
	"function-keyboard__item--plus": "_2T7tW0yZHRoP",
	"function-keyboard__item--plus--active": "_mPJjvQUubTVA",
	"plus": "_-gHJW2BHQcT4",
	"plus--active": "_2k8Z4pG7hHmE"
};

/***/ }),
/* 126 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCAxMzAgMjI4IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAxMzAgMjI4OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7ZmlsbDojMUIyMTI0O30NCgkuc3Qxe2ZpbGw6IzU1NUI1RTt9DQo8L3N0eWxlPg0KPHJlY3QgY2xhc3M9InN0MCIgd2lkdGg9IjMyIiBoZWlnaHQ9IjMyIi8+DQo8cmVjdCBjbGFzcz0ic3QxIiB3aWR0aD0iMjIiIGhlaWdodD0iMjIiLz4NCjxyZWN0IHg9IjQ5IiBjbGFzcz0ic3QwIiB3aWR0aD0iMzIiIGhlaWdodD0iMzIiLz4NCjxyZWN0IHg9IjQ5IiBjbGFzcz0ic3QxIiB3aWR0aD0iMjIiIGhlaWdodD0iMjIiLz4NCjxyZWN0IHg9Ijk4IiBjbGFzcz0ic3QwIiB3aWR0aD0iMzIiIGhlaWdodD0iMzIiLz4NCjxyZWN0IHg9Ijk4IiBjbGFzcz0ic3QxIiB3aWR0aD0iMjIiIGhlaWdodD0iMjIiLz4NCjxyZWN0IHk9IjQ5IiBjbGFzcz0ic3QwIiB3aWR0aD0iMzIiIGhlaWdodD0iMzIiLz4NCjxyZWN0IHk9IjQ5IiBjbGFzcz0ic3QxIiB3aWR0aD0iMjIiIGhlaWdodD0iMjIiLz4NCjxyZWN0IHg9IjQ5IiB5PSI0OSIgY2xhc3M9InN0MCIgd2lkdGg9IjMyIiBoZWlnaHQ9IjMyIi8+DQo8cmVjdCB4PSI0OSIgeT0iNDkiIGNsYXNzPSJzdDEiIHdpZHRoPSIyMiIgaGVpZ2h0PSIyMiIvPg0KPHJlY3QgeD0iOTgiIHk9IjQ5IiBjbGFzcz0ic3QwIiB3aWR0aD0iMzIiIGhlaWdodD0iMzIiLz4NCjxyZWN0IHg9Ijk4IiB5PSI0OSIgY2xhc3M9InN0MSIgd2lkdGg9IjIyIiBoZWlnaHQ9IjIyIi8+DQo8cmVjdCB5PSI5OCIgY2xhc3M9InN0MCIgd2lkdGg9IjMyIiBoZWlnaHQ9IjMyIi8+DQo8cmVjdCB5PSI5OCIgY2xhc3M9InN0MSIgd2lkdGg9IjIyIiBoZWlnaHQ9IjIyIi8+DQo8cmVjdCB4PSI0OSIgeT0iOTgiIGNsYXNzPSJzdDAiIHdpZHRoPSIzMiIgaGVpZ2h0PSIzMiIvPg0KPHJlY3QgeD0iNDkiIHk9Ijk4IiBjbGFzcz0ic3QxIiB3aWR0aD0iMjIiIGhlaWdodD0iMjIiLz4NCjxyZWN0IHg9Ijk4IiB5PSI5OCIgY2xhc3M9InN0MCIgd2lkdGg9IjMyIiBoZWlnaHQ9IjMyIi8+DQo8cmVjdCB4PSI5OCIgeT0iOTgiIGNsYXNzPSJzdDEiIHdpZHRoPSIyMiIgaGVpZ2h0PSIyMiIvPg0KPHJlY3QgeT0iMTQ3IiBjbGFzcz0ic3QwIiB3aWR0aD0iMzIiIGhlaWdodD0iMzIiLz4NCjxyZWN0IHk9IjE0NyIgY2xhc3M9InN0MSIgd2lkdGg9IjIyIiBoZWlnaHQ9IjIyIi8+DQo8cmVjdCB4PSI0OSIgeT0iMTQ3IiBjbGFzcz0ic3QwIiB3aWR0aD0iMzIiIGhlaWdodD0iMzIiLz4NCjxyZWN0IHg9IjQ5IiB5PSIxNDciIGNsYXNzPSJzdDEiIHdpZHRoPSIyMiIgaGVpZ2h0PSIyMiIvPg0KPHJlY3QgeD0iOTgiIHk9IjE0NyIgY2xhc3M9InN0MCIgd2lkdGg9IjMyIiBoZWlnaHQ9IjMyIi8+DQo8cmVjdCB4PSI5OCIgeT0iMTQ3IiBjbGFzcz0ic3QxIiB3aWR0aD0iMjIiIGhlaWdodD0iMjIiLz4NCjxyZWN0IHk9IjE5NiIgY2xhc3M9InN0MCIgd2lkdGg9IjMyIiBoZWlnaHQ9IjMyIi8+DQo8cmVjdCB5PSIxOTYiIGNsYXNzPSJzdDEiIHdpZHRoPSIyMiIgaGVpZ2h0PSIyMiIvPg0KPHJlY3QgeD0iNDkiIHk9IjE5NiIgY2xhc3M9InN0MCIgd2lkdGg9IjMyIiBoZWlnaHQ9IjMyIi8+DQo8cmVjdCB4PSI0OSIgeT0iMTk2IiBjbGFzcz0ic3QxIiB3aWR0aD0iMjIiIGhlaWdodD0iMjIiLz4NCjxyZWN0IHg9Ijk4IiB5PSIxOTYiIGNsYXNzPSJzdDAiIHdpZHRoPSIzMiIgaGVpZ2h0PSIzMiIvPg0KPHJlY3QgeD0iOTgiIHk9IjE5NiIgY2xhc3M9InN0MSIgd2lkdGg9IjIyIiBoZWlnaHQ9IjIyIi8+DQo8L3N2Zz4NCg=="

/***/ }),
/* 127 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _HostMessageOverlay = __webpack_require__(128);

var _HostMessageOverlay2 = _interopRequireDefault(_HostMessageOverlay);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _HostMessageOverlay2.default;

/***/ }),
/* 128 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _status = __webpack_require__(4);

var STATUS = _interopRequireWildcard(_status);

var _HostMessageOverlay = __webpack_require__(129);

var _HostMessageOverlay2 = _interopRequireDefault(_HostMessageOverlay);

var _MessageOverlay = __webpack_require__(132);

var _MessageOverlay2 = _interopRequireDefault(_MessageOverlay);

var _LoadingSpinner = __webpack_require__(136);

var _LoadingSpinner2 = _interopRequireDefault(_LoadingSpinner);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HostMessageOverlay = function (_Component) {
  _inherits(HostMessageOverlay, _Component);

  function HostMessageOverlay() {
    _classCallCheck(this, HostMessageOverlay);

    return _possibleConstructorReturn(this, (HostMessageOverlay.__proto__ || Object.getPrototypeOf(HostMessageOverlay)).apply(this, arguments));
  }

  _createClass(HostMessageOverlay, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          i18n = _props.i18n,
          status = _props.status,
          fixed = _props.fixed;


      var visible = false;
      var icon = false;
      var message = '';

      switch (status) {
        case STATUS.HOST.DISPLAY_CHANGE:
          visible = true;
          icon = _react2.default.createElement(_LoadingSpinner2.default, null);
          message = '';
          break;
        case STATUS.HOST.SESSION_CHANGE:
          visible = true;
          icon = _react2.default.createElement(_LoadingSpinner2.default, null);
          message = _react2.default.createElement(
            'div',
            null,
            i18n.message('notGetScreen'),
            ' ',
            _react2.default.createElement('br', null),
            i18n.message('checkRemoteStatus')
          );
          break;
        case STATUS.HOST.PAUSE:
          visible = true;
          icon = _react2.default.createElement('img', { className: _HostMessageOverlay2.default['icon-pause'], alt: '' });
          message = i18n.message('hostPause');
          break;
        default:
          visible = false;
          break;
      }

      return _react2.default.createElement(
        _MessageOverlay2.default,
        { visible: visible, fixed: fixed, icon: icon },
        message
      );
    }
  }]);

  return HostMessageOverlay;
}(_react.Component);

exports.default = HostMessageOverlay;

/***/ }),
/* 129 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(130);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(2)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./HostMessageOverlay.scss", function() {
		var newContent = require("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./HostMessageOverlay.scss");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 130 */
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__(16);
exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "._2-ZnIn5CxPol {\n  content: url(" + escape(__webpack_require__(131)) + "); }\n", ""]);

// exports
exports.locals = {
	"icon-pause": "_2-ZnIn5CxPol"
};

/***/ }),
/* 131 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMi4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCA1ODAgNTgwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1ODAgNTgwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7ZmlsbDojRkZGRkZGO30NCjwvc3R5bGU+DQo8Zz4NCgk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMjkwLjEsNDRjMzMuMiwwLDY1LjQsNi41LDk1LjcsMTkuM2MyOS4zLDEyLjQsNTUuNiwzMC4xLDc4LjIsNTIuN2MyMi42LDIyLjYsNDAuMyw0OC45LDUyLjcsNzguMg0KCQljMTIuOCwzMC4zLDE5LjMsNjIuNSwxOS4zLDk1LjdzLTYuNSw2NS40LTE5LjMsOTUuN0M1MDQuMyw0MTUsNDg2LjYsNDQxLjMsNDY0LDQ2NGMtMjIuNiwyMi42LTQ4LjksNDAuMy03OC4yLDUyLjcNCgkJYy0zMC4zLDEyLjgtNjIuNSwxOS4zLTk1LjcsMTkuM3MtNjUuNC02LjUtOTUuNy0xOS4zYy0yOS4zLTEyLjQtNTUuNi0zMC4xLTc4LjItNTIuN2MtMjIuNi0yMi42LTQwLjMtNDguOS01Mi43LTc4LjINCgkJYy0xMi44LTMwLjMtMTkuMy02Mi41LTE5LjMtOTUuN3M2LjUtNjUuNCwxOS4zLTk1LjdjMTIuNC0yOS4zLDMwLjEtNTUuNiw1Mi43LTc4LjJjMjIuNi0yMi42LDQ4LjktNDAuMyw3OC4yLTUyLjcNCgkJQzIyNC43LDUwLjUsMjU2LjksNDQsMjkwLjEsNDQgTTI5MC4xLDBDMTMwLDAsMC4zLDEyOS44LDAuMywyOTBTMTMwLDU4MCwyOTAuMSw1ODBTNTgwLDQ1MC4yLDU4MCwyOTBTNDUwLjIsMCwyOTAuMSwwTDI5MC4xLDB6Ii8+DQo8L2c+DQo8Zz4NCgk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMjQwLjMsMzkyaC0xYy0xNi4yLDAtMjkuNS0xMy4zLTI5LjUtMjkuNXYtMTQ1YzAtMTYuMiwxMy4zLTI5LjUsMjkuNS0yOS41aDFjMTYuMiwwLDI5LjUsMTMuMywyOS41LDI5LjV2MTQ1DQoJCUMyNjkuOCwzNzguNywyNTYuNSwzOTIsMjQwLjMsMzkyeiIvPg0KCTxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik0zNDEuMywzOTJoLTFjLTE2LjIsMC0yOS41LTEzLjMtMjkuNS0yOS41di0xNDVjMC0xNi4yLDEzLjMtMjkuNSwyOS41LTI5LjVoMWMxNi4yLDAsMjkuNSwxMy4zLDI5LjUsMjkuNXYxNDUNCgkJQzM3MC44LDM3OC43LDM1Ny41LDM5MiwzNDEuMywzOTJ6Ii8+DQo8L2c+DQo8L3N2Zz4NCg=="

/***/ }),
/* 132 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _MessageOverlay = __webpack_require__(133);

var _MessageOverlay2 = _interopRequireDefault(_MessageOverlay);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _MessageOverlay2.default;

/***/ }),
/* 133 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _MessageOverlay = __webpack_require__(134);

var _MessageOverlay2 = _interopRequireDefault(_MessageOverlay);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MessageOverlay = function (_Component) {
  _inherits(MessageOverlay, _Component);

  function MessageOverlay() {
    _classCallCheck(this, MessageOverlay);

    return _possibleConstructorReturn(this, (MessageOverlay.__proto__ || Object.getPrototypeOf(MessageOverlay)).apply(this, arguments));
  }

  _createClass(MessageOverlay, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          visible = _props.visible,
          fixed = _props.fixed,
          icon = _props.icon;

      if (!visible) return null;

      var wrapperStyle = 'wrapper' + (fixed ? '--fixed' : '');

      return _react2.default.createElement(
        'div',
        { className: _MessageOverlay2.default['message-overlay'] },
        _react2.default.createElement(
          'div',
          { className: _MessageOverlay2.default[wrapperStyle] },
          _react2.default.createElement(
            'div',
            { className: _MessageOverlay2.default['message'] },
            icon,
            _react2.default.createElement(
              'div',
              { className: _MessageOverlay2.default['text'] },
              children
            )
          )
        )
      );
    }
  }]);

  return MessageOverlay;
}(_react.Component);

exports.default = MessageOverlay;

/***/ }),
/* 134 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(135);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(2)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./MessageOverlay.scss", function() {
		var newContent = require("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./MessageOverlay.scss");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "._2xbN68Ab-Nbt {\n  display: inline-block;\n  position: absolute;\n  background: rgba(0, 0, 0, 0.5);\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  z-index: 80; }\n\n._3MHe1a7zMJ0T {\n  z-index: 90;\n  position: relative;\n  top: 50%;\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%); }\n  ._2H2Gg7VxepaE {\n    z-index: 90;\n    position: relative;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n    position: fixed; }\n\n._p0WZ1b_nChrJ {\n  color: white;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column; }\n\n._PUaWDGktUIBc {\n  text-align: center;\n  font-size: 24px;\n  font-weight: normal;\n  line-height: 1.3;\n  margin-top: 20px; }\n", ""]);

// exports
exports.locals = {
	"message-overlay": "_2xbN68Ab-Nbt",
	"wrapper": "_3MHe1a7zMJ0T",
	"wrapper--fixed": "_2H2Gg7VxepaE",
	"message": "_p0WZ1b_nChrJ",
	"text": "_PUaWDGktUIBc"
};

/***/ }),
/* 136 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _LoadingSpinner = __webpack_require__(137);

var _LoadingSpinner2 = _interopRequireDefault(_LoadingSpinner);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _LoadingSpinner2.default;

/***/ }),
/* 137 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _LoadingSpinner = __webpack_require__(138);

var _LoadingSpinner2 = _interopRequireDefault(_LoadingSpinner);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var LoadingSpinner = function LoadingSpinner(_ref) {
  var theme = _ref.theme;

  var bars = [].concat(_toConsumableArray(Array(12).keys())).map(function (num) {
    return _react2.default.createElement('div', { key: num });
  });

  var loadingStyle = 'loading' + (theme ? '--' + theme : '');

  return _react2.default.createElement(
    'div',
    { className: _LoadingSpinner2.default[loadingStyle] },
    theme === 'fade-circle' ? bars : ''
  );
};

exports.default = LoadingSpinner;

/***/ }),
/* 138 */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(139);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(2)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./LoadingSpinner.scss", function() {
		var newContent = require("!!../../../node_modules/css-loader/index.js??ref--1-1!../../../node_modules/postcss-loader/lib/index.js??ref--1-2!../../../node_modules/sass-loader/lib/loader.js!./LoadingSpinner.scss");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 139 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "._YhKVy4ddF_Oq {\n  display: block;\n  position: relative;\n  width: 40px;\n  height: 40px;\n  border-width: 6px;\n  border-style: solid;\n  border-color: rgba(255, 255, 255, 0.3) rgba(255, 255, 255, 0.3) #fff #fff;\n  border-radius: 50%;\n  -webkit-animation: _3Se-Cjdw-sLv 1s linear infinite;\n          animation: _3Se-Cjdw-sLv 1s linear infinite; }\n\n@-webkit-keyframes _3Se-Cjdw-sLv {\n  0% {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg); }\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg); } }\n\n@keyframes _3Se-Cjdw-sLv {\n  0% {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg); }\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg); } }\n\n._vhIi9ztnFh0b {\n  height: 100px;\n  position: relative;\n  width: 100px;\n  border-radius: 100%;\n  -webkit-transition: all 0.5s ease-in-out;\n  transition: all 0.5s ease-in-out;\n  border: 2px solid;\n  border-color: transparent #fff transparent #FFF;\n  -webkit-animation: _257lVHkLT83u 1.5s linear 0s infinite normal;\n          animation: _257lVHkLT83u 1.5s linear 0s infinite normal;\n  -webkit-transform-origin: 50% 50%;\n          transform-origin: 50% 50%; }\n\n@-webkit-keyframes _257lVHkLT83u {\n  0% {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg); }\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg); } }\n\n@keyframes _257lVHkLT83u {\n  0% {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg); }\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg); } }\n\n._ThI3vDdtGUS3 {\n  position: absolute;\n  top: 45px;\n  text-align: center;\n  width: 100px;\n  color: #c1c1c1;\n  font-size: 14px;\n  font-weight: normal;\n  letter-spacing: 0.1em;\n  -webkit-animation: _2wc2rwoYjEiR 2s linear 0s infinite normal;\n          animation: _2wc2rwoYjEiR 2s linear 0s infinite normal; }\n\n@-webkit-keyframes _2wc2rwoYjEiR {\n  0% {\n    opacity: 0; }\n  20% {\n    opacity: 0; }\n  50% {\n    opacity: 1; }\n  100% {\n    opacity: 0; } }\n\n@keyframes _2wc2rwoYjEiR {\n  0% {\n    opacity: 0; }\n  20% {\n    opacity: 0; }\n  50% {\n    opacity: 1; }\n  100% {\n    opacity: 0; } }\n\n._ueGeCj3DNaaz {\n  display: inline-block;\n  position: relative;\n  width: 64px;\n  height: 64px; }\n\n._ueGeCj3DNaaz div {\n  -webkit-transform-origin: 32px 32px;\n          transform-origin: 32px 32px;\n  -webkit-animation: _ueGeCj3DNaaz 1.2s linear infinite;\n          animation: _ueGeCj3DNaaz 1.2s linear infinite; }\n\n._ueGeCj3DNaaz div:after {\n  content: \" \";\n  display: block;\n  position: absolute;\n  top: 3px;\n  left: 29px;\n  width: 5px;\n  height: 14px;\n  border-radius: 20%;\n  background: #3da9f5; }\n\n._ueGeCj3DNaaz div:nth-child(1) {\n  -webkit-transform: rotate(0deg);\n          transform: rotate(0deg);\n  -webkit-animation-delay: -1.1s;\n          animation-delay: -1.1s; }\n\n._ueGeCj3DNaaz div:nth-child(2) {\n  -webkit-transform: rotate(30deg);\n          transform: rotate(30deg);\n  -webkit-animation-delay: -1s;\n          animation-delay: -1s; }\n\n._ueGeCj3DNaaz div:nth-child(3) {\n  -webkit-transform: rotate(60deg);\n          transform: rotate(60deg);\n  -webkit-animation-delay: -0.9s;\n          animation-delay: -0.9s; }\n\n._ueGeCj3DNaaz div:nth-child(4) {\n  -webkit-transform: rotate(90deg);\n          transform: rotate(90deg);\n  -webkit-animation-delay: -0.8s;\n          animation-delay: -0.8s; }\n\n._ueGeCj3DNaaz div:nth-child(5) {\n  -webkit-transform: rotate(120deg);\n          transform: rotate(120deg);\n  -webkit-animation-delay: -0.7s;\n          animation-delay: -0.7s; }\n\n._ueGeCj3DNaaz div:nth-child(6) {\n  -webkit-transform: rotate(150deg);\n          transform: rotate(150deg);\n  -webkit-animation-delay: -0.6s;\n          animation-delay: -0.6s; }\n\n._ueGeCj3DNaaz div:nth-child(7) {\n  -webkit-transform: rotate(180deg);\n          transform: rotate(180deg);\n  -webkit-animation-delay: -0.5s;\n          animation-delay: -0.5s; }\n\n._ueGeCj3DNaaz div:nth-child(8) {\n  -webkit-transform: rotate(210deg);\n          transform: rotate(210deg);\n  -webkit-animation-delay: -0.4s;\n          animation-delay: -0.4s; }\n\n._ueGeCj3DNaaz div:nth-child(9) {\n  -webkit-transform: rotate(240deg);\n          transform: rotate(240deg);\n  -webkit-animation-delay: -0.3s;\n          animation-delay: -0.3s; }\n\n._ueGeCj3DNaaz div:nth-child(10) {\n  -webkit-transform: rotate(270deg);\n          transform: rotate(270deg);\n  -webkit-animation-delay: -0.2s;\n          animation-delay: -0.2s; }\n\n._ueGeCj3DNaaz div:nth-child(11) {\n  -webkit-transform: rotate(300deg);\n          transform: rotate(300deg);\n  -webkit-animation-delay: -0.1s;\n          animation-delay: -0.1s; }\n\n._ueGeCj3DNaaz div:nth-child(12) {\n  -webkit-transform: rotate(330deg);\n          transform: rotate(330deg);\n  -webkit-animation-delay: 0s;\n          animation-delay: 0s; }\n\n@-webkit-keyframes _ueGeCj3DNaaz {\n  0% {\n    opacity: 1; }\n  100% {\n    opacity: 0; } }\n\n@keyframes _ueGeCj3DNaaz {\n  0% {\n    opacity: 1; }\n  100% {\n    opacity: 0; } }\n", ""]);

// exports
exports.locals = {
	"loading": "_YhKVy4ddF_Oq",
	"loading-rotate": "_3Se-Cjdw-sLv",
	"loading--sharp-spinner": "_vhIi9ztnFh0b",
	"rotate-loading": "_257lVHkLT83u",
	"loading-text": "_ThI3vDdtGUS3",
	"loading-text-opacity": "_2wc2rwoYjEiR",
	"loading--fade-circle": "_ueGeCj3DNaaz"
};

/***/ })
/******/ ]);